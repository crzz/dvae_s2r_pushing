# AUTORL

This is the accompanying repository for the article "Sim-to-Real via Latent Prediction: Transferring Visual Non-Prehensile Manipulation Policies".
Refer to the article for an explanation of the method details.

## Installation

To make everything work correctly you will need to prepare your ROS workspace with lr_gym and some related dependencies, you will also need to set up a python virtual environment.

First, clone these git repositories in your workspace:
```
cd src
git clone --depth 1 --branch frontiers_2210 https://gitlab.com/crzz/dvae_s2r_pushing autoencoding_rl
git clone --depth 1 --branch frontiers_2210 https://github.com/c-rizz/lr_gym
git clone --depth 1 --branch frontiers_2210 https://github.com/c-rizz/lr_panda
git clone --depth 1 --branch frontiers_2210 https://github.com/c-rizz/lr_panda_moveit_config
git clone --depth 1 --branch frontiers_2210 https://github.com/c-rizz/lr_realsense
```

Then create the python virtual environment:
```
mkdir virtualenv
cd virtualenv
python3 -m venv lr_gym_sb3
cd ..
. virtualenv/lr_gym_sb3/bin/activate
pip install -I -r src/lr_gym/lr_gym/requirements_sb3.txt
pip install -I -r src/autoencoding_rl/requirements.txt
catkin build
. devel/setup.bash
```

If you have a recent GPU you may need to install the proper torch version supporting your hardware. You can do so with:
```
pip install -I torch==1.11.0+cu113 torchvision==0.12.0+cu113 --extra-index-url https://download.pytorch.org/whl/cu113
```

## Examples

In `src/autoencoding_rl/experiments` you can find launcher scripts for solving the different environments with the different methods.
The scripts solve_pandaMoveitPushing2DOF_dvae3.py and solve_pandaMoveitPushing2DOF_dvae3_freeze.py were used ot generate the results in the article.

You can call the scripts with rosrun from your workspace root. The output for each run will be stored in a folder named the same as the script.

Some example videos are available in the ./media folder
