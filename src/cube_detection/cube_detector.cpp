#include <ros/ros.h>
// PCL specific includes
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/transforms.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>


#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/uniform_sampling.h>
#include <pcl/point_types_conversion.h>
#include <pcl/common/geometry.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/crop_box.h>
#include <pcl/common/pca.h>

#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <cmath>
#include <string>
#include <sstream>

#include <lr_gym_utils/LinkStates.h>

bool publishFilteredCloud = true;

double voxelSize = 0.003;


ros::Publisher detectedObjectsPublisher;
ros::Publisher filteredCloudPub;
ros::Publisher cubeCloudPub;
ros::Publisher pcaCloudPub;
ros::Publisher cubePosePub;
ros::Publisher cubeLinkStatePub;

double minx= 0.15;
double maxx= 0.85;
double miny=-0.37;
double maxy= 0.37;
double minz= 0.02;
double maxz= 0.07;


std::string camera1_frame_id = "side_camera_color_optical_frame";
std::string camera2_frame_id = "front_camera_color_optical_frame";
std::string base_frame_id = "world";

tf2_ros::Buffer tfBuffer;

typedef pcl::PointXYZRGB PointType;

std::string end_effector_link = "pushing_ee_tip";
geometry_msgs::PoseStamped ee_pose;
bool got_ee_pose = false;
double ee_radius = 0.03;

int total_callback_calls = 0;
std::chrono::duration<double> total_callback_duration(0);

std::deque<geometry_msgs::PoseStamped> last_poses;
double averagingDuration = 0.5;

bool enableOrientationEstimation = false;

geometry_msgs::TransformStamped getTransform(std::string source_frame, std::string target_frame)
{
    geometry_msgs::TransformStamped transformMsg;
    bool got_transform = false;
	while(!got_transform && ros::ok())
	{
        try{
            transformMsg = tfBuffer.lookupTransform(target_frame,source_frame,ros::Time(), ros::Duration(10.0));
            got_transform=true;
        }
        catch (tf2::TransformException &ex)
        {
            ROS_WARN_STREAM("getTransform('"<<source_frame<<"', '"<<target_frame<<"'): "<<ex.what());
            ros::Duration(1.0).sleep();
        }
    }
    return transformMsg;
}

void link_state_cb(const lr_gym_utils::LinkStatesConstPtr& inputMsg)
{
    bool found = false;
    lr_gym_utils::LinkState eeLinkState;
    for(lr_gym_utils::LinkState ls : inputMsg->link_states)
    {
        if(end_effector_link.compare(ls.link_name) == 0)
        {
            found = true;
            eeLinkState = ls;
            break;
        }
    }

    if(!found)
        return;
    geometry_msgs::PoseStamped ee_pose_tmp = eeLinkState.pose;
    geometry_msgs::TransformStamped  transform = getTransform(ee_pose_tmp.header.frame_id,base_frame_id);
    tf2::doTransform(ee_pose_tmp, ee_pose_tmp, transform); // ensure pose is in correct frame
    ee_pose = ee_pose_tmp;
    ee_pose.header.stamp = inputMsg->header.stamp;
    got_ee_pose = true;
}

void removeCylinder(pcl::PointCloud<PointType>::Ptr input, pcl::PointCloud<PointType>::Ptr output, double cx, double cy, double radius, bool keepInside = false)
{
    // ROS_WARN_STREAM("input has "<<input->size()<<" points");
    pcl::PointIndices::Ptr to_keep(new pcl::PointIndices());
    pcl::ExtractIndices<PointType> extract;
    for (int i = 0; i < (*input).size(); i++)
    {
        PointType& p = input->points[i];
        double dist = std::sqrt((p.x-cx)*(p.x-cx) + (p.y-cy)*(p.y-cy));
        if ((keepInside && dist <= radius) or (!keepInside && dist > radius))
            to_keep->indices.push_back(i);
    }

    // ROS_WARN_STREAM("will keep "<<to_keep->indices.size()<<" points");
    extract.setInputCloud(input);
    extract.setIndices(to_keep);
    extract.setNegative(false);//keep the indices
    extract.filter(*output);
}

void getAvgXy(pcl::PointCloud<PointType>::Ptr input, double& avgx, double& avgy)
{
    double tx = 0;
    double ty = 0;
    for (int i = 0; i < (*input).size(); i++)
    {
        PointType& p = input->points[i];
        tx += p.x;
        ty += p.y;
    }
    avgx = tx/(*input).size();
    avgy = ty/(*input).size();
}

void mergeClouds(const sensor_msgs::PointCloud2ConstPtr& pointcloudMsg1, const sensor_msgs::PointCloud2ConstPtr& pointcloudMsg2, pcl::PointCloud<PointType>::Ptr merged_cloud)
{
	// ROS_INFO("mergeClouds...");
    // Transofrm to world frame
	sensor_msgs::PointCloud2 transformedCloud1;
    geometry_msgs::TransformStamped  camera1TransformMsg = getTransform(pointcloudMsg1->header.frame_id,base_frame_id);
    tf2::doTransform(*pointcloudMsg1,transformedCloud1, camera1TransformMsg);
	sensor_msgs::PointCloud2 transformedCloud2;
    geometry_msgs::TransformStamped  camera2TransformMsg = getTransform(pointcloudMsg2->header.frame_id,base_frame_id);
    tf2::doTransform(*pointcloudMsg2,transformedCloud2, camera2TransformMsg);

    // Transfrom to PCL type
	pcl::PCLPointCloud2 cloud1;
	pcl_conversions::toPCL(transformedCloud1, cloud1);
	pcl::PCLPointCloud2 cloud2;
	pcl_conversions::toPCL(transformedCloud2, cloud2);

	//now in "cloud" we have the cloud transformed to be relative to /map and in pcl format

	//ora devo convertire in un altro tipo ancora
	pcl::PointCloud<PointType>::Ptr cloud1_pcl(new pcl::PointCloud<PointType>());
	pcl::fromPCLPointCloud2(cloud1,*cloud1_pcl);
	pcl::PointCloud<PointType>::Ptr cloud2_pcl(new pcl::PointCloud<PointType>());
	pcl::fromPCLPointCloud2(cloud2,*cloud2_pcl);

    *merged_cloud+=*cloud1_pcl;
    *merged_cloud+=*cloud2_pcl;
}

tf2::Quaternion estimateOrientation(pcl::PointCloud<PointType>::Ptr& cubeCloud_noee, double cube_x, double cube_y, double& refined_cube_x, double& refined_cube_y)
{
    if(!enableOrientationEstimation)
    {
        tf2::Quaternion orientation;
        orientation.setRPY( 0, 0, 0);
        return orientation;
    }
    // Crop further
    double cropHalfSize = 0.06;
	Eigen::Vector4f min(cube_x-cropHalfSize,cube_y-cropHalfSize,0.03,1);
	Eigen::Vector4f max(cube_x+cropHalfSize,cube_y+cropHalfSize,0.075,1);
	pcl::CropBox< PointType >  cropBox(false);
	cropBox.setMin(min);
	cropBox.setMax(max);
	cropBox.setInputCloud(cubeCloud_noee);
	pcl::PointCloud<PointType>::Ptr cloud_cropped(new pcl::PointCloud<PointType>());
	cropBox.filter(*cloud_cropped);


	pcl::PointCloud<PointType>::Ptr filtered_cloud (new pcl::PointCloud<PointType>());
	pcl::StatisticalOutlierRemoval<PointType> outlierRemover;
	outlierRemover.setInputCloud(cloud_cropped);
	outlierRemover.setMeanK(10);
	outlierRemover.setStddevMulThresh(1.5);
	outlierRemover.filter(*filtered_cloud);

    //flatten pointcloud
    for (auto& point: *filtered_cloud)
        point.z = 0;

    //subsample to fix the high density on the sides due to the flattening
	pcl::PointCloud<PointType>::Ptr sampled_cloud (new pcl::PointCloud<PointType>());
	pcl::UniformSampling<PointType> uniform_sampling;
	uniform_sampling.setInputCloud (filtered_cloud);
	uniform_sampling.setRadiusSearch (voxelSize);
	uniform_sampling.filter(*sampled_cloud);

    if(publishFilteredCloud)
    {
        sensor_msgs::PointCloud2 output;
        pcl::toROSMsg(*sampled_cloud, output);
        output.header.stamp = ros::Time::now();
        output.header.frame_id = base_frame_id;
        pcaCloudPub.publish (output);
    }

    getAvgXy(sampled_cloud, refined_cube_x, refined_cube_y);

    if(sampled_cloud->size()<3)
    {
        ROS_WARN("LEss than 3 point in cloud, cannot do PCA, returning (0,0,0,1) orientation.");
        tf2::Quaternion orientation;
        orientation.setRPY( 0, 0, 0);
        return orientation;
    }
    pcl::PCA<PointType> pca = pcl::PCA<PointType>();
    pca.setInputCloud(sampled_cloud);
    Eigen::Vector3f eigenVals = pca.getEigenValues();
    Eigen::Matrix3f eigenVecs = pca.getEigenVectors();

    int maxEigenval = 0;
    for(int i=0;i<3;i++)
    {
      if(eigenVals[i]>eigenVals[maxEigenval])
        maxEigenval=i;
    }
    int midEigenval = 0;
    for(int i=0;i<3;i++)
    {
      if(eigenVals[i]>eigenVals[maxEigenval] && i!=maxEigenval)
        maxEigenval=i;
    }

    auto axis = eigenVecs.col(maxEigenval); //The max eigenval may be due to bad camera extrinsic calibration, take the second
    double angleZ = atan2(axis(1),axis(0)) - 3.14159/2;
    
    tf2::Quaternion orientation;
    orientation.setRPY( 0, 0, angleZ);
    return orientation;
}




void cloud_cb (const sensor_msgs::PointCloud2ConstPtr& pointcloudMsg1, const sensor_msgs::PointCloud2ConstPtr& pointcloudMsg2)
{
    // ROS_INFO_STREAM("pointcloudMsg1 delay = "<< ((ros::Time::now() - pointcloudMsg1->header.stamp).toSec()));
    // ROS_INFO_STREAM("pointcloudMsg2 delay = "<< ((ros::Time::now() - pointcloudMsg2->header.stamp).toSec()));
    auto t0 = std::chrono::steady_clock::now();

	pcl::PointCloud<PointType>::Ptr cloud_pcl(new pcl::PointCloud<PointType>());
    mergeClouds(pointcloudMsg1,pointcloudMsg2,cloud_pcl);
	// ROS_DEBUG("cloud size = %lu",cloud_pcl->size());



    // ROS_INFO("cropping");
	Eigen::Vector4f min(minx,miny,minz,1);
	Eigen::Vector4f max(maxx,maxy,maxz,1);
	pcl::CropBox< PointType >  cropBox(false);
	cropBox.setMin(min);
	cropBox.setMax(max);
	cropBox.setInputCloud(cloud_pcl);
	pcl::PointCloud<PointType>::Ptr cloud_cropped(new pcl::PointCloud<PointType>());
	cropBox.filter(*cloud_cropped);


	// ROS_INFO("Subsampling");
	pcl::PointCloud<PointType>::Ptr sampled_cloud (new pcl::PointCloud<PointType>());
	pcl::UniformSampling<PointType> uniform_sampling;
	uniform_sampling.setInputCloud (cloud_cropped);
	uniform_sampling.setRadiusSearch (voxelSize);
	uniform_sampling.filter(*sampled_cloud);

	// ROS_INFO("Filtering outliers");
	pcl::PointCloud<PointType>::Ptr filteredSampled_cloud (new pcl::PointCloud<PointType>());
	pcl::StatisticalOutlierRemoval<PointType> outlierRemover;
	outlierRemover.setInputCloud(sampled_cloud);
	outlierRemover.setMeanK(10);
	outlierRemover.setStddevMulThresh(1.5);
	outlierRemover.filter(*filteredSampled_cloud);
	//ROS_INFO("cloud size = %lu",filtered_cloud->size());

	// ROS_WARN("filtered, subsampled and cropped cloud size = %lu",cloud_cropped->size());

    //Remove non-black
	pcl::PointCloud<PointType>::Ptr cloud_black(new pcl::PointCloud<PointType>());
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
    pcl::ExtractIndices<PointType> extract;
    for (int i = 0; i < filteredSampled_cloud->size(); i++)
    {
        std::uint32_t rgb = *reinterpret_cast<int*>(&(filteredSampled_cloud->points[i].rgb));
        std::uint8_t r = (rgb >> 16) & 0x0000ff;
        std::uint8_t g = (rgb >> 8)  & 0x0000ff;
        std::uint8_t b = (rgb)       & 0x0000ff;
        double l = 0.2126*r/255.0 + 0.7152*g/255.0 + 0.0722*b/255.0;
        if (l > 0.5) // e.g. remove all pts below zAvg
            inliers->indices.push_back(i);
    }
    extract.setInputCloud(filteredSampled_cloud);
    extract.setIndices(inliers);
    extract.setNegative(true);
    extract.filter(*cloud_black);
    // ROS_WARN_STREAM("cloud_black points ("<<cloud_black->size()<<")");

	if(publishFilteredCloud)
	{
		sensor_msgs::PointCloud2 output;
		pcl::toROSMsg(*cloud_black, output);
		output.header.stamp = pointcloudMsg1->header.stamp;
		output.header.frame_id = base_frame_id;
		filteredCloudPub.publish (output);
	}

    double ee_x = 100;
    double ee_y = 100;    
    if(got_ee_pose)
    {
        double td = (ee_pose.header.stamp - pointcloudMsg1->header.stamp).toSec();
        if(abs(td) > 1.0)
            ROS_WARN_STREAM("High time difference between pointcloud and ee pose. td = "<<td<<"s");

        if(ee_pose.pose.position.z > -0.02 && ee_pose.pose.position.z<0.08)
        {
            ee_x = ee_pose.pose.position.x;
            ee_y = ee_pose.pose.position.y;
        }
        else
        {
            // ROS_INFO("ee not in manipulation range");
            // End effector is not in manipulation range (either not in gripper or higher than cube)
        }

    }
    else
    {
        ROS_ERROR("Missing ee pose. Cannot remove end effector from pointcloud.");
    }

    bool done = false;
    bool enlarged = false;
    double cube_x,cube_y;
    getAvgXy(cloud_black,cube_x,cube_y);
    // ROS_WARN_STREAM("first pos = "<<cube_x<<", "<<cube_y);  
    pcl::PointCloud<PointType>::Ptr cloud_black_cropped = pcl::PointCloud<PointType>::Ptr(new pcl::PointCloud<PointType>());
    removeCylinder(cloud_black, cloud_black_cropped, cube_x, cube_y, 0.15, true);//Remove far points
    // ROS_WARN_STREAM("cloud_black_cropped points ("<<cloud_black_cropped->size()<<")");

    double ee_radius_loc = ee_radius;
    pcl::PointCloud<PointType>::Ptr cloud_noee;
    while(!done)
    {
        cloud_noee = pcl::PointCloud<PointType>::Ptr(new pcl::PointCloud<PointType>());
        removeCylinder(cloud_black_cropped, cloud_noee, ee_x, ee_y, ee_radius_loc);

        if(publishFilteredCloud)
        {
            sensor_msgs::PointCloud2 output;
            pcl::toROSMsg(*cloud_noee, output);
            output.header.stamp = pointcloudMsg1->header.stamp;
            output.header.frame_id = base_frame_id;
            cubeCloudPub.publish (output);
        }
        //Still, the cube may be hidden behind the ee

        if(cloud_black_cropped->size() < 50)
        {
            ROS_WARN_STREAM("Too few points ("<<cloud_black_cropped->size()<<"), discarding cube detection");
            return;
        }
        // ROS_INFO("cube cloud size = %lu",cloud_black->size());
        getAvgXy(cloud_noee,cube_x,cube_y);
        double dx = cube_x-ee_x;
        double dy = cube_y-ee_y;
        if(sqrt(dx*dx+dy*dy)>0.1 && !enlarged)
        {
            ee_radius_loc = ee_radius + 0.02;
            enlarged = true;
        }
        else
        {
            done = true;
        }

    }

    
    tf2::Quaternion orientation = estimateOrientation(cloud_noee, cube_x, cube_y, cube_x, cube_y);




    geometry_msgs::PoseStamped cubePose;
    cubePose.header.stamp = pointcloudMsg1->header.stamp;
    cubePose.header.frame_id = base_frame_id;
    cubePose.pose.position.x = cube_x;
    cubePose.pose.position.y = cube_y;
    cubePose.pose.position.z = 0.025;
    cubePose.pose.orientation.x = orientation.x();
    cubePose.pose.orientation.y = orientation.y();
    cubePose.pose.orientation.z = orientation.z();
    cubePose.pose.orientation.w = orientation.w();

    //ROS_INFO_STREAM("Raw cube pose: "<<cubePose.pose.position.x <<", "<<cubePose.pose.position.y);

    geometry_msgs::PoseStamped avgPose = cubePose;
    int avg_count = 1;
    double yaw_sum = 0;
    tf2::Matrix3x3 om(tf2::Quaternion(cubePose.pose.orientation.x,cubePose.pose.orientation.y,cubePose.pose.orientation.z,cubePose.pose.orientation.w));
    double r,p,y;
    om.getRPY(r,p,y);
    yaw_sum = y;
    for(geometry_msgs::PoseStamped pos : last_poses)
    {
        if((cubePose.header.stamp - pos.header.stamp).toSec() < averagingDuration)
        {
            avg_count++;
            avgPose.pose.position.x += pos.pose.position.x;
            avgPose.pose.position.y += pos.pose.position.y;
            avgPose.pose.position.z += pos.pose.position.z;
            tf2::Matrix3x3 om(tf2::Quaternion(pos.pose.orientation.x,pos.pose.orientation.y,pos.pose.orientation.z,pos.pose.orientation.w));
            om.getRPY(r,p,y);
            yaw_sum += y;
        }
    }
    last_poses.push_back(cubePose);
    if(last_poses.size()>10)
        last_poses.pop_front();
    avgPose.pose.position.x /= avg_count;
    avgPose.pose.position.y /= avg_count;
    avgPose.pose.position.z /= avg_count;
    double avg_yaw = yaw_sum/avg_count;
    tf2::Quaternion avg_orient;
    avg_orient.setRPY(0,0,avg_yaw);
    avgPose.pose.orientation.x = avg_orient.x();
    avgPose.pose.orientation.y = avg_orient.y();
    avgPose.pose.orientation.z = avg_orient.z();
    avgPose.pose.orientation.w = avg_orient.w();

    //ROS_INFO_STREAM("Avg cube pose: "<<avgPose.pose.position.x <<", "<<avgPose.pose.position.y<<" avg_count = "<<avg_count);


    cubePosePub.publish(avgPose);

    lr_gym_utils::LinkState cubeLinkState;
    cubeLinkState.header = avgPose.header;
    cubeLinkState.link_name = "cube";
    cubeLinkState.model_name = "cube";
    cubeLinkState.pose = avgPose;
    cubeLinkState.twist = geometry_msgs::Twist();
    lr_gym_utils::LinkStates linkStates;
    linkStates.header = cubeLinkState.header;
    linkStates.link_states.push_back(cubeLinkState);
    cubeLinkStatePub.publish(linkStates);

    auto tf = std::chrono::steady_clock::now();

    std::chrono::duration<double> duration = tf-t0;
    total_callback_duration += duration;
    total_callback_calls+=1;
    
    if(total_callback_duration.count()/total_callback_calls > 0.2)
        ROS_WARN_STREAM("cube detector seems to be running slow, did you compile with optimizations? (catkin build -DCMAKE_BUILD_TYPE=Release)");
    ROS_DEBUG_STREAM("Pose delay = "<< ((ros::Time::now() - avgPose.header.stamp).toSec()));
    ROS_DEBUG_STREAM("average cube detection duration = "<<(total_callback_duration.count()/total_callback_calls));
}





int main (int argc, char** argv)
{
	// Initialize ROS
	ros::init (argc, argv, "cube_detector");
	ros::NodeHandle nh;


	//get the transform between the world and the camera
    tf2_ros::TransformListener listener(tfBuffer);
    bool customParams=false;
	
	nh.param("voxel_size", voxelSize, 0.005);

	filteredCloudPub = nh.advertise<sensor_msgs::PointCloud2> ("filteredCloud", 1);
    cubeCloudPub = nh.advertise<sensor_msgs::PointCloud2> ("cubeCloud", 1);
    pcaCloudPub = nh.advertise<sensor_msgs::PointCloud2> ("pcaCloud", 1);
    cubePosePub = nh.advertise<geometry_msgs::PoseStamped> ("cube_pose", 1);
    cubeLinkStatePub = nh.advertise<lr_gym_utils::LinkStates> ("link_states", 1);

	ros::Subscriber sub_ls = nh.subscribe ("link_states", 1, link_state_cb);

    message_filters::Subscriber<sensor_msgs::PointCloud2> pc1_sub(nh, "/side_camera/depth/color/points", 1);
    message_filters::Subscriber<sensor_msgs::PointCloud2> pc2_sub(nh, "/front_camera/depth/color/points", 1);

    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::PointCloud2, sensor_msgs::PointCloud2> PC_policy;
    // ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
    message_filters::Synchronizer<PC_policy> sync(PC_policy(1), pc1_sub, pc2_sub);
    sync.setInterMessageLowerBound(ros::Duration(0.5*1/30)); //half of 30 fps
    //sync.setMaxIntervalDuration(ros::Duration(0.15)); 
    sync.setAgePenalty(2); 
    sync.registerCallback(boost::bind(&cloud_cb, _1, _2));


	ros::spin();
}