#include <ros/ros.h>
// PCL specific includes
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_sensor_msgs/tf2_sensor_msgs.h>
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/static_transform_broadcaster.h>

#include <string>
#include <sstream>

#include <cstdlib>
#include <fstream>
#include <iostream>

#include <apriltag_ros/AprilTagDetectionArray.h>
#include <Eigen/Geometry>
#include <boost/filesystem.hpp>


bool tryGetTransform(std::string source, std::string target, tf2_ros::Buffer& buffer, Eigen::Affine3d& ret)
{
    try{
        geometry_msgs::TransformStamped cameraTransformMsg = buffer.lookupTransform(target,source,ros::Time(), ros::Duration(1.0));
        ret = tf2::transformToEigen(cameraTransformMsg);
        return true;
    }
    catch (tf2::TransformException &ex)
    {
        ROS_WARN_STREAM("camera_registration: "<<ex.what());
        return false;
    }
}

Eigen::Affine3d getTransform(std::string source, std::string target, tf2_ros::Buffer& buffer)
{
    Eigen::Affine3d ret;
    bool got_transform = false;
    while(!got_transform)
    {
        bool r = tryGetTransform(source,target,buffer,ret);
        if(!r)
            ros::Duration(0.1).sleep();
        got_transform = r;
    }

    return ret;
}

void printTransform(geometry_msgs::TransformStamped t)
{
    ROS_INFO_STREAM(" x y z qx qy qz qw: " <<t.transform.translation.x<<" "<<t.transform.translation.y<<
                                        " "<<t.transform.translation.z<<" "<<t.transform.rotation.x<<
                                        " "<<t.transform.rotation.y<<" "<<t.transform.rotation.z<<
                                        " "<<t.transform.rotation.w);
}

std::string transformToString(geometry_msgs::TransformStamped tf)
{
    std::stringstream ss;
    ss <<   tf.transform.translation.x<<" "<<tf.transform.translation.y<<
            " "<<tf.transform.translation.z<<" "<<tf.transform.rotation.x<<
            " "<<tf.transform.rotation.y<<" "<<tf.transform.rotation.z<<
            " "<<tf.transform.rotation.w;
    return ss.str();
}

std::string frontCam_frame_id = "front_camera_link";
std::string sideCam_frame_id = "side_camera_link";
std::string tag_frame_id = "tag_3";
std::string world_frame_id = "world";

Eigen::Affine3d tag2frontcam;
bool gotCam1 = false;
Eigen::Affine3d tag2sidecam;
bool gotCam2 = false;
Eigen::Affine3d worldToTag;
tf2_ros::Buffer tfBuffer;
std::string configfile_path;

void tag_cb(const apriltag_ros::AprilTagDetectionArrayConstPtr& inputMsg)
{
    ROS_INFO("Got tag detections");
    for(apriltag_ros::AprilTagDetection atd : inputMsg->detections)
    {
        if( atd.id.size() == 1 && atd.id[0] == 3)
        {
            ROS_INFO_STREAM("Got tag from "<<atd.pose.header.frame_id);
            Eigen::Affine3d optical2link;
            if(tryGetTransform(frontCam_frame_id,atd.pose.header.frame_id,tfBuffer, optical2link))
            {
                Eigen::Affine3d cam_optical2tag;
                Eigen::fromMsg(atd.pose.pose.pose, cam_optical2tag);
                tag2frontcam = cam_optical2tag.inverse() * optical2link;
                gotCam1 = true;
                ROS_INFO("Got tag from front cam");
            }
            else if(tryGetTransform(sideCam_frame_id,atd.pose.header.frame_id,tfBuffer, optical2link))
            {
                Eigen::Affine3d cam_optical2tag;
                Eigen::fromMsg(atd.pose.pose.pose, cam_optical2tag);
                tag2sidecam = cam_optical2tag.inverse() * optical2link;
                gotCam2 = true;
                ROS_INFO("Got tag from side cam");
            }
        }
    }
    if(gotCam1 && gotCam2)
    {
        ROS_INFO("Computing registration");

        Eigen::Affine3d world2sidecam = worldToTag * tag2sidecam;
        Eigen::Affine3d world2frontcam = worldToTag * tag2frontcam;

        geometry_msgs::TransformStamped world2Tag_msg = tf2::eigenToTransform(worldToTag);
        geometry_msgs::TransformStamped world2sidecam_msg = tf2::eigenToTransform(world2sidecam);
        geometry_msgs::TransformStamped world2frontcam_msg = tf2::eigenToTransform(world2frontcam);

        ROS_INFO_STREAM(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n"<<
                        "   worldToTag : "<<transformToString(world2Tag_msg)<<"\n"<<
                        "   tag2sidecam_msg : "<<transformToString(tf2::eigenToTransform(tag2sidecam))<<"\n"<<
                        "   tag2frontcam_msg : "<<transformToString(tf2::eigenToTransform(tag2frontcam))<<"\n"<<
                        "   world2sidecam_msg : "<<transformToString(world2sidecam_msg)<<"\n"<<
                        "   world2frontcam_msg : "<<transformToString(world2frontcam_msg)<<"\n"<<
                        ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");

        boost::filesystem::path cfg_folder = configfile_path;
        cfg_folder.remove_filename();
        boost::filesystem::create_directories(cfg_folder);
        std::ofstream configfile(configfile_path);
        configfile <<"side_cam_tf: "<< transformToString(world2sidecam_msg)<<" world "<<sideCam_frame_id<<" 100"<<std::endl;    
        configfile <<"front_cam_tf: "<< transformToString(world2frontcam_msg)<<" world "<<frontCam_frame_id<<" 100"<<std::endl;
        configfile.close();

        ROS_INFO_STREAM("Wrote tf to "<<configfile_path<<" you can now use real_cams_tfs.launch. If needed you can try tuning the file manually.");



        ros::shutdown();
    }
}


int main (int argc, char** argv)
{
	// Initialize ROS
	ros::init (argc, argv, "camera_registration");
	ros::NodeHandle nh;
    int delay = 15;
    for(int i=0;i<delay;i++)
    {
        ros::Duration(1).sleep();
        ROS_INFO_STREAM("Will compute registration in "<<delay-i);
    }

    configfile_path = std::string(std::getenv("HOME"))+"/.ros/autoencoding_rl/configs.yaml";


    ROS_INFO_STREAM("To use this node you must first place an apriltag with id=3 and size 0.161 precisely at (0.525,0,0),\n"<<
                    "which means on the table, at the center of the manipulation area. I suggest you use the robot to point \n"<<
                    "precisely at the position. The orientation is also important, should be the same as the world frame (x \n"<<
                    "pointing from robot to camera, z upward. If you wish to change these values, check the apriltag_tags.yaml and \n"<<
                    "the calibrate_cameras.launch file.\n"<<
                    "This node will then locate the tag from the 2 cameras and save the camera transform into a configuration\n"<<
                    " file ("<<configfile_path<<"), which will be used by real_cams_tfs.launch."
                    "A precise calibration is needed, primarily by cube_detector.cpp.");

	//get the transform between the world and the camera
    tf2_ros::TransformListener listener(tfBuffer);


    worldToTag = getTransform(tag_frame_id,world_frame_id,tfBuffer);
    ROS_INFO("Got tag global position");


   	ros::Subscriber sub_tags = nh.subscribe("/tag_detections", 1, tag_cb);

    ros::AsyncSpinner spinner(2);
    spinner.start();
    ros::waitForShutdown();
}