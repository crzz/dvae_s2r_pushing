from cgi import print_environ
import collections
import datetime
from doctest import UnexpectedException
from xml.dom.minidom import Attr

import cv2
import torchvision
import torch as th
import numpy as np
import matplotlib.pyplot as plt
import os
from typing import Callable, Dict, OrderedDict, Any, Optional, Union, List, Tuple
from stable_baselines3.common.callbacks import BaseCallback
import lr_gym.utils.dbg.ggLog as ggLog
import lr_gym
from pyvirtualdisplay import Display
import re
import time
import multiprocessing
import shutil
from shutil import copyfile
import gym
import traceback
import autoencoding_rl
from autoencoding_rl.nets.Parallel import Parallel
from pathlib import Path
from typing import NamedTuple
import rospkg
import copy
# Transition = collections.namedtuple('Transition', ('observation', 'next_observation', 'action', 'reward', 'done'))
class Transition(NamedTuple):
    observation: Dict[int,th.Tensor]
    next_observation: Dict[int,th.Tensor]
    action: th.Tensor
    reward: th.Tensor
    done: th.Tensor


def transitionToDevice(transition : Transition, torchDevice):
    values = [None]*len(transition)
    for i in range(len(transition)):
        item = transition[i]
        if isinstance(item, th.Tensor):
            values[i] = item.to(torchDevice, non_blocking=True)
        elif isinstance(item, dict):
            values[i] = {}
            for k,v in item.items():
                values[i][k] = v.to(torchDevice, non_blocking=True)
    ret = Transition(*values)
    return ret

def splitTransitionBatch(transition_batch : Transition, splits : List[int]):
    if sum(splits) != len(transition_batch.done):
        raise AttributeError(f"Splits {splits} don't sum up to transition length {len(transition_batch.done)}")
    ret = []
    start = 0
    # ggLog.info(f"Splits = {splits}")
    for split_size in splits:
        end = start+split_size
        values = [None]*len(transition_batch)
        for i in range(len(transition_batch)): # loop through obs, next_obs, acton, reward, done
            item = transition_batch[i]
            if isinstance(item, th.Tensor):
                values[i] = item[start:end]
            elif isinstance(item, dict):
                values[i] = {}
                for k,v in item.items():
                    values[i][k] = v[start:end]
        ret.append(Transition(*values))
        start = end
    return ret

def replay_data2transition_batch(replay_data):
    return Transition( observation = replay_data.observations,
                       next_observation = replay_data.next_observations,
                       action = replay_data.actions,
                       reward = replay_data.rewards,
                       done = replay_data.dones)

def tensorToHumanCvImageRgb(imgTorch : th.Tensor) -> np.ndarray:
    if len(imgTorch.size())==2:
        imgTorch = imgTorch.unsqueeze(0)
    channels = imgTorch.size()[0]
    if channels == 1:
        imgTorch = imgTorch.repeat((3,1,1))
    elif channels == 3:
        imgTorch = imgTorch
    elif channels == 9:
        # Then it seems to be 3-frame stacking of rgb images
        frame1 = torchvision.transforms.functional.rgb_to_grayscale(imgTorch[0:3])
        frame2 = torchvision.transforms.functional.rgb_to_grayscale(imgTorch[3:6])
        frame3 = torchvision.transforms.functional.rgb_to_grayscale(imgTorch[6:9])
        imgTorch = th.cat([frame1,frame2,frame3], dim=0)
    else:
        raise AttributeError(f"Unsupported image shape {imgTorch.size()}")
    imgPIL = torchvision.transforms.ToPILImage(mode="RGB")(imgTorch)#Using bgr to publish (opencv standard)
    # imgCv = imgPIL
    imgCv = cv2.cvtColor(np.array(imgPIL), cv2.COLOR_RGB2BGR)
    return imgCv

def wrapper_s(args):
    try:
        seed = args[0]
        folderName = args[1]
        runFunction = args[2]
        resumeModelFile = args[3]
        seedFolder = folderName+f"/seed_{seed}"
        print(f"out folder name = {seedFolder}")
        print(f"Running with seed = {seed}")
        print(f"pid = {os.getpid()}")
        os.makedirs(folderName,exist_ok=True)
        lr_gym.utils.utils.createSymlink(src = folderName, dst = str(Path(folderName).parent.absolute())+"/latest")
        time.sleep(seed)
        # if resumeModelFile is not None:
        #     os.makedirs(seedFolder,exist_ok=True)            
        return runFunction(seed=seed, folderName=seedFolder, resumeModelFile=resumeModelFile)
    except Exception as e:
        ggLog.error(f"Run failed with exception: {e}\n"+
                    f"Stacktrace:\n"+
                    f"{''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))}")
        return None

def detectFolderArgs(resumeFolder):
    ret = {}

    for seedDir in os.listdir(resumeFolder):
        if seedDir.startswith("seed_") and os.path.isdir(resumeFolder+"/"+seedDir) and not os.path.islink(resumeFolder+"/"+seedDir):
            seed = int(seedDir.split("_")[-1])
            checkpointsDir = resumeFolder+"/"+seedDir+"/checkpoints"
            checkpoints = os.listdir(checkpointsDir)
            replay_buffers = [file for file in checkpoints if file.startswith("model_checkpoint_replay_buffer_")]
            if len(replay_buffers)>2:
                raise RuntimeError("Could not select replay buffer, there should be at most 2")
            elif len(replay_buffers)==0:
                # raise RuntimeError("No Replay Buffer found")
                model_checkpoint_files = [file for file in checkpoints if file.startswith("model_checkpoint_")]
                newest_checkpoint_steps = max([int(file.split("_")[2]) for file in model_checkpoint_files])
                model_to_reload = checkpointsDir+"/model_checkpoint_"+str(newest_checkpoint_steps)+"_steps"
            else:
                if len(replay_buffers)==2:
                    rp_steps = [int(rp.split("_")[4]) for rp in replay_buffers]
                    if rp_steps[0] > rp_steps[1]: # Choose the older one, if there's two then the second one is potentially half-saved
                        replay_buffer = replay_buffers[1]
                    else:
                        replay_buffer = replay_buffers[0]
                elif len(replay_buffers)==1:
                    replay_buffer = replay_buffers[0]
                rp_buff_ep = replay_buffer.split("_")[4]
                rp_buff_step = replay_buffer.split("_")[5]
                for file in checkpoints:
                    if file.startswith("model_checkpoint_") and file.split("_")[2] == rp_buff_ep and file.split("_")[3] == rp_buff_step:
                        model_to_reload = checkpointsDir+"/"+file[:-4] # remove extension
                # model_to_reload = checkpointsDir+f"/model_checkpoint_{rp_buff_ep}_{???}_{rp_buff_step}_steps"

            ret[seed] = {"modelToLoad" : model_to_reload}
    
    return ret

def rebuild_run_folder(newFolderName, seed, resumeFolder):
    seedFolder = newFolderName+f"/seed_{seed}"
    os.makedirs(seedFolder,exist_ok=True)
    os.makedirs(seedFolder+"/feature_extractor",exist_ok=True)
    copyfile(src=resumeFolder+f"/seed_{seed}/GymEnvWrapper_log.csv", dst=seedFolder+"/GymEnvWrapper_log.csv")
    copyfile(src=resumeFolder+f"/seed_{seed}/feature_extractor/simple_feature_extractor_log.csv", dst=seedFolder+"/feature_extractor/simple_feature_extractor_log.csv")

def runAutoBatchSize(function : Callable[[Transition,], Any], transition_batch : Transition, max_batch_size : int = 1024):
    samples_number = len(transition_batch.done)
    batch_size = min([samples_number,max_batch_size])
    done = False
    while not done:
        splits = []
        th.cuda.empty_cache()
        # ggLog.info(f"gpu usage:\n {lr_gym.utils.utils.getGpuMemUsage()}")
        # Split samples in batches
        c = 0
        while c<samples_number:
            split_size = min([batch_size,samples_number-c])
            splits.append(split_size)
            c+=split_size
        transition_batches = splitTransitionBatch(transition_batch = transition_batch, splits = splits)
        # ggLog.info(f"validation splits = {len(transition_batches)}")
        # ggLog.info(f"splitted: gpu usage:\n {lr_gym.utils.utils.getGpuMemUsage()}")
        # Compute loss across batches
        rets = []
        try:
            for batch in transition_batches:
                # tc0 = time.monotonic()
                rets.append(function(batch))
                # tcf = time.monotonic()
                # ggLog.info(f"runAutoBatchSize computation {tcf-tc0}")
            done = True
        except RuntimeError as e:
            if 'out of memory' in str(e) and batch_size > 1:
                ggLog.warn(f"Cuda out of memory, decreasing validation batch size from {batch_size} to {batch_size/2}. Error was: {e}")
                ggLog.warn(f"Exception was: {''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))}")
                batch_size = int(batch_size/2)
                batch = autoencoding_rl.utils.transitionToDevice(transition = transition_batch, torchDevice ="cpu")                
            else:
                ggLog.error(f"Cuda out of memory, batch_size={batch_size}")
                raise e
    return sum(rets)/len(rets)



def evaluator(args): 
    file = args[0]
    evalFunction = args[1]
    folderName = args[2]
    steps = int(re.sub("[^0-9]", "", os.path.basename(file)))
    print("Will now evaluate model "+file+"...")
    return evalFunction(fileToLoad=file, folderName=folderName+"/runs/"+str(steps))
    
def launchRun(runFunction,
            evalFunction,
            evalModelFile,
            seedsNum,
            seedsOffset, 
            maxProcs,
            xvfb,
            launchFilePath,
            seeds : List[int] = None,
            resumeFolder : str = None,
            pretrainedModelFile : str = None):
    if xvfb:
        disp = Display()
        disp.start()    
   
    if evalModelFile is not None:
        run_id = "eval_"+datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
    else:
        run_id = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')

    script_out_folder = os.getcwd()+"/"+os.path.basename(launchFilePath)
    folderName = script_out_folder+"/"+run_id
    os.makedirs(folderName)
    source_copy = folderName+"/autoencoding_rl"
    shutil.copytree(rospkg.RosPack().get_path("autoencoding_rl"), source_copy)
    
    if evalModelFile is not None:
        files=lr_gym.utils.utils.fileGlobToList(evalModelFile)
        files = [os.path.abspath(f) for f in files]
        lr_gym.utils.utils.evaluateSavedModels(evaluator=evaluator, files=files, args=(evalFunction,folderName), maxProcs=maxProcs)
    else:
        processes = maxProcs
        if resumeFolder is None:
            ggLog.info("Loading pretrained")
            if seeds == None:
                seeds = [x+seedsOffset for x in range(seedsNum)]
                if pretrainedModelFile is not None:
                    pretrainedModelFile = os.path.abspath(pretrainedModelFile)
                argss = [(seed,folderName,runFunction,pretrainedModelFile) for seed in seeds]
        else:
            resumeFolder = os.path.abspath(resumeFolder)
            ggLog.info("Resuming training")
            if pretrainedModelFile is not None:
                raise AttributeError("Incompatible arguments, cannot specify both pretrainedModelFile and resumeFolder")
            if seeds is not None:
                raise AttributeError("Incompatible arguments, cannot specify both seeds and resumeFolder")
            det_args = detectFolderArgs(resumeFolder)
            for seed in det_args.keys():
                rebuild_run_folder(folderName, seed, resumeFolder)
            argss = [(seed,folderName,runFunction,det_args[seed]["modelToLoad"]) for seed in det_args]

        ggLog.info(f"Will launch {argss}") 

        if len(argss) == 1:
            ggLog.info(f"Launching in main process")
            for args in argss:
                wrapper_s(args)
        else:
            ggLog.info(f"Launching in subprocesses")
            with multiprocessing.Pool(processes, maxtasksperchild=1) as p:
                eval_results = p.map(wrapper_s, argss, chunksize=1)

    if xvfb:    
        disp.stop()




def stitchImages(images : List[List[np.ndarray]]):

    for r in images:
        if len(r)!=len(images[0]):
            raise AttributeError("Rows should have all the same length")

    for r in range(len(images)):
        row = images[r]
        for c in range(len(row)):
            img = row[c]
            if img.shape != images[0][0].shape:
                raise AttributeError(f"Images should all have the same size but images[0][0].shape = {images[0][0].shape} and images[{r}][{c}].shape = {images[r][c].shape}")
            if img.dtype != images[0][0].dtype:
                raise AttributeError(f"Images should all have the same dtype but images[0][0].dtype = {images[0][0].dtype} and images[{r}][{c}].dtype = {images[r][c].dtype}")
    
    rows = len(images)
    cols = len(images[0])

    if len(images[0][0].shape)>2:
        img_channels = images[0][0].shape[2]
    else:
        img_channels = 1
    imgs_width   = images[0][0].shape[1]
    imgs_height  = images[0][0].shape[0]
    imgs_dtype   = images[0][0].dtype


    for row in range(rows):
        for col in range(cols):
            img = images[row][col]
            if len(img.shape)==2:
                img = np.expand_dims(images[row][col], axis = 2)
            images[row][col] = img
            if img.shape[1] != imgs_width or img.shape[0]!=imgs_height or len(img.shape)!=3 or img.shape[2] != img_channels or img.dtype != imgs_dtype:
                raise RuntimeError(f"Invalid image at row={row}, col={col}. Has shape {img.shape} and dtype={img.dtype}.")
    # print(images[0][0])
    spacing = int(imgs_width/5)

    tot_width = imgs_width*cols + spacing*(cols+1)
    tot_height = imgs_height*rows + spacing*(rows+1)

    out_img = np.zeros((tot_height,tot_width,img_channels),dtype=imgs_dtype)

    for row in range(rows):
        for col in range(cols):
            x_start = spacing*(col+1) + imgs_width*col
            x_end = x_start + imgs_width
            y_start = spacing*(row+1) + imgs_height*row
            y_end = y_start + imgs_height
            out_img[y_start:y_end, x_start:x_end, :] = images[row][col]

    return out_img
class ImageComparisonWindow():
    def __init__(self, outFolder : str = None, gui : bool = False):
        self._gui = gui
        self._rows = 3
        self._cols = 5

        self.dbgOutFolder = outFolder
        if self.dbgOutFolder is not None:
            try:
                os.makedirs(self.dbgOutFolder)
            except FileExistsError:
                pass
        #self._dbg_autoenc_figs = [self._figure_autoenc_train.add_subplot(self._rows,self._cols,i+1) for i in range(self._rows*self._cols)]
        #plt.gray()
        if gui:
            self._figure_autoenc_train = plt.figure(num = 1, figsize=(15, 3*self._rows)) # size in inches
            self._mainaxes = self._figure_autoenc_train.add_axes([0.05,0.05,0.95,0.95])
            self._figure_autoenc_train.show()
            plt.ion()
            plt.show()

    def displayExample(self, image_batches : List[th.Tensor], id : str = None):


        if isinstance(image_batches[0], dict):
            # Find image observations in the dicts
            img_key = None
            for k,v in image_batches[0].items():
                if v is not None:
                    if len(v.size()) == 3 or len(v.size()) == 4:
                        if img_key is not None:
                            raise RuntimeError("Only one image pre observation is supported!")
                        img_key = k
            if img_key is None:
                raise RuntimeError("No image observation found in observation dict")
            for i in range(len(image_batches)):
                image_batches[i] = image_batches[i][img_key]
        
        batch_shape = image_batches[0].size()
        
        if len(batch_shape)!=4:
            raise AttributeError(f"Unexpected batch_shape {batch_shape}")
        
        if id is None:
            id = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
        
        # empty = np.zeros((64,64,3))

        # input = self._makeViewable(input)
        # predicted = self._makeViewable(predicted)
        # truth = self._makeViewable(truth)

        images = [[tensorToHumanCvImageRgb(img) for img in batch] for batch in image_batches]
        img_shape = images[0][0].shape
        for row in range(len(images)):
            for col in range(len(images[row])):
                img = images[row][col]
                if img.shape != img_shape:
                    raise AttributeError(f"images are not all the same shape: img[{0}].size()={img.size()}, img[{i}].size()={img.size()}")
        
        big_img = stitchImages(images)



        if self.dbgOutFolder is not None:
            cv2.imwrite(filename=self.dbgOutFolder+"/autoencPretrain_epoch"+str(id)+".png",img=big_img)

        if self._gui:
            plt.clf()
            # for i in range(self._cols):
            #     self._dbg_autoenc_figs[i].imshow(images[0][i])
            #     self._dbg_autoenc_figs[i+self._cols].imshow(images[1][i])
            #     self._dbg_autoenc_figs[i+self._cols*2].imshow(images[2][i])
            #self._figure.canvas.draw_idle()
            self._mainaxes.imshow(big_img)
            for i in range(1):
                plt.pause(0.1)

    # def _makeViewable(self, img):
    #     channels = img.size()[1]
    #     if channels == 9:
    #         # Then it seems to be 3-frame stacking of rgb images
    #         frame1 = torchvision.transforms.functional.rgb_to_grayscale(img[:,0:3])
    #         frame2 = torchvision.transforms.functional.rgb_to_grayscale(img[:,3:6])
    #         frame3 = torchvision.transforms.functional.rgb_to_grayscale(img[:,6:9])
    #         ret = th.cat([frame1,frame2,frame3], dim=1)
    #     elif channels == 1:
    #         ret = img
    #     else:
    #         raise RuntimeError(f"Unsupported image shape {img.size()}")
    #     return ret


class ObsConverter:

    class SpaceInfo():
        def __init__(self, indexes, is_img, shape, dtype):
            self.indexes = indexes
            self.is_img = is_img
            self.shape = shape
            self.dtype = dtype

        def __repr__(self):
            return f"[{self.indexes}, {self.is_img}, {self.shape}, {self.dtype}]"

    @staticmethod
    def _is_img(box_space):
        obs_shape = box_space.shape
        if len(obs_shape)==1:
            return False
        elif len(obs_shape)==2 or len(obs_shape)==3:
            return True
        else:
            raise UnexpectedException(f"Unexpected obs_shape {obs_shape}")

    @staticmethod
    def _img_shape_chw(box_space):
        obs_shape = box_space.shape
        if len(obs_shape)==2:
            # 1-channel image
            return (1, obs_shape[0], obs_shape[1])
        elif len(obs_shape)==3:
            # multi-channel image
            return (obs_shape[0], obs_shape[1], obs_shape[2])
        else:
            raise UnexpectedException(f"Unexpected image obs_shape {obs_shape}")

    @staticmethod
    def _get_space_info(obs_space):
        if isinstance(obs_space, gym.spaces.Box):
            is_img = ObsConverter._is_img(obs_space)
            if is_img:
                shape = ObsConverter._img_shape_chw(obs_space)
            else:
                shape = (obs_space.shape[0],)
            dtype = obs_space.dtype
            return [ObsConverter.SpaceInfo([], is_img, shape, dtype)]
        elif isinstance(obs_space, gym.spaces.Dict):
            ret = []
            for sub_obs_key, sub_obs_space in obs_space.spaces.items():
                sub_info = ObsConverter._get_space_info(sub_obs_space)
                for si in sub_info:
                    ret.append(ObsConverter.SpaceInfo([sub_obs_key]+si.indexes, si.is_img, si.shape, si.dtype))
            return ret
        else:
            raise UnexpectedException(f"Unexpected obs_space {obs_space}")

    @staticmethod
    def _get_space_structure(obs_space):        
        if isinstance(obs_space, gym.spaces.Box):
            return None
        elif isinstance(obs_space, gym.spaces.Dict):
            ret = {}
            for sub_obs_key, sub_obs_space in obs_space.spaces.items():
                ret[sub_obs_key] = ObsConverter._get_space_structure(sub_obs_space)
            return ret
        else:
            raise UnexpectedException(f"Unexpected obs_space {obs_space}")

    @staticmethod
    def _get_sub_obs(obs, indexes):
        for i in indexes:
            obs = obs[i]
        return obs

    def __init__(self, observation_shape : gym.spaces.Dict, hide_achieved_goal : bool = True):
        self._original_obs_space = observation_shape
        
        if not isinstance(observation_shape, gym.spaces.Dict):
            raise AttributeError(f"observation_shape must be a gym.spaces.Dict, if it is not just wrap it to be one")

        self._original_obs_space_structure = self._get_space_structure(observation_shape)
        self._hide_achieved_goal = hide_achieved_goal
        self._obs_elements = self._get_space_info(observation_shape)
        # ggLog.info(f"observation_shape analysis:")
        # for idxs, val in enumerate(self._obs_elements):
        #     ggLog.info(f"{idxs}: {val}")

        self._vec_part_idxs = []
        self._vec_parts_sizes = []
        self._vectorPartSize = 0
        for obs in self._obs_elements:
            if not obs.is_img:
                if obs.indexes[0] != "achieved_goal" and obs.indexes[0] != "desired_goal":
                    self._vec_part_idxs.append(obs.indexes)
                    self._vectorPartSize += obs.shape[0]
                    self._vec_parts_sizes.append(obs.shape[0])
        for obs in self._obs_elements: #keep desired goal at the end
            if not obs.is_img:
                if obs.indexes[0] == "desired_goal":
                    self._vec_part_idxs.append(obs.indexes)
                    self._vectorPartSize += obs.shape[0]
                    self._vec_parts_sizes.append(obs.shape[0])
        if not self._hide_achieved_goal:  # if it must be visible add also achieved goal
            for obs in self._obs_elements:
                if not obs.is_img:
                    if obs.indexes[0] == "achieved_goal":
                        self._vec_part_idxs.append(obs.indexes)
                        self._vectorPartSize += obs.shape[0]
                        self._vec_parts_sizes.append(obs.shape[0])

        

        self._img_part_indexes = None
        for obs in self._obs_elements:
            if obs.is_img:
                if self._img_part_indexes is not None:
                    raise RuntimeError(f"observation has more than one image, currently not supported: {self._obs_elements}")
                self._img_part_indexes = obs.indexes
                self._image_channels = obs.shape[0]
                self._image_height = obs.shape[1]
                self._image_width = obs.shape[2]
                self._img_dtype = obs.dtype

        if self._img_dtype == np.uint8:
            self._img_pixel_range = [0,255]
        elif self._img_dtype == np.float32:
            self._img_pixel_range = [0,1]
        else:
            raise NotImplemented(f"Unsupported image dtype {self._img_dtype}")

    def vectorPartSize(self) -> int:
        return self._vectorPartSize

    def imageSizeCHW(self) -> Tuple[int,int,int]:
        return (self._image_channels, self._image_height, self._image_width)

    def getVectorPart(self, observation_batch : Dict[str,th.Tensor]):
        if self._vectorPartSize == 0:
            imgPart = self.getImgPart(observation_batch)
            if len(imgPart.size())<4:
                batch_size = 1
            else:
                batch_size = imgPart.size()[0]
            return th.empty(size=(batch_size,0)).to(imgPart.device) #same batch size as the image part
        vec = []
        for idxs in self._vec_part_idxs:
            vec.append(self._get_sub_obs(observation_batch, idxs))
        return th.cat(vec,dim=1)

    def getImgPart(self, observation_batch : Dict[str,th.Tensor]):
        return self._get_sub_obs(observation_batch, self._img_part_indexes)

    def buildDictObs(self, vectorPart_batch : th.Tensor, imgPart_batch : th.Tensor):
        obs = copy.deepcopy(self._original_obs_space_structure)
        pos = 0
        for i in range(len(self._vec_part_idxs)):
            idxs = self._vec_part_idxs[i]
            vec_size = self._vec_parts_sizes[i]
            self._get_sub_obs(obs, idxs[:-1])[idxs[-1]] = vectorPart_batch[pos:pos+vec_size]
            pos+=vec_size
        self._get_sub_obs(obs, self._img_part_indexes[:-1])[self._img_part_indexes[-1]] = imgPart_batch
        return obs

    def getImgDtype(self):
        return self._img_dtype

    def getImgPixelRange(self):
        return self._img_pixel_range
    
    def to_standard_tensors(self, obs_batch, device):        
        for idxs in self._vec_part_idxs:
            self._get_sub_obs(obs_batch, idxs[:-1])[idxs[-1]] = th.as_tensor(self._get_sub_obs(obs_batch, idxs), device = device)
        self._get_sub_obs(obs_batch, self._img_part_indexes[:-1])[self._img_part_indexes[-1]] = th.as_tensor(self._get_sub_obs(obs_batch, self._img_part_indexes), device = device)
        while len(self._get_sub_obs(obs_batch, self._img_part_indexes).size())<4:
            self._get_sub_obs(obs_batch, self._img_part_indexes[:-1])[self._img_part_indexes[-1]] = self._get_sub_obs(obs_batch, self._img_part_indexes).unsqueeze(dim = 0)
        return obs_batch


def obs_to_batch(obs):
    if not isinstance(obs, dict):
        return np.expand_dims(obs, axis = 0)
    else:
        for k in obs.keys():
             obs[k] = np.expand_dims(obs[k], axis = 0)
        return obs


def obs_from_batch(obs, i):
    if not isinstance(obs, dict):
        return obs[i]
    else:
        for k in obs.keys():
             obs[k] = obs[k][i]
        return obs
    # def _buildFeatureVec(self, vectorPart_batch : th.Tensor, encodedImgPart_batch : th.Tensor):
    #     return th.cat([vectorPart_batch,encodedImgPart_batch], dim = 1)

    # def _getFvecVecPart(self, featureVec_batch : th.Tensor):
    #     return featureVec_batch[:,0:self._vectorPartSize]
    
    # def _getFvecEncImgPart(self, featureVec_batch : th.Tensor):
    #     return featureVec_batch[:,self._vectorPartSize:self._vectorPartSize+self.encoding_size()]

def imgFromObs(observation_batch):
    if isinstance(observation_batch, th.Tensor):
        img = observation_batch
    elif isinstance(observation_batch, dict):
        for obs_name, obs in observation_batch.items():
            if obs is not None:
                dims = len(obs.size())
                if dims==3 or dims==4:
                    img = obs
    return img

def vecFromObs(observation_batch):
    vec = None
    if isinstance(observation_batch, th.Tensor):
        batch_size = observation_batch.size()[0]
        dims = len(observation_batch.size())
        if dims==2:
            vec = observation_batch
    elif isinstance(observation_batch, dict):
        for obs_name, obs in observation_batch.items():
            if obs is not None:
                batch_size = obs.size()[0]
                dims = len(obs.size())
                if dims==2:
                    vec = obs
    if vec is None:
        vec = th.empty(size=(batch_size,0))
    return vec
        

def puttext(img, string, origin, rowheight):
    for i, line in enumerate(string.split('\n')):
        cv2.putText(img,
                    text = line,
                    org=(origin[0],int(origin[1]+rowheight*i)),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale = 0.5,
                    color = (255,255,255))
def build_s2s_img(observation, action, latentExtractor, reward):
    #Convert to tensors and unsqueeze if necessary
    thAct = th.as_tensor(action).to(latentExtractor.torchDevice())
    thAct = thAct.squeeze() #remove dimension due to automatic vec_env
    if len(thAct.size())==0: #For actions of size 1
        thAct_batch = thAct.unsqueeze(0).unsqueeze(0)
    elif len(thAct.size())==1:
        thAct_batch = thAct.unsqueeze(0)
    else:
        raise AttributeError("Unexpected action shape "+str(thAct.size()))
    if isinstance(observation, th.Tensor) or isinstance(observation, np.ndarray):
        thObservation = th.as_tensor(observation).to(latentExtractor.torchDevice())
        thObservation = thObservation.squeeze() #remove dimension due to automatic vec_env
        if len(thObservation.size())==3:
            thObservation_batch = thObservation.unsqueeze(0)
        else:
            raise AttributeError("Unexpected observation shape "+str(thObservation.size()))
    elif isinstance(observation, dict):
        thObservation_batch = {}
        for obs_name, obs in observation.items():
            thObs = th.as_tensor(obs).to(latentExtractor.torchDevice())
            thObs = thObs.squeeze() #remove dimension due to automatic vec_env
            dims = len(thObs.size())
            if dims==3:
                #Assume it is a single multi-channel image
                thObservation_batch[obs_name] = thObs.unsqueeze(0) # make it a batch
            elif dims==2:
                #Assume it is a single single-channel image
                thObservation_batch[obs_name] = thObs.unsqueeze(0).unsqueeze(0) # make it a batch of multi-channels images (with 1 channel)
            elif dims==1:
                #Assume it is a single vector
                thObservation_batch[obs_name] = thObs.unsqueeze(0) # make it a batch
            else:
                raise AttributeError("Unexpected observation shape "+str(thObs.size()))
    else:
        raise AttributeError(f"Unsupported observation type {type(observation)}")
        
    
    with th.no_grad():
        encodedObs_batch = latentExtractor.encode(thObservation_batch)
        if isinstance(latentExtractor, autoencoding_rl.latent_extractors.DynLatentExtractor.DynLatentExtractor):
            _, predReward_batch = latentExtractor.predict_dynamics(encodedObs_batch, thAct_batch.to(encodedObs_batch.device))
        else:
            predReward_batch = th.tensor([0])
        decodedObs_batch = latentExtractor.decode(encodedObs_batch, thAct_batch.to(encodedObs_batch.device))


    thimg_encdec = imgFromObs(decodedObs_batch)[0]    
    vec_encdec = vecFromObs(decodedObs_batch)[0].cpu().detach().numpy()
    obs_prepost = latentExtractor.preAndPostProcess(thObservation_batch)
    img_prepost = imgFromObs(obs_prepost)[0]
    vec_prepost = vecFromObs(obs_prepost)[0].cpu().detach().numpy()
    pred_reward = predReward_batch[0].cpu().detach().numpy()[0]
    if isinstance(reward, th.Tensor):
        reward = reward.cpu().item()
    if isinstance(reward, np.ndarray):
        reward = reward[0]

    divider = th.zeros(size=(img_prepost.size()[0],img_prepost.size()[1],2), device=img_prepost.device) # A 2px black line between the two images (2px to keep the width even)
    # thimg_side2side = th.cat((prepost_obs, divider, thimg_encdec), dim=2)

    imgs = [tensorToHumanCvImageRgb(img) for img in [img_prepost, divider, thimg_encdec]]
    npimg = np.concatenate(imgs, axis = 1)

    print_vec_state = True
    if print_vec_state:
        height = 720
        width = int(height/npimg.shape[0]*npimg.shape[1])
        npimg = cv2.resize(npimg,dsize=(width, int(height*0.9)),interpolation=cv2.INTER_NEAREST)
        textImgHeight = int(height*0.1)
        textImg = np.zeros(shape = (textImgHeight, width, 3), dtype=np.uint8)
        puttext(textImg, str(vec_prepost)+"\n"+str(reward), (5,int(textImgHeight*0.5)), textImgHeight*0.25)
        puttext(textImg, str(vec_encdec)+"\n"+str(pred_reward), (int(width/2+5),int(textImgHeight*0.5)), textImgHeight*0.25)
        npimg = np.concatenate([npimg,textImg], axis = 0)
        # print("shape =",npimg.shape)
    # ggLog.info(f"Built img of shape {npimg.shape}")
    return npimg, [vec_prepost, vec_encdec], [reward, pred_reward]

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = v1/np.linalg.norm(v1)
    v2_u = v2/np.linalg.norm(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


class AutoencodingSAC_VideoSaver(BaseCallback):
    """
    A custom callback that derives from ``BaseCallback``.

    :param verbose: (int) Verbosity level 0: not output 1: info 2: debug
    """
    def __init__(self, verbose=0,
                        outFps : float = 10,
                        outFolder : str = "AutoencodingSAC_VideoSaver_"+datetime.datetime.now().strftime('%Y%m%d-%H%M%S'),
                        saveBestEpisodes = False, 
                        saveFrequency_ep = 1,
                        saveFrequency_step = -1):
        super(AutoencodingSAC_VideoSaver, self).__init__(verbose)
        self._saveBestEpisodes = saveBestEpisodes
        self._saveFrequency_ep = saveFrequency_ep
        self._saveFrequency_step = saveFrequency_step
        self._bestReward = float("-inf")
        self._episode_thimages = []
        self._episode_reward = 0
        self._episode_count = 0

        self._outFps = outFps
        self._outFolder = outFolder
        self._last_ep_duration = 0
        self._last_saved_ep_steps = -1
        self._tot_timesteps_seen = 0
        self._last_saved_ep = -1

        self._obssBuffer = [[]]
        self._actionsBuffer = [[]]
        self._rewardsBuffer = [[]]
        try:
            os.makedirs(os.path.dirname(self._outFolder))
        except FileExistsError:
            pass
        # Those variables will be accessible in the callback
        # (they are defined in the base class)
        # The RL model
        # self.model = None  # type: BaseAlgorithm
        # An alias for self.model.get_env(), the environment used for training
        # self.training_env = None  # type: Union[gym.Env, VecEnv, None]
        # Number of time the callback was called
        # self.n_calls = 0  # type: int
        # self.num_timesteps = 0  # type: int
        # local and global variables
        # self.locals = None  # type: Dict[str, Any]
        # self.globals = None  # type: Dict[str, Any]
        # The logger object, used to report things in the terminal
        # self.logger = None  # stable_baselines3.common.logger
        # # Sometimes, for event callback, it is useful
        # # to have access to the parent object
        # self.parent = None  # type: Optional[BaseCallback]

    def _init_callback(self) -> None:
        from autoencoding_rl.AutoencodingSAC2 import AutoencodingSAC2

        if not isinstance(self.model, AutoencodingSAC2):
            raise RuntimeError("AutoencodingSAC_VideoSaver can only be used with AutoencodingSAC2.")
        

    def _on_step(self):
        # ggLog.info(f"AutoencodingSAC_VideoSaver: on_step, locals = {self.locals}")
        self._obssBuffer[-1].append(self.locals["new_obs"]) # Take current step observation
        self._actionsBuffer[-1].append(self.locals["actions"]) # Take current step observation
        self._rewardsBuffer[-1].append(self.locals["rewards"]) # Take current step observation
        if len(self.locals["dones"])!=1:
            raise AttributeError(f"AutoencodingSAC_VideoSaver does not support vec_envs, but received {len(self.locals['dones'])} dones for one frame")
        done = self.locals["dones"][0]
        if done:
            # ggLog.info(f"AutoencodingSAC_VideoSaver: terminated episode")
            self._obssBuffer.append([])
            self._actionsBuffer.append([])
            self._rewardsBuffer.append([])
        return True

    def _on_rollout_end(self) -> bool:

        full_eps_count = len(self._obssBuffer)-1
        
        for ep in range(full_eps_count):
            self._episode_count += 1
            self._tot_timesteps_seen += len(self._obssBuffer[ep])
            if len(self._rewardsBuffer[ep][0]) != 1:
                raise AttributeError(f"AutoencodingSAC_VideoSaver does not support vec_envs, but received {len(self._rewardsBuffer[ep])} rewards for one frame")
            ep_reward = sum([reward[0] for reward in self._rewardsBuffer[ep]])
            # ggLog.info(f"AutoencodingSAC_VideoSaver: episode {self._episode_count}, reward = {ep_reward} ")
            save_ep_as_best = False
            save_ep_as_periodic = False
            if ep_reward > self._bestReward:
                self._bestReward = ep_reward
                save_ep_as_best = True
            if self._saveFrequency_step>0 and int(self._tot_timesteps_seen/self._saveFrequency_step) != int(self._last_saved_ep_steps/self._saveFrequency_step):
                save_ep_as_periodic = True
                self._last_saved_ep_steps = self._tot_timesteps_seen
            if self._saveFrequency_ep>0 and int(self._episode_count/self._saveFrequency_ep) != int(self._last_saved_ep/self._saveFrequency_ep):
                save_ep_as_periodic = True
                self._last_saved_ep = self._episode_count

            if save_ep_as_best or save_ep_as_periodic:
                s2s_npimgs = []
                for frame in range(len(self._obssBuffer[ep])):
                    img, vecs = build_s2s_img(self._obssBuffer[ep][frame], self._actionsBuffer[ep][frame], self.model.get_latent_extractor())
                    s2s_npimgs.append(img)
                if save_ep_as_periodic:
                    self._saveVideo(self._outFolder+("/ep_"+(f"{self._episode_count}".zfill(9)))+f"_{ep_reward}.mp4v", s2s_npimgs)
                if save_ep_as_best:
                    self._saveVideo(self._outFolder+("/best_ep_"+(f"{self._episode_count}".zfill(9)))+f"_{ep_reward}.mp4v", s2s_npimgs)
        
        self._obssBuffer = [self._obssBuffer[-1]]
        self._actionsBuffer = [self._actionsBuffer[-1]]
        self._rewardsBuffer = [self._rewardsBuffer[-1]]

    def _saveVideo(self, outFilename : str, s2s_npimgs):
        if len(s2s_npimgs)>0:
            ggLog.info(f"AutoencodingSAC_VideoSaver saving {len(s2s_npimgs)} frames video to "+outFilename)
            #outFile = self._outVideoFile+str(self._episodeCounter).zfill(9)
            if not outFilename.endswith(".avi"):
                outFilename+=".avi"
            in_resolution_wh = (s2s_npimgs[0].shape[1], s2s_npimgs[0].shape[0]) # npimgs are hwc
            # ggLog.info(f"in_resolutio_wh = {in_resolution_wh}")
            height = 480
            out_resolution_wh = (int(height/in_resolution_wh[1]*in_resolution_wh[0]), height)
            # ggLog.info(f"out_resolution_wh = {out_resolution_wh}")
            videoWriter = cv2.VideoWriter(  outFilename,
                                            cv2.VideoWriter_fourcc(*'MPEG'),
                                            self._outFps,
                                            out_resolution_wh)
            for npimg in s2s_npimgs:
                # img = tensorToHumanCvImageRgb(thimg)
                npimg = cv2.resize(npimg,dsize=out_resolution_wh,interpolation=cv2.INTER_NEAREST)             
                npimg = self._preproc_frame(npimg)
                videoWriter.write(npimg)
            videoWriter.release()
            # print(f"Saved video image format = {self._frameBuffer[0].shape}\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
            # time.sleep(10)

    
    def _preproc_frame(self, img_whc):
        if img_whc.dtype == np.float32:
            img_whc = np.uint8(img_whc*255)

        if len(img_whc.shape) == 2:
            img_whc = np.expand_dims(img_whc,axis=2)
        if img_whc.shape[2] == 1:
            img_whc = np.repeat(img_whc,3,axis=2)
        
        if img_whc.shape[2] != 3 or img_whc.dtype != np.uint8:
            raise RuntimeError(f"Unsupported image format, dtpye={img_whc.dtype}, shape={img_whc.shape}")
        return img_whc



class OnStep_passthrough(BaseCallback):
    """
    A custom callback that derives from ``BaseCallback``.

    :param verbose: (int) Verbosity level 0: not output 1: info 2: debug
    """
    def __init__(self, sub_callback, verbose=0):
        super().__init__(verbose)
        self._subcb = sub_callback

    def _on_step(self):
        self._subcb.update_locals(self.locals)
        return self._subcb._on_step()

def build_mlp_net(arch, input_size, output_size,  ensemble_size=1, last_activation_class = th.nn.Identity, return_ensemble_mean = False,
                    hidden_activations = th.nn.LeakyReLU):
        
    if arch == "identity":
        if input_size != output_size:
            raise AttributeError(f"Requested identity mlp, but input_size!=output_size: {input_size} != {output_size}")
        net = Parallel([last_activation_class()], return_mean=return_ensemble_mean)
    elif isinstance(arch, (list, tuple)):
        nets = []
        for _ in range(ensemble_size):
            layersizes = ([input_size] + 
                            list(arch) + 
                            [output_size])
            layers = []
            for i in range(len(layersizes)-1):
                layers.append(th.nn.Linear(layersizes[i],layersizes[i+1]))
                if i < len(layersizes) - 2:
                    layers.append(hidden_activations())
            layers.append(last_activation_class())
            nets.append(th.nn.Sequential(*layers))
        net = Parallel(nets, return_mean=return_ensemble_mean)
    else:
        raise AttributeError(f"Invalid arch {arch}")
    return net



class LearningRateConf:
    def __init__(self, scheduler_class : Callable[[th.optim.Optimizer],Any], initial_lr, scheduler_kwargs = {}):
        if scheduler_class is None:
            scheduler_class = NopSchedule
        self._scheduler_class = scheduler_class
        self._scheduler_kwargs = scheduler_kwargs
        self.initial_lr = initial_lr
        self.build_scheduler = lambda opt: self._scheduler_class(optimizer = opt, **self._scheduler_kwargs)

class NopSchedule:
    # TODO: make it an actual torch schedule or something more proper
    def __init__(self, optimizer):
        pass
    def step(self):
        pass

def cmpObs(t1 : Union[th.Tensor, Dict[Union[str, int], th.Tensor]], t2 : Union[th.Tensor, Dict[Union[str, int], th.Tensor]]) -> bool:
    if type(t1)!=type(t2):
        return False
    if isinstance(t1, th.Tensor):
        return th.equal(t1,t2)
    elif isinstance(t1,Dict):
        if t1.keys()!=t2.keys():
            return False
        all_equal = True
        for k in t1.keys():
            all_equal &= cmpObs(t1[k], t2[k])
        return all_equal
    else:
        raise AttributeError(f"Unexpected obs types {type(t1), type(t2)}")

def takeTime():
    th.cuda.synchronize()
    return time.monotonic()