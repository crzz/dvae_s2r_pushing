#!/usr/bin/env python3

import argparse
import pandas as pd
from  enum import IntEnum
import cv2 as cv
import numpy as np
import os
from tqdm import tqdm


def drawTraj(traj_img, pts, linecolor, start_color, end_color, minx, miny, pix_per_meter):
    pts[:,1] = (pts[:,1] - minx)*pix_per_meter
    pts[:,0] = (pts[:,0] - miny)*pix_per_meter
    # print(pts)
    pts = pts.astype(np.int32)
    new = np.zeros((traj_img.shape[0],traj_img.shape[1],4), dtype= np.float32)
    cv.polylines(new, pts = [pts], isClosed = False, color = linecolor, thickness = 1)
    cv.circle(traj_img,pts[-1], 3, end_color, -1)
    cv.circle(traj_img,pts[0], 3, start_color, -1)

    for ch in range(3):
        new[:,:,ch] = new[:,:,ch] * new[:,:,3]
        traj_img[:,:,ch] = traj_img[:,:,ch] * (1-new[:,:,3])
    new = new[:,:,0:3]
    return traj_img + new




def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("--csvfiles", nargs="+", required=False, type=str, help="Csv file(s) to read from")
    ap.add_argument("--format", required=False, default="png", type=str, help="format of the output file")
    ap.add_argument("--out", required=False, default=None, type=str, help="Filename for the output plot")
    ap.add_argument("--outfname", required=False, default=None, type=str, help="Name of the output file (without path)")
    ap.add_argument("--drawee", default=False, action='store_true', help="Draw end effector trajectories")
    ap.add_argument("--nocube", default=False, action='store_true', help="Don't draw cube trajectories")
    ap.add_argument("--split", required=False, default=None, type=int, help="Split specified files in groups of this size and draw one by one")
    ap.add_argument("--step", required=False, default=None, type=int, help="Step split start by this amount, if None then step = split")

    ap.set_defaults(feature=True)
    args = vars(ap.parse_args())


    csvfiles = args["csvfiles"]
    drawee = args["drawee"]
    out_path = args["out"]
    outfname = args["outfname"]
    fformat = args["format"]
    nocube = args["nocube"]
    split = args["split"]
    step = args["step"] 
    if step is None:
        step = split

    if split is None:
        drawFiles(csvfiles, drawee, out_path, outfname, fformat, nocube)
    else:
        for i in range(0,len(csvfiles), step):
            drawFiles(csvfiles[i:i+split], drawee, out_path, outfname, fformat, nocube)

def drawFiles(csvfiles, drawee, out_path, outfname, fformat, nocube):

    VEC_STATE_IDX = IntEnum("VEC_STATE_IDX", [
        "TIP_X",
        "TIP_Y",
        "TIP_Z",
        "TIP_ROLL",
        "TIP_PITCH",
        "TIP_YAW",
        "JOINT1_POS",
        "JOINT2_POS",
        "JOINT3_POS",
        "JOINT4_POS",
        "JOINT5_POS",
        "JOINT6_POS",
        "JOINT7_POS",
        "ACTION_FAILS",
        "CUBE_X",
        "CUBE_Y",
        "CUBE_YAW",
        "TIME",
        "GOAL_X",
        "GOAL_Y",
        "MAX_STEPS",
        "GOAL_TOLERANCE",
        "TERMINATE_ON_SUCCESS"
    ], start=0)

    commonPath = os.path.commonpath([os.path.abspath(os.path.dirname(cf)) for cf in csvfiles])
    commonFilenamePrefix = os.path.commonprefix([os.path.abspath(os.path.basename(cf)) for cf in csvfiles])
    nums = [int(os.path.basename(cf).split("_")[1].split(".")[0]) for cf in csvfiles]
    commonRealPath = os.path.realpath(commonPath) # absolute path without links
    if out_path is None:
        if outfname is None:
            # num = commonFilenamePrefix.split("_")[1].split(".")[0]
            # num = num + "x"*(6-len(num))
            # outfname = "plot_"+num
            outfname = f"plot_{min(nums):06d}_{max(nums):06d}"
        out_path = commonRealPath+"/"+outfname+"."+fformat
        
    ep = 0
    outer_border = 0.05
    area = [[0.3,0.75],[-0.225,0.225]]
    minx = area[0][0]-outer_border
    maxx = area[0][1]+outer_border
    miny = area[1][0]-outer_border
    maxy = area[1][1]+outer_border

    img_size = 1000
    img = np.full((img_size,img_size,3), fill_value = (0.1,0.1,0.1), dtype = np.float32)
    pix_per_meter = img_size/(maxx - minx)

    for csvfile in tqdm(csvfiles):
        df = np.genfromtxt(csvfile, delimiter = ",")
        if len(df) == 0:
            continue
        # print(ep)
        np.set_printoptions(precision=4, suppress=True)
        # print(df)

        goal = df[0,[VEC_STATE_IDX.GOAL_Y,VEC_STATE_IDX.GOAL_X]]
        goal_tolerance = df[0,VEC_STATE_IDX.GOAL_TOLERANCE]

        # print(np.linalg.norm(df[:,[VEC_STATE_IDX.CUBE_Y, VEC_STATE_IDX.CUBE_X]] - goal, axis = 1))
        cube_traj = df[:,[VEC_STATE_IDX.CUBE_Y, VEC_STATE_IDX.CUBE_X]]
        did_succeed = np.any(np.linalg.norm(cube_traj - goal, axis = 1) < goal_tolerance)
        # print(pts)
        if not nocube:
            if did_succeed:
                traj_color = (0.9,0.9,0.9,0.3)
                start_color = (0,0.9,0)
            else:
                traj_color = (0,0,0.9,0.3)
                start_color = (0,0,0.9)
            img = drawTraj(img, cube_traj, traj_color, start_color, (0.9,0,0), minx, miny, pix_per_meter)
        if drawee:
            img = drawTraj(img, df[:,[VEC_STATE_IDX.TIP_Y, VEC_STATE_IDX.TIP_X]], (0,1,1,0.3), (0,1,1), (0,0,1), minx, miny, pix_per_meter)

        ep += 1
    

    # img = traj_img[:,:,0:3]
    # print(img.shape)
    area_pix = np.array(area, dtype = np.float32).transpose()
    # print(area_pix)
    area_pix[:,0] = (area_pix[:,0] - minx)*pix_per_meter
    area_pix[:,1] = (area_pix[:,1] - miny)*pix_per_meter
    # print(area_pix)
    area_pix = area_pix.astype(np.int32)
    cv.rectangle(img, area_pix[0], area_pix[1],(0,0,1),1)


    goal_pix = (goal - np.array([miny,minx]))/np.array([maxy - miny, maxx - minx])*np.array([img_size, img_size])
    goal_pix = goal_pix.astype(np.int32)
    # print(goal_pix)
    goal_tolerance_pix = int(goal_tolerance*pix_per_meter)
    cv.circle(img, goal_pix, goal_tolerance_pix, color = (0,1,1), thickness = 1)
    img = img * 255
    cv.imwrite(out_path, img)
    print(f"saved to {out_path}")



if __name__ == "__main__":
    main()