from pympler import tracker, muppy, refbrowser, summary, asizeof
import tracemalloc
import linecache
import numpy as np
import torch as th

from stable_baselines3.common.callbacks import BaseCallback

import lr_gym.utils.dbg.ggLog as ggLog


class MemTrackCallback(BaseCallback):
    """
    A custom callback that derives from ``BaseCallback``.

    :param verbose: (int) Verbosity level 0: not output 1: info 2: debug
    """
    def __init__(self, verbose=0, period = 200):
        super().__init__(verbose)
        self._rollouts_count = 0
        self._period = period
        tracemalloc.start()
        self._snapshot = tracemalloc.take_snapshot()

    def _on_step(self):
        return True

    def display_top(self, snapshot, key_type='lineno', limit=10):
        snapshot = snapshot.filter_traces((
            tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
            tracemalloc.Filter(False, "<unknown>"),
        ))
        top_stats = snapshot.statistics(key_type)

        print("Top %s lines" % limit)
        for index, stat in enumerate(top_stats[:limit], 1):
            frame = stat.traceback[0]
            print("#%s: %s:%s: %.1f KiB"
                % (index, frame.filename, frame.lineno, stat.size / 1024))
            line = linecache.getline(frame.filename, frame.lineno).strip()
            if line:
                print('    %s' % line)

        other = top_stats[limit:]
        if other:
            size = sum(stat.size for stat in other)
            print("%s other: %.1f KiB" % (len(other), size / 1024))
        total = sum(stat.size for stat in top_stats)
        print("Total allocated size: %.1f KiB" % (total / 1024))

    def _on_rollout_end(self) -> bool:
        self._rollouts_count += 1
        if self._rollouts_count % self._period == 0:
            snapshot2 = tracemalloc.take_snapshot()
            mem_size, mem_peak = tracemalloc.get_traced_memory()
            ggLog.info(f"Memory usage = {mem_size}, peak = {mem_peak}")
            # mem = self._mem
            # topMemClasses = sorted(mem.create_summary(), reverse=True, key=itemgetter(2))[:10]
            # topMemClasses_str = ""
            # for tc in topMemClasses:
            #     # tc[2] = str(tc[2]/1024/1024)+"MB"
            #     topMemClasses_str += str(tc)+"\n"
            # ggLog.info(f"Top memory users (class, #objects, bytes):\n {topMemClasses_str}")

            allObjs = muppy.get_objects()
            # allObjs = list(reversed(muppy.sort(allObjs)[-100:]))
            summary.print_(summary.summarize(allObjs))


            ndarrs = list(filter(lambda x : type(x) == np.ndarray, muppy.get_objects()))
            # top100_ndarrs = list(reversed(muppy.sort(ndarrs)[-100:]))
            # ggLog.info(f"Top 100 ndarrays sizes: {[a.shape for a in top100_ndarrs]}")
            ndarr_by_shape = {}
            for ndarr in ndarrs:
                if ndarr.shape not in ndarr_by_shape:
                    ndarr_by_shape[ndarr.shape] = 0
                ndarr_by_shape[ndarr.shape]+=1
            ndarr_by_shape_str = ""
            for s in ndarr_by_shape:
                ndarr_by_shape_str += f"{s} : {ndarr_by_shape[s]} \n"
            ggLog.info(f"len(ndarrs) = {len(ndarrs)}")
            ggLog.info(f"ndarr_by_shape_str = {ndarr_by_shape_str}")

            thtensors = list(filter(lambda x : type(x) == th.Tensor, muppy.get_objects()))
            # top100_thtensors = list(reversed(muppy.sort(thtensors)[-100:]))
            # ggLog.info(f"Top 100 thtensors sizes: {[a.shape for a in top100_thtensors]}")
            thtensors_by_shape = {}
            cpu_size_bytes = 0
            gpu_size_bytes = 0
            for thtensor in thtensors:
                if thtensor.shape not in thtensors_by_shape:
                    thtensors_by_shape[thtensor.shape] = 0
                thtensors_by_shape[thtensor.shape]+=1
                if thtensor.device == th.device("cpu"):
                    cpu_size_bytes += thtensor.nelement()*thtensor.element_size()
                else:
                    gpu_size_bytes += thtensor.nelement()*thtensor.element_size()

            thtensors_by_shape_str = ""
            for s in thtensors_by_shape:
                thtensors_by_shape_str += f"{s} : {thtensors_by_shape[s]} \n"
            ggLog.info(f"len(thtensors) = {len(thtensors)}, cpu size = {cpu_size_bytes} = {cpu_size_bytes/1024/1024}MB, gpu_size_bytes = {gpu_size_bytes} = {gpu_size_bytes/1024/1024}MB")
            ggLog.info(f"thtensors_by_shape_str = {thtensors_by_shape_str}")


            top_stats = snapshot2.compare_to(self._snapshot, 'lineno')
            ggLog.info("[ Top 10 differences ]")
            for stat in top_stats[:10]:
                print(str(stat))
            self._snapshot = snapshot2
            self.display_top(snapshot2)
            # asacs = list(filter(lambda x : type(x) == AutoencodingSAC2, muppy.get_objects()))
            # ggLog.info(f"len(asacs) = : {len(asacs)}")

            
