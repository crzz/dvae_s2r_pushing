import torch as th
import os
import datetime
import time
from typing import Dict, OrderedDict, Any, Optional, List
# import torchviz
# import traceback

import lr_gym.utils.dbg.ggLog as ggLog

import autoencoding_rl
from autoencoding_rl.utils import Transition
from autoencoding_rl.latent_extractors.SimpleLatentExtractor import SimpleLatentExtractor
from autoencoding_rl.latent_extractors.BaseAutoencoder import BaseAutoencoder
import inspect
import yaml
import wandb

class DynLatentExtractor(SimpleLatentExtractor):
    def __init__(self,
                 dyn_ae : BaseAutoencoder,
                 debug_out_folder : Optional[str] = None,
                 torchDevice : str = "cuda",
                 checkFinite : bool = True,
                 bestModelThreshold : float = 0.9,
                 logarithmic_loss : bool = True):
        super().__init__(   autoencoder = dyn_ae,
                            torchDevice = torchDevice,
                            debug_out_folder = debug_out_folder,
                            bestModelThreshold = bestModelThreshold,
                            logarithmic_loss=logarithmic_loss)
        self._checkFinite = checkFinite

        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        self._init_args = values
        self._init_args.pop("self")
        self._init_args.pop("dyn_ae")
        self._init_args["dyn_ae_args"] = dyn_ae.get_init_args()
        self._init_args["dyn_ae_class"] = str(type(dyn_ae))
        # print(f"self._init_args = {self._init_args}")
        os.makedirs(debug_out_folder, exist_ok=True)
        with open(debug_out_folder+"/init_args.yaml", "w") as input_args_yamlfile:
            yaml.dump(self._init_args,input_args_yamlfile, default_flow_style=None)


    def displayDebugInfo(self, transition_batch_example : Transition, id : str = None) -> None:
        if id is None:
            id = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
        if self._comparisonWindow is not None:
            with th.no_grad():
                obs_batch = transition_batch_example.observation
                next_obs_batch = transition_batch_example.next_observation
                preprocessed_obs_batch = self._latestAutoencoder.preprocess_observations(obs_batch)
                preprocessed_next_obs_batch = self._latestAutoencoder.preprocess_observations(next_obs_batch)

                preprocessed_transition = Transition(   preprocessed_obs_batch,
                                                        preprocessed_next_obs_batch,
                                                        transition_batch_example.action,
                                                        transition_batch_example.reward,
                                                        transition_batch_example.done)
                
                state = self._bestAutoencoder.encode(preprocessed_transition.observation)
                reconstructed_obs = self._bestAutoencoder.decode(state)
                predicted_state, predicted_reward = self._bestAutoencoder.predict_dynamics(state, preprocessed_transition.action)
                predicted_obs = self._bestAutoencoder.decode(predicted_state)

                predicted_obs = self._latestAutoencoder.postprocess_observations(predicted_obs)
                reconstructed_obs = self._latestAutoencoder.postprocess_observations(reconstructed_obs)
                postprocessed_obss = self._latestAutoencoder.postprocess_observations(preprocessed_transition.observation)
                postprocessed_next_obss = self._latestAutoencoder.postprocess_observations(preprocessed_transition.next_observation)
            
            self._comparisonWindow.displayExample(  [postprocessed_obss,
                                                     postprocessed_next_obss,
                                                     reconstructed_obs,
                                                     predicted_obs],
                                                    id = id)


    def train_model(self, training_batch : Transition, validation_batch : Transition = None, episode_count : int = -1):
        """Perform one training iteration on the provided batch

        Parameters
        ----------
        training_batch : Transition
            A batch of transitions (Actually a transition of batches)
        """
        
        #print("observation_batch.type()="+str(observation_batch.type()))
        # t0 = time.monotonic()

        preprocessed_training_batch = Transition(   self._latestAutoencoder.preprocess_observations(training_batch.observation),
                                                    self._latestAutoencoder.preprocess_observations(training_batch.next_observation),
                                                    training_batch.action,
                                                    training_batch.reward,
                                                    training_batch.done)

        # ggLog.info("Train")
        train_ret = self._latestAutoencoder.train_model(preprocessed_training_batch)       
        if train_ret is None:
            return
        loss, sub_losses = train_ret
        sub_losses["loss"] = loss

        wandb.log(sub_losses)
        sub_losses = {n : l.detach().item() for n,l in sub_losses.items()}


        # ggLog.info("Choosing best")
        self._setBestModel(validation_batch)

        self._train_iterations_done += 1
        self._training_samples_used += training_batch.done.size()[0]
        self._info["train_iterations_done"] = self._train_iterations_done
        self._info["training_samples_used"] = self._training_samples_used
        self._info["time_from_start"] = time.monotonic()-self._initTime
        self._info["loss"] = loss
        self._info.update(sub_losses)
        self._info["skippedMinibatches"] = self._latestAutoencoder._skippedMinibatchesCount
        self._info["episode_count"] = episode_count
        for k,v in self._info.items():
            if isinstance(v, th.Tensor):
                self._info[k] = v.item()
        self._writeToLog()


        return self._info["loss"]


    def evaluate(self, transition_batch : Transition, use_latest : bool = False) -> th.Tensor:
        """Evaluate model loss on the provided batch

        Parameters
        ----------
        transition_batch : Transition
            A batch of transitions (Actually a transition of batches)
        """

        ae = self._bestAutoencoder
        if use_latest:
            ae = self._latestAutoencoder
        
        try:
            with th.no_grad():
                # batch = autoencoding_rl.utils.transitionToDevice(transition = batch, torchDevice = self._torchDevice)
                ae.eval() # put model in eval mode            
                #print("transition_batch.observation['camera'].dtype = ",transition_batch.observation['camera'].dtype)
                preprocessed_batch = Transition(self._latestAutoencoder.preprocess_observations(transition_batch.observation),
                                                self._latestAutoencoder.preprocess_observations(transition_batch.next_observation),
                                                transition_batch.action,
                                                transition_batch.reward,
                                                transition_batch.done)
                losses = ae.compute_losses(preprocessed_batch)
                # batch = autoencoding_rl.utils.transitionToDevice(transition = batch, torchDevice ="cpu")
                ae.train() # put model in train mode) # autoencoding_rl.utils.runAutoBatchSize(compute_loss, transition_batch)
            loss = losses[0]
        except RuntimeError as e:
            ggLog.warn(f"Evaluation failed with error {e}")
            loss = th.tensor([float("inf")], dtype=th.float32, device=self._torchDevice)
        return loss

    def predict_dynamics(self, encoded_obs_batch : th.Tensor, action_batch : th.Tensor = None):
        self._bestAutoencoder.eval()
        with th.no_grad():
            next_encObs_batch, reward_batch = self._bestAutoencoder.predict_dynamics(encoded_obs_batch, action_batch)

        return next_encObs_batch, reward_batch

    def decode(self, encoded_obs_batch : th.Tensor, action_batch : th.Tensor = None):

        self._bestAutoencoder.eval()
        with th.no_grad():
            next_encObs_batch, reward_batch = self._bestAutoencoder.predict_dynamics(encoded_obs_batch, action_batch) #It's not really the "encoded next image" as the two latent distributions are not necessarily the same
            next_decObs_batch = self._bestAutoencoder.decode(next_encObs_batch)
            next_obs_batch = self._latestAutoencoder.postprocess_observations(next_decObs_batch)

        return next_obs_batch
