from autoencoding_rl.latent_extractors.autoencoder.AE_encoder import AE_encoder
from autoencoding_rl.nets.Parallel import Parallel

import torch as th
import torch.nn as nn
from typing import Tuple

import lr_gym.utils.dbg.ggLog as ggLog

class VAE_encoder(nn.Module):
    def __init__(self, latent_space_size : int,
                    image_channels_num : int = 1,
                    net_input_width : int = 64,
                    net_input_height : int = 64,
                    ensemble_size = 1,
                    backbone : str = "conv",
                    checkDimensions : bool = True,
                    torchDevice : str = "cuda",
                    use_coord_conv : bool = False,
                    mu_activation_class = th.nn.Identity):
        super().__init__()
        self._checkDimensions = checkDimensions
        self._input_width  = net_input_width
        self._input_height = net_input_height
        self._input_channels = image_channels_num
        self._latent_space_size = latent_space_size
        self._ensemble_size = ensemble_size
        self._mu_activation_class = mu_activation_class
        self._backbone = backbone.lower()

        # if backbone == "mobilenetv3" or backbone == "resnet18" or backbone == "bigconv":
        #     self._conv_ensemble_size = 1
        # else:
        #     self._conv_ensemble_size = ensemble_size
        self._conv_ensemble_size = ensemble_size
        self.encoders = Parallel([AE_encoder(  image_channels_num = image_channels_num,
                                                net_input_width = net_input_width,
                                                net_input_height = net_input_height,
                                                backbone = backbone,
                                                checkDimensions = checkDimensions,
                                                torchDevice = torchDevice,
                                                use_coord_conv = use_coord_conv)
                                    for _ in range(self._conv_ensemble_size)])
        self._mu_scaling = 1.0
        self._logvar_scaling = 1.0


        enc_out_shape = self.encoders[0].output_shape
        self._enc_out_size = 1
        for s in enc_out_shape:
            self._enc_out_size *= s
        linear_input_size = self._enc_out_size
        hidden_size = latent_space_size*2
        self.fcs  = nn.ModuleList([  nn.Sequential(nn.Linear(linear_input_size, hidden_size), th.nn.LeakyReLU())
                                    for _ in range(ensemble_size)])
        self.fc_mus  = nn.ModuleList([  nn.Sequential(nn.Linear(hidden_size, latent_space_size), self._mu_activation_class())
                                    for _ in range(ensemble_size)])
        self.fc_logvars = nn.ModuleList([   nn.Sequential(nn.Linear(hidden_size, latent_space_size))
                                        for _ in range(ensemble_size)])

        def init_weights(m):
            if isinstance(m, nn.Linear):
                th.nn.init.zeros_(m.weight) # see https://stackoverflow.com/questions/49634488/keras-variational-autoencoder-nan-loss

        self.fc_logvars.apply(init_weights)

    @property
    def latent_space_size(self):
        return self._latent_space_size

    def input_width(self):
        return self._input_width

    def input_height(self):
        return self._input_height

    def input_channels(self):
        return self._input_channels

    def forward(self, x: th.Tensor) -> Tuple[th.Tensor, th.Tensor]:
        """

        Parameters
        ----------
        x : th.Tensor
            Batch of input images. Shape should be [batch_size,input_channels,input_height,input_width]

        Returns
        -------
        List[th.Tensor]
            Two batches, containing latent space mean and log-variance for each input image.
            Size of each of the two batches will be (batch_size, latent_space_size)
        """
        if self._checkDimensions:
            assert x.size() == (x.size()[0],self._input_channels,self._input_height, self._input_width), f"Image batch should have size {(x.size()[0],self._input_channels,self._input_height, self._input_width)}, but it is {x.size()}"
        

        # ggLog.info(f"VAE_encoder.forward(): x.size() = {x.size()}")
        batch_size = x.size()[0]
        enc_outs = self.encoders(x).unbind(dim=1) # returns tuple of tensors
        if self._checkDimensions:
            for enc_out in enc_outs:
                # ggLog.info(f"VAE_encoder.forward(): enc_out.size() = {enc_out.size()}")
                assert len(enc_out.size())==2, "Encoder output should have two dimensions (because it's a batch)"
                assert enc_out.size()==(batch_size,self._enc_out_size), f"Encoder outputs should have size (batch_size, enc_out)=={(batch_size,self._enc_out_size)}, but enc_out.size() = {enc_outs[-1].size()}"
            
        fc_outs = []
        for i,fc in enumerate(self.fcs):
            fc_outs.append(fc(enc_outs[i % len(enc_outs)])) # The encoder section is not always made into an ensemble, in that case enc_outs has len == 1
        hidden = th.stack(fc_outs)

        fc_mus_outs = []
        for i,fc_mu in enumerate(self.fc_mus):
            fc_mus_outs.append(fc_mu(hidden[i]))
        mus = th.stack(fc_mus_outs)
        
        fc_logvars_outs = []
        for i,fc_logvar in enumerate(self.fc_logvars):
            fc_logvars_outs.append(fc_logvar(hidden[i]))
        logvars = th.stack(fc_logvars_outs)
        
        # ggLog.info(f"VAE_encoder.forward(): mus.size() = {mus.size()}")
        # ggLog.info(f"VAE_encoder.forward(): logvars.size() = {logvars.size()}")
        mus = mus * self._mu_scaling
        logvars = logvars * self._logvar_scaling

        #simple mean
        mu = th.mean(mus, dim=0)
        logvar = th.mean(logvars,dim=0)

        return (mu, logvar)
            
    def sample(self, mu : th.Tensor, logvar : th.Tensor):
        std = th.exp(0.5 * logvar) # std = sqrt(var) = sqrt(e^logvar) = e^(0.5*logvar)
        eps = th.randn(std.size(), device=mu.device) # sample from unit gaussian
        return eps * std + mu

    