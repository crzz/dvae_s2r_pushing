#!/usr/bin/env python3


import torch as th
from abc import abstractmethod
from typing import Dict, Union, List, Tuple
import gym

from autoencoding_rl.utils import ObsConverter
from autoencoding_rl.latent_extractors.BaseAutoencoder import BaseAutoencoder

class MultiObsAutoencoder(BaseAutoencoder):

    def __init__(self,  observation_shape : Union[Dict[str,List[int]],Tuple[int,int,int], gym.spaces.Dict], torchDevice):
        super().__init__()
        self._obs_converter = ObsConverter(observation_shape = observation_shape)
        self._img_shape_chw = self._obs_converter.imageSizeCHW()
        self._vec_part_size = self._obs_converter.vectorPartSize()

        self._img_channels = self._img_shape_chw[0]
        self._img_height = self._img_shape_chw[1]
        self._img_width = self._img_shape_chw[2]

        self._torchDevice = torchDevice

        pixMinMax = self._obs_converter.getImgPixelRange()
        self._pixel_range = th.tensor(pixMinMax[1]-pixMinMax[0], dtype=th.float32, device=self._torchDevice)

    def get_image_part(self, observation_batch : th.Tensor):
        return self._obs_converter.getImgPart(observation_batch)

    def get_vector_part(self, observation_batch : th.Tensor):
        return self._obs_converter.getVectorPart(observation_batch)

    def get_image_part_shape(self):
        return self._img_shape_chw

    def get_vector_part_size(self):
        return self._vec_part_size

    def preprocess_observations(self, observation_batch : Dict[str, th.Tensor]):
        # observation_batch = copy.deepcopy(observation_batch)
        #print("observation_batch.dtype = ",self._obs_converter.getImgPart(observation_batch).dtype)
        observation_batch = self._obs_converter.to_standard_tensors(observation_batch, device=self._torchDevice)
        img_batch = self.get_image_part(observation_batch)
        if img_batch.size()[2] != self._img_height or img_batch.size()[3] != self._img_width:
            img_batch = self._resizeToInput(img_batch)
        else:
            img_batch = img_batch

        # Scale to [-1,1]
        img_batch = img_batch.to(th.float32)/self._pixel_range * 2 - 1

        vec_batch = self.get_vector_part(observation_batch)
        return { self.IMAGE : img_batch,
                 self.VECTOR : vec_batch}   


    def postprocess_observations(self, observation_batch : Dict[int,th.Tensor]):
        unnormalized_img_batch = (observation_batch[self.IMAGE] + 1)/2
        return self._obs_converter.buildDictObs(vectorPart_batch = observation_batch[self.VECTOR],
                                                imgPart_batch = unnormalized_img_batch)


    @th.jit.export
    def input_height(self):
        return self._img_height

    @th.jit.export
    def input_width(self):
        return self._img_width

    @th.jit.export
    def input_channels(self):
        return self._img_channels