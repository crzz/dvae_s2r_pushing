import math
import torch
import torch.nn as nn

from autoencoding_rl.nets.SimpleDeconvNet import SimpleDeconvNet
from autoencoding_rl.nets.DeconvNet import DeconvNet

class SimpleDecoder(nn.Module):

    def __init__(self, encoding_size : int, image_channels_num : int = 1, net_output_width : int = 64, net_output_height : int = 64):
        super().__init__()
        self._output_width  = net_output_width
        self._output_height = net_output_height
        self._output_channels = image_channels_num
        self._encoding_size = encoding_size


        if self._output_height!=self._output_width:
            raise NotImplementedError("Currently only square images are supported")

        depth = int(math.log2(self._output_height))-2
        minDepth = 1
        if depth < minDepth:
            raise NotImplementedError(f"Image size should be at least {2**(minDepth+2)}, but it's {self._output_height}")
        if 2**(depth+2) != self._output_height:
            raise NotImplementedError(f"Image size should be a power of 2, but it's {self._output_height}")

        filters_numbers = [min(2**(5+d),256) for d in range(depth)]
        filters_numbers.reverse()
        # deconvNet = SimpleDeconvNet()


        deconvNet = DeconvNet(  output_channels=self._output_channels,
                                output_height=self._output_height,
                                output_width=self._output_width,
                                filters_number=filters_numbers)

        #((H-8)/2-4)x((W-8)/2-4)
        linear_in_size = self._encoding_size
        linear_out_size = deconvNet.input_size[0]*deconvNet.input_size[1]*deconvNet.input_size[2]

        self.decoder = nn.Sequential(   nn.Linear(linear_in_size, linear_out_size),
                                        nn.ReLU(),
                                        nn.Unflatten(dim = 1, unflattened_size = deconvNet.input_size),
                                        deconvNet
                                        )

    def encoding_size(self):
        return self._encoding_size

    @property
    def output_width(self):
        return self._output_width

    @property
    def output_height(self):
        return self._output_height

    @property
    def output_channels(self):
        return self._output_channels

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """Decode a batch of images.

        Parameters
        ----------
        x : torch.Tensor
            Batch of encoded images. Size must be (batch_size, self.encoding_size())

        Returns
        -------
        x : torch.Tensor
            A batch of images. Size will be (batch_size, img_width, img_height)

        """
        assert x.size() == (x.size()[0],self._encoding_size), f"Encoded vector should have size {(x.size()[0],self._encoding_size)}, not {x.size()}"
        return self.decoder(x)
