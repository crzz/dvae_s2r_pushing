import math
import torch
import torch.nn as nn

from autoencoding_rl.nets.SimpleConvNet import SimpleConvNet
from autoencoding_rl.nets.ConvNet import ConvNet

class SimpleEncoder(nn.Module):

    def __init__(self, encoding_size : int, image_channels_num : int = 1, net_input_width : int = 64, net_input_height : int = 64):
        super().__init__()
        self._input_width  = net_input_width
        self._input_height = net_input_height
        self._input_channels = image_channels_num
        self._encoding_size = encoding_size

        if self._input_height!=self._input_width:
            raise NotImplementedError("Currently only square images are supported")

        depth = int(math.log2(self._input_height))-2
        minDepth = 1
        if depth < minDepth:
            raise NotImplementedError(f"Image size should be at least {2**(minDepth+2)}, but it's {self._input_height}")
        if 2**(depth+2) != self._input_height:
            raise NotImplementedError(f"Image size should be a power of 2, but it's {self._input_height}")

        filters_numbers = [min([2**(5+d),256]) for d in range(depth)]
        # convNet = SimpleConvNet(image_channels_num = image_channels_num,
        #                         image_width = self._input_width,
        #                         image_height = self._input_height)

        convNet = ConvNet(  image_channels = self._input_channels,
                            image_width = self._input_width,
                            image_height = self._input_height,
                            filters_number = filters_numbers)

        #((H-8)/2-4)x((W-8)/2-4)
        linear_in_size = 1
        for d in convNet.output_shape:
            linear_in_size *= d
        linear_out_size = self._encoding_size

        #print(f"linear_size = {linear_in_size}, {linear_out_size}, ")
        self.encoder = nn.Sequential(   convNet,

                                        nn.Flatten(),
                                        nn.Linear(linear_in_size, linear_out_size),
                                        nn.ReLU())

    def encoding_size(self):
        return self._encoding_size

    def input_width(self):
        return self._input_width

    def input_height(self):
        return self._input_height

    def input_channels(self):
        return self._input_channels

    def forward(self, x : torch.Tensor) -> torch.Tensor:
        """Perform a forward pass of the network with the provided batch.

        Parameters
        ----------
        x : torch.Tensor
            A batch of images. Size must be (batch_size, img_height, img_width)

        Returns
        -------
        x : torch.Tensor
            A batch of encoded images. Size will be (batch_size, encoding_size)

        """
        assert x.size() == (x.size()[0],self._input_channels,self._input_height, self._input_width), f"Image should have size {(x.size()[0],self._input_channels,self._input_height, self._input_width)}, but it is {x.size()}"

        return self.encoder(x)

