from distutils.command.build import build
import torch as th
import torch.nn as nn
from typing import Tuple


from autoencoding_rl.nets.ConvNet import ConvNet
from autoencoding_rl.nets.Parallel import Parallel
import lr_gym.utils.dbg.ggLog as ggLog
from autoencoding_rl.nets.ResNet18 import ResNet18
from autoencoding_rl.nets.MobileNetV3 import MobileNetV3
from autoencoding_rl.utils import build_mlp_net
from operator import mul
from functools import reduce
class AE_encoder(nn.Module):
    def __init__(self, image_channels_num : int = 1,
                    net_input_width : int = 64,
                    net_input_height : int = 64,
                    backbone : str = "conv",
                    checkDimensions : bool = True,
                    torchDevice : str = "cuda",
                    use_coord_conv : bool = False,
                    ensemble_size : int = 1,
                    fc_net_hiddens = None,
                    output_size = None):
        super().__init__()
        self._checkDimensions = checkDimensions
        self._input_width  = net_input_width
        self._input_height = net_input_height
        self._input_channels = image_channels_num
        self._backbone = backbone.lower()
        self._ensemble_size = ensemble_size

        if self._input_height!=self._input_width:
            raise NotImplementedError("Only square images are supported")

        if self._backbone=="mobilenetv3":
            self.encoder = Parallel([MobileNetV3(   image_channels = self._input_channels,
                                                    image_width = self._input_width,
                                                    image_height = self._input_height)
                                     for _ in range(self._ensemble_size)])
        elif self._backbone=="resnet18":
            self.encoder = Parallel([MobileNetV3(   image_channels = self._input_channels,
                                                    image_width = self._input_width,
                                                    image_height = self._input_height)
                                     for _ in range(self._ensemble_size)])
        elif self._backbone=="conv":
            if self._input_height==64 or self._input_width==64:
                enc_layers_channels_num = [32, 64, 128, 256]
            elif self._input_height==84 or self._input_width==84:
                enc_layers_channels_num = [32, 64, 128, 192, 256]
                enc_layers_strides      = [2,   1,  2,  2,  2]
            elif self._input_height==128 or self._input_width==128:
                enc_layers_channels_num = [32, 64, 128, 192, 256]
            else:
                raise NotImplementedError(f"Currently only 64x64 or 128x128 network input is supported with convnet backbone. You asked for height={self._input_height}, width={self._input_width}")
            for i in range(len(enc_layers_channels_num)):
                enc_layers_channels_num[i] *= ensemble_size
            self.encoder = ConvNet( image_channels = self._input_channels,
                                                    image_width = self._input_width,
                                                    image_height = self._input_height,
                                                    filters_number = enc_layers_channels_num,
                                                    torchDevice = torchDevice,
                                                    use_coord_conv = use_coord_conv)
        elif self._backbone=="conv_small":
            enc_layers_strides = None
            if self._input_height==64 or self._input_width==64:
                enc_layers_channels_num = [32, 32, 32, 32]
            elif self._input_height==84 or self._input_width==84:
                enc_layers_channels_num = [32, 32, 32, 32, 32]
                # enc_layers_strides      = [2,   1,  1,  1,  1]
            elif self._input_height==128 or self._input_width==128:
                enc_layers_channels_num = [32, 32, 32, 32, 32]
            else:
                raise NotImplementedError(f"Currently only 64x64 or 128x128 network input is supported with convnet backbone. You asked for height={self._input_height}, width={self._input_width}")
            for i in range(len(enc_layers_channels_num)):
                enc_layers_channels_num[i] *= ensemble_size
            self.encoder = ConvNet( image_channels = self._input_channels,
                                                    image_width = self._input_width,
                                                    image_height = self._input_height,
                                                    filters_number = enc_layers_channels_num,
                                                    strides = enc_layers_strides,
                                                    use_coord_conv = use_coord_conv)
        else:
            raise AttributeError(f"Unknown backbone '{self._backbone}'")

        self._conv_out_shape = self.encoder.output_shape
        self._conv_out_size = 1
        for s in self._conv_out_shape:
            self._conv_out_size *= s
        self._conv_out_size = int(self._conv_out_size/self._ensemble_size)

        if fc_net_hiddens is not None:
            self._fc = build_mlp_net(fc_net_hiddens,self._conv_out_size,output_size,ensemble_size=ensemble_size,last_activation_class=th.nn.LeakyReLU)
            self._output_size = output_size
        else:
            self._fc = None
            if output_size is not None:
                raise AttributeError("output_size must be specified only if fc_net_hiddens is specified")
            self._output_size = self._conv_out_size



    @property
    def output_shape(self):
        return (self._output_size,)

    def input_width(self):
        return self._input_width

    def input_height(self):
        return self._input_height

    def input_channels(self):
        return self._input_channels

    def forward(self, x: th.Tensor) -> Tuple[th.Tensor, th.Tensor]:
        """

        Parameters
        ----------
        x : th.Tensor
            Batch of input images. Shape should be [batch_size,input_channels,input_height,input_width]
 
        Returns
        -------
        List[th.Tensor]
            Two batches, containing latent space mean and log-variance for each input image.
            Size of each of the two batches will be (batch_size, latent_space_size)
        """
        if self._checkDimensions:
            assert x.size() == (x.size()[0],self._input_channels,self._input_height, self._input_width), f"Image batch should have size {(x.size()[0],self._input_channels,self._input_height, self._input_width)}, but it is {x.size()}"

        # ggLog.info(f"VAE_encoder.forward(): x.size() = {x.size()}")
        conv_out = self.encoder(x) # outputs a (batch_size, ensemble_size*filters[-1], x, y) tensor
        # ggLog.info(f"conv_out.size() = {conv_out.size()}")
        if self._backbone == "conv" or self._backbone == "conv_small":
            conv_out = th.reshape(conv_out,(self._ensemble_size, -1, int(self._conv_out_shape[0]/self._ensemble_size), self._conv_out_shape[1], self._conv_out_shape[2]))
            conv_out = th.flatten(conv_out, start_dim = 2) # now we have (ensemble_size, batch_size, filters[-1]*x*y)
        # ggLog.info(f"conv_out2.size() = {conv_out.size()}")
        if self._fc is not None:
            fc_outs = self._fc(th.unbind(conv_out,1,dim=0)) # now we have (ensemble_size, batch_size, output_size)
        else:
            fc_outs = conv_out
        # ggLog.info(f"fc_outs.size() = {fc_outs.size()}")
        # fc_out = th.mean(fc_outs, dim = 0) # now it's (batch_size, output_size)
        
        if self._checkDimensions:
            batch_size = x.size()[0]
            assert len(fc_outs.size())==3, "Encoder output should have three dimensions (ensemble,batch,output)"
            assert fc_outs.size()==(self._ensemble_size, batch_size,self._output_size), f"Encoder outputs should have size (nsemble_sze, batch_size, out_size)=={(self._ensemble_size, batch_size,self._output_size)}, but enc_out.size() = {fc_out.size()}"
            
        return fc_outs
            

    