from turtle import back
import torch as th
import torch.nn as nn
from typing import List
import torchvision
from torchvision.transforms.functional import InterpolationMode
import lr_gym.utils.dbg.ggLog as ggLog

from autoencoding_rl.nets.DeconvNet import DeconvNet
from autoencoding_rl.nets.Parallel import Parallel


class AE_decoder(nn.Module):
    def __init__(self,  latent_space_size : int,
                        output_channels_num : int = 1,
                        net_output_width : int = 64,
                        net_output_height : int = 64,
                        checkDimensions : bool = True,
                        torchDevice : str = "cuda",
                        backbone = "conv",
                        ensemble_size = 1):
        super().__init__()
        self._checkDimensions = checkDimensions
        self._output_width  = net_output_width
        self._output_height = net_output_height
        self._output_channels = output_channels_num
        self._latent_space_size = latent_space_size
        self._resizeToOutput = None
        self._backbone = backbone
        self._ensemble_size = ensemble_size

        if self._output_height!=self._output_width:
            raise NotImplementedError("Only square images are supported")

        dec_layers_scales = None
        dec_layers_strides = None
        dec_kernel_sizes = None
        dec_paddings = None
        if self._backbone=="conv":
            if self._output_height==64 and self._output_width==64:
                dec_layers_channels_num = [256, 128, 64, 32]
                dec_kernel_sizes =        [  3,   3,  3,  5]
                self._net_output_size = self._output_width
                dec_input_shape = (dec_layers_channels_num[0],4,4)
            elif self._output_height==84 and self._output_width==84:
                # dec_layers_channels_num = [256, 128, 64, 32, 32]
                # dec_layers_scales  = [2,2,2,2,2]
                # self._net_output_size = 160
                # dec_input_shape = (dec_layers_channels_num[0],5,5)
                # intMode = InterpolationMode.NEAREST # if th.is_deterministic() else InterpolationMode.BILINEAR  # Bilinear does not support deterministic
                # self._resizeToOutput = torchvision.transforms.Resize((self._output_height,self._output_width),
                #                                                         interpolation=intMode)
                dec_layers_channels_num = [256, 128, 64,  32]
                dec_layers_scales =       [  2,   2,  2, 2.1]
                dec_kernel_sizes =        [  3,   3,  3,   3]
                dec_paddings =            [  1,   1,  1,   1]
                self._net_output_size = self._output_width
                dec_input_shape = (dec_layers_channels_num[0],5,5)
            elif self._output_height==128 and self._output_width==128:
                dec_layers_channels_num = [256, 192, 128, 64, 32]
                dec_kernel_sizes =        [  3,   3,  3,   3,  3]
                self._net_output_size = self._output_width
                dec_input_shape = (dec_layers_channels_num[0],4,4)
            elif self._output_height==256 and self._output_width==256:
                dec_layers_channels_num = [256, 192, 128, 64, 64, 32]
                dec_kernel_sizes =        [  3,   3,   3,  3,  3,  3]
                self._net_output_size = self._output_width
                dec_input_shape = (dec_layers_channels_num[0],4,4)
            elif self._output_height==224 and self._output_width==224:
                dec_layers_channels_num = [256, 192, 128, 64, 64, 32]
                dec_kernel_sizes =        [  3,   3,   3,  3,  3,  3]
                self._net_output_size = 256
                dec_input_shape = (dec_layers_channels_num[0],4,4)
                intMode = InterpolationMode.NEAREST # if th.is_deterministic() else InterpolationMode.BILINEAR  # Bilinear does not support deterministic
                self._resizeToOutput = torchvision.transforms.Resize((self._output_height,self._output_width),
                                                                        interpolation=intMode)
            else:
                raise NotImplementedError(f"Requested network output size is not supported. You asked for backbone='{backbone}' height={self._output_height}, width={self._output_width}")
        elif self._backbone=="conv_small":
            
            if self._output_height==64 and self._output_width==64:
                dec_layers_channels_num = [32, 32, 32, 32]
                dec_kernel_sizes =        [ 3,  3,  3,  3]
                self._net_output_size = self._output_width
                dec_input_shape = (dec_layers_channels_num[0],4,4)
            elif self._output_height==84 and self._output_width==84:
                # dec_layers_channels_num = [256, 128, 64, 32, 32]
                # dec_layers_scales  = [2,2,2,2,2]
                # self._net_output_size = 160
                # dec_input_shape = (dec_layers_channels_num[0],5,5)
                # intMode = InterpolationMode.NEAREST # if th.is_deterministic() else InterpolationMode.BILINEAR  # Bilinear does not support deterministic
                # self._resizeToOutput = torchvision.transforms.Resize((self._output_height,self._output_width),
                #                                                         interpolation=intMode)
                dec_layers_channels_num = [32, 32, 32,  32]
                dec_layers_scales =       [ 2,  2,  2, 2.1]
                dec_kernel_sizes =        [ 3,  3,  3,   3]
                dec_paddings =            [ 1,  1,  1,   1]
                self._net_output_size = self._output_width
                dec_input_shape = (dec_layers_channels_num[0],5,5)
            elif self._output_height==128 and self._output_width==128:
                dec_layers_channels_num = [32, 32, 32, 32, 32]
                dec_kernel_sizes =        [ 3,  3,  3,  3,  3]
                self._net_output_size = self._output_width
                dec_input_shape = (dec_layers_channels_num[0],4,4)
            elif self._output_height==256 and self._output_width==256:
                dec_layers_channels_num = [32, 32, 32, 32, 32, 32]
                dec_kernel_sizes =        [ 3,  3,  3,  3,  3,  3]
                self._net_output_size = self._output_width
                dec_input_shape = (dec_layers_channels_num[0],4,4)
            elif self._output_height==224 and self._output_width==224:
                dec_layers_channels_num = [32, 32, 32, 32, 32, 32]
                dec_kernel_sizes =        [ 3,  3,  3,  3,  3,  3]
                self._net_output_size = 256
                dec_input_shape = (dec_layers_channels_num[0],4,4)
                intMode = InterpolationMode.NEAREST # if th.is_deterministic() else InterpolationMode.BILINEAR  # Bilinear does not support deterministic
                self._resizeToOutput = torchvision.transforms.Resize((self._output_height,self._output_width),
                                                                        interpolation=intMode)
            else:
                raise NotImplementedError(f"Requested network output size is not supported. You asked for backbone='{backbone}' height={self._output_height}, width={self._output_width}")
            

        # ggLog.info(f"dec_input_shape = {dec_input_shape}")
        # ggLog.info(f"dec_layers_channels_num = {dec_layers_channels_num}")
        deconvNet = Parallel([DeconvNet(output_channels = self._output_channels,
                                        output_width = self._net_output_size,
                                        output_height = self._net_output_size,
                                        filters_number = dec_layers_channels_num,
                                        scales = dec_layers_scales,
                                        strides = dec_layers_strides,
                                        kernel_sizes = dec_kernel_sizes,
                                        paddings = dec_paddings,
                                        torchDevice = torchDevice,
                                        input_shape_chw = dec_input_shape) 
                                for _ in range(self._ensemble_size)],
                                return_mean=True)
        
        
        linear_out_size = dec_input_shape[0]*dec_input_shape[1]*dec_input_shape[2]
        self.decoder  = nn.Sequential(  nn.Linear(self._latent_space_size, linear_out_size),                                        
                                        nn.ReLU(),
                                        nn.Unflatten(dim = 1, unflattened_size = dec_input_shape),
                                        deconvNet)



    @property
    def latent_space_size(self):
        return self._latent_space_size

    @property
    def output_width(self):
        return self._output_width

    @property
    def output_height(self):
        return self._output_height

    @property
    def output_channels(self):
        return self._output_channels

    def forward(self, x: th.Tensor) -> th.Tensor:
        """Decode a batch of images.

        Parameters
        ----------
        x : torch.Tensor
            Batch of encoded images. Size must be (batch_size, self.latent_space_size)

        Returns
        -------
        x : torch.Tensor
            A batch of images. Size will be (batch_size, output_height, output_width)

        """
        if self._checkDimensions:
            assert x.size() == (x.size()[0],self._latent_space_size), f"latent-vecotrs batch should have size {(x.size()[0],self._latent_space_size)}, not {x.size()}"

        result = self.decoder(x)

        # ggLog.info(f"result raw size = {result.size()}")
        if self._resizeToOutput is not None:
            result = self._resizeToOutput(result)
        # ggLog.info(f"result resized size = {result.size()}")
        
        return result
            


    