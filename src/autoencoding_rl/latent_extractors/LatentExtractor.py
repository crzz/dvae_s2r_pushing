from abc import ABC, abstractmethod
from typing import Dict, OrderedDict, Any, List, Union
import csv
import os
import time
import torch as th
import torch.nn as nn
import traceback
from autoencoding_rl.utils import Transition, ImageComparisonWindow    
import lr_gym.utils.dbg.ggLog as ggLog
import yaml
import inspect

class LatentExtractor(ABC, nn.Module):



    def __init__(self,  debug_out_folder : str = None):
        super().__init__()
        self.dbgOutFolder = debug_out_folder
        self._initTime = time.monotonic()
        self._did_setup_logs = False
        self._info : Dict[str,Any] = {}
        self._debug_mode = True
        self._comparisonWindow = None

        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        self._init_args = values
        self._init_args.pop("self")

    @abstractmethod
    def train_model(self, training_batch : Transition, validation_batch : Transition) -> float:
        raise NotImplementedError()

    @abstractmethod
    def evaluate(self, transition_batch : Transition):
        raise NotImplementedError()

    @abstractmethod
    def encode(self, observation_batch : Dict[Union[str, int], th.Tensor]):
        raise NotImplementedError()

    @abstractmethod
    def preAndPostProcess(self, observation_batch : Dict[Union[str, int], th.Tensor]):
        raise NotImplementedError()

    @abstractmethod
    def reset(self):
        raise NotImplementedError()

    @abstractmethod
    def encoding_size(self) -> int:
        raise NotImplementedError()

    @abstractmethod
    def displayDebugInfo(self, transition_batch_example : Transition, id : str = None) -> None:
        raise NotImplementedError()


    @abstractmethod
    def decode(self, encodedImg_batch : th.Tensor, action_batch : th.Tensor):
        raise NotImplementedError()

    @abstractmethod
    def torchDevice(self):
        raise NotImplementedError()

    def setupDbgLogs(self):
        # ggLog.info(f"dbg_info_keys = {dbg_info_keys}")
        # traceback.print_stack()
        # time.sleep(5)
        # print("\n\n\n\n")
        if self.dbgOutFolder is not None:
            os.makedirs(self.dbgOutFolder, exist_ok=True)
            self._logFileName = self.dbgOutFolder + "/simple_feature_extractor_log.csv"

            self._comparisonWindow = ImageComparisonWindow(outFolder = self.dbgOutFolder)
            try:
                os.makedirs(os.path.dirname(self._logFileName))
            except FileExistsError:
                pass
            fileAlreadyExisted = os.path.exists(self._logFileName)
            self._logFile = open(self._logFileName, "a")
            self._logFileCsvWriter = csv.writer(self._logFile, delimiter = ",")
            if not fileAlreadyExisted:
                # ggLog.info("Creating simple_feature_extractor_log, keys =",self._info.keys())
                self._logFileCsvWriter.writerow(self._info.keys())
            with open(self.dbgOutFolder+"/init_args.yaml", "w") as input_args_yamlfile:
                yaml.dump(self._init_args,input_args_yamlfile, default_flow_style=None)
            
            for name,ae in self.getAutoencoders().items():
                with open(self.dbgOutFolder+f"/ae_{name}_hyperparams.yaml", "w") as outfile:
                    yaml.dump(ae.get_hyperparams(),outfile, default_flow_style=None)

    def _writeToLog(self):
        if not self._did_setup_logs:
            self._dbg_info_keys = self._info.keys()
            self.setupDbgLogs()
            self._did_setup_logs = True
        self._logFileCsvWriter.writerow(self._info.values())
        self._logFile.flush()