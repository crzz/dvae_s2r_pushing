from abc import ABC, abstractmethod
from typing import Dict, OrderedDict, Any, List, Union
import csv
import os
import time
import torch as th
import numpy as np
import traceback
from autoencoding_rl.utils import Transition, ImageComparisonWindow    
from autoencoding_rl.latent_extractors.LatentExtractor import LatentExtractor
from autoencoding_rl.utils import ObsConverter
import gym
import lr_gym.utils.dbg.ggLog as ggLog


class FakeLatentExtractor(LatentExtractor):



    def __init__(self,  torchDevice,
                        encoding_size,
                        observation_shape,
                        debug_out_folder : str = None,
                        encoding_noise_std : float = 0.0):
        super().__init__(debug_out_folder)
        self._encoding_size = encoding_size
        self._torchDevice = torchDevice
        self._encoding_noise_std = encoding_noise_std
        if len(observation_shape.spaces.keys())>1 or not isinstance(observation_shape, gym.spaces.Dict):
            raise AttributeError("Only dict observations with only one element are supported")
        self._dict_key = list(observation_shape.spaces.keys())[0]
        if np.prod(observation_shape[self._dict_key].shape)!=self.encoding_size():
            raise AttributeError(f"observation_shape {observation_shape} and encoding_size {encoding_size} do not match")
        

    def train_model(self, training_batch : Transition, validation_batch : Transition = None, episode_count : int = -1):
        return 0

    def evaluate(self, transition_batch : Transition, use_latest : bool = False) -> th.Tensor:
        return 0

    def encode(self, observation_batch : Dict[Union[str, int], th.Tensor]):
        observation_batch = observation_batch[self._dict_key].to(self._torchDevice)
        if self.training:
            # ggLog.info("FakeLatentExtractor: injecting noise")
            observation_batch = th.normal(observation_batch, std = self._encoding_noise_std)
        # else:
        #     ggLog.info("FakeLatentExtractor: no noise")
        
        return observation_batch

    def preAndPostProcess(self, observation_batch : Dict[Union[str, int], th.Tensor]):
        return observation_batch

    def reset(self):
        pass

    def encoding_size(self) -> int:
        return self._encoding_size

    def displayDebugInfo(self, transition_batch_example : Transition, id : str = None) -> None:
        pass

    def decode(self,  encoded_obs_batch : th.Tensor, action_batch : th.Tensor = None):
        return {self._dict_key: encoded_obs_batch}

    def torchDevice(self):
        return self._torchDevice
    
    def train_iterations_count(self):
        return 0

    
    def getModelSizeRecap(self):
        return "FakeLatentExtractor: 0 parameters"