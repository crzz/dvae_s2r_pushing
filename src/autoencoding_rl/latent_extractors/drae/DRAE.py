
from typing import Tuple

import torch as th
import torch.nn as nn
from torchvision import transforms
from typing import Dict, Union, List, Tuple, Callable, Final

import lr_gym.utils.dbg.ggLog as ggLog
import lr_gym

from autoencoding_rl.latent_extractors.vae.VAE_encoder import VAE_encoder
from autoencoding_rl.latent_extractors.autoencoder.AE_decoder import AE_decoder
from autoencoding_rl.utils import Transition
from autoencoding_rl.utils import ObsConverter
from autoencoding_rl.latent_extractors.BaseAutoencoder import BaseAutoencoder
import lr_gym.utils.dbg.dbg_img as dbg_img
from autoencoding_rl.utils import tensorToHumanCvImageRgb
from torchvision.transforms.functional import InterpolationMode
from autoencoding_rl.nets.Parallel import Parallel
from autoencoding_rl.latent_extractors.autoencoder.AE_encoder import AE_encoder

import gym
import inspect

@th.jit.script
def latent_loss_func(latent_loss_weight, latent_state_batch):
    # https://arxiv.org/pdf/1903.12436.pdf
    return latent_loss_weight*th.log((0.5 * latent_state_batch.pow(2).sum(dim=1)).mean())

@th.jit.script
def prediction_loss_func(   reward_weight,
                            vec_weight,
                            reward_batch,
                            predicted_reward_batch,
                            next_vec_batch,
                            predicted_vec_batch,
                            next_img_batch,
                            predicted_img_batch):
    # Second part of the loss should theoretically maxmise:
    #    E_{z~q_theta}[log(p_phi(x_i|z))]
    # Which with q_theta(z|x_i) = N(mu, e^logvar) can be reparametrized:
    #    1/L*∑_i∈[0,L] [log(p_phi(x_i | mu_theta + var_theta * eps)] with eps~N(0,I)
    # i.e. it's a maximum log-likelihood term, in which phi and theta must be optimized
    # At this point there's some considerations:
    #  - we can just set L = 1 if batch sizes are big enough (~100)
    #  - Following what section 5.1 of "A Tutorial on VAEs: From Bayes' Rule to Lossless Compression" by Roland Yu says,
    #    we use a logMSE loss (and it does make a nice difference from normal MSE)
    reward_loss = reward_weight*th.log(th.nn.functional.mse_loss(predicted_reward_batch, reward_batch)+0.0000001)
    if next_vec_batch.size()[1] != 0:
        vec_loss = vec_weight*th.log(th.nn.functional.mse_loss(next_vec_batch, predicted_vec_batch)+0.0000001)
    else:
        vec_loss = th.tensor(0)



    # next_img = (next_img_batch[0]+1)/2
    # pred_img = (predicted_img_batch[0]+1)/2
    # recdiff = (th.abs(next_img_batch[0]-predicted_img_batch[0])+1)/2
    # thimg_side2side = th.cat((next_img, pred_img, recdiff), dim=2)
    # dbg_img.helper.publishDbgImg("reconstruction_diff", tensorToHumanCvImageRgb(thimg_side2side))



    img_loss = th.log(th.nn.functional.mse_loss(next_img_batch, predicted_img_batch)+0.0000001)
    reconstruction_loss = img_loss + reward_loss + vec_loss

    return reconstruction_loss, img_loss, reward_loss, vec_loss

@th.jit.script
def reconstruction_loss_func(reconstruction_loss_weight,
                                input_img_batch,
                                reconstructed_img_batch):
    return reconstruction_loss_weight * th.log(th.nn.functional.mse_loss(input_img_batch, reconstructed_img_batch))

         
def weight_init(m):
    """Custom weight init for Conv2D and Linear layers."""
    if isinstance(m, nn.Linear):
        nn.init.orthogonal_(m.weight.data)
        m.bias.data.fill_(0.0)
    elif isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
        # delta-orthogonal init from https://arxiv.org/pdf/1806.05393.pdf
        assert m.weight.size(2) == m.weight.size(3)
        m.weight.data.fill_(0.0)
        if m.bias is not None:
            m.bias.data.fill_(0.0)
        mid = m.weight.size(2) // 2
        gain = nn.init.calculate_gain('relu')
        nn.init.orthogonal_(m.weight.data[:, :, mid, mid], gain)

class DRAE(BaseAutoencoder):
    
    def __init__(self,  observation_shape : Union[Dict[str,List[int]],Tuple[int,int,int], gym.spaces.Dict],
                        img_encoding_size: int,
                        action_size: int,
                        dynamics_nn_arch: Tuple[int, int] = None,
                        rewardLossWeight : float = 1.0,

                        vecLossWeight : float = 0.005,
                        torchDevice : str = "cuda",
                        encoder_ensemble_size = 1,
                        dynamics_ensemble_size = 1,
                        dynamics_model_class : type = None,
                        backbone : str = "conv",
                        deconv_backbone : str = None,
                        augment_callback : Callable[[th.Tensor], th.Tensor] = None,
                        checkDimensions : bool = True,
                        checkFinite : bool = True,
                        e2eResidual : bool = False,
                        reconstructionLossWeight : float = 1.0,
                        state_combiner_nn_arch : List[int] = "identity",
                        
                        frame_stack_size : int = 1,
                        decoder_weight_decay_lambda : float = 1e-7,
                        latent_loss_weight : float = 0.1,
                        learning_rate : float = 0.0001,
                        use_coord_conv : bool = True):

        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        self._init_args = values
        self._init_args.pop("self")

        self.IMAGE = 1
        self.VECTOR = 2
        self._checkDimensions : Final[bool] = checkDimensions
        self._checkFinite : Final[bool] = checkFinite
        self._e2eResidual = e2eResidual
        self._decoder_weight_decay_lambda = decoder_weight_decay_lambda
        self._learning_rate = learning_rate
        self._use_coord_conv = use_coord_conv

        self._skippedMinibatchesCount = 0
        self._skippedMinibatchesStreakCount = 0

        super().__init__()

        if dynamics_model_class is not None and dynamics_nn_arch is not None:
            raise AttributeError("You can only set one at a time of dynamics_nn_arch and dynamics_model_class")
        if dynamics_model_class is None and dynamics_nn_arch is None:
            raise AttributeError("You must set either dynamics_nn_arch or dynamics_model_class. None of the two is set.")


        self._obs_converter = ObsConverter(observation_shape = observation_shape)
        img_shape_chw = self._obs_converter.imageSizeCHW()
        self._vec_part_size = self._obs_converter.vectorPartSize()
        self._torchDevice = torchDevice
        self._observation_shape = observation_shape

        self._img_channels = img_shape_chw[0]
        self._img_height = img_shape_chw[1]
        self._img_width = img_shape_chw[2]
        self._img_encoding_size = img_encoding_size
        self._action_size = action_size
        self._dynamics_nn_arch = dynamics_nn_arch
        self._state_combiner_nn_arch = state_combiner_nn_arch
        self._rewardWeight = th.tensor(rewardLossWeight, device=self._torchDevice)
        self._latent_loss_weight = th.tensor(latent_loss_weight, device=self._torchDevice)
        self._vec_lossWeight = th.tensor(vecLossWeight, device=self._torchDevice)
        self._img_encoder_ensemble_size = encoder_ensemble_size
        self._dynamics_ensemble_size = dynamics_ensemble_size
        self._dynamics_model_class = dynamics_model_class
        self._backbone = backbone
        self._deconv_backbone = deconv_backbone if deconv_backbone is not None else backbone
        self._deconv_backbone = "conv_small" if self._deconv_backbone == "conv_small" else "conv"
        self._augment_callback = augment_callback
        self._frame_stack_size = frame_stack_size

        self._reconstruction_loss_weight = th.tensor(reconstructionLossWeight, device=self._torchDevice)

        self._resizeToInput = transforms.Resize((self._img_height,self._img_width),interpolation=InterpolationMode.BILINEAR)

        self._buildModels()

    
    def _buildModels(self):
        self._img_encoder = VAE_encoder( latent_space_size = self._img_encoding_size,
                                        image_channels_num = self._img_channels,
                                        net_input_width = self._img_width,
                                        net_input_height = self._img_height,
                                        ensemble_size = self._img_encoder_ensemble_size,
                                        backbone = self._backbone,
                                        checkDimensions = self._checkDimensions,
                                        torchDevice = self._torchDevice,
                                        use_coord_conv = self._use_coord_conv)
        # ggLog.info(f"Created encoder with {self._img_channels} input channels")

        if self._state_combiner_nn_arch == "identity":
            self._state_combiners = nn.ModuleList([th.nn.Identity()])
        else:
            combiner_nets = []
            for _ in range(1): # Actually, don't do ensembles here
                layersizes = []
                layersizes.append(self.encoding_size())
                layersizes += list(self._state_combiner_nn_arch)
                layersizes.append(self.encoding_size())
                layers = []
                for i in range(len(layersizes)-1):
                    layers.append(th.nn.Linear(layersizes[i],layersizes[i+1]))
                    layers.append(th.nn.ReLU())
                combiner_nets.append(th.nn.Sequential(*layers))
            self._state_combiners = nn.ModuleList(combiner_nets)

        dyn_nets = []
        for _ in range(self._dynamics_ensemble_size):
            if self._dynamics_model_class is None:
                layersizes = []
                layersizes.append(self.encoding_size()+self._action_size)
                layersizes += list(self._dynamics_nn_arch)
                layersizes.append(self.encoding_size()+1)
                layers = []
                for i in range(len(layersizes)-1):
                    layers.append(th.nn.Linear(layersizes[i],layersizes[i+1]))
                    layers.append(th.nn.ReLU())
                dyn_nets.append(th.nn.Sequential(*layers))
            else:
                dyn_nets.append(self._dynamics_model_class())
        self._dynamics_nets = nn.ModuleList(dyn_nets)

        if self._img_channels % self._frame_stack_size != 0:
            raise RuntimeError(f"image channels (={self._img_channels}) are not divisible by frame_stack_size (={self._frame_stack_size}")
        self._decoder = AE_decoder(latent_space_size = self.encoding_size(),
                                    output_channels_num = int(self._img_channels/self._frame_stack_size),
                                    net_output_width = self._img_width,
                                    net_output_height = self._img_height,
                                    torchDevice = self._torchDevice,
                                    backbone = self._deconv_backbone)
        ggLog.info(f"Created decoder with {int(self._img_channels/self._frame_stack_size)} output channels")

        self._enc_optimizer = th.optim.Adam(self._img_encoder.parameters(), lr = self._learning_rate)
        self._dec_optimizer = th.optim.Adam(self._decoder.parameters(), lr = self._learning_rate, weight_decay=self._decoder_weight_decay_lambda)
        self._dyn_optimizer = th.optim.Adam(self._dynamics_nets.parameters(), lr = self._learning_rate)
        self._optimizers = [self._enc_optimizer, self._dec_optimizer, self._dyn_optimizer]
        if self._state_combiner_nn_arch != "identity":
            self._comb_optimizer = th.optim.Adam(self._state_combiners.parameters(), lr = self._learning_rate)
            self._optimizers.append(self._comb_optimizer)
        for opt in self._optimizers: opt.zero_grad()

        self.apply(weight_init)

    def reset(self):        
        self._buildModels()


    def train_model(self, preprocessed_transition_batch):
        # th.cuda.synchronize()
        # tpcl = time.monotonic()
        self.train() # Put module in train mode
        for opt in self._optimizers: opt.zero_grad()
        with th.set_grad_enabled(True):
            try:
                loss, reconstruction_loss, latent_loss, reward_loss = self.compute_losses(preprocessed_transition_batch)
            except RuntimeError as e:
                # Look the other way an go on
                self._skippedMinibatchesCount += 1
                self._skippedMinibatchesStreakCount += 1
                ggLog.warn(f"RuntimeError during compute_losses(). Skipping minibatch (counter = {self._skippedMinibatchesStreakCount}. Exception = "+str(e))
                if self._skippedMinibatchesStreakCount > 1000:
                    raise RuntimeError(f"Skipped {self._skippedMinibatchesStreakCount} consecutive minibatches, giving up. (This probably means there's NaNs in the net)")
                return
            
        if self._checkFinite:
            if not th.all(th.isfinite(loss)):
                ggLog.warn(f"Invalid loss encountered! Skipping minibatch. lossVal = {loss.item()}")
                return
            if th.abs(loss)>100:
                ggLog.warn(f"Suspicious lossVal detected, something wrong? lossVal = {loss.item()} rec_loss = {reconstruction_loss.item()} kld_loss = {kld_loss.item()} reward_loss = {reward_loss.item()}")
        
        try:
            loss.backward()
        except RuntimeError as e:
            # Look the other way an go on
            self._skippedMinibatchesCount += 1
            self._skippedMinibatchesStreakCount += 1
            ggLog.warn(f"RuntimeError during loss.backward(). lossVal = {loss.item()}. Skipping minibatch (counter = {self._skippedMinibatchesStreakCount}. Exception = "+str(e))
            if self._skippedMinibatchesStreakCount > 1000:
                raise RuntimeError(f"Skipped {self._skippedMinibatchesStreakCount} consecutive minibatches, giving up. (This probably means there's NaNs in the net)")
            return

        self._skippedMinibatchesStreakCount = 0
        for opt in self._optimizers: opt.step()

        sublosses = {"reconstruction_loss" : reconstruction_loss,
                     "latent_loss" : latent_loss,
                     "reward_loss" : reward_loss}
        return loss, sublosses


    def encode(self, observation_batch : Dict[int,th.Tensor]):
        img_batch = observation_batch[self.IMAGE]
        vec_batch = observation_batch[self.VECTOR]
        
        mu, logvar = self._img_encoder(img_batch)
        enc_img_batch = mu


        raw_combined_state_batch = th.cat([enc_img_batch,vec_batch], dim = 1)

        combined_state_batches = [sp(raw_combined_state_batch) for sp in self._state_combiners]
        combined_state_batch = th.mean(th.stack(combined_state_batches), dim=0)
        
        return combined_state_batch

    def decode(self, state_batch : th.Tensor):
        #return self._decoder(dynamic_encoding_batch) #Gives a (batch_size, observations_channels_num, observation_height, observation_width) output
        predicted_vec_batch = state_batch[:,self._img_encoding_size : self._img_encoding_size + self._vec_part_size]
        predicted_img_batch = self._decoder(state_batch) #Gives a (batch_size, observations_channels_num, observation_height, observation_width) output

        predicted_observation_batch = { self.IMAGE : predicted_img_batch,
                                        self.VECTOR : predicted_vec_batch}
        return predicted_observation_batch

    def predict_dynamics(self, state_batch : th.Tensor, action_batch : th.Tensor):
        if self._checkDimensions:
            assert state_batch.size()[0] == action_batch.size()[0], \
                f"state batch and action batch have different sizes, respectively {state_batch.size()[0]} and {action_batch.size()[0]}"
            assert state_batch.size()[1] == self.encoding_size(), \
                f"States have wrong size, should be {self.encoding_size()}, but it's {state_batch.size()[1]}"
            assert action_batch.size()[1] == self._action_size, \
                f"Actions have wrong size, should be {self._action_size} but it's {action_batch.size()[1]}"


        # ggLog.info(f"self._action_size = {self._action_size}")
        # ggLog.info(f"self.encoding_size() = {self.encoding_size()}")
        #Concatenate states and actions
        state_action_batch = th.cat((state_batch, action_batch), 1) #Gives a (batch_size, dyn_encoding_size+action_size) output
        nextstate_reward_batches = [dnet(state_action_batch) for dnet in self._dynamics_nets] #Gives a list of (batch_size, dyn_encoding_size+1) outputs
        nextstate_reward_batch = th.mean(th.stack(nextstate_reward_batches), dim=0) # Average out the ensemble outputs
        nextstate_batch, reward_batch = th.split(nextstate_reward_batch, [self.encoding_size(), 1], 1)
        #nextstate_batch now has size (batch_size, dyn_encoding_size)
        #reward_batch now has size (batch_size, 1) (still 2-dimensional)
        return nextstate_batch, reward_batch

    def forward(self, transition_batch : Transition):
        img_batch = transition_batch.observation[self.IMAGE]
        vec_batch = transition_batch.observation[self.VECTOR]
        action_batch = transition_batch.action
        if self._checkDimensions:
            assert action_batch.size()[0] == img_batch.size()[0], \
                    f"Observation batch and action batch should have the same length. Action batch size = {action_batch.size()[0]}, image batch size = {img_batch.size()[0]}. Action tensor size = {action_batch.size()[0]}. image tensor size = {img_batch.size()[0]}"
            assert action_batch.size()[0] == vec_batch.size()[0], \
                    f"Vector batch and action batch should have the same length. Action batch size = {action_batch.size()[0]}, vector batch size = {vec_batch.size()[0]}. Action tensor size = {action_batch.size()}. Vector tensor size = {vec_batch.size()}"
            assert img_batch.size() == (img_batch.size()[0], self._img_channels, self._img_height, self._img_width), \
                    f"image size should be (Any, {self._img_channels}, {self._img_height}, {self._img_width}), instead it is  {img_batch.size()}"
            assert action_batch.size()[1] == self._action_size, \
                    f"Each action should have size {self._action_size}, not {action_batch.size()[1]}. Tensor has size {action_batch.size()}"
            assert vec_batch.size()[1] == self._vec_part_size, \
                    f"Each vector observation should have size {self._vec_part_size}, not {vec_batch.size()[1]}. Tensor has size {vec_batch.size()}"
        if self._checkFinite:
            if not th.all(th.isfinite(img_batch)): raise RuntimeError(f"Invalid value detected in img_batch! nan_detected = {th.any(th.isnan(img_batch))}")
            if not th.all(th.isfinite(vec_batch)): raise RuntimeError(f"Invalid value detected in vec_batch! nan_detected = {th.any(th.isnan(vec_batch))}")
            if not th.all(th.isfinite(action_batch)): raise RuntimeError(f"Invalid value detected in action_batch! nan_detected = {th.any(th.isnan(action_batch))}")

        augmented_img_batch = self._augment(img_batch)
        augmented_obs_batch = { self.IMAGE  : augmented_img_batch,
                                self.VECTOR : vec_batch}

        state_batch = self.encode(augmented_obs_batch)

        if self._checkFinite:
            if not th.all(th.isfinite(state_batch)): raise RuntimeError(f"Invalid value detected in state_batch! state_batch = {state_batch}")
        
        reconstructed_input_obs = self.decode(state_batch) #.detach())

        predicted_state_batches = []
        predicted_reward_batches = []
        predicted_img_batches = []
        current_state_batch = state_batch
        for i in range(self._frame_stack_size):
            predicted_state_batch, predicted_reward_batch = self.predict_dynamics(current_state_batch, action_batch)
            predicted_state_batches.append(predicted_state_batch)
            predicted_reward_batches.append(predicted_reward_batch)
            predicted_img_batch = self.decode(predicted_state_batch)
            predicted_img_batches.append(predicted_img_batch)
            current_state_batch = predicted_state_batch

        predicted_reward_batch = th.sum(th.stack(predicted_reward_batches), dim = 0) # Assumes rewards are summed in the frame stack
        framestacked_predicted_img_batch = th.cat([obsbatch[self.IMAGE] for obsbatch in predicted_img_batches], dim = 1)
        predicted_stacked_observation_batch = { self.IMAGE  : framestacked_predicted_img_batch,
                                                self.VECTOR : predicted_img_batches[-1][self.VECTOR]} 
        #predicted_state_batch now has size (batch_size, dyn_encoding_size)
        #predicted_reward_batch now has size (batch_size, 1) (still 2-dimensional)

        if self._e2eResidual:
            predicted_stacked_observation_batch[self.IMAGE] = predicted_stacked_observation_batch[self.IMAGE] + img_batch
        
        if self._checkFinite:
            assert [th.all(th.isfinite(o)) for o in predicted_stacked_observation_batch.values()]
            assert th.all(th.isfinite(predicted_reward_batch))
        
        # pred_img = (predicted_observation_batch[self.IMAGE][0]+1)/2
        # dbg_img.helper.publishDbgImg("forward", tensorToHumanCvImageRgb(pred_img))
        # ggLog.info(f"MultiObsDynVAE.forward(): id(self)={id(self)}")

        return (predicted_stacked_observation_batch, predicted_reward_batch, reconstructed_input_obs, state_batch)


    def _loss_functions(self,   input_observation_batch : Dict[int,th.Tensor],
                                next_observation_batch : Dict[int,th.Tensor],
                                reward_batch : th.Tensor,
                                predicted_observation_batch : Dict[int,th.Tensor],
                                predicted_reward_batch,
                                reconstructed_obs_batch,
                                latent_state_batch) -> Tuple[th.Tensor,th.Tensor,th.Tensor,th.Tensor]:
        # ggLog.info("computing dyn vae loss")
        # ggLog.info(f"next_observation_batch.size() = {next_observation_batch.size()}")
        # ggLog.info(f"predicted_observation_batch.size() = {predicted_observation_batch.size()}")
        # ggLog.info(f"predicted_reward_batch.size() = {predicted_reward_batch.size()}")
        # ggLog.info(f"reward_batch.size() = {reward_batch.size()}")
        # x_i = input_image
        # (mu, logvar) describe the latent distribution conditioned by x_i

        input_img_batch = input_observation_batch[self.IMAGE]
        reconstructed_img_batch = reconstructed_obs_batch[self.IMAGE]
        next_img_batch = next_observation_batch[self.IMAGE]
        predicted_img_batch =  predicted_observation_batch[self.IMAGE]
        next_vec_batch = next_observation_batch[self.VECTOR]
        predicted_vec_batch =  predicted_observation_batch[self.VECTOR]
        if self._checkFinite:
            if not th.all(th.isfinite(next_img_batch)): raise RuntimeError(f"Invalid next_img_batch isnan={th.any(th.isnan(next_img_batch))}")
            if not th.all(th.isfinite(predicted_img_batch)): raise RuntimeError("Invalid predicted_img_batch isnan={th.any(th.isnan(predicted_img_batch))}")
            if not th.all(th.isfinite(next_vec_batch)): raise RuntimeError("Invalid next_vec_batch isnan={th.any(th.isnan(next_vec_batch))}")
            if not th.all(th.isfinite(predicted_vec_batch)): raise RuntimeError("Invalid predicted_vec_batch isnan={th.any(th.isnan(predicted_vec_batch))}")

        latent_loss = latent_loss_func(self._latent_loss_weight, latent_state_batch)
        if self._checkFinite:
            if not th.all(th.isfinite(latent_loss)): raise RuntimeError(f"Invalid KL-divergence loss {latent_loss}")


        prediction_loss, img_loss, reward_loss, vec_loss = prediction_loss_func(self._rewardWeight,
                                                                                self._vec_lossWeight,
                                                                                reward_batch,
                                                                                predicted_reward_batch,
                                                                                next_vec_batch,
                                                                                predicted_vec_batch,
                                                                                next_img_batch,
                                                                                predicted_img_batch)

        if self._checkFinite:
            if not th.all(th.isfinite(reward_loss)): raise RuntimeError(f"Invalid reward loss {reward_loss}")
            if not th.all(th.isfinite(vec_loss)): raise RuntimeError(f"Invalid vector-part loss {vec_loss}")
            if not th.all(th.isfinite(img_loss)): raise RuntimeError(f"Invalid image-part loss {img_loss}")
            if not th.all(th.isfinite(prediction_loss)): raise RuntimeError(f"Invalid reconstruction loss {prediction_loss}")


        reconstruction_loss = reconstruction_loss_func(self._reconstruction_loss_weight,
                                                        input_img_batch[:,0:int(self._img_channels/self._frame_stack_size)],
                                                        reconstructed_img_batch)
        # print(f"{reconstruction_loss}+{self._kld_loss_weight}*{kld_loss}")

        loss = prediction_loss + latent_loss + reconstruction_loss

        # ggLog.info("computed dyn vae loss")
        return loss, prediction_loss, latent_loss, reward_loss


    def compute_loss(self,  transition_batch : Transition):
        return self.compute_losses(transition_batch)[0]

    def compute_losses(self,  transition_batch : Transition) -> Tuple[th.Tensor,th.Tensor,th.Tensor,th.Tensor]:
        predicted_observation_batch, predicted_reward_batch, reconstructed_observation_batch, latent_state_batch = self(transition_batch)
        return  self._loss_functions(   transition_batch.observation,
                                        transition_batch.next_observation,
                                        transition_batch.reward,
                                        predicted_observation_batch,
                                        predicted_reward_batch,
                                        reconstructed_observation_batch,
                                        latent_state_batch)

    def _augment(self, img_batch : th.Tensor):
        if self._augment_callback is None or not self.training:
            return img_batch
        else:
            return self._augment_callback(img_batch)




    def encoding_size(self):
        return self._img_encoding_size + self._vec_part_size

    def input_height(self):
        return self._img_height

    def input_width(self):
        return self._img_width

    def input_channels(self):
        return self._img_channels

    def action_size(self):
        return self._action_size


    def preprocess_observations(self, observation_batch : Dict[str, th.Tensor]):
        #print("observation_batch.dtype = ",self._obs_converter.getImgPart(observation_batch).dtype)
        resized_img_batch = self._resizeToInput(self._obs_converter.getImgPart(observation_batch).to(self._torchDevice))

        pixMinMax = self._obs_converter.getImgPixelRange()
        resized_img_batch = resized_img_batch.to(th.float32)/(pixMinMax[1]-pixMinMax[0])

        # print(f"resized_img_batch = {resized_img_batch.dtype}, min = {th.min(resized_img_batch)}, max = {th.max(resized_img_batch)}")
        
        # Now input should be in the [0,1] range
        # We move it to [-1,1]
        normalized_img_batch = resized_img_batch*2 - 1
        #print("normalized_img_batch = ",normalized_img_batch.dtype)
        return { self.IMAGE : normalized_img_batch,
                 self.VECTOR : self._obs_converter.getVectorPart(observation_batch).to(self._torchDevice)}   

    def postprocess_observations(self, observation_batch : Dict[int,th.Tensor]):
        unnormalized_img_batch = (observation_batch[self.IMAGE] + 1)/2
        return self._obs_converter.buildDictObs(vectorPart_batch = observation_batch[self.VECTOR],
                                                imgPart_batch = unnormalized_img_batch)

    def freeze_encoders(self):
        self._img_encoder.requires_grad_(False)

    def unfreeze_encoders(self):
        self._img_encoder.requires_grad_(True)

    def freeze_decoder(self):
        self._decoder.requires_grad_(False)

    def unfreeze_decoder(self):
        self._decoder.requires_grad_(True)

    def freeze_dynamics(self):
        self._dynamics_nets.requires_grad_(False)

    def unfreeze_dynamics(self):
        self._dynamics_nets.requires_grad_(True)

    def get_init_args(self):
        return self._init_args