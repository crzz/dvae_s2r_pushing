
from typing import Tuple

from autoencoding_rl.nets.Parallel import Parallel

import torch as th
from torchvision import transforms
from typing import Dict, Union, List, Tuple, Callable, Final

import lr_gym.utils.dbg.ggLog as ggLog
import time

from autoencoding_rl.latent_extractors.autoencoder.AE_decoder import AE_decoder
from autoencoding_rl.latent_extractors.autoencoder.AE_encoder import AE_encoder
from autoencoding_rl.utils import LearningRateConf, Transition, NopSchedule
from autoencoding_rl.utils import build_mlp_net
from torchvision.transforms.functional import InterpolationMode
from autoencoding_rl.latent_extractors.MultiObsAutoencoder import MultiObsAutoencoder

import gym
import inspect

@th.jit.script
def latent_loss_func(latent_loss_weight, latent_state_batch, uselog : bool):
    # https://arxiv.org/pdf/1903.12436.pdf
    loss = latent_loss_weight*(0.5 * latent_state_batch.pow(2).sum(dim=1)).mean()

    return loss

@th.jit.script
def reconstruction_loss_func(   reward_weight,
                                vec_weight,
                                reward_batch,
                                predicted_reward_batch,
                                next_vec_batch,
                                predicted_vec_batch,
                                next_img_batch,
                                predicted_img_batch,
                                uselog : bool):

    reward_loss = th.nn.functional.mse_loss(predicted_reward_batch, reward_batch)
    img_loss = th.nn.functional.mse_loss(next_img_batch, predicted_img_batch)
    if uselog:
        reward_loss = th.log(reward_loss + 1e-8)
        img_loss = th.log(img_loss + 1e-8)
    reward_loss = reward_loss * reward_weight

    if next_vec_batch.size()[1] != 0:
        vec_loss = th.nn.functional.mse_loss(next_vec_batch, predicted_vec_batch)
        if uselog:
            vec_loss = th.log(vec_loss + 1e-8)
    else:
        vec_loss = th.tensor(0)
    vec_loss = vec_loss * vec_weight


    reconstruction_loss = img_loss + reward_loss + vec_loss

    return reconstruction_loss, img_loss, reward_loss, vec_loss

class DRAE0(MultiObsAutoencoder):
    
    def __init__(self,  observation_shape : Union[Dict[str,List[int]],Tuple[int,int,int], gym.spaces.Dict],
                        img_encoding_size: int,
                        action_size: int,
                        dynamics_nn_arch: Tuple[int, int] = None,
                        rewardLossWeight : float = 1.0,
                        latentLossWeight : float = 1e-6,
                        vecLossWeight : float = 0.005,
                        torchDevice : str = "cuda",
                        encoder_ensemble_size = 1,
                        dynamics_ensemble_size = 1,
                        dynamics_model_class : type = None,
                        backbone : str = "conv",
                        deconv_backbone : str = None,
                        augment_callback : Callable[[th.Tensor], th.Tensor] = None,
                        checkDimensions : bool = True,
                        checkFinite : bool = True,
                        e2eResidual : bool = False,
                        state_combiner_nn_arch : List[int] = "identity",
                        learning_rate : float = 0.0001,
                        decoder_weight_decay_lambda : float = 1e-7,
                        loglosses : bool = False,
                        use_coord_conv : bool = True,
                        vec_encoder_nn_arch = "identity",
                        img_decoder_ensemble_size = 1,
                        autotune_weights = True,
                        vec_decoder_nn_arch = None,
                        latent_space_activation = th.nn.ReLU):

        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        self._init_args = values
        self._init_args.pop("self")

        if isinstance(learning_rate, (float, int)):
            learning_rate = LearningRateConf(None, learning_rate)
        elif not isinstance(learning_rate, LearningRateConf):
            raise AttributeError(f"Unsupported learning_rate {learning_rate}")

        self.IMAGE = 1
        self.VECTOR = 2
        self._checkDimensions : Final[bool] = checkDimensions
        self._checkFinite : Final[bool] = checkFinite
        self._e2eResidual = e2eResidual
        self._learning_rate_conf = learning_rate
        self._decoder_weight_decay_lambda = decoder_weight_decay_lambda
        self._loglosses : Final = loglosses
        self._use_coord_conv = use_coord_conv
        self._autotune_weights = autotune_weights

        super().__init__(observation_shape=observation_shape, torchDevice = torchDevice)

        if dynamics_model_class is not None and dynamics_nn_arch is not None:
            raise AttributeError("You can only set one at a time of dynamics_nn_arch and dynamics_model_class")
        if dynamics_model_class is None and dynamics_nn_arch is None:
            raise AttributeError("You must set either dynamics_nn_arch or dynamics_model_class. None of the two is set.")


        self._observation_shape = observation_shape

        self._img_encoding_size = img_encoding_size
        self._action_size = action_size
        self._dynamics_nn_arch = dynamics_nn_arch
        self._state_combiner_nn_arch = state_combiner_nn_arch
        self._rewardWeight : Final = th.tensor(rewardLossWeight, device=self._torchDevice)
        self._latent_loss_weight : Final = th.tensor(latentLossWeight, device=self._torchDevice)
        self._vec_lossWeight : Final = th.tensor(vecLossWeight, device=self._torchDevice)
        self._img_encoder_ensemble_size = encoder_ensemble_size
        self._img_decoder_ensemble_size = img_decoder_ensemble_size
        self._dynamics_ensemble_size = dynamics_ensemble_size
        self._dynamics_model_class = dynamics_model_class
        self._backbone = backbone
        self._deconv_backbone = deconv_backbone if deconv_backbone is not None else backbone
        self._deconv_backbone = "conv_small" if self._deconv_backbone == "conv_small" else "conv"
        self._augment_callback = augment_callback
        self._skippedMinibatchesCount = 0
        self._skippedMinibatchesStreakCount = 0

        self._vec_encoder_nn_arch = vec_encoder_nn_arch
        if vec_decoder_nn_arch is None:
            vec_decoder_nn_arch = vec_encoder_nn_arch
        self._vec_decoder_nn_arch = vec_decoder_nn_arch

        self._latent_space_activation = latent_space_activation

        self._resizeToInput = transforms.Resize((self._img_height,self._img_width),interpolation=InterpolationMode.BILINEAR)

        self._buildModels()


    def getModelSizeRecap(self):
        total_params_count = sum(p.numel() for p in self.parameters() if p.requires_grad)
        r = ""                           
        r += f"DRAE0 total paramer count = {total_params_count}"
        r += f"    - img_encoder = {sum(p.numel() for p in self._img_encoder.parameters() if p.requires_grad)}"
        r += f"    - img_decoder = {sum(p.numel() for p in self._img_decoder.parameters() if p.requires_grad)}"
        r += f"    - dynamics = {sum(p.numel() for p in self._dynamics_nets.parameters() if p.requires_grad)}"
        r += f"    - combiners = {sum(p.numel() for p in self._state_combiners.parameters() if p.requires_grad)}"
        r += f"    - vec_encoder = {sum(p.numel() for p in self._vec_encoder.parameters() if p.requires_grad)}"
        r += f"    - vec_decoder = {sum(p.numel() for p in self._vec_decoder.parameters() if p.requires_grad)}"
        return r

    def _buildModels(self):


        if self._state_combiner_nn_arch == "identity":
            encoders_activation = self._latent_space_activation
            combiner_activation = th.nn.Identity
            decombiner_activation = th.nn.Identity
        else:
            encoders_activation = th.nn.LeakyReLU
            combiner_activation = th.nn.Tanh
            decombiner_activation = th.nn.LeakyReLU

        #    IMG      VEC
        #     |        |
        #    enc      enc
        #     |________|
        #         |
        #       comb
        #         |
        # ACTION——|
        #         |
        #        dyn
        #         |
        #         |——REWARD
        #         |
        #      decomb
        #         |
        #     |‾‾‾‾‾‾‾‾|
        #    dec      dec
        #     |        |
        #    IMG      VEC 



        self._img_encoder = Parallel([AE_encoder( image_channels_num = self._img_channels,
                                                    net_input_width = self._img_width,
                                                    net_input_height = self._img_height,
                                                    backbone = self._backbone,
                                                    checkDimensions = self._checkDimensions,
                                                    torchDevice = self._torchDevice,
                                                    use_coord_conv = self._use_coord_conv,
                                                    fc_net_arch = [],
                                                    output_size=self._img_encoding_size,
                                                    last_activation_class=encoders_activation,
                                                    layerNorm=True)
                                        for _ in range(self._img_encoder_ensemble_size)],
                                        return_mean=True)

        self._vec_encoder = build_mlp_net(self._vec_encoder_nn_arch, self.get_vector_part_size(), self.get_vector_part_size(),
                                                last_activation_class=encoders_activation, return_ensemble_mean=True)

        self._state_combiners = build_mlp_net(self._state_combiner_nn_arch, self.encoding_size(), self.encoding_size(),
                                                    last_activation_class=combiner_activation,  return_ensemble_mean=True)


        if self._dynamics_model_class is None:
            self._dynamics_nets = build_mlp_net(self._dynamics_nn_arch,
                                                self.encoding_size() + self._action_size,
                                                self.encoding_size() + 1,
                                                last_activation_class=self._latent_space_activation,
                                                return_ensemble_mean=True)
        else:
            self._dynamics_nets = Parallel([self._dynamics_model_class() for _ in range(self._dynamics_ensemble_size)], return_mean=True)
        

        self._state_decombiners = build_mlp_net(self._state_combiner_nn_arch, self.encoding_size(), self.encoding_size(),
                                                      last_activation_class=decombiner_activation, return_ensemble_mean=True)

        self._vec_decoder = build_mlp_net(self._vec_decoder_nn_arch, self.encoding_size(), self.get_vector_part_size(),
                                                last_activation_class=th.nn.Identity, return_ensemble_mean=True)

        self._img_decoder = AE_decoder(latent_space_size = self.encoding_size(),
                                       output_channels_num = self._img_channels,
                                       net_output_width = self._img_width,
                                       net_output_height = self._img_height,
                                       torchDevice = self._torchDevice,
                                       backbone = self._deconv_backbone,
                                       ensemble_size=self._img_decoder_ensemble_size)

        # Loss weighting as in "Multi-Task Learning Using Uncertainty to Weigh Losses for Scene Geometry and Semantics" by Kendall, Gal, Cipolla
        self._log_vars = th.nn.Parameter(th.tensor([0.0, 0.0, 0.0, 0.0],dtype=th.float32, device=self._torchDevice))
        
        
        self._img_enc_optimizer = th.optim.Adam(self._img_encoder.parameters(), lr = self._learning_rate_conf.initial_lr)
        self._img_dec_optimizer = th.optim.Adam(self._img_decoder.parameters(), lr = self._learning_rate_conf.initial_lr, weight_decay=self._decoder_weight_decay_lambda)
        self._dyn_optimizer = th.optim.Adam(self._dynamics_nets.parameters(), lr = self._learning_rate_conf.initial_lr)
        self._log_vars_optimizer = th.optim.Adam(self._img_encoder.parameters(), lr = self._learning_rate_conf.initial_lr)

        self._optimizers = [self._img_enc_optimizer, 
                            self._img_dec_optimizer,
                            self._dyn_optimizer,
                            self._log_vars_optimizer]

        if self._vec_encoder_nn_arch != "identity":
            self._vec_enc_optimizer = th.optim.Adam(self._vec_encoder.parameters(), lr = self._learning_rate_conf.initial_lr)
            self._optimizers.append(self._vec_enc_optimizer)
        if self._vec_decoder_nn_arch != "identity":
            self._vec_dec_optimizer = th.optim.Adam(self._vec_decoder.parameters(), lr = self._learning_rate_conf.initial_lr, weight_decay=self._decoder_weight_decay_lambda)
            self._optimizers.append(self._vec_dec_optimizer)


        if self._state_combiner_nn_arch != "identity":
            self._comb_optimizer = th.optim.Adam(self._state_combiners.parameters(), lr = self._learning_rate_conf.initial_lr)
            self._decomb_optimizer = th.optim.Adam(self._state_decombiners.parameters(), lr = self._learning_rate_conf.initial_lr)
            self._optimizers.append(self._comb_optimizer)
            self._optimizers.append(self._decomb_optimizer)
                            
        for opt in self._optimizers: opt.zero_grad(set_to_none=True)
        self._schedulers = [self._learning_rate_conf.build_scheduler(opt) for opt in self._optimizers]


    def reset(self):        
        self._buildModels()


    def train_model(self, preprocessed_transition_batch):
        # th.cuda.synchronize()
        # t0 = time.monotonic()
        self.train() # Put module in train mode
        for opt in self._optimizers: opt.zero_grad(set_to_none=True)
        with th.set_grad_enabled(True):
            try:
                loss, reconstruction_loss, latent_loss, reward_loss, img_loss, vec_loss = self.compute_losses(preprocessed_transition_batch)
            except RuntimeError as e:
                # Look the other way an go on
                self._skippedMinibatchesCount += 1
                self._skippedMinibatchesStreakCount += 1
                ggLog.warn(f"RuntimeError during compute_losses(). Skipping minibatch (counter = {self._skippedMinibatchesStreakCount}. Exception = "+str(e))
                if self._skippedMinibatchesStreakCount > 1000:
                    raise RuntimeError(f"Skipped {self._skippedMinibatchesStreakCount} consecutive minibatches, giving up. (This probably means there's NaNs in the net)")
                return
            
        if self._checkFinite:
            if not th.all(th.isfinite(loss)):
                ggLog.warn(f"Invalid loss encountered! Skipping minibatch. lossVal = {loss.item()}")
                return
            if th.abs(loss)>100:
                ggLog.warn(f"Suspicious lossVal detected, something wrong? lossVal = {loss.item()} rec_loss = {reconstruction_loss.item()} kld_loss = {latent_loss.item()} reward_loss = {reward_loss.item()}")
        
        try:
            loss.backward()
            th.nn.utils.clip_grad_value_(self.parameters(), 10)
        except RuntimeError as e:
            # Look the other way an go on
            self._skippedMinibatchesCount += 1
            self._skippedMinibatchesStreakCount += 1
            ggLog.warn(f"RuntimeError during loss.backward(). lossVal = {loss.item()}. Skipping minibatch (counter = {self._skippedMinibatchesStreakCount}. Exception = "+str(e))
            if self._skippedMinibatchesStreakCount > 1000:
                raise RuntimeError(f"Skipped {self._skippedMinibatchesStreakCount} consecutive minibatches, giving up. (This probably means there's NaNs in the net)")
            return

        self._skippedMinibatchesStreakCount = 0
        for opt in self._optimizers: opt.step()
        for scheduler in self._schedulers: scheduler.step()

        sub_losses = {  "reconstruction_loss" : reconstruction_loss,
                        "latent_loss" : latent_loss,
                        "reward_loss" : reward_loss,
                        "img_loss" : img_loss,
                        "vec_loss" : vec_loss}

        
        # th.cuda.synchronize()
        # tf = time.monotonic()
        # ggLog.info(f"ttrain = {tf-t0}")
        return loss, sub_losses

    @th.jit.export
    def extract_img_latent_distribution(self, img_batch : th.Tensor):
        return self._img_encoder(img_batch)

    @th.jit.export
    def encode(self, observation_batch : Dict[int,th.Tensor]):
        img_batch = observation_batch[self.IMAGE]
        vec_batch = observation_batch[self.VECTOR]
        enc_img_batch = self.extract_img_latent_distribution(img_batch)
        if self._checkFinite:
            assert th.all(th.isfinite(enc_img_batch))
        
        enc_vec_batch = self._vec_encoder(vec_batch)

        raw_combined_state_batch = th.cat([enc_img_batch,enc_vec_batch], dim = 1)
        
        comb_state_batch = self._state_combiners(raw_combined_state_batch)
        
        return comb_state_batch

    @th.jit.export
    def decode(self, predicted_state_batch : th.Tensor):

        decombined_state_batch = self._state_decombiners(predicted_state_batch)
        # predicted_vec_batch = decombined_state_batch[:,self._img_encoding_size : self._img_encoding_size + self.get_vector_part_size()]
        
        dec_predicted_vec_batch = self._vec_decoder(decombined_state_batch)
        dec_predicted_img_batch = self._img_decoder(decombined_state_batch) #Gives a (batch_size, observations_channels_num, observation_height, observation_width) output

        predicted_observation_batch = { self.IMAGE : dec_predicted_img_batch,
                                        self.VECTOR : dec_predicted_vec_batch}
        return predicted_observation_batch

    @th.jit.export
    def predict_dynamics(self, state_batch : th.Tensor, action_batch : th.Tensor):
        if self._checkDimensions:
            assert state_batch.size()[0] == action_batch.size()[0], \
                f"state batch and action batch have different sizes, respectively {state_batch.size()[0]} and {action_batch.size()[0]}"
            assert state_batch.size()[1] == self.encoding_size(), \
                f"States have wrong size, should be {self.encoding_size()}, but it's {state_batch.size()[1]}"
            assert action_batch.size()[1] == self._action_size, \
                f"Actions have wrong size, should be {self._action_size} but it's {action_batch.size()[1]}"


        # ggLog.info(f"self._action_size = {self._action_size}")
        # ggLog.info(f"self.encoding_size() = {self.encoding_size()}")
        #Concatenate states and actions
        state_action_batch = th.cat((state_batch, action_batch), 1) #Gives a (batch_size, dyn_encoding_size+action_size) output
        nextstate_reward_batch = self._dynamics_nets(state_action_batch)
        nextstate_batch, reward_batch = th.split(nextstate_reward_batch, [self.encoding_size(), 1], 1)
        #nextstate_batch now has size (batch_size, dyn_encoding_size)
        #reward_batch now has size (batch_size, 1) (still 2-dimensional)
        return nextstate_batch, reward_batch

    @th.jit.export
    def forward(self, transition_batch : Transition):
        img_batch = transition_batch.observation[self.IMAGE]
        vec_batch = transition_batch.observation[self.VECTOR]
        action_batch = transition_batch.action
        if self._checkDimensions:
            assert action_batch.size()[0] == img_batch.size()[0], \
                    f"Observation batch and action batch should have the same length. Action batch size = {action_batch.size()[0]}, image batch size = {img_batch.size()[0]}. Action tensor size = {action_batch.size()[0]}. image tensor size = {img_batch.size()[0]}"
            assert action_batch.size()[0] == vec_batch.size()[0], \
                    f"Vector batch and action batch should have the same length. Action batch size = {action_batch.size()[0]}, vector batch size = {vec_batch.size()[0]}. Action tensor size = {action_batch.size()}. Vector tensor size = {vec_batch.size()}"
            assert img_batch.size() == (img_batch.size()[0], self._img_channels, self._img_height, self._img_width), \
                    f"image size should be (Any, {self._img_channels}, {self._img_height}, {self._img_width}), instead it is  {img_batch.size()}"
            assert action_batch.size()[1] == self._action_size, \
                    f"Each action should have size {self._action_size}, not {action_batch.size()[1]}. Tensor has size {action_batch.size()}"
            assert vec_batch.size()[1] == self.get_vector_part_size(), \
                    f"Each vector observation should have size {self.get_vector_part_size()}, not {vec_batch.size()[1]}. Tensor has size {vec_batch.size()}"

        if self._checkFinite:
            if not th.all(th.isfinite(img_batch)):
                raise RuntimeError(f"Invalid value detected in img_batch! nan_detected = {th.any(th.isnan(img_batch))}")
            if not th.all(th.isfinite(vec_batch)):
                raise RuntimeError(f"Invalid value detected in vec_batch! nan_detected = {th.any(th.isnan(vec_batch))}")
            if not th.all(th.isfinite(action_batch)):
                raise RuntimeError(f"Invalid value detected in action_batch! nan_detected = {th.any(th.isnan(action_batch))}")

        augmented_img_batch = self._augment(img_batch)

        augmented_obs_batch = { self.IMAGE  : augmented_img_batch,
                                self.VECTOR : vec_batch}

        state_batch = self.encode(augmented_obs_batch)

        if self._checkFinite:
            if not th.all(th.isfinite(state_batch)):
                raise RuntimeError(f"Invalid value detected in state_batch! state_batch = {state_batch}")
        
        predicted_state_batch, predicted_reward_batch = self.predict_dynamics(state_batch, action_batch)
        #predicted_state_batch now has size (batch_size, dyn_encoding_size)
        #predicted_reward_batch now has size (batch_size, 1) (still 2-dimensional)

        #Will now use 'static' features vectors and predicted states to predict the observation
        predicted_observation_batch = self.decode(predicted_state_batch) #Gives a (batch_size, observations_channels_num, observation_height, observation_width) output
        if self._e2eResidual:
            predicted_observation_batch[self.IMAGE] = predicted_observation_batch[self.IMAGE] + img_batch
        
        if self._checkFinite:
            assert [th.all(th.isfinite(o)) for o in predicted_observation_batch.values()]
            assert th.all(th.isfinite(predicted_reward_batch))
        
        # pred_img = (predicted_observation_batch[self.IMAGE][0]+1)/2
        # dbg_img.helper.publishDbgImg("forward", tensorToHumanCvImageRgb(pred_img))
        # ggLog.info(f"MultiObsDynVAE.forward(): id(self)={id(self)}")

        return (predicted_observation_batch, predicted_reward_batch, state_batch)


    @th.jit.export
    def _loss_functions(self,   input_observation_batch : Dict[int,th.Tensor],
                                next_observation_batch : Dict[int,th.Tensor],
                                reward_batch : th.Tensor,
                                state_batch : th.Tensor,
                                predicted_observation_batch : Dict[int,th.Tensor],
                                predicted_reward_batch) -> Tuple[th.Tensor,th.Tensor,th.Tensor,th.Tensor]:
        # ggLog.info("computing dyn vae loss")
        # ggLog.info(f"next_observation_batch.size() = {next_observation_batch.size()}")
        # ggLog.info(f"predicted_observation_batch.size() = {predicted_observation_batch.size()}")
        # ggLog.info(f"predicted_reward_batch.size() = {predicted_reward_batch.size()}")
        # ggLog.info(f"reward_batch.size() = {reward_batch.size()}")
        # x_i = input_image
        # (mu, logvar) describe the latent distribution conditioned by x_i

        next_img_batch = next_observation_batch[self.IMAGE]
        predicted_img_batch =  predicted_observation_batch[self.IMAGE]
        next_vec_batch = next_observation_batch[self.VECTOR]
        predicted_vec_batch =  predicted_observation_batch[self.VECTOR]
        if self._checkFinite:
            if not th.all(th.isfinite(reward_batch)): raise RuntimeError(f"Invalid reward_batch isnan={th.any(th.isnan(reward_batch))}")
            if not th.all(th.isfinite(predicted_reward_batch)): raise RuntimeError(f"Invalid reward_batch isnan={th.any(th.isnan(predicted_reward_batch))}")
            if not th.all(th.isfinite(next_img_batch)): raise RuntimeError(f"Invalid next_img_batch isnan={th.any(th.isnan(next_img_batch))}")
            if not th.all(th.isfinite(predicted_img_batch)): raise RuntimeError("Invalid predicted_img_batch isnan={th.any(th.isnan(predicted_img_batch))}")
            if not th.all(th.isfinite(next_vec_batch)): raise RuntimeError("Invalid next_vec_batch isnan={th.any(th.isnan(next_vec_batch))}")
            if not th.all(th.isfinite(predicted_vec_batch)): raise RuntimeError("Invalid predicted_vec_batch isnan={th.any(th.isnan(predicted_vec_batch))}")

        latent_loss = latent_loss_func(self._latent_loss_weight, state_batch, self._loglosses)
        if self._checkFinite:
            if not th.all(th.isfinite(latent_loss)): raise RuntimeError(f"Invalid KL-divergence loss {latent_loss}")

        reconstruction_loss, img_loss, reward_loss, vec_loss = reconstruction_loss_func(self._rewardWeight,
                                                                                        self._vec_lossWeight,
                                                                                        reward_batch,
                                                                                        predicted_reward_batch,
                                                                                        next_vec_batch,
                                                                                        predicted_vec_batch,
                                                                                        next_img_batch,
                                                                                        predicted_img_batch,
                                                                                        self._loglosses)

        nextpred_s2s = th.cat([input_observation_batch[self.VECTOR], next_vec_batch, predicted_vec_batch], dim=1).cpu().detach().numpy()                                                                                            
        # ggLog.info(f"nextpred_s2s = {nextpred_s2s}, \n vec_loss = {vec_loss}")

        if self._checkFinite:
            if not th.all(th.isfinite(reward_loss)): raise RuntimeError(f"Invalid reward loss {reward_loss}, \n reward_batch = {reward_batch} \n predicted_reward_batch = {predicted_reward_batch}")
            if not th.all(th.isfinite(vec_loss)): raise RuntimeError(f"Invalid vector-part loss {vec_loss}")
            if not th.all(th.isfinite(img_loss)): raise RuntimeError(f"Invalid image-part loss {img_loss}")
            if not th.all(th.isfinite(reconstruction_loss)): raise RuntimeError(f"Invalid reconstruction loss {reconstruction_loss}")

        # print(f"{reconstruction_loss}+{self._kld_loss_weight}*{kld_loss}")

        if self._autotune_weights:
            loss = (th.exp(-self._log_vars[0]) * latent_loss + 0.5*self._log_vars[0] +
                    th.exp(-self._log_vars[1]) * img_loss  + 0.5*self._log_vars[1]  +
                    th.exp(-self._log_vars[2]) * vec_loss + 0.5*self._log_vars[2] +
                    th.exp(-self._log_vars[3]) * reward_loss + 0.5*self._log_vars[3])
        else:
            loss = latent_loss + img_loss + vec_loss + reward_loss


        # ggLog.info("computed dyn vae loss")
        return loss, reconstruction_loss, latent_loss, reward_loss, img_loss, vec_loss

    # @th.jit.export
    # def _loss_function(self,     input_observation_batch : Dict[int,th.Tensor],
    #                             next_observation_batch : Dict[int,th.Tensor],
    #                             reward_batch : th.Tensor,
    #                             state_batch : th.Tensor,
    #                             predicted_observation_batch : Dict[int,th.Tensor],
    #                             predicted_reward_batch) -> th.Tensor:
    #     loss, reconstruction_loss, latent_loss, reward_loss, img_loss, vec_loss = self._loss_functions(input_observation_batch,
    #                                                                 next_observation_batch,
    #                                                                 reward_batch,
    #                                                                 state_batch,
    #                                                                 predicted_observation_batch,
    #                                                                 predicted_reward_batch)

    #     return loss

    @th.jit.export
    def compute_loss(self,  transition_batch : Transition):
        predicted_observation_batch, predicted_reward_batch, state_batch = self(transition_batch)
        return self._loss_functions(transition_batch.observation,
                                    transition_batch.next_observation,
                                    transition_batch.reward,
                                    state_batch,
                                    predicted_observation_batch,
                                    predicted_reward_batch)[0]

    @th.jit.export
    def compute_losses(self,  transition_batch : Transition) -> Tuple[th.Tensor,th.Tensor,th.Tensor,th.Tensor]:
        predicted_observation_batch, predicted_reward_batch, state_batch = self(transition_batch)
        losses = self._loss_functions(  transition_batch.observation,
                                        transition_batch.next_observation,
                                        transition_batch.reward,
                                        state_batch,
                                        predicted_observation_batch,
                                        predicted_reward_batch)
        return losses

    @th.jit.export
    def _augment(self, img_batch : th.Tensor):
        if self._augment_callback is None or not self.training:
            return img_batch
        else:
            return self._augment_callback(img_batch)




    @th.jit.export
    def encoding_size(self):
        return self._img_encoding_size + self.get_vector_part_size()

    @th.jit.export
    def input_height(self):
        return self._img_height

    @th.jit.export
    def input_width(self):
        return self._img_width

    @th.jit.export
    def input_channels(self):
        return self._img_channels

    @th.jit.export
    def action_size(self):
        return self._action_size


    def freeze_encoders(self):
        self._img_encoder.requires_grad_(False)

    def unfreeze_encoders(self):
        self._img_encoder.requires_grad_(True)

    def freeze_decoder(self):
        self._img_decoder.requires_grad_(False)

    def unfreeze_decoder(self):
        self._img_decoder.requires_grad_(True)

    def freeze_dynamics(self):
        self._dynamics_nets.requires_grad_(False)

    def unfreeze_dynamics(self):
        self._dynamics_nets.requires_grad_(True)

    def get_init_args(self):
        return self._init_args

        