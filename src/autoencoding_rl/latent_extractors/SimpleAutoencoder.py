#!/usr/bin/env python3


import torch as th
from typing import Tuple, List
from torchvision import transforms
from autoencoding_rl.latent_extractors.BaseAutoencoder import BaseAutoencoder
from autoencoding_rl.latent_extractors.autoencoder.SimpleEncoder import SimpleEncoder
from autoencoding_rl.latent_extractors.autoencoder.SimpleDecoder import SimpleDecoder

class SimpleAutoencoder(BaseAutoencoder):

    # def conv2d_out_resolution(in_size : Tuple[int,int], ksize : int, stride : int) -> Tuple[int,int]:
    #     out_size_x = int((in_size[0] - ksize)/stride + 1)
    #     out_size_y = int((in_size[0] - ksize)/stride + 1)
    #     return (out_size_x, out_size_y)

    def __init__(self, encoding_size : int, image_channels_num : int = 1, net_input_width : int = 64, net_input_height : int = 64):
        super().__init__()
        self._input_width  = net_input_width
        self._input_height = net_input_height
        self._input_channels = image_channels_num
        self._encoding_size = encoding_size
        
        self._resizeToInput = transforms.Resize((self._input_height,self._input_width))

        self._buildModels()

    def encoding_size(self):
        return self._encoding_size

    def input_width(self):
        return self._input_width

    def input_height(self):
        return self._input_height

    def input_channels(self):
        return self._input_channels

    def _buildModels(self):
        self.encoder = SimpleEncoder(self._encoding_size, self._input_channels, self._input_width,self._input_height)
        self.decoder = SimpleDecoder(self._encoding_size, self._input_channels, self._input_width, self._input_height)
        self._optimizer = th.optim.Adam(self.parameters(), lr = self._learning_rate)
        self._optimizer.zero_grad()

    def reset(self):        
        self._buildModels()


    def train_model(self, preprocessed_obs_batch):
        # th.cuda.synchronize()
        # tpcl = time.monotonic()
        self.train() # Put module in train mode
        with th.set_grad_enabled(True):
            #print("observation_batch.type()="+str(observation_batch.type()))
            loss = self._latestAutoencoder.compute_loss(preprocessed_obs_batch)
            loss.backward()
            th.nn.utils.clip_grad_value_(self._latestAutoencoder.parameters(), 10)
            self._optimizer.step()

        return loss, {}

        
    def forward(self, x : th.Tensor) -> th.Tensor:
        """Perform a forward pass of the network with the provided batch.

        Parameters
        ----------
        x : th.Tensor
            A batch of images. Size must be (batch_size, img_width, img_height)

        Returns
        -------
        x : th.Tensor
            A batch of encoded images. Size will be (batch_size, img_width, img_height)

        """
        assert x.size()[1] == self._input_channels, f"Image should have {self.input_channels()} channels, not {x.size()[1]}, tensor has size: {x.size()}"
        assert x.size()[2] == self._input_height, f"height should be {self.input_height()}, not {x.size()[2]}, tensor has size: {x.size()}"
        assert x.size()[3] == self._input_width, f"width should be {self.input_width()}, not {x.size()[3]}, tensor has size: {x.size()}"
        return self.decoder(self.encoder(x))

    def encode(self, x : th.Tensor) -> th.Tensor:
        """Encode a batch of images.

        Parameters
        ----------
        x : th.Tensor
            A batch of images. Size must be (batch_size, img_width, img_height)

        Returns
        -------
        th.Tensor
            Batch of encoded images. Size will be (batch_size, self.encoding_size())
        """
        # assert x.size()[1] == self._input_channels, f"Image should have {self.input_channels()} channels, not {x.size()[1]}, tensor has size: {x.size()}"
        # assert x.size()[2] == self._input_height, f"height should be {self.input_height()}, not {x.size()[2]}, tensor has size: {x.size()}"
        # assert x.size()[3] == self._input_width, f"width should be {self.input_width()}, not {x.size()[3]}, tensor has size: {x.size()}"
        return self.encoder(x)

    def decode(self, x : th.Tensor) -> th.Tensor:
        """Decode a batch of images.

        Parameters
        ----------
        x : th.Tensor
            Batch of encoded images. Size must be (batch_size, self.encoding_size())

        Returns
        -------
        x : th.Tensor
            A batch of images. Size will be (batch_size, img_width, img_height)

        """
        assert x.size()[1] == self._encoding_size, f"Encoded vector should have size {self._encoding_size}, not {x.size()[1]}, tensor has size {x.size()}"
        return self.decoder(x)

    def _loss_function(self, input_image_batch : th.Tensor, output_image_batch : th.Tensor):
        return th.nn.MSELoss()(output_image_batch, input_image_batch)

    def compute_loss(self, input_image_batch : th.Tensor):
        output_image_batch = self(input_image_batch)
        return self._loss_function(input_image_batch, output_image_batch)
    
    def preprocess_observations(self, observation_batch : th.Tensor):
        resized_batch = self._resizeToInput(observation_batch)
        # Input should be in the [0,1] range, as this is what torchvision.transforms.ToTensor does
        normalized = resized_batch*2 - 1
        return normalized
        # return resized_batch

    def postprocess_observations(self, observation_batch : th.Tensor):
        return (observation_batch+1)/2
        # return observation_batch

