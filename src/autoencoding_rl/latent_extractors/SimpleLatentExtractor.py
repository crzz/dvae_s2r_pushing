from enum import auto
import torch as th
import torchvision
# import time
import numpy as np
import datetime
import time
from typing import Dict, OrderedDict, Any, Optional, Union, List, Tuple
import io
import os
import copy

import lr_gym.utils.dbg.dbg_img as dbg_img
import lr_gym.utils.dbg.ggLog as ggLog

from autoencoding_rl.utils import splitTransitionBatch, tensorToHumanCvImageRgb, Transition, ImageComparisonWindow
from autoencoding_rl.latent_extractors.SimpleAutoencoder import SimpleAutoencoder
from autoencoding_rl.latent_extractors.BaseAutoencoder import BaseAutoencoder
from autoencoding_rl.latent_extractors.LatentExtractor import LatentExtractor
import autoencoding_rl
import wandb

class SimpleLatentExtractor(LatentExtractor):
    def __init__(self,
                 autoencoder : BaseAutoencoder,
                 debug_out_folder : Optional[str] = None,
                 torchDevice : str = "cuda",
                 bestModelThreshold = 0.9,
                 logarithmic_loss : bool = True):
        super().__init__(debug_out_folder)
        
        self._sourceAutoencoder = autoencoder
        self._torchDevice = torchDevice
        self.set_hyperparams(bestModelThreshold, logarithmic_loss)

        self._checkFinite = False

        self._sourceAutoencoder.to(device=self._torchDevice)
        self._encoder_frozen = False
        self._decoder_frozen = False
        self._dynamics_frozen = False
        self._train_iterations_done = 0
        self._training_samples_used = 0
        self.reset()


    def set_hyperparams(self, bestModelThreshold = None, logarithmic_loss = None):
        if bestModelThreshold is not None:
            self._bestModelThreshold = bestModelThreshold
            self._enable_best_tracking = True #self._bestModelThreshold > 0
        if logarithmic_loss is not None:
            self._logarithmic_loss = logarithmic_loss

    def torchDevice(self):
        return self._torchDevice

    def displayDebugInfo(self, transition_batch_example : Transition, id : str = None) -> None:
        if id is None:
            id = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
        if self._comparisonWindow is not None:
            with th.no_grad():
                preprocessed_obs_batch = self._sourceAutoencoder.preprocess_observations(transition_batch_example.observation.to(self._torchDevice))
                encodedDecoded_obs_batch = self._bestAutoencoder(preprocessed_obs_batch)
                if isinstance(encodedDecoded_obs_batch,tuple):
                    encodedDecoded_obs_batch = encodedDecoded_obs_batch[0]

                encodedDecoded_obs_batch = self._sourceAutoencoder.postprocess_observations(encodedDecoded_obs_batch)
                preprocessed_obs_batch = self._sourceAutoencoder.postprocess_observations(preprocessed_obs_batch)
            
            self._comparisonWindow.displayExample(  [preprocessed_obs_batch,
                                                     preprocessed_obs_batch,
                                                     encodedDecoded_obs_batch],
                                                    id = id)

    def train_model(self, training_batch : Transition, validation_batch : Transition) -> float:
        """Perform one training iteration on the provided batch

        Parameters
        ----------
        transition_batch : Transition
            A batch of transitions (Actually a transition of batches)
        """
        tt0 = time.monotonic()
        obs_batch = training_batch.observation.to(self._torchDevice)
        lossVal : float = 0
        self._latestAutoencoder.train() # put model in train mode
        preprocessed_obss = self._sourceAutoencoder.preprocess_observations(obs_batch)
        
        loss, sub_losses = self._latestAutoencoder.train_model(preprocessed_obss)
        lossVal = loss.item()
        ttf = time.monotonic()
        
        t0v = time.monotonic()
        self._setBestModel(validation_batch)
        tfv = time.monotonic()
        ggLog.info(f"Best model selection = {tfv-t0v}, Training = {ttf-tt0}")

        self._train_iterations_done += 1
        self._training_samples_used += 1
        self._info["train_iterations_done"] = self._train_iterations_done
        self._info["training_samples_used"] = self._training_samples_used
        self._info["time_from_start"] = time.monotonic()-self._initTime

        return lossVal

    def _setBestToLatest(self):
        # ggLog.info(f"PYTORCH_JIT = {os.environ['PYTORCH_JIT']}")
        # t0 = time.monotonic()
        # self._bestAutoencoder = copy.deepcopy(self._latestAutoencoder)
        if self._enable_best_tracking:
            self._bestAutoencoder.load_state_dict(self._latestAutoencoder.state_dict())
        else:
            self._bestAutoencoder = self._latestAutoencoder
        # self._bestAutoencoder.load_state_dict(self._latestAutoencoder.state_dict())
        # th.cuda.synchronize()
        # tf = time.monotonic()
        # ggLog.info(f"best autoencoder copy time = {tf-t0}")


    def _setBestModel(self, validation_batch : Transition) -> bool:
        val_loss_best = float("nan")
        val_loss_latest = float("nan")
        ratio = float("nan")
        # Check if the latest AE is the best
        latest_is_better = True
        if validation_batch is not None and self._enable_best_tracking:
            # th.cuda.synchronize()
            # tpe = time.monotonic()
            val_loss_best = self.evaluate(validation_batch)
            val_loss_latest = self.evaluate(validation_batch, use_latest = True)
            # th.cuda.synchronize()
            # tde = time.monotonic()
            # print(f"Evaluations = {tde-tpe}s")
            if self._logarithmic_loss:
                e2latest = th.exp(val_loss_latest)
                e2best = th.exp(val_loss_best)
            else:
                e2latest = val_loss_latest
                e2best = val_loss_best
                
            ratio = (e2latest/e2best).item()
            latest_is_better = ratio < self._bestModelThreshold
            val_loss_latest = val_loss_latest.item()
            val_loss_best = val_loss_best.item()
            if self._bestModelThreshold < 0: # we always update but still do validation, for logging purposes, to disable it set validation_batch_size ot zero
                latest_is_better = True
            # ggLog.info(f"{latest_is_better} = {val_loss_best} < {val_loss_best}")
        if latest_is_better:
            self._setBestToLatest()


        # self._bestAutoencoder = self._latestAutoencoder


        self._info["val_loss_latest"] = val_loss_latest
        self._info["val_loss_best"] = val_loss_best
        self._info["best_changed"] = latest_is_better
        self._info["latest_on_best"] = ratio

        return latest_is_better
        # ggLog.info(f"val_loss_latest = {val_loss_latest}, val_loss_best={val_loss_best}") #", validation_batch = {validation_batch}")

    def evaluate(self, transition_batch : Transition, use_latest : bool = False) -> th.Tensor:
        """Evaluate model loss on the provided batch

        Parameters
        ----------
        transition_batch : Transition
            A batch of transitions (Actually a transition of batches)
        """

        ae = self._bestAutoencoder
        if use_latest:
            ae = self._latestAutoencoder

        def compute_loss(batch):
            with th.no_grad():
                ae.eval() # put model in eval mode
                obs_batch = batch.observation.to(self._torchDevice)
                preprocessed_obss = ae.preprocess_observations(obs_batch)
                l = ae.compute_loss(preprocessed_obss)
                ae.train() # put model in train mode
                return l
        try:
            loss = compute_loss(transition_batch) # autoencoding_rl.utils.runAutoBatchSize(compute_loss, transition_batch)
        except RuntimeError as e:
            ggLog.warn(f"Evaluation failed with error {e}")
            loss = th.tensor([float("inf")], dtype=th.float32, device=self._torchDevice)
        return loss

    def encode(self, observation_batch : Dict[Union[str, int], th.Tensor]):
        
        self._bestAutoencoder.eval()
        # t0 = time.monotonic()
        with th.no_grad():

            preprocessed_obs_batch = self._sourceAutoencoder.preprocess_observations(observation_batch)

            if self._checkFinite:
                for k, v in preprocessed_obs_batch.items():
                    if not th.all(th.isfinite(v)):
                        if th.any(th.isnan(v)):
                            raise RuntimeError(f"nan values in preprocessed_obs_batch['{k}'] {preprocessed_obs_batch}")
                        else:
                            raise RuntimeError(f"inf values in preprocessed_obs_batch['{k}'] {preprocessed_obs_batch}")
            # ggLog.info(f"sle.encode(): preprocessed_obs_batch['image'].size() = {preprocessed_obs_batch['image'].size()}")

            # t1 = time.monotonic()
            #print(f"resize took {t1-t0}s")
            if self._checkFinite:
                for k, v in preprocessed_obs_batch.items():
                    if not th.all(th.isfinite(v)):
                        if th.any(th.isnan(v)):
                            raise RuntimeError(f"nan values in preprocessed_obs_batch['{k}'] {preprocessed_obs_batch}")
                        else:
                            raise RuntimeError(f"inf values in preprocessed_obs_batch['{k}'] {preprocessed_obs_batch}")

            feature_vec_batch = self._bestAutoencoder.encode(preprocessed_obs_batch)
            if self._checkFinite:
                assert th.all(th.isfinite(feature_vec_batch)), f"non-finite values in feature vector {feature_vec_batch}"
            # t2 = time.monotonic()
            #print(f"encode took {t2-t1}s")
        # t4 = time.monotonic()
        return feature_vec_batch

    def preAndPostProcess(self, observation_batch : Dict[Union[str, int], th.Tensor]):
        with th.no_grad():
            pre = self._sourceAutoencoder.preprocess_observations(observation_batch)
            post = self._sourceAutoencoder.postprocess_observations(pre)
        return post

    def decode(self, encoded_obs_batch : th.Tensor, action_batch : th.Tensor = None):
        self._bestAutoencoder.eval()
        with th.no_grad():
            decoded_obs_batch = self._bestAutoencoder.decode(encoded_obs_batch)
            postproc_decoded_obs_batch = self._sourceAutoencoder.postprocess_observations(decoded_obs_batch)
        return postproc_decoded_obs_batch

    def encoding_size(self):
        return self._bestAutoencoder.encoding_size()


    def reset(self):
        self._reset(True, True, True)

    def _reset(self, enc, dec, dyn):
        if enc and dec and dyn:
            self._sourceAutoencoder.reset()
            self._latestAutoencoder = copy.deepcopy(self._sourceAutoencoder)
        else:
            if enc:
                self._latestAutoencoder.reset_encoders() #Reinitialize weights
            if dec:
                self._latestAutoencoder.reset_decoders() #Reinitialize weights
            if dyn:
                self._latestAutoencoder.reset_dynamics() #Reinitialize weights

        self._bestAutoencoder = copy.deepcopy(self._latestAutoencoder)
        self._bestAutoencoder.to(self._torchDevice)
        self._latestAutoencoder.to(self._torchDevice)
        self._setBestToLatest()

        if self._encoder_frozen:
            self.freeze_encoder()
        if self._decoder_frozen:
            self.freeze_decoder()
        if self._dynamics_frozen:
            self.freeze_dynamics()

        wandb.watch(self._latestAutoencoder, log = 'all')


    def reset_encoders(self):
        self._reset(enc = True, dec = False, dyn = False)
    
    def reset_decoders(self):
        self._reset(enc = False, dec = True, dyn = False)
    
    def reset_dynamics(self):
        self._reset(enc = False, dec = False, dyn = True)
    

    def freeze_encoder(self):
        self._sourceAutoencoder.freeze_encoders()
        self._latestAutoencoder.freeze_encoders()
        self._bestAutoencoder.freeze_encoders()
        self._encoder_frozen = True

    def unfreeze_encoder(self):
        self._sourceAutoencoder.unfreeze_encoders()
        self._latestAutoencoder.unfreeze_encoders()
        self._bestAutoencoder.unfreeze_encoders()
        self._encoder_frozen = False

    def freeze_decoder(self):
        self._sourceAutoencoder.freeze_decoder()
        self._latestAutoencoder.freeze_decoder()
        self._bestAutoencoder.freeze_decoder()
        self._decoder_frozen = True

    def unfreeze_decoder(self):
        self._sourceAutoencoder.unfreeze_decoder()
        self._latestAutoencoder.unfreeze_decoder()
        self._bestAutoencoder.unfreeze_decoder()
        self._decoder_frozen = False

    def freeze_dynamics(self):
        self._sourceAutoencoder.freeze_dynamics()
        self._latestAutoencoder.freeze_dynamics()
        self._bestAutoencoder.freeze_dynamics()
        self._dynamics_frozen = True

    def unfreeze_dynamics(self):
        self._sourceAutoencoder.unfreeze_dynamics()
        self._latestAutoencoder.unfreeze_dynamics()
        self._bestAutoencoder.unfreeze_dynamics()
        self._dynamics_frozen = False

    def train_iterations_count(self):
        return self._train_iterations_done

    def getModelSizeRecap(self):
        return self._bestAutoencoder.getModelSizeRecap()
        return self._bestAutoencoder.getModelSizeRecap()

    def getAutoencoders(self):
        return {"latest" : self._latestAutoencoder, "best" : self._bestAutoencoder, "source" : self._sourceAutoencoder}