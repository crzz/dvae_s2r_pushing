
from typing import Tuple
from autoencoding_rl.latent_extractors.dyn_vae.DVAE import DVAE

import torch as th
import torch.nn as nn
from torchvision import transforms
from typing import Dict, Union, List, Tuple, Callable, Final

import lr_gym.utils.dbg.ggLog as ggLog

from autoencoding_rl.latent_extractors.vae.VAE_encoder import VAE_encoder
from autoencoding_rl.latent_extractors.autoencoder.AE_decoder import AE_decoder
from autoencoding_rl.utils import Transition
from autoencoding_rl.latent_extractors.MultiObsAutoencoder import MultiObsAutoencoder
from torchvision.transforms.functional import InterpolationMode
from autoencoding_rl.utils import build_mlp_net
from autoencoding_rl.nets.Parallel import Parallel

import gym
import inspect
from dataclasses import dataclass
import torchviz

@th.jit.script
def kld_loss_func(weight, log_var_batch, mu_batch):
    # Compute KL-divergence loss
    # D_kl(q_theta(z|x_i) || p(z))
    # with:  q_theta(z|x_i) = N(mu(x_i), e^logvar(x_i))
    #        p(z) = N(0,I)
    # using mean instead of sum for the inner term to avoid dependance on the latent vector size
    return weight*-th.mean(0.5 * th.mean(1 + log_var_batch - mu_batch ** 2 - log_var_batch.exp(), dim = 1), dim = 0)



class MultiObsDynVAE3(DVAE):
    
    @dataclass
    class Hyperparams:
        reward_loss_weight_th = None
        kld_loss_weight_th = None
        vec_loss_weight_th = None
        rec_loss_weight_th = None
        reward_loss_weight = None
        kld_loss_weight = None
        vec_loss_weight = None
        rec_loss_weight = None
        checkDimensions = None
        checkFinite = None
        learning_rate = None

    def __init__(self,  observation_shape : Union[Dict[str,List[int]],Tuple[int,int,int], gym.spaces.Dict],
                        img_encoding_size: int,
                        action_size: int,
                        dynamics_nn_arch: Tuple[int, int] = None,
                        rewardLossWeight : float = 1.0,
                        kldLossWeight : float = 0.0001,

                        vecLossWeight : float = 0.005,
                        torchDevice : str = "cuda",
                        encoder_ensemble_size = 1,
                        dynamics_ensemble_size = 1,
                        dynamics_model_class : type = None,
                        backbone : str = "conv",
                        deconv_backbone : str = None,
                        augment_callback : Callable[[th.Tensor], th.Tensor] = None,
                        checkDimensions : bool = True,
                        checkFinite : bool = True,
                        e2eResidual : bool = False,
                        reconstructionLossWeight : float = 1.0,
                        state_combiner_nn_arch : List[int] = "identity",
                        
                        frame_stack_size : int = 1,
                        learning_rate : float = 0.0001,
                        
                        latent_space_activation = th.nn.Tanh,
                        vec_decoder_nn_arch = None,
                        vec_encoder_nn_arch = "identity",
                        img_decoder_ensemble_size = 1,
                        
                        reward_scaling : float = 1,
                        reward_nn_arch: Tuple[int, int] = None,
                        reward_ensemble_size = 1,
                        log_reward : bool = False):

        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        self._init_args = values
        self._init_args.pop("self")

        self.IMAGE = 1
        self.VECTOR = 2
        self._e2eResidual = e2eResidual
        if vec_decoder_nn_arch is None:
            vec_decoder_nn_arch = vec_encoder_nn_arch
        self._vec_decoder_nn_arch = vec_decoder_nn_arch
        self._vec_encoder_nn_arch = vec_encoder_nn_arch
        self._img_decoder_ensemble_size = img_decoder_ensemble_size
        self._log_reward = log_reward

        self._hyperparams = self.Hyperparams()
        self._hyperparams.learning_rate = learning_rate

        # For backward compatibility
        if hasattr(self, "_rewardWeight"):
            self._hyperparams.reward_loss_weight = self._rewardWeight
        if hasattr(self, "_kld_loss_weight"):
            self._hyperparams.kld_loss_weight = self._kld_loss_weight
        if hasattr(self, "_vec_lossWeight"):
            self._hyperparams.vec_loss_weight = self._vec_lossWeight
        if hasattr(self, "_reconstruction_loss_weight"):
            self._hyperparams.rec_loss_weight = self._reconstruction_loss_weight
        if hasattr(self, "_learning_rate"):
            self._hyperparams.learning_rate = self._learning_rate
        

        super().__init__(observation_shape=observation_shape, torchDevice=torchDevice)

        self.set_hyperparams(rewardLossWeight,
                            kldLossWeight,
                            vecLossWeight,
                            reconstructionLossWeight,
                            checkDimensions,
                            checkFinite,
                            learning_rate = None)

        if dynamics_model_class is not None and dynamics_nn_arch is not None:
            raise AttributeError("You can only set one at a time of dynamics_nn_arch and dynamics_model_class")
        if dynamics_model_class is None and dynamics_nn_arch is None:
            raise AttributeError("You must set either dynamics_nn_arch or dynamics_model_class. None of the two is set.")



        self._img_encoding_size = img_encoding_size
        self._action_size = action_size
        self._dynamics_nn_arch = dynamics_nn_arch
        self._reward_nn_arch = reward_nn_arch
        self._state_combiner_nn_arch = state_combiner_nn_arch
        self._img_encoder_ensemble_size = encoder_ensemble_size
        self._dynamics_ensemble_size = dynamics_ensemble_size
        self._dynamics_model_class = dynamics_model_class
        self._backbone = backbone
        self._deconv_backbone = deconv_backbone if deconv_backbone is not None else backbone
        self._deconv_backbone = "conv_small" if self._deconv_backbone == "conv_small" else "conv"
        self._augment_callback = augment_callback
        self._frame_stack_size = frame_stack_size
        self._latent_space_activation = latent_space_activation
        self._skippedMinibatchesCount = 0
        self._skippedMinibatchesStreakCount = 0
        self._reward_scaling = th.tensor(reward_scaling, device=self._torchDevice)
        self._reward_ensemble_size = reward_ensemble_size


        self._resizeToInput = transforms.Resize((self._img_height,self._img_width),interpolation=InterpolationMode.BILINEAR)

        if self._state_combiner_nn_arch == "identity":
            self._encoders_activation = th.nn.Identity
            self._combiner_activation = self._latent_space_activation
            self._decombiner_activation = th.nn.Identity
            self._reward_activation = th.nn.LeakyReLU
        else:
            self._encoders_activation = th.nn.Identity
            self._combiner_activation = self._latent_space_activation
            self._decombiner_activation = th.nn.Identity
            self._reward_activation = th.nn.LeakyReLU

        self._buildModels()


    def set_hyperparams(self, reward_loss_weight,
                              kld_loss_weight,
                              vec_loss_weight,
                              reconstruction_loss_weight,
                              checkDimensions,
                              checkFinite,
                              learning_rate):
        if reward_loss_weight is not None: self._hyperparams.reward_loss_weight = reward_loss_weight
        if kld_loss_weight is not None: self._hyperparams.kld_loss_weight = kld_loss_weight
        if vec_loss_weight is not None: self._hyperparams.vec_loss_weight = vec_loss_weight
        if reconstruction_loss_weight is not None: self._hyperparams.rec_loss_weight = reconstruction_loss_weight
        if checkDimensions is not None: self._hyperparams.checkDimensions = checkDimensions
        if checkFinite is not None: self._hyperparams.checkFinite = checkFinite

        self._hyperparams.reward_loss_weight_th = th.tensor(self._hyperparams.reward_loss_weight, device=self._torchDevice)
        self._hyperparams.kld_loss_weight_th = th.tensor(self._hyperparams.kld_loss_weight, device=self._torchDevice)
        self._hyperparams.vec_loss_weight_th = th.tensor(self._hyperparams.vec_loss_weight, device=self._torchDevice)
        self._hyperparams.rec_loss_weight_th = th.tensor(self._hyperparams.rec_loss_weight, device=self._torchDevice)
        

        if learning_rate is not None:
            self._hyperparams.learning_rate = learning_rate
            self._buildOptimizer()

    def get_hyperparams(self):
        return self._hyperparams

    def _buildEncoders(self):

        self._img_encoder = VAE_encoder( latent_space_size = self._img_encoding_size,
                                        image_channels_num = self._img_channels,
                                        net_input_width = self._img_width,
                                        net_input_height = self._img_height,
                                        ensemble_size = self._img_encoder_ensemble_size,
                                        backbone = self._backbone,
                                        checkDimensions = self._hyperparams.checkDimensions,
                                        torchDevice = self._torchDevice,
                                        mu_activation_class=self._encoders_activation)
        # ggLog.info(f"Created encoder with {self._img_channels} input channels")

        self._vec_encoder = build_mlp_net(self._vec_encoder_nn_arch, self.get_vector_part_size(), self.get_vector_part_size(),
                                                last_activation_class=self._encoders_activation, return_ensemble_mean=True)

    def _buildDecoders(self):
        self._vec_decoder = build_mlp_net(self._vec_decoder_nn_arch, self.encoding_size(), self.get_vector_part_size(),
                                                last_activation_class=th.nn.Identity, return_ensemble_mean=True)

        if self._img_channels % self._frame_stack_size != 0:
            raise RuntimeError(f"image channels (={self._img_channels}) are not divisible by frame_stack_size (={self._frame_stack_size}")
        self._img_decoder = AE_decoder(latent_space_size = self.encoding_size(),
                                        output_channels_num = int(self._img_channels/self._frame_stack_size),
                                        net_output_width = self._img_width,
                                        net_output_height = self._img_height,
                                        torchDevice = self._torchDevice,
                                        backbone = self._deconv_backbone,
                                        ensemble_size=self._img_decoder_ensemble_size)

    def _buildDynamics(self):
        if self._reward_nn_arch is not None:
            if self._dynamics_model_class is None:
                self._dynamics_nets = build_mlp_net(self._dynamics_nn_arch,
                                                    self.encoding_size() + self._action_size,
                                                    self.encoding_size(),
                                                    last_activation_class=self._latent_space_activation,
                                                    return_ensemble_mean=True,
                                                    ensemble_size=self._dynamics_ensemble_size)
            else:
                self._dynamics_nets = Parallel([self._dynamics_model_class() for _ in range(self._dynamics_ensemble_size)], return_mean=True)
        
            self._reward_net = build_mlp_net(   self._reward_nn_arch,
                                                self.encoding_size()*2 + self._action_size,
                                                1,
                                                last_activation_class=self._reward_activation,
                                                return_ensemble_mean=True,
                                                ensemble_size=self._reward_ensemble_size)
        else:
            if self._dynamics_model_class is None:
                self._dynamics_nets = build_mlp_net(self._dynamics_nn_arch,
                                                    self.encoding_size() + self._action_size,
                                                    self.encoding_size() +1,
                                                    last_activation_class=self._latent_space_activation,
                                                    return_ensemble_mean=True,
                                                    ensemble_size=self._dynamics_ensemble_size)
            else:
                self._dynamics_nets = Parallel([self._dynamics_model_class() for _ in range(self._dynamics_ensemble_size)], return_mean=True)

    def _buildOptimizer(self):
        self._optimizer = th.optim.Adam(self.parameters(), lr = self._hyperparams.learning_rate)
        self._optimizer.zero_grad()



    def _buildModels(self):

        self._buildEncoders()

        self._state_combiners = build_mlp_net(self._state_combiner_nn_arch, self.encoding_size(), self.encoding_size(),
                                                    last_activation_class=self._combiner_activation,  return_ensemble_mean=True)
        self._buildDynamics()

        self._state_decombiners = build_mlp_net(self._state_combiner_nn_arch, self.encoding_size(), self.encoding_size(),
                                                      last_activation_class=self._decombiner_activation, return_ensemble_mean=True)

        self._buildDecoders()

        self._buildOptimizer()




    @th.jit.export
    def encode(self, observation_batch : Dict[int,th.Tensor], deterministic = True, mulogvar_return : List[th.Tensor] = None):
        img_batch = observation_batch[self.IMAGE]
        vec_batch = observation_batch[self.VECTOR]
        mu, log_var = self.extract_img_latent_distribution(img_batch)
        if self._hyperparams.checkFinite:
            assert th.all(th.isfinite(mu))
            assert th.all(th.isfinite(log_var))
        # ggLog.info(f"MultiObsDynVAE.encode(): mu = {mu} \t log_var = {log_var}")
        if deterministic:
            enc_img_batch = mu
        else:
            enc_img_batch = self._img_encoder.sample(mu, log_var)

        enc_vec_batch = self._vec_encoder(vec_batch)

        # enc_img_batch = self._img_encoder.sample(mu, log_var) #Gives a (batch_size, dyn_encoding_size) output
        # ggLog.info(f"MultiObsDynVAE.encode(): id(self)={id(self)}, enc_img_batch (mean,std) = ({th.mean(enc_img_batch, dim=0)},\t {th.std(enc_img_batch, dim = 0)})")
        raw_combined_state_batch = th.cat([enc_img_batch,enc_vec_batch], dim = 1)
        # raw_combined_state = th.tanh(raw_combined_state)
        comb_state_batch = self._state_combiners(raw_combined_state_batch)

        if mulogvar_return is not None:
            mulogvar_return[0] = mu
            mulogvar_return[1] = log_var
        return comb_state_batch

    @th.jit.export
    def decode(self, predicted_state_batch : th.Tensor):
        decombined_state_batch = self._state_decombiners(predicted_state_batch)
        
        dec_predicted_vec_batch = self._vec_decoder(decombined_state_batch)
        dec_predicted_img_batch = self._img_decoder(decombined_state_batch)
        
        predicted_observation_batch = { self.IMAGE : dec_predicted_img_batch,
                                        self.VECTOR : dec_predicted_vec_batch}
        return predicted_observation_batch

    @th.jit.export
    def predict_dynamics(self, state_batch : th.Tensor, action_batch : th.Tensor):
        if self._hyperparams.checkDimensions:
            assert state_batch.size()[0] == action_batch.size()[0], \
                f"state batch and action batch have different sizes, respectively {state_batch.size()[0]} and {action_batch.size()[0]}"
            assert state_batch.size()[1] == self.encoding_size(), \
                f"States have wrong size, should be {self.encoding_size()}, but it's {state_batch.size()[1]}"
            assert action_batch.size()[1] == self._action_size, \
                f"Actions have wrong size, should be {self._action_size} but it's {action_batch.size()[1]}"


        # ggLog.info(f"self._action_size = {self._action_size}")
        # ggLog.info(f"self.encoding_size() = {self.encoding_size()}")
        #Concatenate states and actions
        state_action_batch = th.cat((state_batch, action_batch), 1) #Gives a (batch_size, dyn_encoding_size+action_size) output
        nextstate_batch = self._dynamics_nets(state_action_batch)
        if self._reward_nn_arch is not None:
            state_action_nextstate_batch = th.cat([state_action_batch, nextstate_batch], dim=1)
            # state_action_nextstate_batch = state_action_nextstate_batch.detach()
            reward_batch = self._reward_net(state_action_nextstate_batch)
            reward_batch = reward_batch * self._reward_scaling
        else:
            nextstate_batch, reward_batch = th.split(nextstate_batch, [self.encoding_size(), 1], 1)
        #nextstate_batch now has size (batch_size, dyn_encoding_size)
        #reward_batch now has size (batch_size, 1) (still 2-dimensional)
        return nextstate_batch, reward_batch

    @th.jit.export
    def forward(self, transition_batch : Transition):
        img_batch = transition_batch.observation[self.IMAGE]
        vec_batch = transition_batch.observation[self.VECTOR]
        action_batch = transition_batch.action
        if self._hyperparams.checkDimensions:
            assert action_batch.size()[0] == img_batch.size()[0], \
                    f"Observation batch and action batch should have the same length. Action batch size = {action_batch.size()[0]}, image batch size = {img_batch.size()[0]}. Action tensor size = {action_batch.size()[0]}. image tensor size = {img_batch.size()[0]}"
            assert action_batch.size()[0] == vec_batch.size()[0], \
                    f"Vector batch and action batch should have the same length. Action batch size = {action_batch.size()[0]}, vector batch size = {vec_batch.size()[0]}. Action tensor size = {action_batch.size()}. Vector tensor size = {vec_batch.size()}"
            assert img_batch.size() == (img_batch.size()[0], self._img_channels, self._img_height, self._img_width), \
                    f"image size should be (Any, {self._img_channels}, {self._img_height}, {self._img_width}), instead it is  {img_batch.size()}"
            assert action_batch.size()[1] == self._action_size, \
                    f"Each action should have size {self._action_size}, not {action_batch.size()[1]}. Tensor has size {action_batch.size()}"
            assert vec_batch.size()[1] == self._vec_part_size, \
                    f"Each vector observation should have size {self._vec_part_size}, not {vec_batch.size()[1]}. Tensor has size {vec_batch.size()}"

        if self._hyperparams.checkFinite:
            if not th.all(th.isfinite(img_batch)):
                raise RuntimeError(f"Invalid value detected in img_batch! nan_detected = {th.any(th.isnan(img_batch))}")
            if not th.all(th.isfinite(vec_batch)):
                raise RuntimeError(f"Invalid value detected in vec_batch! nan_detected = {th.any(th.isnan(vec_batch))}")
            if not th.all(th.isfinite(action_batch)):
                raise RuntimeError(f"Invalid value detected in action_batch! nan_detected = {th.any(th.isnan(action_batch))}")

        augmented_img_batch = self._augment(img_batch)

        augmented_obs_batch = { self.IMAGE  : augmented_img_batch,
                                self.VECTOR : vec_batch}
        mulogvar = [None,None] # Needed by loss computation
        state_batch = self.encode(augmented_obs_batch, deterministic = False, mulogvar_return = mulogvar)
        mu = mulogvar[0]
        log_var = mulogvar[1]

        if self._hyperparams.checkFinite:
            if not th.all(th.isfinite(state_batch)):
                raise RuntimeError(f"Invalid value detected in state_batch! state_batch = {state_batch}")
        
        reconstructed_obs0 = self.decode(state_batch) #.detach())

        predicted_state_batches = []
        predicted_reward_batches = []
        predicted_observation_batches = []
        current_state_batch = state_batch
        for i in range(self._frame_stack_size):
            predicted_state_batch, predicted_reward_batch = self.predict_dynamics(current_state_batch, action_batch)
            predicted_state_batches.append(predicted_state_batch)
            predicted_reward_batches.append(predicted_reward_batch)
            predicted_observation_batch = self.decode(predicted_state_batch)
            predicted_observation_batches.append(predicted_observation_batch)
            current_state_batch = predicted_state_batch

        predicted_reward_batch = th.sum(th.stack(predicted_reward_batches), dim = 0) # Assumes rewards are summed in the frame stack
        predicted_stacked_observation_batch = { self.IMAGE  : th.cat([obsbatch[self.IMAGE] for obsbatch in predicted_observation_batches], dim = 1),
                                                self.VECTOR : predicted_observation_batches[-1][self.VECTOR]} 
        #predicted_state_batch now has size (batch_size, dyn_encoding_size)
        #predicted_reward_batch now has size (batch_size, 1) (still 2-dimensional)

        if self._e2eResidual:
            predicted_observation_batch[self.IMAGE] = predicted_observation_batch[self.IMAGE] + img_batch
        
        if self._hyperparams.checkFinite:
            assert [th.all(th.isfinite(o)) for o in predicted_observation_batch.values()]
            assert th.all(th.isfinite(predicted_reward_batch))
        
        # pred_img = (predicted_observation_batch[self.IMAGE][0]+1)/2
        # dbg_img.helper.publishDbgImg("forward", tensorToHumanCvImageRgb(pred_img))
        # ggLog.info(f"MultiObsDynVAE.forward(): id(self)={id(self)}")

        return (predicted_stacked_observation_batch, predicted_reward_batch, mu, log_var, reconstructed_obs0)










    @th.jit.export
    def _loss_functions(self,   input_observation_batch : Dict[int,th.Tensor],
                                next_observation_batch : Dict[int,th.Tensor],
                                reward_batch : th.Tensor,
                                mu_batch : th.Tensor,
                                log_var_batch : th.Tensor,
                                predicted_observation_batch : Dict[int,th.Tensor],
                                predicted_reward_batch,
                                reconstructed_obs_batch) -> Tuple[th.Tensor,th.Tensor,th.Tensor,th.Tensor]:
        # ggLog.info("computing dyn vae loss")
        # ggLog.info(f"next_observation_batch.size() = {next_observation_batch.size()}")
        # ggLog.info(f"predicted_observation_batch.size() = {predicted_observation_batch.size()}")
        # ggLog.info(f"predicted_reward_batch.size() = {predicted_reward_batch.size()}")
        # ggLog.info(f"reward_batch.size() = {reward_batch.size()}")
        # x_i = input_image
        # (mu, logvar) describe the latent distribution conditioned by x_i
        

        input_img_batch = input_observation_batch[self.IMAGE]
        reconstructed_img_batch = reconstructed_obs_batch[self.IMAGE]
        next_img_batch = next_observation_batch[self.IMAGE]
        predicted_img_batch =  predicted_observation_batch[self.IMAGE]

        input_vec_batch = input_observation_batch[self.VECTOR]
        reconstructed_vec_batch = reconstructed_obs_batch[self.VECTOR]
        next_vec_batch = next_observation_batch[self.VECTOR]
        predicted_vec_batch =  predicted_observation_batch[self.VECTOR]

        if self._hyperparams.checkDimensions:
            batch_size = input_img_batch.size()[0]
            assert input_img_batch.size() == (batch_size, self._img_channels, self._img_height, self._img_width), \
                    f"image size should be (Any, {self._img_channels}, {self._img_height}, {self._img_width}), instead it is  {input_img_batch.size()}"
            assert next_img_batch.size() == (batch_size, self._img_channels, self._img_height, self._img_width), \
                    f"image size should be (Any, {self._img_channels}, {self._img_height}, {self._img_width}), instead it is  {next_img_batch.size()}"
            assert predicted_img_batch.size() == (batch_size, self._img_channels, self._img_height, self._img_width), \
                    f"image size should be (Any, {self._img_channels}, {self._img_height}, {self._img_width}), instead it is  {predicted_img_batch.size()}"
            assert next_vec_batch.size()[1] == self._vec_part_size, \
                    f"Each vector observation should have size {self._vec_part_size}, not {next_vec_batch.size()[1]}. Tensor has size {next_vec_batch.size()}"
            assert predicted_vec_batch.size()[1] == self._vec_part_size, \
                    f"Each vector observation should have size {self._vec_part_size}, not {predicted_vec_batch.size()[1]}. Tensor has size {predicted_vec_batch.size()}"
            assert reconstructed_img_batch.size() == (batch_size, self._img_channels, self._img_height, self._img_width), \
                    f"image size should be (Any, {self._img_channels}, {self._img_height}, {self._img_width}), instead it is  {reconstructed_img_batch.size()}"
            assert reward_batch.size() == (batch_size,1), \
                    f"Each reward batch must have size {(batch_size,)}, tensor has size {reward_batch.size()}"
            assert predicted_reward_batch.size() == (batch_size,1), \
                    f"Each predicted_reward batch must have size {(batch_size,)}, tensor has size {reward_batch.size()}"
            assert mu_batch.size() == (batch_size, self._img_encoding_size), \
                    f"Each mu batch must have size {(batch_size, self.encoding_size())}, tensor has size {mu_batch.size()}"
            assert log_var_batch.size() == (batch_size, self._img_encoding_size), \
                    f"Each mu batch must have size {(batch_size, self.encoding_size())}, tensor has size {log_var_batch.size()}"


        if self._hyperparams.checkFinite:
            if not th.all(th.isfinite(reward_batch)): raise RuntimeError(f"Invalid reward_batch isnan={th.any(th.isnan(reward_batch))}")
            if not th.all(th.isfinite(predicted_reward_batch)): raise RuntimeError(f"Invalid reward_batch isnan={th.any(th.isnan(predicted_reward_batch))}")
            if not th.all(th.isfinite(next_img_batch)): raise RuntimeError(f"Invalid next_img_batch isnan={th.any(th.isnan(next_img_batch))}")
            if not th.all(th.isfinite(predicted_img_batch)): raise RuntimeError("Invalid predicted_img_batch isnan={th.any(th.isnan(predicted_img_batch))}")
            if not th.all(th.isfinite(next_vec_batch)): raise RuntimeError("Invalid next_vec_batch isnan={th.any(th.isnan(next_vec_batch))}")
            if not th.all(th.isfinite(predicted_vec_batch)): raise RuntimeError("Invalid predicted_vec_batch isnan={th.any(th.isnan(predicted_vec_batch))}")

        kld_loss = kld_loss_func(self._hyperparams.kld_loss_weight_th, log_var_batch, mu_batch)
        if self._hyperparams.checkFinite:
            if not th.all(th.isfinite(kld_loss)): raise RuntimeError(f"Invalid KL-divergence loss {kld_loss}")

        reconstruction_loss, img_rec_loss, vec_rec_loss = self.obs_reconstruction_loss_func(self._hyperparams.vec_loss_weight_th,
                                                                                    vec_batch = predicted_vec_batch,
                                                                                    true_vec_batch = input_vec_batch,
                                                                                    img_batch = reconstructed_img_batch,
                                                                                    true_img_batch = input_img_batch[:,0:int(self._img_channels/self._frame_stack_size)])
        reconstruction_loss = self._hyperparams.rec_loss_weight_th*reconstruction_loss
        

        reward_batch_scaled = reward_batch / self._reward_scaling
        predicted_reward_batch_scaled = predicted_reward_batch / self._reward_scaling
        if self._log_reward:
            predicted_reward_batch_scaled = th.log(1+predicted_reward_batch_scaled)
            reward_batch_scaled = th.log(1+reward_batch_scaled)
        prediction_loss, img_loss, reward_loss, vec_loss = self.prediction_loss_func(self._hyperparams.reward_loss_weight_th,
                                                                                     self._hyperparams.vec_loss_weight_th,
                                                                                     reward_batch_scaled,
                                                                                     predicted_reward_batch_scaled,
                                                                                     next_vec_batch,
                                                                                     predicted_vec_batch,
                                                                                     next_img_batch,
                                                                                     predicted_img_batch)
        # ggLog.info(f"reward_batch_scaled = {reward_batch_scaled}, predicted_reward_batch_scaled = {predicted_reward_batch_scaled}, {reward_loss}")
        if self._hyperparams.checkFinite:
            if not th.all(th.isfinite(reward_loss)): raise RuntimeError(f"Invalid reward loss {reward_loss}")
            if not th.all(th.isfinite(vec_loss)): raise RuntimeError(f"Invalid vector-part loss {vec_loss}")
            if not th.all(th.isfinite(img_loss)): raise RuntimeError(f"Invalid image-part loss {img_loss}")
            if not th.all(th.isfinite(prediction_loss)): raise RuntimeError(f"Invalid reconstruction loss {prediction_loss}")

        # print(f"{reconstruction_loss}+{self._kld_loss_weight}*{kld_loss}")

        # prediction_loss = img_loss + vec_loss
        loss = prediction_loss + kld_loss + reconstruction_loss


        # if self.training:
        #     # ggLog.info(f"loss = {loss} type = {type(loss)}")
        #     torchviz.make_dot(loss, params=dict(self.named_parameters())).render("MultiObsDynVAE3", format="pdf")
        #     exit(0)

        # ggLog.info("computed dyn vae loss")
        return loss, prediction_loss, kld_loss, reward_loss, img_loss, vec_loss, reconstruction_loss





    @th.jit.export
    def compute_losses(self,  transition_batch : Transition) -> Tuple[th.Tensor,th.Tensor,th.Tensor,th.Tensor]:
        predicted_observation_batch, predicted_reward_batch, mu_batch, log_var_batch, reconstructed_observation_batch = self(transition_batch)
        return  self._loss_functions(   transition_batch.observation,
                                        transition_batch.next_observation,
                                        transition_batch.reward,
                                        mu_batch,
                                        log_var_batch,
                                        predicted_observation_batch,
                                        predicted_reward_batch,
                                        reconstructed_obs_batch=reconstructed_observation_batch)


    def freeze_dynamics(self):
        self._dynamics_nets.requires_grad_(False)
        if self._reward_nn_arch is not None:
            self._reward_net.requires_grad_(False)

    def unfreeze_dynamics(self):
        self._dynamics_nets.requires_grad_(True)
        if self._reward_nn_arch is not None:
            self._reward_net.requires_grad_(True)