
from typing import Tuple

import torch as th
import torch.nn as nn
from torchvision import transforms
from typing import Dict, Union, List, Tuple

import lr_gym.utils.dbg.ggLog as ggLog
import lr_gym

from autoencoding_rl.latent_extractors.vae.VAE_encoder import VAE_encoder
from autoencoding_rl.latent_extractors.autoencoder.AE_decoder import AE_decoder
from autoencoding_rl.utils import Transition
from autoencoding_rl.utils import ObsConverter
from autoencoding_rl.latent_extractors.BaseAutoencoder import BaseAutoencoder
from autoencoding_rl.latent_extractors.dyn_vae.MultiObsDynVAE3 import MultiObsDynVAE3
from typing import Callable
import gym

class MultiObsVAE3(MultiObsDynVAE3):


    def __init__(self,  observation_shape : Union[Dict[str,List[int]],Tuple[int,int,int], gym.spaces.Dict],
                        img_encoding_size: int,
                        action_size: int,
                        dynamics_nn_arch: Tuple[int, int] = None,
                        rewardLossWeight : float = 1.0,
                        kldLossWeight : float = 0.0001,

                        vecLossWeight : float = 0.005,
                        torchDevice : str = "cuda",
                        encoder_ensemble_size = 1,
                        dynamics_ensemble_size = 1,
                        dynamics_model_class : type = None,
                        backbone : str = "conv",
                        deconv_backbone : str = None,
                        augment_callback : Callable[[th.Tensor], th.Tensor] = None,
                        checkDimensions : bool = True,
                        checkFinite : bool = True,
                        e2eResidual : bool = False,
                        reconstructionLossWeight : float = 1.0,
                        state_combiner_nn_arch : List[int] = "identity",
                        
                        frame_stack_size : int = 1,
                        learning_rate : float = 0.0001,
                        
                        latent_space_activation = th.nn.Tanh,
                        vec_decoder_nn_arch = None,
                        vec_encoder_nn_arch = "identity",
                        img_decoder_ensemble_size = 1,
                        
                        reward_scaling : float = 1,
                        reward_nn_arch: Tuple[int, int] = None,
                        reward_ensemble_size = 1,
                        log_reward : bool = False):

        class NopDynModel(nn.Module):
            def forward(self, state_action_batch):
                batch_size = state_action_batch.shape[0]
                encoding_size = state_action_batch.shape[1]-action_size
                pred_state = state_action_batch[:,0:encoding_size] #Just keep the encoding part of the input, discard the action
                # ggLog.info(f"pred_state.shape = {pred_state.shape}")
                pred_reward = th.zeros(size=[batch_size,1], dtype= pred_state.dtype, device=pred_state.device)
                # ggLog.info(f"pred_reward.shape = {pred_reward.shape}")
                ret = th.cat([pred_state,pred_reward], dim=1)
                # ggLog.info(f"ret.shape = {ret.shape}")
                return ret

        super().__init__(   observation_shape = observation_shape,
                            img_encoding_size = img_encoding_size,
                            action_size = action_size,
                            dynamics_nn_arch = None,
                            rewardLossWeight = rewardLossWeight,
                            kldLossWeight = kldLossWeight,

                            vecLossWeight = vecLossWeight,
                            torchDevice = torchDevice,
                            encoder_ensemble_size = encoder_ensemble_size,
                            dynamics_ensemble_size = 1,
                            dynamics_model_class = NopDynModel,
                            backbone = backbone,
                            deconv_backbone = deconv_backbone,
                            augment_callback = augment_callback,
                            checkDimensions = checkDimensions,
                            checkFinite = checkFinite,
                            e2eResidual = e2eResidual,
                            reconstructionLossWeight = reconstructionLossWeight,
                            state_combiner_nn_arch = state_combiner_nn_arch,
                            
                            frame_stack_size = frame_stack_size,
                            learning_rate = learning_rate,
                            
                            latent_space_activation = latent_space_activation,
                            vec_decoder_nn_arch = vec_decoder_nn_arch,
                            vec_encoder_nn_arch = vec_encoder_nn_arch,
                            img_decoder_ensemble_size = img_decoder_ensemble_size,
                            
                            reward_scaling = reward_scaling,
                            reward_nn_arch = reward_nn_arch,
                            reward_ensemble_size=reward_ensemble_size,
                            log_reward=log_reward)


    def _loss_functions(self,   input_observation_batch : Dict[str,th.Tensor],
                                next_observation_batch : Dict[str,th.Tensor],
                                reward_batch : th.Tensor,
                                mu_batch : th.Tensor,
                                log_var_batch : th.Tensor,
                                predicted_observation_batch : Dict[str,th.Tensor],
                                predicted_reward_batch,
                                reconstructed_obs_batch) -> Tuple[th.Tensor,th.Tensor,th.Tensor]:

        return super()._loss_functions( input_observation_batch = input_observation_batch,
                                        next_observation_batch = input_observation_batch,
                                        reward_batch = reward_batch,
                                        mu_batch = mu_batch,
                                        log_var_batch = log_var_batch,
                                        predicted_observation_batch = predicted_observation_batch,
                                        predicted_reward_batch= predicted_reward_batch,
                                        reconstructed_obs_batch = reconstructed_obs_batch)