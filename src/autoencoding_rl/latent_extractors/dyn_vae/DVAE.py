

from autoencoding_rl.latent_extractors.MultiObsAutoencoder import MultiObsAutoencoder
import lr_gym.utils.dbg.ggLog as ggLog
from autoencoding_rl.utils import Transition

import torch as th

class DVAE(MultiObsAutoencoder):


    def train_model(self, preprocessed_transition_batch):
        # th.cuda.synchronize()
        # tpcl = time.monotonic()
        self.train() # Put module in train mode
        self._optimizer.zero_grad(set_to_none=True)
        with th.set_grad_enabled(True):
            try:
                loss, prediction_loss, kld_loss, reward_loss, img_loss, vec_loss, reconstruction_loss = self.compute_losses(preprocessed_transition_batch)
            except RuntimeError as e:
                # Look the other way an go on
                self._skippedMinibatchesCount += 1
                self._skippedMinibatchesStreakCount += 1
                ggLog.warn(f"RuntimeError during compute_losses(). Skipping minibatch (counter = {self._skippedMinibatchesStreakCount}. Exception = "+str(e))
                if self._skippedMinibatchesStreakCount > 1000:
                    raise RuntimeError(f"Skipped {self._skippedMinibatchesStreakCount} consecutive minibatches, giving up. (This probably means there's NaNs in the net)")
                return
            
        if self._hyperparams.checkFinite:
            if not th.all(th.isfinite(loss)):
                ggLog.warn(f"Invalid loss encountered! Skipping minibatch. lossVal = {loss.item()}")
                return
            if th.abs(loss)>100:
                ggLog.warn(f"Suspicious lossVal detected, something wrong? lossVal = {loss.item()} rec_loss = {prediction_loss.item()} kld_loss = {kld_loss.item()} reward_loss = {reward_loss.item()}")
        
        try:
            loss.backward()
            # reward_loss.backward()
        except RuntimeError as e:
            # Look the other way an go on
            self._skippedMinibatchesCount += 1
            self._skippedMinibatchesStreakCount += 1
            ggLog.warn(f"RuntimeError during loss.backward(). lossVal = {loss.item()}. Skipping minibatch (counter = {self._skippedMinibatchesStreakCount}. Exception = "+str(e))
            if self._skippedMinibatchesStreakCount > 1000:
                raise RuntimeError(f"Skipped {self._skippedMinibatchesStreakCount} consecutive minibatches, giving up. (This probably means there's NaNs in the net)")
            return

        th.nn.utils.clip_grad_norm_(self.parameters(), 10.0)

        self._skippedMinibatchesStreakCount = 0
        self._optimizer.step()

        # detach losses to free up memory
        sublosses = {"prediction_loss" : prediction_loss.detach(),
                     "kld_loss" : kld_loss.detach(),
                     "reward_loss" : reward_loss.detach(),
                     "img_loss" : img_loss.detach(),
                     "vec_loss" : vec_loss.detach(),
                     "rec_loss" : reconstruction_loss.detach()}
        return loss.detach(), sublosses

    @th.jit.export
    def extract_img_latent_distribution(self, img_batch : th.Tensor):
        return self._img_encoder(img_batch)


    @th.jit.export
    def compute_loss(self,  transition_batch : Transition):
        return self.compute_losses(transition_batch)[0]


    @th.jit.export
    def _augment(self, img_batch : th.Tensor):
        if self._augment_callback is None or not self.training:
            return img_batch
        else:
            return self._augment_callback(img_batch)




    @th.jit.export
    def encoding_size(self):
        return self._img_encoding_size + self._vec_part_size

    @th.jit.export
    def action_size(self):
        return self._action_size



    def freeze_encoders(self):
        self._img_encoder.requires_grad_(False)
        self._vec_encoder.requires_grad_(False)

    def unfreeze_encoders(self):
        self._img_encoder.requires_grad_(True)
        self._vec_encoder.requires_grad_(True)

    def freeze_decoder(self):
        self._img_decoder.requires_grad_(False)
        self._vec_decoder.requires_grad_(False)

    def unfreeze_decoder(self):
        self._img_decoder.requires_grad_(True)
        self._vec_decoder.requires_grad_(True)

    def freeze_dynamics(self):
        self._dynamics_nets.requires_grad_(False)

    def unfreeze_dynamics(self):
        self._dynamics_nets.requires_grad_(True)

    def get_init_args(self):
        return self._init_args

    
    @th.jit.export
    @staticmethod
    def obs_reconstruction_loss_func(vec_weight,
                                    vec_batch,
                                    true_vec_batch,
                                    img_batch,
                                    true_img_batch):
        # Second part of the loss should theoretically maxmise:
        #    E_{z~q_theta}[log(p_phi(x_i|z))]
        # Which with q_theta(z|x_i) = N(mu, e^logvar) can be reparametrized:
        #    1/L*∑_i∈[0,L] [log(p_phi(x_i | mu_theta + var_theta * eps)] with eps~N(0,I)
        # i.e. it's a maximum log-likelihood term, in which phi and theta must be optimized
        # At this point there's some considerations:
        #  - we can just set L = 1 if batch sizes are big enough (~100)
        #  - Following what section 5.1 of "A Tutorial on VAEs: From Bayes' Rule to Lossless Compression" by Roland Yu says,
        #    we use a logMSE loss (and it does make a nice difference from normal MSE)
        # reward_loss = reward_weight*th.log(th.nn.functional.mse_loss(predicted_reward_batch, reward_batch)+0.0000001)
        if vec_batch.size()[1] != 0:
            vec_rec_loss = vec_weight*th.log(th.nn.functional.mse_loss(vec_batch, true_vec_batch)+0.0000001)
        else:
            vec_rec_loss = th.tensor(0)



        # next_img = (next_img_batch[0]+1)/2
        # pred_img = (predicted_img_batch[0]+1)/2
        # recdiff = (th.abs(next_img_batch[0]-predicted_img_batch[0])+1)/2
        # thimg_side2side = th.cat((next_img, pred_img, recdiff), dim=2)
        # dbg_img.helper.publishDbgImg("reconstruction_diff", tensorToHumanCvImageRgb(thimg_side2side))



        img_rec_loss = th.log(th.nn.functional.mse_loss(img_batch, true_img_batch)+0.0000001)
        rec_loss = img_rec_loss + vec_rec_loss

        return rec_loss, img_rec_loss, vec_rec_loss

    @th.jit.export
    @staticmethod
    def prediction_loss_func(   reward_weight,
                                vec_weight,
                                reward_batch,
                                predicted_reward_batch,
                                next_vec_batch,
                                predicted_vec_batch,
                                next_img_batch,
                                predicted_img_batch):
        # Second part of the loss should theoretically maxmise:
        #    E_{z~q_theta}[log(p_phi(x_i|z))]
        # Which with q_theta(z|x_i) = N(mu, e^logvar) can be reparametrized:
        #    1/L*∑_i∈[0,L] [log(p_phi(x_i | mu_theta + var_theta * eps)] with eps~N(0,I)
        # i.e. it's a maximum log-likelihood term, in which phi and theta must be optimized
        # At this point there's some considerations:
        #  - we can just set L = 1 if batch sizes are big enough (~100)
        #  - Following what section 5.1 of "A Tutorial on VAEs: From Bayes' Rule to Lossless Compression" by Roland Yu says,
        #    we use a logMSE loss (and it does make a nice difference from normal MSE)
        reward_loss = reward_weight*th.log(th.nn.functional.mse_loss(predicted_reward_batch, reward_batch) + 0.0000001)
        
        obs_rec_loss, img_rec_loss, vec_rec_loss = DVAE.obs_reconstruction_loss_func(vec_weight =vec_weight,
                                                                                    vec_batch = predicted_vec_batch,
                                                                                    true_vec_batch = next_vec_batch,
                                                                                    img_batch = predicted_img_batch,
                                                                                    true_img_batch = next_img_batch)
        prediction_loss = obs_rec_loss + reward_loss

        return prediction_loss, img_rec_loss, reward_loss, vec_rec_loss



    def getModelSizeRecap(self):
        total_params_count = sum(p.numel() for p in self.parameters() if p.requires_grad)
        r = ""                           
        r += f"DAE total paramer count = {total_params_count}\n"
        r += f"    - img_encoder = {sum(p.numel() for p in self._img_encoder.parameters() if p.requires_grad)}\n"
        r += f"    - img_decoder = {sum(p.numel() for p in self._img_decoder.parameters() if p.requires_grad)}\n"
        r += f"    - dynamics = {sum(p.numel() for p in self._dynamics_nets.parameters() if p.requires_grad)}\n"
        r += f"    - vec_encoder = {sum(p.numel() for p in self._vec_encoder.parameters() if p.requires_grad)}\n"
        r += f"    - vec_decoder = {sum(p.numel() for p in self._vec_decoder.parameters() if p.requires_grad)}\n"
        if hasattr(self,"_reward_net"):
            r += f"    - reward_net = {sum(p.numel() for p in self._reward_net.parameters() if p.requires_grad)}\n"
        full_recap = "Detail:\n"
        for name, param in self.named_parameters():
            if param.requires_grad:
                full_recap += f" - {name} : {param.numel()}\n"
        r += full_recap
        
        return r