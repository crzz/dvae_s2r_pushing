#!/usr/bin/env python3


import torch as th
import torch.nn as nn
from abc import ABC, abstractmethod


class BaseAutoencoder(nn.Module, ABC):

    @abstractmethod
    def encoding_size(self):
        raise NotImplementedError()

    @abstractmethod
    def input_width(self):
        raise NotImplementedError()

    @abstractmethod
    def input_height(self):
        raise NotImplementedError()

    @abstractmethod
    def input_channels(self):
        raise NotImplementedError()


    @abstractmethod
    def forward(self, x : th.Tensor) -> th.Tensor:
        raise NotImplementedError()


    @abstractmethod
    def encode(self, x : th.Tensor) -> th.Tensor:
        raise NotImplementedError()


    @abstractmethod
    def decode(self, x : th.Tensor) -> th.Tensor:
        raise NotImplementedError()

    @abstractmethod
    def compute_loss(self, input_image_batch : th.Tensor):
        raise NotImplementedError()

    @abstractmethod
    def preprocess_observations(self, observation_batch : th.Tensor) -> th.Tensor:
        """Preprocess the provided observations for network input

        Parameters
        ----------
        observation_batch : th.Tensor
            A batch of observations

        Returns
        -------
        th.Tensor
            Batch of preprocessed images
        """
        raise NotImplementedError()

    @abstractmethod
    def postprocess_observations(self, observation_batch : th.Tensor) -> th.Tensor:
        """Processes a batch of observations produced as output by the autoencoder in a 
           format that can be correctly displayed. In practice the output should 
           be compatible with torchvision.transforms.ToPILImage

        Parameters
        ----------
        observation_batch : th.Tensor
            A batch of observations produced by the autoencoder output

        Returns
        -------
        th.Tensor
            A batch of observations in a standard format
        """
        raise NotImplementedError()

    @abstractmethod
    def _buildModels(self):
        raise NotImplementedError()

    # @abstractmethod
    def _buildEncoders(self):
        raise NotImplementedError()

    # @abstractmethod
    def _buildDecoders(self):
        raise NotImplementedError()

    # @abstractmethod
    def _buildDynamics(self):
        raise NotImplementedError()

    def reset(self): 
        self._buildModels()

    def reset_encoders(self):
        self._buildEncoders()
        self._buildOptimizer()
    
    def reset_decoders(self):
        self._buildDecoders()
        self._buildOptimizer()
    
    def reset_dynamics(self):
        self._buildDynamics()
        self._buildOptimizer()
    
    @abstractmethod
    def train_model(self, preprocessed_transition_batch):
        raise NotImplementedError()

    @abstractmethod
    def get_init_args(self):
        raise NotImplementedError()