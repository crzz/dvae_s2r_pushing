#!/usr/bin/env python3

import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import argparse
import time
import signal
import os
import pandas
from typing import List
import numpy as np
from pandas.api.types import is_string_dtype

fig = None
ax = None

def makePlot(csvfile : str,
                x_data_id : str,
                max_x : float,
                min_x : float,
                y_data_ids : List[str],
                y_data_colors=List[np.ndarray],
                maxy : float = None,
                miny : float = None,
                avglen : int = 20,
                noraw : bool = False):
    df = pd.read_csv(csvfile)
    print(f" cols = "+str(list(df.columns)))


    if max_x is not None:
        df = df.loc[df[x_data_id] < max_x]

    # df = df.apply(lambda x: pd.to_numeric(x, errors='coerce')).dropna() # fix for bug in latentextractor, drop lines that have strings in loss

    if "best_changed" in df:
        # print(df)
        df["best_changed"].fillna(value=-1,inplace=True)
        # print(is_string_dtype(df["best_changed"]))
        # print(df)
        df["best_changed"] = df["best_changed"].replace({'True':True,'False':False})
        df["best_changed"] = df["best_changed"].astype(bool)
        # print(df)
        # df["best_changed"] = df["best_changed"].astype(int)*1000 # boolean to 0-1
        # print(df)

    if x_data_id == "episode_count":
        episodes = pandas.unique(df["episode_count"])
        # for ep in episodes:
        #     print(f"episode={ep} {type(ep)}")
        #     print(f"df['episode_count'] = {df['episode_count']}")
        #     print(f"df['episode_count']==ep = {df['episode_count']==ep}")
        #     print(f"steps={steps}")
        #     if steps==0:
        #         continue
        def f(row):
            if len(df[df['episode_count']==row['episode_count']]) != 0:
                return row['episode_count']+(row['train_iterations_done']-df[df['episode_count']==row['episode_count']]['train_iterations_done'].min())/len(df[df['episode_count']==row['episode_count']])
            else:
                return row['episode_count']
        df['episode_count'] = df.apply( f ,axis=1)
    
    print(f"{len(df[x_data_id])} x samples")
    
    # sns.set_theme(style="darkgrid")
    global fig
    global ax
    if fig is None:
        fig, ax = plt.subplots(1, 1)
    ax.cla()

    #sns.set_style("dark")
    #sns.set_context("paper")

    palette = sns.color_palette("husl", len(y_data_ids))


    for i in range(len(y_data_ids)):
        y_data_id = y_data_ids[i]
        color = palette[i]
        color = tuple([c for c in color])
        color_avg = tuple([c/2 for c in color])
        # print(f"color = {color}")
        # print(f"color_avg = {color_avg}")
        # color = "#%0.2X"%color[0]+"%0.2X"%color[1]+"%0.2X"%color[2]
        # color_avg = "#%0.2X"%color_avg[0]+"%0.2X"%color_avg[1]+"%0.2X"%color_avg[2]

        # df[y_data_id] = np.log(df[y_data_id])

        print(f"{len(df[y_data_id])} y {y_data_id} samples")
        if not noraw:
            p = sns.lineplot(x=df[x_data_id][1:],y=df[y_data_id][1:],color=color, alpha = 0.5, ci=None, label=y_data_id, linewidth = 0.5) #, ax = ax) #
            # ax.plot(df[x_data_id],df[y_data_id],color=color, label = y_data_id, alpha = 0.6) #, ax = ax) #
            avg_y = df[y_data_id].rolling(avglen).mean()
            p = sns.lineplot(x=df[x_data_id],y=avg_y,color=color_avg, alpha = 0.5, ci=None, linewidth = 0.5) #, ax = ax) #
        else:
            # ax.plot(df[x_data_id],df[y_data_id],color=color, label = y_data_id, alpha = 0.6) #, ax = ax) #
            avg_y = df[y_data_id].rolling(avglen).mean()
            p = sns.lineplot(x=df[x_data_id],y=avg_y,color=color, alpha = 0.5, ci=None, linewidth = 0.5, label=y_data_id) #, ax = ax) #

        # ax.plot(df[x_data_id],avg_y,color=color_avg, alpha = 0.6) #, ax = ax) #
        # p.set_yscale("log")
    #plt.legend(loc='lower right', labels=names)
    ax.legend(prop={'size': 4})
    folders = os.path.dirname(args["csvfile"]).split("/")
    ax.set_title(folders[-3]+"/"+folders[-2])
    ax.set_xlabel(x_data_id)
    
    ax.minorticks_on()
    ax.grid(linestyle='dotted',which="both")
    ylab = ""
    for s in y_data_ids:
        ylab += s + ","
    ylab = ylab[:-1]
    ax.set_ylabel(ylab)
    ax.set_xlim(min_x,max_x)
    ax.set_ylim(miny,maxy)
    fig.tight_layout()



ctrl_c_received = False
def signal_handler(sig, frame):
    #print('You pressed Ctrl+C!')
    global ctrl_c_received
    ctrl_c_received = True

ap = argparse.ArgumentParser()
ap.add_argument("--csvfile", required=True, type=str, help="Csv file to read from")
ap.add_argument("--nogui", default=True, action='store_true', help="Dont show the plot window, just save to file")
ap.add_argument("--usetimex", default=False, action='store_true', help="Use time on the x axis")
ap.add_argument("--usesamplesx", default=False, action='store_true', help="Use training samples count on the x axis")
ap.add_argument("--maxx", required=False, default=None, type=float, help="Maximum x value to plot")
ap.add_argument("--minx", required=False, default=None, type=float, help="Minimum x value to plot")
ap.add_argument("--maxy", required=False, default=None, type=float, help="Maximum y value to plot")
ap.add_argument("--miny", required=False, default=None, type=float, help="Minimum y value to plot")
ap.add_argument("--ydataids", nargs="+", required=False, default=None, type=str, help="Ids for y values")
ap.add_argument("--period", required=False, default=-1, type=float, help="Seconds to wait between plot update")
ap.add_argument("--losscomponents", default=False, action='store_true', help="Plot loss components")
ap.add_argument("--noraw", default=False, action='store_true', help="Only print average curves")
ap.add_argument("--avglen", required=False, default=100, type=int, help="Averaging window size")
ap.add_argument("--xdataid", required=False, default="train_iterations_done", type=str, help="Which data to use on x axis")
ap.set_defaults(feature=True)
args = vars(ap.parse_args())
signal.signal(signal.SIGINT, signal_handler)

matplotlib.rcParams['figure.raise_window'] = False
#matplotlib.use('Tkagg')
if args["nogui"]:
    plt.ioff()
else:
    plt.ion()

if args["usetimex"] and args["usesamplesx"]:
    print("You cannot use --usetimex and --usesamplesx at the same time")
    exit(0)

x_data_id = args["xdataid"]

if args["losscomponents"]:
    y_data_ids=["loss", "gen_loss", "kld_loss"]
elif args["ydataids"] is not None:
    y_data_ids=args["ydataids"]
else:
    y_data_ids=["loss", "val_loss_latest", "val_loss_best", "best_changed"]

#fig, ax = plt.subplots(figsize=(11, 8.5))
while not ctrl_c_received:
    #print("Plotting")
    try:
        makePlot(args["csvfile"], x_data_id, max_x = args["maxx"], min_x = args["minx"], y_data_ids=y_data_ids,
                 y_data_colors=[ np.array([64,64,64], dtype=np.int32),
                                 np.array([255,64,64], dtype=np.int32),
                                 np.array([64,64,255], dtype=np.int32),
                                 np.array([64,255,64], dtype=np.int32)],
                 miny = args["miny"], maxy=args["maxy"],
                 avglen = args["avglen"],
                 noraw = args["noraw"])
        outfile = os.path.dirname(args["csvfile"])+"/loss.pdf"
        plt.savefig(outfile)
        print(f" Saved to {outfile}")
        #plt.show(block=True)
        if not args["nogui"]:
            #plt.draw()
            if args["once"]:
                plt.show(block=True)
                break
            else:
                plt.show(block=False)
            plt.pause(0.01)
    except pandas.errors.EmptyDataError:
        print("No data...")
    except FileNotFoundError:
        print("File not present...")
    if args["period"] <= 0:
        break
    time.sleep(args["period"])
