from autoencoding_rl.latent_extractors.BaseAutoencoder import BaseAutoencoder
import torch as th
import torch.nn as nn
from stable_baselines3.common.torch_layers import BaseFeaturesExtractor
from stable_baselines3.common.preprocessing import is_image_space, get_flattened_obs_dim
from stable_baselines3.common.type_aliases import TensorDict, Schedule
from typing import Dict, Optional, Union, List, Type, Any, Callable
import gym
from collections import deque
import copy
import warnings
from stable_baselines3.common.torch_layers import NatureCNN, get_actor_critic_arch

from stable_baselines3.sac.policies import SACPolicy, Actor
from stable_baselines3.common.distributions import SquashedDiagGaussianDistribution, StateDependentNoiseDistribution
from stable_baselines3.common.preprocessing import get_action_dim
from autoencoding_rl.utils import cmpObs, build_mlp_net
import lr_gym.utils.dbg.ggLog as ggLog

class DecoupledExtractor(BaseFeaturesExtractor):
    """
    """

    def __init__(self, observation_space: gym.spaces.Dict,
                        latent_extractor_constructor : Callable,
                        debug_out_folder : str = None,
                        enable_cache = True):
        if debug_out_folder is not None:
            latent_extractor = latent_extractor_constructor(debug_out_folder = debug_out_folder)
        else:
            latent_extractor = latent_extractor_constructor()
        super().__init__(observation_space, features_dim=latent_extractor.encoding_size())
        self.latent_extractor = [None]
        self.latent_extractor[0] = latent_extractor # Use a list to avoid module registration (To avoid the optimizer looking at these parameters)
        self._enable_cache = enable_cache
        self._cachesize = 10
        self._last_input_outputs = deque(maxlen=self._cachesize)


    def forward(self, observations: Dict[Union[str, int], th.Tensor]) -> th.Tensor:
        with th.no_grad():
            if self._enable_cache:
                current_train_count = self.latent_extractor[0].train_iterations_count()
                for inout in self._last_input_outputs:
                    # Check if ionp0ut observation is the same and latent_extractor hasn't been trained further
                    if current_train_count==inout[2] and cmpObs(observations, inout[0]):
                        return inout[1]
                # ggLog.info("Obs not in extractor cache")
                output = self.latent_extractor[0].encode(observations).detach()
                self._last_input_outputs.append((copy.deepcopy(observations), output.clone().detach(), current_train_count))
                return output
            else:
                return self.latent_extractor[0].encode(observations).detach()

    def train(self, mode):
        super().train(mode)
        self.latent_extractor[0].train(mode)

    def eval(self):
        super().eval()
        self.latent_extractor[0].eval()


class EnsembleActor(Actor):
    def __init__(
        self,
        observation_space: gym.spaces.Space,
        action_space: gym.spaces.Space,
        net_arch: List[int],
        features_extractor: nn.Module,
        features_dim: int,
        activation_fn: Type[nn.Module] = nn.ReLU,
        use_sde: bool = False,
        log_std_init: float = -3,
        full_std: bool = True,
        sde_net_arch: Optional[List[int]] = None,
        use_expln: bool = False,
        clip_mean: float = 2.0,
        normalize_images: bool = True,
        ensemble_size : int = 1
    ):
        super(Actor,self).__init__(
            observation_space,
            action_space,
            features_extractor=features_extractor,
            normalize_images=normalize_images,
            squash_output=True,
        )

        # Save arguments to re-create object at loading
        self.use_sde = use_sde
        self.sde_features_extractor = None
        self.net_arch = net_arch
        self.features_dim = features_dim
        self.activation_fn = activation_fn
        self.log_std_init = log_std_init
        self.sde_net_arch = sde_net_arch
        self.use_expln = use_expln
        self.full_std = full_std
        self.clip_mean = clip_mean

        if sde_net_arch is not None:
            warnings.warn("sde_net_arch is deprecated and will be removed in SB3 v2.4.0.", DeprecationWarning)

        action_dim = get_action_dim(self.action_space)
        # latent_pi_net = create_mlp(features_dim, -1, net_arch, activation_fn)
        self.latent_pi = build_mlp_net(net_arch[:-1], features_dim, net_arch[-1], ensemble_size=ensemble_size,
                                    last_activation_class = activation_fn, return_ensemble_mean = True)

        last_layer_dim = net_arch[-1] if len(net_arch) > 0 else features_dim

        if self.use_sde:
            self.action_dist = StateDependentNoiseDistribution(
                action_dim, full_std=full_std, use_expln=use_expln, learn_features=True, squash_output=True
            )
            self.mu, self.log_std = self.action_dist.proba_distribution_net(
                latent_dim=last_layer_dim, latent_sde_dim=last_layer_dim, log_std_init=log_std_init
            )
            # Avoid numerical issues by limiting the mean of the Gaussian
            # to be in [-clip_mean, clip_mean]
            if clip_mean > 0.0:
                self.mu = nn.Sequential(self.mu, nn.Hardtanh(min_val=-clip_mean, max_val=clip_mean))
        else:
            self.action_dist = SquashedDiagGaussianDistribution(action_dim)
            self.mu = nn.Linear(last_layer_dim, action_dim)
            self.log_std = nn.Linear(last_layer_dim, action_dim)

class DecoupledExtractorPolicy(SACPolicy):
    """
    Policy class (with both actor and critic) for SAC.
    :param observation_space: Observation space
    :param action_space: Action space
    :param lr_schedule: Learning rate schedule (could be constant)
    :param net_arch: The specification of the policy and value networks.
    :param activation_fn: Activation function
    :param use_sde: Whether to use State Dependent Exploration or not
    :param log_std_init: Initial value for the log standard deviation
    :param sde_net_arch: Network architecture for extracting features
        when using gSDE. If None, the latent features from the policy will be used.
        Pass an empty list to use the states as features.
    :param use_expln: Use ``expln()`` function instead of ``exp()`` when using gSDE to ensure
        a positive standard deviation (cf paper). It allows to keep variance
        above zero and prevent it from growing too fast. In practice, ``exp()`` is usually enough.
    :param clip_mean: Clip the mean output when using gSDE to avoid numerical instability.
    :param features_extractor_class: Features extractor to use.
    :param normalize_images: Whether to normalize images or not,
         dividing by 255.0 (True by default)
    :param optimizer_class: The optimizer to use,
        ``th.optim.Adam`` by default
    :param optimizer_kwargs: Additional keyword arguments,
        excluding the learning rate, to pass to the optimizer
    :param n_critics: Number of critic networks to create.
    :param share_features_extractor: Whether to share or not the features extractor
        between the actor and the critic (this saves computation time)
    """

    def __init__(
        self,
        observation_space: gym.spaces.Space,
        action_space: gym.spaces.Space,
        lr_schedule: Schedule,
        net_arch: Optional[Union[List[int], Dict[str, List[int]]]] = None,
        activation_fn: Type[nn.Module] = nn.ReLU,
        use_sde: bool = False,
        log_std_init: float = -3,
        sde_net_arch: Optional[List[int]] = None,
        use_expln: bool = False,
        clip_mean: float = 2.0,
        features_extractor_class: Type[BaseFeaturesExtractor] = DecoupledExtractor,
        features_extractor_kwargs: Optional[Dict[str, Any]] = None,
        normalize_images: bool = False,
        optimizer_class: Type[th.optim.Optimizer] = th.optim.Adam,
        optimizer_kwargs: Optional[Dict[str, Any]] = None,
        n_critics: int = 2,
        share_features_extractor: bool = True,
        share_target_feature_extractor = False,
        actor_ensemble_size = 1
    ):
        if features_extractor_class != DecoupledExtractor:
            AttributeError("Only DecoupledExtractor is supported")
        if not share_features_extractor:
            AttributeError("Only shared feature extractor is supported, share_features_extractor must be true")
        if normalize_images:
            AttributeError("Image preprocessing is done by the latent extractor itself, normalize_images must be false")

        self._enable_latent_extractor_target = not share_target_feature_extractor

        super(SACPolicy, self).__init__(
            observation_space,
            action_space,
            features_extractor_class,
            features_extractor_kwargs,
            optimizer_class=optimizer_class,
            optimizer_kwargs=optimizer_kwargs,
            squash_output=True,
        )

        if net_arch is None:
            if features_extractor_class == NatureCNN:
                net_arch = []
            else:
                net_arch = [256, 256]

        actor_arch, critic_arch = get_actor_critic_arch(net_arch)

        self.net_arch = net_arch
        self.activation_fn = activation_fn
        self.net_args = {
            "observation_space": self.observation_space,
            "action_space": self.action_space,
            "net_arch": actor_arch,
            "activation_fn": self.activation_fn,
            "normalize_images": normalize_images,
        }
        self.actor_kwargs = self.net_args.copy()
        self.actor_kwargs["ensemble_size"]=actor_ensemble_size

        if sde_net_arch is not None:
            warnings.warn("sde_net_arch is deprecated and will be removed in SB3 v2.4.0.", DeprecationWarning)

        sde_kwargs = {
            "use_sde": use_sde,
            "log_std_init": log_std_init,
            "use_expln": use_expln,
            "clip_mean": clip_mean,
        }
        self.actor_kwargs.update(sde_kwargs)
        self.critic_kwargs = self.net_args.copy()
        self.critic_kwargs.update(
            {
                "n_critics": n_critics,
                "net_arch": critic_arch,
                "share_features_extractor": share_features_extractor,
            }
        )

        self.actor, self.actor_target = None, None
        self.critic, self.critic_target = None, None
        self.share_features_extractor = share_features_extractor

        self._build(lr_schedule)



    def _build(self, lr_schedule: Schedule) -> None:
        # Changed from default so that target net shares the feature extractor with the others
        # Will still be polyak averaged but it shouldn't matter, just averages with itself

        self.actor = self.make_actor()
        self.actor.optimizer = self.optimizer_class(self.actor.parameters(), lr=lr_schedule(1), **self.optimizer_kwargs)

        if self.share_features_extractor:
            self.critic = self.make_critic(features_extractor=self.actor.features_extractor)
            # Do not optimize the shared features extractor with the critic loss
            # otherwise, there are gradient computation issues
            critic_parameters = [param for name, param in self.critic.named_parameters() if "features_extractor" not in name]
        else:
            # Create a separate features extractor for the critic
            # this requires more memory and computation
            self.critic = self.make_critic(features_extractor=None)
            critic_parameters = self.critic.parameters()

        if self._enable_latent_extractor_target:
            # Critic target does not share the features extractor with critic
            self.critic_target = self.make_critic(features_extractor=None)
            self.critic_target.load_state_dict(self.critic.state_dict())
            # Copy latent_extractor weights
            self.critic_target.features_extractor.latent_extractor[0].load_state_dict(self.critic.features_extractor.latent_extractor[0].state_dict())
        else:
            # Critic target shares the features extractor with critic
            self.critic_target = self.make_critic(features_extractor=self.actor.features_extractor)
            self.critic_target.load_state_dict(self.critic.state_dict())

        self.critic.optimizer = self.optimizer_class(critic_parameters, lr=lr_schedule(1), **self.optimizer_kwargs)

        # Target networks should always be in eval mode
        self.critic_target.set_training_mode(False)

    def make_actor(self, features_extractor: Optional[BaseFeaturesExtractor] = None) -> Actor:
        actor_kwargs = self._update_features_extractor(self.actor_kwargs, features_extractor)
        return EnsembleActor(**actor_kwargs).to(self.device)