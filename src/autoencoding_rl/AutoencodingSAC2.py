
from email.policy import strict
from queue import Full
from stable_baselines3 import SAC
from typing import Union, Optional, Any, Dict, Tuple, List, Iterable, Type, Callable
import torch as th
from stable_baselines3.common.off_policy_algorithm import OffPolicyAlgorithm
from torch.nn import functional as F
from stable_baselines3.common.utils import polyak_update
from stable_baselines3.common import logger
import numpy as np
from stable_baselines3.common.buffers import ReplayBuffer
import gym
import time
import pathlib
import io
import os
import pickle
from tqdm import tqdm
import queue

from stable_baselines3.common.noise import ActionNoise
from stable_baselines3.common.type_aliases import GymEnv, MaybeCallback
from stable_baselines3.common.base_class import BaseAlgorithm
from stable_baselines3.common.callbacks import CallbackList
import stable_baselines3

from autoencoding_rl.latent_extractors.LatentExtractor import LatentExtractor
from autoencoding_rl.utils import AutoencodingSAC_VideoSaver, replay_data2transition_batch, OnStep_passthrough, LearningRateConf, NopSchedule
import lr_gym.utils.dbg.ggLog as ggLog
import lr_gym.utils.dbg.dbg_img as dbg_img
from autoencoding_rl.utils import tensorToHumanCvImageRgb, takeTime
from autoencoding_rl.DecoupledExtractorPolicy import DecoupledExtractor, DecoupledExtractorPolicy
import autoencoding_rl
import autoencoding_rl.utils
import autoencoding_rl.train_helpers
from autoencoding_rl.buffers import RandomHoldoutBuffer, ThDictReplayBuffer_updatable
import threading
import lr_gym.utils
import lr_gym.utils.utils
import code

# from operator import contains, itemgetter
# from pympler import tracker, muppy, refbrowser
# import gc



@th.jit.script
def critic_loss_func(target_q_values : th.Tensor, current_q_values : List[th.Tensor]):
    return 0.5 * th.sum(th.stack([F.mse_loss(current_q, target_q_values) for current_q in current_q_values]))

@th.jit.script
def actor_loss_func(forwards : List[th.Tensor], ent_coef, log_prob):
    q_values_pi = th.cat(forwards, dim=1)
    min_qf_pi, _ = th.min(q_values_pi, dim=1, keepdim=True)
    return (ent_coef * log_prob - min_qf_pi).mean()

@th.jit.script
def ent_coef_loss_func(log_ent_coef, log_prob, target_entropy):
    return -(log_ent_coef * (log_prob + target_entropy).detach()).mean()

@th.jit.script
def compute_target_q(next_q_values : List[th.Tensor], rewards, next_log_prob, ent_coef, gamma):
    next_q_values = th.cat(next_q_values, dim=1)
    next_q_values, _ = th.min(next_q_values, dim=1, keepdim=True)
    # add entropy term
    next_q_values = next_q_values - ent_coef * next_log_prob.reshape(-1, 1)
    # td error + entropy term
    # target_q_values = replay_data.rewards + (1 - replay_data.dones) * self.gamma * next_q_values
    return rewards + gamma * next_q_values

class AutoencodingSAC2(SAC):

    def __init__(   self,
                    env: GymEnv,
                    policy = DecoupledExtractorPolicy,
                    learning_rate: float = 3e-4,
                    buffer_size: int = int(1e6),
                    policy_learning_starts: int = 1000,
                    batch_size: int = 256,
                    tau: float = 0.005,
                    gamma: float = 0.99,
                    policy_train_period : Tuple[int,str] = (1,"step"),
                    policy_gradient_steps: int = 1,
                    action_noise: Optional[ActionNoise] = None,
                    optimize_memory_usage: bool = False,
                    target_entropy: Union[str, float] = "auto",
                    ent_coef: Union[str, float] = "auto",
                    tensorboard_log: Optional[str] = None,
                    create_eval_env: bool = False,
                    policy_kwargs: Dict[str, Any] = None,
                    verbose: int = 0,
                    seed: Optional[int] = None,
                    device: Union[th.device, str] = "auto",
                    _init_setup_model: bool = True,
                    feature_extractor_batch_size : int = 64,
                    feature_extractor_retrain_epochs : int = 100,
                    feature_extractor_grad_steps : int = 100,
                    feature_extractor_train_period : Union[int,str] = (1,"episode"),
                    feature_extractor_retrain_period : int = -1,
                    feature_extractor_retrain_grad_steps : int = int(10000/64),
                    feature_extractor_pretrain_grad_steps : int = int(1000/64),
                    feature_extractor_learning_starts: int = 1000,
                    feature_extractor_tau: int = 1.0,
                    debug : bool = True,
                    parallelize_experience_collection : bool = True,
                    automatic_grad_steps : bool = False,
                    validation_batch_size = 256,
                    policy_retrain_period : int = -1,
                    policy_retrain_grad_steps : int = 1000,
                    replay_buffer_class = RandomHoldoutBuffer,
                    replay_buffer_kwargs = {"buffer_class" : ThDictReplayBuffer_updatable,
                                            "validation_ratio" : 0.1,
                                            "disable" : False,
                                            "storage_torch_device" : "cpu"},
                    reset_on_retrain : bool = False,
                    random_steps = None):

        # Do not chnage these values without checking train(), it only works with these values now
        use_sde = False
        sde_sample_freq = -1
        use_sde_at_warmup = False
        target_update_interval: int = 1

        if policy_kwargs is None:
            policy_kwargs = {}
        
        if policy != DecoupledExtractorPolicy:
            raise AttributeError("Only DecoupledExtractorPolicy is supported")

        self._le_batch_size = None
        self._le_retrain_epochs = None
        self._le_grad_steps = None
        self._le_train_period = None
        self._le_retrain_period = None
        self._le_retrain_grad_steps = None
        self._le_reset_on_retrain = None
        self._le_pretrain_grad_steps = None
        self._le_learning_starts = None
        self._automatic_grad_steps = None
        self._validation_batch_size = None
        self._policy_learning_starts = None
        self._policy_retrain_period = None
        self._policy_retrain_grad_steps = None
        self._policy_train_period = None
        self._policy_grad_steps = None
        self._le_tau = None
        self._random_steps = None

        
        self.set_hyperparams(feature_extractor_batch_size,
                                feature_extractor_retrain_epochs,
                                feature_extractor_grad_steps,
                                feature_extractor_train_period,
                                feature_extractor_retrain_period,
                                feature_extractor_retrain_grad_steps,
                                feature_extractor_pretrain_grad_steps,
                                feature_extractor_learning_starts,
                                automatic_grad_steps,
                                validation_batch_size,
                                policy_learning_starts,
                                policy_retrain_period,
                                policy_retrain_grad_steps,
                                policy_train_period,
                                policy_gradient_steps,
                                feature_extractor_tau,
                                reset_on_retrain,
                                random_steps)

        if self._le_tau == 1.0:
            policy_kwargs["share_target_feature_extractor"] = True

        self._last_le_retraining = float("-inf")
        self._debug_mode = debug
        self._parallelize_experience_collection = parallelize_experience_collection
        self._predictDurationAverage = lr_gym.utils.utils.AverageKeeper(bufferSize = 100)
        self._last_policy_retraining = 0
        self._collected_eps_count = 0
        self._newly_collected_steps_sincelog = 0
        self._ttot_sincelog = 0
        self._dur_policyTrain_sincelog = 0
        self._policy_grad_steps_done_sincelog = 0
        self._dur_aeTrain_sincelog = 0
        self._ae_grad_steps_done_sincelog = 0

        self._last_ep_trained_ae = 0
        self._last_step_trained_ae = 0
        self._last_ep_trained_policy = 0
        self._last_step_trained_policy = 0




        super().__init__(   policy=policy,
                            env=env,
                            learning_rate=learning_rate,
                            buffer_size=buffer_size,
                            learning_starts=self.learning_starts,
                            batch_size=batch_size,
                            tau=tau,
                            gamma=gamma,
                            train_freq=self.train_freq,
                            gradient_steps=policy_gradient_steps,
                            #n_episodes_rollout=n_episodes_rollout,
                            action_noise=action_noise,
                            optimize_memory_usage=optimize_memory_usage,
                            ent_coef=ent_coef,
                            target_update_interval=target_update_interval,
                            target_entropy=target_entropy,
                            use_sde=use_sde,
                            sde_sample_freq=sde_sample_freq,
                            use_sde_at_warmup=use_sde_at_warmup,
                            tensorboard_log=tensorboard_log,
                            create_eval_env=create_eval_env,
                            policy_kwargs=policy_kwargs,
                            verbose=verbose,
                            seed=seed,
                            device=device,
                            _init_setup_model=_init_setup_model,
                            replay_buffer_class = replay_buffer_class,
                            replay_buffer_kwargs = replay_buffer_kwargs)

    @staticmethod
    def _none2default(value, default):
        return value if value is not None else default

    def set_hyperparams(self,
                        feature_extractor_batch_size = None,
                        feature_extractor_retrain_epochs = None,
                        feature_extractor_grad_steps = None,
                        feature_extractor_train_period = None,
                        feature_extractor_retrain_period = None,
                        feature_extractor_retrain_grad_steps = None,
                        feature_extractor_pretrain_grad_steps = None,
                        feature_extractor_learning_starts = None,
                        automatic_grad_steps = None,
                        validation_batch_size = None,
                        policy_learning_starts = None,
                        policy_retrain_period = None,
                        policy_retrain_grad_steps = None,
                        policy_train_period = None,
                        policy_gradient_steps = None,
                        feature_extractor_tau = None,
                        reset_on_retrain = None,
                        random_steps = None):

        self._le_batch_size = self._none2default(feature_extractor_batch_size, self._le_batch_size)
        self._le_retrain_epochs = self._none2default(feature_extractor_retrain_epochs,self._le_retrain_epochs)
        self._le_grad_steps = self._none2default(feature_extractor_grad_steps,self._le_grad_steps)
        self._le_train_period = self._none2default(feature_extractor_train_period,self._le_train_period)
        self._le_retrain_period = self._none2default(feature_extractor_retrain_period,self._le_retrain_period)
        self._le_retrain_grad_steps = self._none2default(feature_extractor_retrain_grad_steps,self._le_retrain_grad_steps)
        self._le_pretrain_grad_steps = self._none2default(feature_extractor_pretrain_grad_steps,self._le_pretrain_grad_steps)
        self._le_learning_starts = self._none2default(feature_extractor_learning_starts,self._le_learning_starts)
        self._automatic_grad_steps = self._none2default(automatic_grad_steps,self._automatic_grad_steps)
        self._validation_batch_size = self._none2default(validation_batch_size,self._validation_batch_size)
        self._policy_learning_starts = self._none2default(policy_learning_starts,self._policy_learning_starts)
        self._policy_retrain_period = self._none2default(policy_retrain_period,self._policy_retrain_period)
        self._policy_retrain_grad_steps = self._none2default(policy_retrain_grad_steps,self._policy_retrain_grad_steps)
        self._policy_train_period = self._none2default(policy_train_period,self._policy_train_period)
        self._policy_grad_steps = self._none2default(policy_gradient_steps,self._policy_grad_steps)
        self._le_tau = self._none2default(feature_extractor_tau,self._le_tau)
        self._le_reset_on_retrain = self._none2default(reset_on_retrain,self._le_reset_on_retrain)
        self._random_steps = self._none2default(random_steps,self._random_steps)

        if self._random_steps is not None:
            self.learning_starts = self._random_steps
        else:
            self.learning_starts = min(policy_learning_starts, feature_extractor_learning_starts)

        if isinstance(self._policy_train_period,int):
            self._policy_train_period = (self._policy_train_period,"step")
        if self._le_train_period[1] == "episode":
            if self._le_train_period[0] != 1: raise AttributeError("Episode-based feature_extractor_train_period only supports 1-episode rollouts")
        elif self._le_train_period[1] != "step":
            raise AttributeError(f"Unexpected feature_extractor_train_period {self._le_train_period}")

        # set train_freq to the minimum between _le_train_period and _policy_train_period
        if self._policy_train_period[1]=="episode" and self._le_train_period[1] == "step":
            self.train_freq = self._le_train_period
        elif self._policy_train_period[1]=="step" and self._le_train_period[1] == "episode":
            self.train_freq = self._policy_train_period
        elif self._policy_train_period[1]==self._le_train_period[1]:
            if self._policy_train_period[0]<self._le_train_period[0]:
                self.train_freq = self._policy_train_period
            else:
                self.train_freq = tuple(self._le_train_period)

        self._convert_train_freq()





    def _collect(self, callback, asynchronous : bool = False):
        if asynchronous:
            t0_dupl = time.monotonic()
            blobs = (io.BytesIO(),io.BytesIO(),io.BytesIO(),io.BytesIO())
            self.save(blobs)
            for b in blobs:
                b.seek(0)
            self._collector_model = AutoencodingSAC2.load(blobs, env = self.env, custom_objects={"buffer_size":1000, "seed" : None},
                                                            fe_logfolder = self.get_latent_extractor().dbgOutFolder+"_collect") #TODO: choose a suitable buffer size depending on train_freq
            for b in blobs:
                b.close()
            self._duplication_duration = time.monotonic() - t0_dupl

            current_cuda_device = th.cuda.current_device()
            self._t0_collection = time.monotonic()
            self._tf_collection = 0

            def collect():
                th.cuda.set_device(current_cuda_device)
                # ggLog.info("collecting...")
                # nopCallback = CallbackList([])
                cb = OnStep_passthrough(callback)
                self._collector_model.set_env(self.env)
                self._collector_model._last_obs = self._last_obs
                self._collector_model._setup_learn(self._total_timesteps,
                                                self._eval_env,
                                                cb,
                                                self._eval_freq,
                                                self._n_eval_episodes,
                                                self._eval_log_path,
                                                False,
                                                self._tb_log_name)
                self._collector_model.start_time = self.start_time
                self._new_collected_rollout = self._collector_model.collect_rollouts(
                    self.env,
                    train_freq=self._collector_model.train_freq,
                    action_noise=self._collector_model.action_noise,
                    callback=cb,
                    learning_starts=self._random_steps,
                    replay_buffer=self._collector_model.replay_buffer,
                    log_interval=self._log_interval,
                )
                self._tf_collection = time.monotonic()
                self._collection_done.set()
                # dur_collection = tf_collection-t0_collection
                
            callback._on_rollout_start()
            self._collectionThread = threading.Thread(target=collect)
            self._collectionThread.start()
        else:
            self._t0_collection = time.monotonic()
            cb = OnStep_passthrough(callback)
            cb.init_callback(self)
            callback._on_rollout_start()
            self._new_collected_rollout = self.collect_rollouts(self.env,
                                            train_freq=self.train_freq,
                                            action_noise=self.action_noise,
                                            callback=cb,
                                            learning_starts=self._random_steps,
                                            replay_buffer=self.replay_buffer,
                                            log_interval=self._log_interval)
            self._tf_collection = time.monotonic()
            self._collection_done.set()
        #print(f"Collected {rollout.episode_timesteps} steps in {dur_collection}s ({rollout.episode_timesteps/(tf_collection-t0_collection)}fps)")

    def _wait_collection_end(self, callback, asynchronous : bool = False):
        if asynchronous:
            self._collectionThread.join()
            # ggLog.info(f"Got {collected_steps} transitions from collector thread")
            # prevSize = self.replay_buffer.size()
            self.replay_buffer.update(self._collector_model.replay_buffer)
            # sizeInc = self.replay_buffer.size() - prevSize
            # ggLog.info(f"Replay buffer grew of {sizeInc} to {self.replay_buffer.size()}")

            self.last_collected_rollout = self._new_collected_rollout
            self.num_timesteps = self._collector_model.num_timesteps
            self._episode_num = self._collector_model._episode_num
            self._last_obs = self._collector_model._last_obs
            self._predictDurationAverage = self._collector_model._predictDurationAverage
        else:
            self.last_collected_rollout = self._new_collected_rollout
        self._collection_duration = self._tf_collection-self._t0_collection
        self._last_collected_steps = self.last_collected_rollout.episode_timesteps
        self._collection_done.clear()
        callback._on_rollout_end()





















    def _should_retrain_fe(self):
        # If pretraining has to be done
        if self._le_pretrain_grad_steps > 0 and self._last_le_retraining < 0 and self.num_timesteps >= self._le_learning_starts:
            return True
        # If peridic retraining is enabled and it's time to do it
        if self._le_retrain_period > 0 and (self.num_timesteps - self._last_le_retraining) > self._le_retrain_period and self.num_timesteps >= self._le_learning_starts:
            return True
        return False

    def _should_retrain_policy(self):
        # If peridic retraining is enabled and it's time to do it
        if self._policy_retrain_period > 0 and (self.num_timesteps - self._last_policy_retraining) > self._policy_retrain_period:
            return True
        return False

    # def _has_video_saver(self,callback):
    #     if isinstance(callback, AutoencodingSAC_VideoSaver):
    #         return True
    #     if isinstance(callback, CallbackList):
    #         callback = callback.callbacks
    #     if isinstance(callback, Iterable):
    #         for cb in callback:
    #             if self._has_video_saver(cb):
    #                 return True
    #     return False

    @staticmethod
    def comp_grad_steps(num_timesteps, train_period : Tuple[int,str], grad_steps : int, collected_episodes : int, last_step_trained : int, last_ep_trained : int, last_ep_duration):
        # ggLog.info(f"0: last_ep_duration = {last_ep_duration}")
        # ggLog.info(f"0: train_period = {train_period}")
        if train_period[1] == "step":
            if num_timesteps - last_step_trained >= train_period[0]:
                grad_steps_to_do = grad_steps
            else:
                grad_steps_to_do = 0
        elif train_period[1] == "episode":
            if collected_episodes - last_ep_trained >= train_period[0]:
                grad_steps_to_do = grad_steps
                if grad_steps_to_do==-1:
                    grad_steps_to_do = num_timesteps - last_step_trained
            else:
                grad_steps_to_do = 0
        else:
            raise AttributeError(f"Invalid training period {train_period}")
        return grad_steps_to_do

    def learn(  self,
                total_timesteps: int,
                callback: MaybeCallback = None,
                log_interval: int = 4,
                eval_env: Optional[GymEnv] = None,
                eval_freq: int = -1,
                n_eval_episodes: int = 5,
                tb_log_name: str = "run",
                eval_log_path: Optional[str] = None,
                reset_num_timesteps: bool = True) -> "OffPolicyAlgorithm":
        """[summary]

        Returns
        -------
        [type]
            [description]

        Raises
        ------
        AttributeError
            [description]
        """

        self._total_timesteps = total_timesteps
        self._eval_env = eval_env
        self._eval_freq = eval_freq
        self._n_eval_episodes = n_eval_episodes
        self._eval_log_path = eval_log_path
        self._reset_num_timesteps = reset_num_timesteps
        self._tb_log_name = tb_log_name
        self._log_interval = log_interval
        if reset_num_timesteps:
            self._collected_eps_count = 0
            self._last_le_retraining = float("-inf")
        if isinstance(self._le_train_period, str) and self._le_train_period == "episode":
            self._le_train_period = (1,"episode")

        total_timesteps, callback = self._setup_learn(
            total_timesteps, eval_env, callback, eval_freq, n_eval_episodes, eval_log_path, reset_num_timesteps, tb_log_name
        )

        callback.on_training_start(locals(), globals())


        self.last_collected_rollout = None
        self._new_collected_rollout = None
        self._collection_done = threading.Event()
        self._collection_done.clear()
        self._duplication_duration = 0
        self._timelastlog = time.monotonic()
        self._last_ep_trained_ae = -1
        self._last_ep_trained_policy = -1


        while self.num_timesteps < total_timesteps:

            # ggLog.info(f"Learning, self.num_timesteps = {self.num_timesteps} ")
            # ggLog.info(f"num_timesteps = {self.num_timesteps}, total_timesteps = {total_timesteps}")
            t0 = time.monotonic()

            le_or_sac_learn_starts = min(self._policy_learning_starts, self._le_learning_starts)

            # Non need to parallelize if not doing training yet, network duplication takes time
            parallelize_collection = self._parallelize_experience_collection and self.num_timesteps >= le_or_sac_learn_starts

            self._collect(callback, asynchronous=parallelize_collection)
            if not parallelize_collection:
                self._wait_collection_end(callback, asynchronous=parallelize_collection)

            dur_aeTrain = 0
            dur_policyTrain = 0
            training_iterations = 0
            ae_grad_steps_done = 0
            policy_grad_steps_done = 0

            if self.num_timesteps > 0 and self.num_timesteps >= le_or_sac_learn_starts and self.replay_buffer.size()>=min(self.batch_size, self._le_batch_size):

                #Perform autoencoder re-training (reset weights and do a few epochs)
                if self._should_retrain_fe():
                    ggLog.info("Retraining feature extractor...")
                    its_pretrain = self._last_le_retraining < 0
                    if its_pretrain:
                        grad_steps_per_epoch = self._le_pretrain_grad_steps 
                    else:
                        grad_steps_per_epoch = self._le_retrain_grad_steps
                    self._last_le_retraining = self.num_timesteps - (self.num_timesteps % self._le_retrain_period)
                    if its_pretrain:
                        self._last_policy_retraining = self._last_le_retraining
                    autoencoding_rl.train_helpers.retrainFeatureExtractor(  epochs = self._le_retrain_epochs,
                                                                            grad_steps_per_epoch = grad_steps_per_epoch,
                                                                            log_id = f"retrain{self._last_le_retraining}_",
                                                                            latent_extractor=self.get_latent_extractor(),
                                                                            batch_size = self._le_batch_size,
                                                                            replayBuffer = self.replay_buffer,
                                                                            env = self._vec_normalize_env,
                                                                            episode_count = self._collected_eps_count,
                                                                            validation_batch_size=self._validation_batch_size,
                                                                            reset = self._le_reset_on_retrain,
                                                                            evaluate = False)
                    ggLog.info("Feature extractor retraining completed.")

                #Perform policy retraining (reset policy and retrain)
                if self._should_retrain_policy():
                    raise NotImplementedError()
                    ggLog.info("Retraining policy...")
                    self._resetPolicy()
                    self._last_policy_retraining = self.num_timesteps - (self.num_timesteps % self._policy_retrain_period)
                    steps_per_iteration = 10
                    iterations = int(self._policy_retrain_grad_steps/steps_per_iteration)
                    for i in tqdm(range(iterations)):
                        self.train(batch_size=self.batch_size, gradient_steps=steps_per_iteration)
                    self.train(batch_size=self.batch_size, gradient_steps=self._policy_retrain_grad_steps-iterations*steps_per_iteration)                    
                    ggLog.info("Policy retraining completed.")

                # Train feature extractor and policy until needed
                while training_iterations == 0 or (self._automatic_grad_steps and not self._collection_done.is_set()):
                    last_ep_duration = self.last_collected_rollout.episode_timesteps if self.last_collected_rollout is not None else 0
                    # --------- Latent Extractor training (just a few gradient steps) ---------
                    t0_aeTrain = time.monotonic()
                    autoenc_grad_steps = 0
                    if self.num_timesteps > self._le_learning_starts and self._le_grad_steps>0:
                        autoenc_grad_steps = self.comp_grad_steps(self.num_timesteps, self._le_train_period, self._le_grad_steps,
                                                                    self._collected_eps_count, self._last_step_trained_ae, self._last_ep_trained_ae, last_ep_duration)                        
                        autoencoding_rl.train_helpers.trainFeatureExtractor(grad_steps = autoenc_grad_steps,
                                                                            latent_extractor=self.get_latent_extractor(),
                                                                            batch_size = self._le_batch_size,
                                                                            replayBuffer = self.replay_buffer,
                                                                            env = self._vec_normalize_env,
                                                                            validation_batch_size = self._validation_batch_size,
                                                                            episode_count = self._collected_eps_count)
                    dur_aeTrain += time.monotonic() - t0_aeTrain

                    # --------- Policy Training ---------
                    t0_policyTrain = time.monotonic()
                    policy_grad_steps = 0
                    if self.num_timesteps > self._policy_learning_starts:
                        policy_grad_steps = self.comp_grad_steps(self.num_timesteps, self._policy_train_period, self._policy_grad_steps, self._collected_eps_count,
                                                                 self._last_step_trained_policy, self._last_ep_trained_policy, last_ep_duration)
                        if policy_grad_steps > 0:
                            self.train(batch_size=self.batch_size, gradient_steps=policy_grad_steps)
                    dur_policyTrain += time.monotonic()-t0_policyTrain
                    # ggLog.info(f"SAC training: performed {gradient_steps} grad steps with batch_size {self.batch_size} in {dur_policyTrain}s")
                    training_iterations+=1
                    ae_grad_steps_done += autoenc_grad_steps
                    policy_grad_steps_done += policy_grad_steps

                if autoenc_grad_steps>0:
                    self._last_ep_trained_ae = self._collected_eps_count
                    self._last_step_trained_ae = self.num_timesteps
                if policy_grad_steps>0:
                    self._last_ep_trained_policy = self._collected_eps_count
                    self._last_step_trained_policy = self.num_timesteps

            if parallelize_collection:
                self._wait_collection_end(callback, asynchronous=parallelize_collection)
            newly_collected_steps = self.last_collected_rollout.episode_timesteps
            self._collected_eps_count += self.last_collected_rollout.n_episodes

            ttot = float(time.monotonic()-t0)
            self._newly_collected_steps_sincelog += newly_collected_steps
            self._ttot_sincelog = time.monotonic() - self._timelastlog
            self._dur_policyTrain_sincelog += dur_policyTrain
            self._policy_grad_steps_done_sincelog += policy_grad_steps_done
            self._dur_aeTrain_sincelog += dur_aeTrain
            self._ae_grad_steps_done_sincelog += ae_grad_steps_done
            ttrain_on_ttot = (self._dur_policyTrain_sincelog+self._dur_aeTrain_sincelog)/self._ttot_sincelog
            if self.last_collected_rollout.n_episodes > 0:
                ggLog.info( f" \033[1mASAC\033[0m: {self._newly_collected_steps_sincelog} steps in {self._ttot_sincelog:.2f}s, fps = {self._newly_collected_steps_sincelog/self._ttot_sincelog:.2f}."+
                            f" sac_train={self._dur_policyTrain_sincelog:.2f}s ({self._policy_grad_steps_done_sincelog} steps),"+
                            f" ae_train={self._dur_aeTrain_sincelog:.2f} ({self._ae_grad_steps_done_sincelog} steps),"+
                            f" collection={self._collection_duration:.2f}s,"+
                            f" predict_avg_duration={self._predictDurationAverage.getAverage():.9f},"+
                            f" duplication={self._duplication_duration:.2f}"+
                            f" ttr/tot={ttrain_on_ttot:.2f}")
                self._newly_collected_steps_sincelog = 0
                self._dur_policyTrain_sincelog = 0
                self._policy_grad_steps_done_sincelog = 0
                self._dur_aeTrain_sincelog = 0
                self._ae_grad_steps_done_sincelog = 0
                self._timelastlog = time.monotonic()
            
            
            if self.last_collected_rollout.continue_training is False:
                break

            lr_gym.utils.utils.haltOnSigintReceived()


        callback.on_training_end()
        return self


    def offlineTrain(self):
        # total_timesteps = 1000000000
        # callback = None
        # eval_env = None
        # eval_freq = -1
        # n_eval_episodes = 5
        tb_log_name = "run"
        # eval_log_path = None
        reset_num_timesteps = False

        # total_timesteps, callback = self._setup_learn(
        #     total_timesteps, eval_env, callback, eval_freq, n_eval_episodes, eval_log_path, reset_num_timesteps, tb_log_name
        # )
        self._logger = stable_baselines3.common.utils.configure_logger(self.verbose, self.tensorboard_log, tb_log_name, reset_num_timesteps)

        t0_aeTrain = time.monotonic()
        autoencoding_rl.train_helpers.trainFeatureExtractor(grad_steps = self._le_grad_steps,
                                                            latent_extractor=self.get_latent_extractor(),
                                                            batch_size = self._le_batch_size,
                                                            replayBuffer = self.replay_buffer,
                                                            env = self._vec_normalize_env,
                                                            validation_batch_size = self._validation_batch_size)       
        dur_aeTrain = time.monotonic() - t0_aeTrain


        t0_policyTrain = time.monotonic()
        self.train(batch_size=self.batch_size, gradient_steps=self.gradient_steps)
        dur_policyTrain = time.monotonic()-t0_policyTrain
        ggLog.info(f"ASAC offline train: sac_train={dur_policyTrain:.2f}s ({self.gradient_steps} steps, {self.gradient_steps/dur_policyTrain:.2f} fps), ae_train={dur_aeTrain:.2f} ({self._le_grad_steps} steps, {self._le_grad_steps/dur_aeTrain:.2f} fps)")


    def _resetPolicy(self):
        fe_blob = io.BytesIO()
        th.save(self.get_latent_extractor().state_dict(), fe_blob)
        fe_blob.seek(0)
        self.policy._build(lr_schedule=self.lr_schedule)
        self.get_latent_extractor().load_state_dict(th.load(fe_blob))
        fe_blob.close()




















    def predict(
        self,
        observation: np.ndarray,
        state: Optional[np.ndarray] = None,
        mask: Optional[np.ndarray] = None,
        episode_start: Optional[np.ndarray] = None,
        deterministic: bool = False
        ) -> Tuple[np.ndarray, Optional[np.ndarray]]:
        """
        Get the model's action(s) from an observation
        :param observation: the input observation
        :param state: The last states (can be None, used in recurrent policies)
        :param mask: The last masks (can be None, used in recurrent policies)
        :param deterministic: Whether or not to return deterministic actions.
        :return: the model's action and the next state
            (used in recurrent policies)
        """
        t0 = time.monotonic()

        # ggLog.info("predicting")
        # ggLog.info("self._keep_s2s_imgs = {self._keep_s2s_imgs}")
        action, state = self.policy.predict(observation, state, mask, deterministic)
        # ggLog.info(f"thimg_side2side.size() = {thimg_side2side.size()}")
        dbg_img.helper.publishDbgImg("img_s2s",
                                        img_callback=lambda:autoencoding_rl.utils.build_s2s_img(observation,
                                                                                                action,
                                                                                                self.get_latent_extractor(),
                                                                                                float("nan"))[0])
        # code.interact(local=locals())
        # dbg_img.helper.publishDbgImg("img_encdec", img_callback=lambda:tensorToHumanCvImageRgb(imgFromObs(self.get_latent_extractor().decode(encodedImg_batch, thAct_batch.to(encodedImg_batch.device)))))

            
        # ggLog.info("Predict said "+str(ret))
        tf = time.monotonic()
        self._predictDurationAverage.addValue(newValue=tf-t0)
        # ggLog.info(f"Predict took {tf-t0}s")
        return action, state





















    @classmethod
    def load(
        cls,
        path: Union[str, pathlib.Path, Tuple[io.BufferedIOBase,io.BufferedIOBase,io.BufferedIOBase,io.BufferedIOBase]],
        env: Optional[GymEnv] = None,
        device: Union[th.device, str] = "auto",
        custom_objects: Optional[Dict[str, Any]] = None,
        fe_logfolder : str = None,
        load_rng_state : bool = False,
        **kwargs
    ) -> "BaseAlgorithm":
        """
        Load the model from a zip-file
        :param path: path to the file (or a file-like) where to
            load the agent from
        :param env: the new environment to run the loaded model on
            (can be None if you only need prediction from a trained model) has priority over any saved environment
        :param device: Device on which the code should run.
        :param custom_objects: Dictionary of objects to replace
            upon loading. If a variable is present in this dictionary as a
            key, it will not be deserialized and the corresponding item
            will be used instead. Similar to custom_objects in
            ``keras.models.load_model``. Useful when you have an object in
            file that can not be deserialized.
        :param kwargs: extra arguments to change the model when loading
        """
        if fe_logfolder is None:
            raise AttributeError("You must specify fe_logFolder")
           
        if isinstance(path,tuple):
            if len(path)!=4:
                raise AttributeError("If you want to load from a tuple of BufferedIOBase you must pass a 4-tuple, as (sac_in,feature_in,th_rng_state_in,np_rng_state_in)")
            sac_in = path[0]
            fe_in = path[1]
            th_rng_in = path[2]
            np_rng_in = path[3]
            # ggLog.info(f"fe_in is {fe_in.getbuffer().nbytes}b, sac_in is {sac_in.getbuffer().nbytes}b, tot = {fe_in.getbuffer().nbytes + sac_in.getbuffer().nbytes}b = {(fe_in.getbuffer().nbytes + sac_in.getbuffer().nbytes)/1024/1024}MB")
        else:
            sac_in = path
            fe_in = os.path.dirname(path)+"/features_extractor_"+os.path.basename(path)+".zip"
            th_rng_in = os.path.dirname(path)+"/rng_state_"+os.path.basename(path)+".zip"
            np_rng_in = os.path.dirname(path)+"/np_rng_state_"+os.path.basename(path)+".zip"

        if custom_objects is None:
            custom_objects = {}
        if "seed" not in custom_objects:
            custom_objects["seed"] = None
        model = super().load(sac_in,env,device,custom_objects,**kwargs)
        model.get_latent_extractor().load_state_dict(th.load(fe_in))
        model.get_latent_extractor().dbgOutFolder = fe_logfolder
        # model.get_latent_extractor().setupDbgLogs()
        if load_rng_state:
            if isinstance(th_rng_in,io.BufferedIOBase) or os.path.exists(th_rng_in): # TODO: remove in future
                th.random.set_rng_state(th.load(th_rng_in))
            else:
                ggLog.warn(f"AutoencodingSAC: No th rng state file found at {th_rng_in}, will not load the random number generator state.")
        
        if load_rng_state:
            if isinstance(np_rng_in,io.BufferedIOBase) or os.path.exists(np_rng_in): # TODO: remove in future
                if isinstance(np_rng_in,str):
                    with open(np_rng_in,"rb") as np_rng_in_f:
                        np.random.set_state(pickle.load(np_rng_in_f))
                else:
                    np.random.set_state(pickle.load(np_rng_in))
            else:
                ggLog.warn(f"AutoencodingSAC: No np rng state file found at {np_rng_in}, will not load the random number generator state.")
        
        return model


    def save(self,  path: Union[str, pathlib.Path, Tuple[io.BufferedIOBase,io.BufferedIOBase,io.BufferedIOBase,io.BufferedIOBase]],
                    exclude: Optional[Iterable[str]] = None,
                    include: Optional[Iterable[str]] = None) -> None:
        if isinstance(path,tuple):
            if len(path)!=4:
                raise AttributeError("If you want to save to a tuple of BufferedIOBase you must pass a 4-tuple, as (sac_out,feature_extractor_out,th_rng_state_out,np_rng_state_out)")
            sac_out = path[0]
            fe_out = path[1]
            th_rng_out = path[2]
            np_rng_out = path[3]
        else:
            sac_out = path
            fe_out = os.path.dirname(path)+"/features_extractor_"+os.path.basename(path)+".zip"
            th_rng_out = os.path.dirname(path)+"/rng_state_"+os.path.basename(path)+".zip"
            np_rng_out = os.path.dirname(path)+"/np_rng_state_"+os.path.basename(path)+".zip"

        super().save(sac_out, exclude, include)
        th.save(self.get_latent_extractor().state_dict(), fe_out)
        th.save(th.random.get_rng_state(),th_rng_out)
        if isinstance(np_rng_out,str):
            with open(np_rng_out,"wb") as np_rng_out_f:
                pickle.dump(np.random.get_state(),np_rng_out_f)
        else:
            pickle.dump(np.random.get_state(),np_rng_out)

    
    def _excluded_save_params(self) -> List[str]:
        return super()._excluded_save_params() + ["policy.actor.features_extractor",
                                                    "policy.critic.features_extractor",
                                                    "_collectionThread",
                                                    "_collection_done",
                                                    "_collector_model",
                                                    "_eval_env",
                                                    "replay_buffer_kwargs.env"]



















    def _setup_model(self) -> None:

        if isinstance(self.learning_rate, (float, int)):
            self.learning_rate = LearningRateConf(None, self.learning_rate)

        if isinstance(self.learning_rate, LearningRateConf):
            self._lr_conf = self.learning_rate
            self.learning_rate = self._lr_conf.initial_lr
        else:
            raise RuntimeError("Stable-baselines-like schedulers are not supported")

        super()._setup_model()

        # # As suggested by "What Matters In On-Policy Reinforcement Learning?A Large-Scale Empirical Study", section 3.2
        # # Makes initial actions centered on zero
        # with th.no_grad():
        #     self.policy.actor.mu.weight.mul_(0.01)

        # learning rate schedulers
        self._actor_lr_scheduler = self._lr_conf.build_scheduler(self.actor.optimizer)
        self._critic_lr_scheduler = self._lr_conf.build_scheduler(self.critic.optimizer)
        self._ent_coef_lr_scheduler = self._lr_conf.build_scheduler(self.ent_coef_optimizer)





    # def train(self, gradient_steps: int, batch_size: int = 64) -> None:
    #         if self.replay_buffer.size() < batch_size:
    #             ggLog.error("Replay buffer does not have enough data, skipping policy train step")
    #             return

    #         self._target_entropy_th = th.tensor(self.target_entropy, device = self.device, dtype = th.float32)
    #         self._gamma_th = th.tensor(self.gamma, device = self.device, dtype = th.float32)
    #         # Switch to train mode (this affects batch norm / dropout)
    #         self.policy.set_training_mode(True)
    #         # Update optimizers learning rate


    #         # ent_coef_losses, ent_coefs = [], []
    #         # actor_losses, critic_losses = [], []

    #         t = [-1]*15

    #         # th.cuda.synchronize()
    #         # t0 = time.monotonic()

    #         stop = False
    #         try:
    #             dataqueue = queue.Queue(maxsize=min(gradient_steps,10)) #Maximum 10 to avoid CUDA out of memory
    #             # ggLog.info("Created queue")
    #             def loadData():
    #                 # ggLog.info("Started data load")
    #                 for i in range(gradient_steps):
    #                     replay_data = self.replay_buffer.sample(batch_size, env=self._vec_normalize_env)
    #                     loadedBatch = False
    #                     while not (stop or loadedBatch):
    #                         try:
    #                             dataqueue.put(replay_data, timeout=5.0)
    #                             loadedBatch = True
    #                         except Full:
    #                             pass
    #                     if stop:
    #                         break
    #                     # ggLog.info(f"Loaded step {i}. queue size = {dataqueue.qsize()}") # \t sampling = {tdc01-tdc0}, totransition = {tdc1-tdc01}, todevice = {tdc15-tdc1}")
    #             dataloader = threading.Thread(target=loadData)
    #             dataloader.start()




    #             for gradient_step in range(gradient_steps):

    #                 # t[0] += takeTime()
    #                 # Sample replay buffer
    #                 replay_data = dataqueue.get()

    #                 # t[1] += takeTime()

    #                 with th.no_grad():
    #                     # Select action according to policy
    #                     next_actions, next_log_prob = self.actor.action_log_prob(replay_data.next_observations)
    #                     next_q_values= self.critic_target(replay_data.next_observations, next_actions)


    #                 # t[2] += takeTime()

    #                 # To exploit decoupledextractor caching first do all the computations on next_observations
    #                 # then do all the ones on observations

    #                 # Action by the current actor for the sampled state
    #                 actions_pi, log_prob = self.actor.action_log_prob(replay_data.observations)
    #                 log_prob = log_prob.reshape(-1, 1)


    #                 # t[3] += takeTime()

    #                 # Important: detach the variable from the graph
    #                 # so we don't change it with other losses
    #                 # see https://github.com/rail-berkeley/softlearning/issues/60
    #                 ent_coef = th.exp(self.log_ent_coef.detach())
    #                 ent_coef_loss = ent_coef_loss_func(self.log_ent_coef, log_prob, self._target_entropy_th)
    #                 # ent_coef_losses.append(ent_coef_loss.item())

    #                 # ent_coefs.append(ent_coef.item())

    #                 # Optimize entropy coefficient, also called
    #                 # entropy temperature or alpha in the paper
    #                 self.ent_coef_optimizer.zero_grad(set_to_none=True)
    #                 ent_coef_loss.backward()
    #                 self.ent_coef_optimizer.step()
    #                 self._ent_coef_lr_scheduler.step()

    #                 # t[4] += takeTime()

    #                 with th.no_grad():
    #                     target_q_values = compute_target_q( next_q_values = next_q_values,
    #                                                         rewards = replay_data.rewards,
    #                                                         next_log_prob = next_log_prob,
    #                                                         ent_coef = ent_coef,
    #                                                         gamma = self._gamma_th)


    #                 # t[5] += takeTime()

    #                 # Get current Q-values estimates for each critic network
    #                 # using action from the replay buffer
    #                 current_q_values = self.critic(replay_data.observations, replay_data.actions)


    #                 # t[6] += takeTime()

    #                 # Compute critic loss
    #                 # ggLog.info(f"type(target_q_values) = {type(target_q_values)}, type(current_q_values) = {type(current_q_values)}")
    #                 # ggLog.info(f"target_q_values.size() = {target_q_values.size()}, len(current_q_values) = {len(current_q_values)}")
    #                 critic_loss = critic_loss_func(target_q_values,list(current_q_values))
    #                 # critic_losses.append(critic_loss.item())

    #                 # Optimize the critic
    #                 # t[7] += takeTime()

    #                 self.critic.optimizer.zero_grad(set_to_none=True)
    #                 critic_loss.backward()

    #                 # t[8] += takeTime()


    #                 self.critic.optimizer.step()

    #                 # t[9] += takeTime()

    #                 self._critic_lr_scheduler.step()


    #                 # t[10] += takeTime()
    #                 # Compute actor loss
    #                 # Alternative: actor_loss = th.mean(log_prob - qf1_pi)
    #                 # Mean over all critic networks
    #                 actor_loss = actor_loss_func(self.critic.forward(replay_data.observations, actions_pi), ent_coef,log_prob)
    #                 # actor_losses.append(actor_loss.item())

    #                 # Optimize the actor
    #                 # t[11] += takeTime()

    #                 self.actor.optimizer.zero_grad(set_to_none=True)
    #                 actor_loss.backward()

                    
    #                 # t[12] += takeTime()

    #                 # th.cuda.synchronize()
    #                 # t0sa = time.monotonic()

    #                 self.actor.optimizer.step()

    #                 # th.cuda.synchronize()
    #                 # actor_step_t += time.monotonic() - t0sa

    #                 self._actor_lr_scheduler.step()


    #                 # t[13] += takeTime()
    #                 # Update target networks
    #                 polyak_update(self.critic.parameters(), self.critic_target.parameters(), self.tau)
    #                 # Latent extractor is hidden from the policy parameters, target laten_Extractor must be updated separately
    #                 if self._le_tau != 1.0:
    #                     polyak_update(  self.get_latent_extractor().parameters(),
    #                                     self.critic_target.features_extractor.latent_extractor[0].parameters(), 
    #                                     self._le_tau)
    #                     # self.critic_target.features_extractor.latent_extractor[0] =  self.critic.features_extractor.latent_extractor[0]
    #                     # self.critic_target.features_extractor.latent_extractor[0].load_state_dict(self.critic.features_extractor.latent_extractor[0].state_dict())

    #                     # directly copy in the target anything that is not in parameters()
    #                     nonparam_states = self.critic.features_extractor.latent_extractor[0].state_dict()
    #                     for k in dict(self.critic.features_extractor.latent_extractor[0].named_parameters()).keys():
    #                         nonparam_states.pop(k, None)
    #                     self.critic_target.features_extractor.latent_extractor[0].load_state_dict(nonparam_states, strict=False)



    #                 # t[14] += takeTime()

    #                 # input("Press enter")

    #             self._n_updates += gradient_steps

    #             self.logger.record("train/n_updates", self._n_updates, exclude="tensorboard")
    #             # self.logger.record("train/ent_coef", np.mean(ent_coefs))
    #             # self.logger.record("train/actor_loss", np.mean(actor_losses))
    #             # self.logger.record("train/critic_loss", np.mean(critic_losses))
    #             # if len(ent_coef_losses) > 0:
    #             #     self.logger.record("train/ent_coef_loss", np.mean(ent_coef_losses))
    #         finally:
    #             stop = True
    #             dataloader.join()

    #         # th.cuda.synchronize()
    #         # tf = time.monotonic()
    #         # d = [None]*(len(t)-1)
    #         # for i in range(len(t)-1):
    #         #     d[i] = t[i+1] - t[i]
    #         # ggLog.info(f"SAC: {gradient_steps} iterations in {tf-t0:.4f}s ({gradient_steps/(tf-t0)} fps) (d = {d} sum(d)={sum(d)})")


    def get_latent_extractor(self):
        return self.policy.actor.features_extractor.latent_extractor[0]