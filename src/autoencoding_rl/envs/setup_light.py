#!/usr/bin/env python3


import rospy
from controller_manager_msgs.srv import SwitchController
from controller_manager_msgs.srv import ListControllers
from controller_manager_msgs.srv import SwitchControllerRequest
from rospy.exceptions import ROSException
import time

def buildServiceProxy(serviceName, msgType):
    connected = False
    hadFail = False
    while not connected:
        try:
            rospy.wait_for_service(serviceName, timeout=10.0)
            connected = True
            if hadFail:
                rospy.logwarn("Successfully connected to service '"+serviceName+"'.")
        except ROSException as e:
            rospy.logwarn("Failed to wait for service '"+serviceName+"', will now retry...")
            rospy.sleep(1.0)
            hadFail = True
    return rospy.ServiceProxy(serviceName, msgType)

def parse_lists_string(list_string):
    list_string = list_string.strip("[ ]")
    list_string = list_string.split(", ")
    return [ cn.strip("' ") for cn in list_string]

def parse_floats_string(float_list_str):
    float_list_str = float_list_str.strip("[ ]")
    float_list_str = float_list_str.split(", ")
    return [ float(fstr) for fstr in float_list_str]
    

rospy.init_node('setup_light', anonymous=True)
light_name = rospy.get_param("~light_name")
direction = parse_floats_string(str(rospy.get_param("~direction")))
cast_shadows = rospy.get_param("~cast_shadows")
diffuse = rospy.get_param("~diffuse")
specular = parse_floats_string(str(rospy.get_param("~specular")))
attenuation_constant = rospy.get_param("~attenuation_constant")
attenuation_linear = rospy.get_param("~attenuation_linear")
attenuation_quadratic = rospy.get_param("~attenuation_quadratic")
orientation = rospy.get_param("~orientation")

switchController_service = buildServiceProxy("/controller_manager/switch_controller", SwitchController)
listControllers_service = buildServiceProxy("/controller_manager/list_controllers", ListControllers)

waitForControllersLoad(list_string, listControllers_service)
rospy.loginfo("All controllers available")

request = SwitchControllerRequest()
request.start_controllers = list_string
request.stop_controllers = []
request.strictness = SwitchControllerRequest.STRICT
request.start_asap = False
request.timeout = 0.0
rospy.logdebug(str(request))
response = switchController_service(request)
if response.ok:
    rospy.loginfo("All Controllers started successfully")
else:
    rospy.logerror("Failed to start controllers")
