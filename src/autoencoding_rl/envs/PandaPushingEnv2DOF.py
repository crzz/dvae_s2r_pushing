#!/usr/bin/env python3
"""This file implements PandaMoveitReachingEnv."""






from  enum import IntEnum
from autoencoding_rl.utils import tensorToHumanCvImageRgb
from lr_gym.envControllers.MoveitGazeboController import MoveitGazeboController
from lr_gym.envControllers.MoveitRosController import MoveFailError
from lr_gym.envControllers.SimulatedEnvController import SimulatedEnvController
from lr_gym.envs.ControlledEnv import ControlledEnv
from lr_gym.utils.dbg import dbg_img
from lr_gym.utils.utils import JointState, LinkState
from lr_gym.utils.utils import Pose
from lr_gym_utils.play_joint_trajectory import TrajMoveFailError
from nptyping import NDArray
from os import error
from signal import raise_signal
from site import USER_BASE
from typing import Callable, Tuple, List, Dict, Union, Any
import autoencoding_rl.utils
import cv2
import cv2.xphoto
import csv
import gazebo_msgs.srv
import geometry_msgs
import gym
import lr_gym
import lr_gym.utils.beep
import lr_gym.utils.dbg.ggLog as ggLog
import lr_gym.utils.gazebo_models_manager as gazebo_models_manager
import lr_gym_utils.msg
import lr_gym_utils.play_joint_trajectory
import lr_gym_utils.ros_launch_utils
import lr_gym_utils.srv
import math
import numpy as np
import os
import pickle
import quaternion
import quaternionic
import rospkg
import sensor_msgs
import std_msgs
import time
import traceback

Action = NDArray[(2,), np.float32]
Observation = NDArray[(15,), np.float32]
State = List[Union[NDArray[(15+2,), np.float32],sensor_msgs.msg.Image]] #Observation plus 2D cube position

class PandaPushingEnv2DOF(ControlledEnv):
    """This class represents and environment in which a Panda arm is controlled with Moveit to reach a goal pose.

    As moveit_commander is not working with python3 this environment relies on an intermediate ROS node for sending moveit commands.
    """

    action_space_high = np.array([  1, #d_x
                                    1, #d_y
                                    ])
    action_space = gym.spaces.Box(-action_space_high,action_space_high) # 3D translation vector, and grip width
    
    metadata = {'render.modes': ['rgb_array']}

    VEC_STATE_IDX = IntEnum("VEC_STATE_IDX", [
        "TIP_X",
        "TIP_Y",
        "TIP_Z",
        "TIP_ROLL",
        "TIP_PITCH",
        "TIP_YAW",
        "JOINT1_POS",
        "JOINT2_POS",
        "JOINT3_POS",
        "JOINT4_POS",
        "JOINT5_POS",
        "JOINT6_POS",
        "JOINT7_POS",
        "ACTION_FAILS",
        "CUBE_X",
        "CUBE_Y",
        "CUBE_YAW",
        "TIME",
        "GOAL_X",
        "GOAL_Y",
        "MAX_STEPS",
        "GOAL_TOLERANCE",
        "TERMINATE_ON_SUCCESS"
    ], start=0)

    def __init__(   self,
                    goalPosition_xy : Union[Tuple[float,float], Callable[[],Tuple[float,float]]] = (0,0),
                    initialCubePosition_xy : Tuple[float,float] = (0,0),
                    maxStepsPerEpisode : int = 50,
                    operatingArea = np.array([[0.30, -0.225, 0], [0.75, 0.225, 0.5]]),
                    startSimulation : bool = True,
                    backend="gazebo",
                    environmentController = None,
                    real_robot_pc_ip : str = None,
                    wall_sim_speed : bool = True,
                    obs_only_img = False,
                    imgEncoding = "int",
                    camera_resolution = 64,
                    rgb = False,
                    seed = 0,
                    terminate_on_success : bool = True,
                    obs_only_vec = False,
                    log_folder = f"./PandaPushingEnv2DOF/{time.time()}",
                    cube_color = (0.175,0.175,0.175),
                    camera_offset_xyz = (0.0,0.0,0.0),
                    light_direction = [0.01,0.01,-1],
                    camera_offset_rpy = (0.0,0.0,0.0),
                    allow_successful_initial_cube_position = False,
                    prevent_ee_out = False,
                    variations_sampler : str = None):
        """Short summary.

        Parameters
        ----------
        goalPose : Tuple[float,float,float,float,float,float,float]
            end-effector pose to reach (x,y,z, qx,qy,qz,qw)
        maxStepsPerEpisode : int
            maximum number of frames per episode. The step() function will return
            done=True after being called this number of times
        render : bool
            Perform rendering at each timestep
            Disable this if you don't need the rendering
        goalTolerancePosition : float
            Position tolerance under which the goal is considered reached, in meters
        goalToleranceOrientation_rad : float
            Orientation tolerance under which the goal is considered reached, in radiants


        """


        self._getState_duration_avg =lr_gym.utils.utils.AverageKeeper(bufferSize = 100)
        self._getImg_duration_avg =lr_gym.utils.utils.AverageKeeper(bufferSize = 100)
        self._getVec_duration_avg =lr_gym.utils.utils.AverageKeeper(bufferSize = 100)
        self._dbgLogFileCsvWriter = None
        self._log_folder = log_folder
        os.makedirs(self._log_folder, exist_ok=True)

        self._real_robot_pc_ip = real_robot_pc_ip
        self._wall_sim_speed = wall_sim_speed
        self._obs_only_img = obs_only_img
        self._obs_only_vec = obs_only_vec
        self._seed = seed
        self._terminate_on_success = terminate_on_success
        self._allow_successful_initial_cube_position = allow_successful_initial_cube_position 


        self._obs_img_height = camera_resolution
        self._obs_img_width = camera_resolution
        self._imgObsEncoding = imgEncoding
        self._rgb = rgb

        self._cube_color = cube_color
        self._camera_offset_xyz = camera_offset_xyz
        self._camera_offset_rpy = camera_offset_rpy
        self._light_direction = light_direction

        self._panda_joints = [  ("panda","panda_joint1"),
                                ("panda","panda_joint2"),
                                ("panda","panda_joint3"),
                                ("panda","panda_joint4"),
                                ("panda","panda_joint5"),
                                ("panda","panda_joint6"),
                                ("panda","panda_joint7")]
        if variations_sampler is not None:
            if variations_sampler == "sim-minimal":
                colors = [  [0.9, 0.1, 0.1], #0 red
                            [0.1, 0.9, 0.1], #1 green
                            [0.1, 0.1, 0.9], #2 blue
                            [0.1, 0.9, 0.9], #3 cyan
                            [0.9, 0.9, 0.1], #4 yellow
                            [0.9, 0.1, 0.9], #5 magenta
                            [0.7, 0.7, 0.7], #6 grey70
                            [0.5, 0.5, 0.5], #7 grey50
                            [0.3, 0.3, 0.3], #8 grey30
                            [0.9, 0.9, 0.9]] #9 grey90
                self._cube_color = colors[seed%len(colors)]
            elif variations_sampler == "sim-small":
                self._cube_color = [0.9, 0.1, 0.1] # red
                cam_offsets = [[0.05, 0.00,  0.0],   [-0.05,  0.0, 0.0],  [0.0,  0.0, 0.05],  [0.0,   0.0, -0.05]]
                light_dirs =  [[0.50, 0.01, -1.0],   [-0.5,  0.01, -1],   [0.01, 0.5, -1.0],  [0.01, -0.5, -1.0]]
                self._camera_offset_xyz = cam_offsets[(seed*5)%4]
                self._light_direction = light_dirs[int((seed*5)/4)%4]
            elif variations_sampler == "sim-medium":
                self._cube_color = [0.9, 0.1, 0.1] # red
                cam_offsets =     [[0.05,  0.0,  0.15],[ 0.10,  0.0, -0.15],[0.05,   0.15,  0.0],[0.05,  -0.15,  0.0]]
                cam_rpy_offsets = [[0.00,  0.1,  0.00],[ 0.00, -0.2,  0.00],[0.00,   0.00,  0.4],[0.00,   0.00, -0.3]]
                light_dirs =  [[0.5,0.01,-1],[-0.5,0.01,-1],[0.01,0.5,-1],[0.01,-0.5,-1]]
                self._camera_offset_xyz = cam_offsets[(seed*5)%4]
                self._camera_offset_rpy = cam_rpy_offsets[(seed*5)%4]
                self._light_direction = light_dirs[int((seed*5)/4)%4]
            elif variations_sampler == "sim-large":
                self._cube_color = [0.9, 0.1, 0.1] # red
                cam_rpy_offsets = [[0.0, 0.0, 1.5707], [0.0, 0.0, -1.5707]]
                cam_offsets =     [[-0.45, 0.5, 0.0], [-0.5, -0.5, 0.0]]
                self._camera_offset_xyz = cam_offsets[seed%2]
                self._camera_offset_rpy = cam_rpy_offsets[seed%2]
                self._light_direction = [0.01, 0.5, -1.0]
            elif variations_sampler == "real-minimal":
                pass
            elif variations_sampler == "real-small":
                pass
            elif variations_sampler == "real-large":
                pass
            else:
                raise AttributeError(f"Unknown variations_sampler {variations_sampler}")
            ggLog.info(f"PandaPushingEnv2DOF: _seed = {self._seed}")
            ggLog.info(f"PandaPushingEnv2DOF: _cube_color = {self._cube_color}")
            ggLog.info(f"PandaPushingEnv2DOF: _camera_offset_xyz = {self._camera_offset_xyz}")
            ggLog.info(f"PandaPushingEnv2DOF: _camera_offset_rpy = {self._camera_offset_rpy}")
            ggLog.info(f"PandaPushingEnv2DOF: _light_direction = {self._light_direction}")

        self._camera_position = np.array([0.9935, -0.0225, 0.6]) + np.array(self._camera_offset_xyz)
        self._camera_orientation = np.array([0.0, 55/180*math.pi, math.pi]) + np.array(self._camera_offset_rpy)

        if obs_only_vec and obs_only_img:
            raise AttributeError("Cannot use both obs_only_vec and obs_only_img")

        if self._obs_only_vec:
            vec_pure_obs_space_high = np.array([np.finfo(np.float32).max, # end-effector x position
                                                np.finfo(np.float32).max, # end-effector y position
                                                np.finfo(np.float32).max, # cube yaw
                                                1.0])
            vec_goal_obs_space_high = np.array([np.finfo(np.float32).max, # goal x
                                                np.finfo(np.float32).max]) # goal y
            # obs is pure_obs + achieved_goal + desired_goal
            vec_obs_space_high = np.concatenate([vec_pure_obs_space_high,vec_goal_obs_space_high,vec_goal_obs_space_high])
        else:
            vec_pure_obs_space_high = np.array([    np.finfo(np.float32).max, # end-effector x position
                                                    np.finfo(np.float32).max, # end-effector y position
                                                    1.0])
            vec_goal_obs_space_high = np.array([np.finfo(np.float32).max, # goal x
                                                np.finfo(np.float32).max]) # goal y            
            # obs is pure_obs + desired_goal                                        
            vec_obs_space_high = np.concatenate([vec_pure_obs_space_high,vec_goal_obs_space_high])
        vec_obs_space = gym.spaces.Box(-vec_obs_space_high, vec_obs_space_high)
        vec_pure_obs_space = gym.spaces.Box(-vec_pure_obs_space_high, vec_pure_obs_space_high)




        self._img_channels = 3 if rgb else 1
        if imgEncoding == "float":
            img_observation_space = gym.spaces.Box(low=0, high=1,
                                                    shape=(self._img_channels,self._obs_img_height,self._obs_img_width),
                                                    dtype=np.float32)
        elif imgEncoding == "int":
            img_observation_space = gym.spaces.Box(low=0, high=255,
                                                    shape=(self._img_channels, self._obs_img_height, self._obs_img_width),
                                                    dtype=np.uint8)
        else:
            raise AttributeError(f"Unsupported imgEncoding '{imgEncoding}' requested, it can be either 'int' or 'float'")



        if self._obs_only_vec:
            self.observation_space = gym.spaces.Dict({  "vec" : vec_obs_space})     
            self.pure_observation_space = gym.spaces.Dict({  "vec" : vec_pure_obs_space})        
        elif self._obs_only_img:
            self.observation_space = gym.spaces.Dict({  "img" : img_observation_space})
        else:
            self.observation_space = gym.spaces.Dict({  "vec" : vec_obs_space,
                                                        "img" : img_observation_space})
            self.pure_observation_space = gym.spaces.Dict({ "vec" : vec_pure_obs_space,
                                                            "img" : img_observation_space})
        self.goal_space = gym.spaces.Box(-vec_goal_obs_space_high, vec_goal_obs_space_high)


        if environmentController is None:                
            self._environmentController = MoveitGazeboController(jointsOrder = [("panda","panda_joint1"),
                                                                                ("panda","panda_joint2"),
                                                                                ("panda","panda_joint3"),
                                                                                ("panda","panda_joint4"),
                                                                                ("panda","panda_joint5"),
                                                                                ("panda","panda_joint6"),
                                                                                ("panda","panda_joint7")],
                                                            endEffectorLink  = ("panda", "pushing_ee_tip"),
                                                            referenceFrame   = "world",
                                                            initialJointPose = {("panda","panda_joint1") : 0.05766107124508452,
                                                                                ("panda","panda_joint2") : -0.009062073602533426,
                                                                                ("panda","panda_joint3") : 0.057262226934222116,
                                                                                ("panda","panda_joint4") :-2.2185170019521547,
                                                                                ("panda","panda_joint5") : -0.0008399346203410206,
                                                                                ("panda","panda_joint6") : 2.208532241058938,
                                                                                ("panda","panda_joint7") : 0.7849774040414226})
        else:
            self._environmentController = environmentController

        super().__init__(   maxStepsPerEpisode = maxStepsPerEpisode,
                            startSimulation = startSimulation,
                            simulationBackend=backend,
                            environmentController=self._environmentController,
                            is_time_limited=True)

        
        self._camera_name = "front_camera/color/image_raw" #this should work also with the gazebo plugin because of how lr_realsense names the sensors in the urdf
        self._environmentController.setCamerasToObserve([self._camera_name])

        self._environmentController.setJointsToObserve( [("panda","panda_joint1"),
                                                        ("panda","panda_joint2"),
                                                        ("panda","panda_joint3"),
                                                        ("panda","panda_joint4"),
                                                        ("panda","panda_joint5"),
                                                        ("panda","panda_joint6"),
                                                        ("panda","panda_joint7"),
                                                        ("panda","panda_finger_joint1"),
                                                        ("panda","panda_finger_joint2")])


        self._environmentController.setLinksToObserve( [("panda","pushing_ee_tip"),("cube","cube")])

        if isinstance(goalPosition_xy, tuple):
            self._goal_sampler = lambda : goalPosition_xy
        else:
            self._goal_sampler = goalPosition_xy
        self._goalTolerancePosition = 0.05
        self._lastMoveFailed = False
        self._maxPositionChange = 0.025

        self._environmentController.startController()

        self._operatingArea = operatingArea #min xyz, max xyz


        self.success_ratio_buff_size = 50

        self._velocity_scaling = 0.9
        self._acceleration_scaling = 0.8

        self._ee_height = 0.025

        self._reset_counter = 0
        self._successRatio = 0
        self._success = False
        self._ep_min_cube2Goal_dist = float("+inf")
        self._ep_min_cube2tip_dist = float("+inf")
        self._ep_cube2goal_dist_sum = 0
        self._ep_cube2tip_dist_sum = 0
        self._total_cube_travel_dist = 0
        self._total_valid_cube_travel_dist = 0

        self._pushing_orient_quat_xyzw = [-0.70710678,-0.70710678,0.0,0.0]
        self._reset_orientation_quat_xyzw = [1.0,0.0,0.0,0.0]

        self._lastStepGotState = float("-inf")
        self._lastEpGotState = float("-inf")
        self._lastState = None

        self._save_state_vec_history = log_folder is not None
        self._state_vec_history = []
        
        self._ever_initialized = False

        self._ee_spawn_border_dist = 0.02 # distance from the limit of the operating area
        self._cube_spawn_border_dist = 0.08 # distance from the limit of the operating area
        self._prevent_ee_out = prevent_ee_out

    def _logDbgInfoCsv(self):
        new_line = {"getState_duration_avg" : self._getState_duration_avg.getAverage(),
                    "getImg_duration_avg" : self._getImg_duration_avg.getAverage(),
                    "getVec_duration_avg" : self._getVec_duration_avg.getAverage()}
        if self._dbgLogFileCsvWriter is None:
            fname = self._log_folder+"/dbg_log.csv"
            existed = os.path.isfile(fname)
            self._dbgLogFile = open(fname, "a")
            self._dbgLogFileCsvWriter = csv.writer(self._dbgLogFile, delimiter = ",")
            if not existed:
                self._dbgLogFileCsvWriter.writerow(new_line.keys())
        #print("writing csv")
        self._dbgLogFileCsvWriter.writerow(new_line.values())
        self._dbgLogFile.flush()

    def jList2Dict(self, jointPositions : List[float]) -> Dict[Tuple[str,str],float]:
        joints = [  ("panda","panda_joint1"),
                    ("panda","panda_joint2"),
                    ("panda","panda_joint3"),
                    ("panda","panda_joint4"),
                    ("panda","panda_joint5"),
                    ("panda","panda_joint6"),
                    ("panda","panda_joint7")]
        if len(joints) != len(jointPositions):
            raise AttributeError(f"jointPositions has length {len(jointPositions)}, should be {len(joints)}")
        ret = {}
        for i in range(len(joints)):
            ret[joints[i]] = jointPositions[i]
        return ret
    

    def submitAction(self, action : Action) -> None:
        """Plan and execute moveit movement without blocking.

        Parameters
        ----------
        action : Tuple[float, float, float]
            Relative end-effector movement in cartesian space. It is normalized to the max movement distance, i.e.
            this funciont shoult receive values in the [-1,1] range, which are then converted to the proper
            value range.

        """
        # ggLog.info("submitAction()...")
        super().submitAction(action)
        # ggLog.info("received action "+str(action))
        clippedAction = np.clip(np.array(action, dtype=np.float32),-1,1)
        action_xy = clippedAction*self._maxPositionChange
        action_xyz = np.append(action_xy,[0])
        #print("dist action_quat "+str(quaternion.rotation_intrinsic_distance(action_quat,      quaternion.from_euler_angles(0,0,0))))

        currentPose = self._getStateCached()[0][0:6]

        po = np.get_printoptions()
        # np.set_printoptions(precision=4)
        # ggLog.info(f"currentPose = {currentPose}")
        # np.set_printoptions(**po)

        currentPose_xyz = currentPose[0:3]
        currentPose_rpy = currentPose[3:6]
        currentPose_quat = quaternion.from_euler_angles(currentPose_rpy)

        absolute_xyz = currentPose_xyz + action_xyz
        if self._prevent_ee_out:
            eps = 0.005
            minx = self._operatingArea[0][0]
            maxx = self._operatingArea[1][0]
            miny = self._operatingArea[0][1]
            maxy = self._operatingArea[1][1]
            if absolute_xyz[0] <= minx:
                absolute_xyz[0] = minx + eps
            elif absolute_xyz[0] >= maxx:
                absolute_xyz[0] = maxx - eps

            if absolute_xyz[1] <= miny:
                absolute_xyz[1] = miny + eps
            elif absolute_xyz[1] >= maxy:
                absolute_xyz[1] = maxy - eps
        absolute_xyz[2] = self._ee_height
        # absolute_quat = currentPose_quat
        absolute_quat_arr = np.array(self._pushing_orient_quat_xyzw)
        unnorm_action = np.concatenate([absolute_xyz, absolute_quat_arr])
        # ggLog.info("action_quat = "+str(action_quat))
        # ggLog.info("currentPose_rpy = "+str(currentPose_rpy))
        # ggLog.info("currentPose_quat = "+str(currentPose_quat))
        # ggLog.info("absolute_quat = "+str(absolute_quat))
        # ggLog.info("attempting action "+str(action))
        # ggLog.info("unnorm_action = "+str(unnorm_action))

        self._environmentController.setCartesianPoseCommand(linkPoses = {("panda","pushing_ee_tip") : unnorm_action}, do_cartesian = True,
                                                            velocity_scaling = 0.9, acceleration_scaling = 0.5)

    def _reshapeFrame(self, frame):
        npArrImage = lr_gym.utils.utils.image_to_numpy(frame)
        if not self._rgb:
            npArrImage = cv2.cvtColor(npArrImage, cv2.COLOR_BGR2GRAY)

        # dbg_img.helper.publishDbgImg("pre_preproc", img_callback=lambda:npArrImage)

        og_width = npArrImage.shape[1]
        og_height = npArrImage.shape[0]
        npArrImage = npArrImage[0:int(420/480*og_height), int(220/848*og_width):int(708/848*og_width)]
        npArrImage = cv2.bilateralFilter(npArrImage,5,50,50)    
        wb = cv2.xphoto.createSimpleWB()
        wb.setP(0.2)
        npArrImage = wb.balanceWhite(npArrImage)    
        
        # dbg_img.helper.publishDbgImg("mid_preproc", img_callback=lambda:npArrImage)

        # print("shape",npArrImage.shape)
        #imgHeight = npArrImage.shape[0]
        #imgWidth = npArrImage.shape[1]
        #npArrImage = npArrImage[int(imgHeight*0/240.0):int(imgHeight*160/240.0),:] #crop top and bottom, it's an ndarray, it's fast
        npArrImage = cv2.resize(npArrImage, dsize = (self._obs_img_width, self._obs_img_height), interpolation = cv2.INTER_LINEAR)
        # dbg_img.helper.publishDbgImg("mid2_preproc", img_callback=lambda:npArrImage)
        if self._rgb:
            npArrImage = np.transpose(npArrImage, (2,0,1)) # convert channel ordering from HWC to CHW
            npArrImage = np.reshape(npArrImage, (3, self._obs_img_height, self._obs_img_width)) # This may not be needed
        else:
            npArrImage = np.reshape(npArrImage, (self._obs_img_height, self._obs_img_width))
            npArrImage = np.expand_dims(npArrImage, axis=0) # size 1 in channels dimension

        if self._imgObsEncoding == "float":
            npArrImage = np.float32(npArrImage)/255
        elif self._imgObsEncoding == "int":
            npArrImage = np.uint8(npArrImage)
        else:
            raise RuntimeError(f"Unknown img encoding {self._imgObsEncoding}")
        #print("npArrImage.shape = "+str(npArrImage.shape))
        # dbg_img.helper.publishDbgImg("post_preproc", img_callback=lambda:npArrImage)
        return npArrImage

    @staticmethod
    def _getDist_cube2goal(state : State):
        cube_xy = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X,PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y]]


        position_dist2goal = np.linalg.norm(cube_xy - PandaPushingEnv2DOF.getGoalFromState(state))
        
        # ggLog.info(f"cube_xy = {cube_xy} \t self._goalPosition_xy = {self._goalPosition_xy}")
        return position_dist2goal

    @staticmethod
    def _getDist_tip2cube(state : State):
        tip_position_xy = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_X,PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_Y]]
        cube_xy = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X,PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y]]

        position_dist2goal = np.linalg.norm(cube_xy - tip_position_xy)

        # ggLog.info(f"tip_position_xy = {tip_position_xy}")
        return position_dist2goal

    @staticmethod
    def _getCubeMovement(state : State, previousState : State):
        cube_xy = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X,PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y]]
        prev_cube_xy = previousState[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X,PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y]]

        diff = np.linalg.norm(cube_xy - prev_cube_xy)
        
        # ggLog.info(f"cube_xy = {cube_xy} \t self._goalPosition_xy = {self._goalPosition_xy}")
        return diff
    

    

    def checkEpisodeEnded(self, previousState : NDArray[(15,), np.float32], state : NDArray[(15,), np.float32]) -> bool:
        
        success = 0
        ee_position = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_X,PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_Y,PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_Z]]
        cube_position = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X,PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y]]
        goal_position = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_X,PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_Y]]
        cube2Goal_dist = PandaPushingEnv2DOF._getDist_cube2goal(state)
        cube2Tip_dist = PandaPushingEnv2DOF._getDist_tip2cube(state)
        if cube2Goal_dist < self._ep_min_cube2Goal_dist:
            self._ep_min_cube2Goal_dist = cube2Goal_dist
        if cube2Tip_dist < self._ep_min_cube2tip_dist:
            self._ep_min_cube2tip_dist = cube2Tip_dist
        ret = False
        reason = ""
        if not PandaPushingEnv2DOF._is_in_zone(ee_position, self._operatingArea) and not self._prevent_ee_out:
            reason = f"ee out of area"
            ret = True
        elif cube2Goal_dist<self._goalTolerancePosition:
            reason = "Succeded"
            success = 1
            ret = self._terminate_on_success
        if not ret and super().checkEpisodeEnded(previousState, state):
            if super().reachedTimeout():
                reason = "Timed out"
            else:
                reason = "superclass reasons"
            ret =  True
        reason += f"Position = {ee_position}, area = {self._operatingArea}, cube_position = {cube_position}, goal = {goal_position}."
        self._success |= success == 1
        if ret:
            ggLog.info(f"Episode terminated: {reason}, minCube2GoalDistance = {self._ep_min_cube2Goal_dist}, \033[1msuccess={self._success}\033[0m")

        return ret

    @staticmethod
    def _is_in_zone(point, area):
        return (point[0] >= area[0][0] and point[0] <= area[1][0] and
                point[1] >= area[0][1] and point[1] <= area[1][1])

    @staticmethod
    def reward_func(cube2goalDist, tip2cubeDist, cubeDisplacement):
        cubePositionReward = 100/0.4*(0.4-cube2goalDist) # 100 at distance 0, 0 at distance 0.4, linear
        tipPositionReward  = 100/0.4*(0.4-tip2cubeDist) # 100 at distance 0, 0 at distance 0.4, linear
        # if cubeDisplacement < 0.05 or not mayTouch: # remove smal noisy movements
        #     cubeDisplacement = 0
        cubeDisplacementReward = cubeDisplacement*100*20 # 50 if pushed 2.5cm (defautl maximum push)
        return cubePositionReward + 0.5*tipPositionReward + cubeDisplacementReward # max should be 200

    @staticmethod
    def computeReward(previousState : State, state : State, action : int) -> float:        
        if not np.all(np.isfinite(state[0])):
            raise RuntimeError(f"Nan/inf detected in vec state.\n vec = {state[0]}")
        if state[1] is not None:
            if not np.all(np.isfinite(state[1])):
                raise RuntimeError(f"Nan/inf detected in img state.\n img = {state[1]}")
        scaleFactor = 0.1
        goal_tolerance = state[0][PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_TOLERANCE]
        max_steps = state[0][PandaPushingEnv2DOF.VEC_STATE_IDX.MAX_STEPS]
        terminate_on_success = state[0][PandaPushingEnv2DOF.VEC_STATE_IDX.TERMINATE_ON_SUCCESS]

        cube2goalDist = PandaPushingEnv2DOF._getDist_cube2goal(state)
        tip2cubeDist = PandaPushingEnv2DOF._getDist_tip2cube(state)
        cubeDisplacement = PandaPushingEnv2DOF._getCubeMovement(state, previousState)

        reward = PandaPushingEnv2DOF.reward_func(cube2goalDist, tip2cubeDist, cubeDisplacement)


        if cube2goalDist<goal_tolerance:
            if terminate_on_success:
                #As if he stayed here up to the timeout plus a bonus (which roughly triples the reward)
                ep_time = state[0][PandaPushingEnv2DOF.VEC_STATE_IDX.TIME]
                reward = PandaPushingEnv2DOF.reward_func(0,0,0.025) * (max_steps*(1-(ep_time/2+0.5))+1)
            else:
                reward += 200/goal_tolerance*(goal_tolerance-cube2goalDist)

            # ggLog.info(f"succeded: reward = {reward}")
        
        if np.any(np.isnan(reward)):
            raise RuntimeError(f"reward is nan. reward = {reward}")
        if np.any(np.isinf(reward)):
            raise RuntimeError(f"reward is inf. reward = {reward}")

        return reward*scaleFactor

 

    def _sampleInitialPosition(self, cube_position):

        start_area = np.float64(np.copy(self._operatingArea))
        start_area[0] = start_area[0] + self._ee_spawn_border_dist
        start_area[1] = start_area[1] - self._ee_spawn_border_dist
    
        min_dist = 0.075/2 + 0.035/2 + 0.05 # min distance between center of cube and center of ee
        exclusion_zone = np.array([[cube_position[0]-min_dist, cube_position[1]-min_dist],
                                   [cube_position[0]+min_dist, cube_position[1]+min_dist]])

        maxtries = 100000
        tries = 0
        sample = None
        while True:
            point = np.random.rand(3) * (start_area[1] - start_area[0]) + start_area[0]
            point = point[:2]
            if not PandaPushingEnv2DOF._is_in_zone(point, exclusion_zone): # if it is valid return it
                sample = point
                break
            tries += 1
            if tries > maxtries:
                ggLog.error("Failed to sample initial end effector position, using center")
                sample = (start_area[1] + start_area[0])/2
                break
        # ggLog.info(f"Sampled initial ee position {sample} from operating area {start_area}")
        return sample

    def _sampleInitialCubePosition(self):
        start_area = np.float64(np.copy(self._operatingArea))
        start_area[0] = start_area[0] + self._cube_spawn_border_dist
        start_area[1] = start_area[1] - self._cube_spawn_border_dist

        maxtries = 100000
        tries = 0
        sample = None
        while True:
            point = np.random.rand(3) * (start_area[1] - start_area[0]) + start_area[0]
            cube_xy = point[:2]
            position_dist2goal = np.linalg.norm(cube_xy - self._goalPosition_xy)
            if position_dist2goal >= self._goalTolerancePosition + 0.005 or self._allow_successful_initial_cube_position:
                sample =  cube_xy
                break
            tries += 1
            if tries > maxtries:
                ggLog.error("Failed to sample initial cube position, using center with offset")
                sample = (start_area[1] + start_area[0])/2 + np.array([-0.05,0])
                break
        # ggLog.info(f"Sampled initial cube position {sample} from operating area {start_area}")
        return sample
        
    def buildEeGoal(self, position_xyz, orientation_xyzw, frame_id, ee_link, velocity_scaling, acceleration_scaling, do_cartesian):
        goal = lr_gym_utils.msg.MoveToEePoseGoal()
        goal.pose = lr_gym.utils.utils.buildPoseStamped(position_xyz,orientation_xyzw,frame_id) 
        goal.end_effector_link = ee_link
        goal.velocity_scaling = velocity_scaling
        goal.acceleration_scaling = acceleration_scaling
        goal.do_cartesian = do_cartesian
        return goal

    def buildJointGoal(self, pose, velocity_scaling, acceleration_scaling):
        goalj = lr_gym_utils.msg.MoveToJointPoseGoal()
        goalj.velocity_scaling = velocity_scaling
        goalj.acceleration_scaling = acceleration_scaling
        goalj.pose = pose
        return goalj

    def performStep(self):
        prevState = self._getStateCached()
        super().performStep()
        nextState = self._getStateCached()
        prev_cube_xy = prevState[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X,PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y]]
        next_cube_xy = nextState[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X,PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y]]
        dist = math.sqrt((prev_cube_xy[0]-next_cube_xy[0])**2 + (next_cube_xy[1]-next_cube_xy[1])**2)
        self._total_cube_travel_dist += dist
        self._ep_cube2goal_dist_sum += PandaPushingEnv2DOF._getDist_cube2goal(nextState)
        self._ep_cube2tip_dist_sum += PandaPushingEnv2DOF._getDist_tip2cube(nextState)
        self._total_valid_cube_travel_dist += PandaPushingEnv2DOF._getCubeMovement(nextState, prevState)

    def performReset(self):
        if self._save_state_vec_history:
            np.savetxt(self._log_folder+f"/ep_{self._reset_counter:06d}.csv", np.array(self._state_vec_history, dtype=np.float32), delimiter=",")
            self._logDbgInfoCsv()
        
        if not isinstance(self._environmentController, SimulatedEnvController):
            self._setCollisionObjects(stay_up=False)
            linkStates = self._environmentController.getLinksState(requestedLinks=[("panda","pushing_ee_tip"),("cube","cube")])
            tipPose = linkStates[("panda","pushing_ee_tip")].pose
            tipPoseArr = [  tipPose.position[0],
                            tipPose.position[1],
                            tipPose.position[2],
                            tipPose.orientation.x,
                            tipPose.orientation.y,
                            tipPose.orientation.z,
                            tipPose.orientation.w]
            center_xy = (0.525,0)
            #move a bit inward and up
            tipPoseArr[0] = 0.5*tipPoseArr[0]+0.5*center_xy[0]
            tipPoseArr[1] = 0.5*tipPoseArr[1]+0.5*center_xy[1]
            tipPoseArr[2] = self._ee_height
            try:
                self._environmentController.moveToEePoseSync(   tipPoseArr,True,
                                                                self._velocity_scaling, self._acceleration_scaling,
                                                                "pushing_ee_tip", "world", blocking = False)
                tipPoseArr[0] = 0.2*tipPoseArr[0]+0.8*center_xy[0]
                tipPoseArr[1] = 0.2*tipPoseArr[1]+0.8*center_xy[1]
                tipPoseArr[2] = 0.1
                self._environmentController.moveToEePoseSync(   tipPoseArr,True,
                                                                self._velocity_scaling, self._acceleration_scaling,
                                                                "pushing_ee_tip", "world", blocking = False)
            except MoveFailError as e:
                ggLog.warn(f"Initial reset moves failed, skipping")
            self._setCollisionObjects(stay_up=True)
        super().performReset()


    def _setCollisionObjects(self, stay_up : bool):
        self._environmentController.clearCollisionObjects()
        self._environmentController.addCollisionBox(pose_xyz_xyzw = [    0,      0, -1,0,0,0,1], size_xyz = (10,5,2.02)) #bottom
        self._environmentController.addCollisionBox(pose_xyz_xyzw = [    1,      0,  0,0,0,0,1], size_xyz = (0.2,5,2)) #front 0.9m
        self._environmentController.addCollisionBox(pose_xyz_xyzw = [    0,   0.47,  0,0,0,0,1], size_xyz = (5,0.2,2)) #left 0.37m
        self._environmentController.addCollisionBox(pose_xyz_xyzw = [    0,   -0.6,  0,0,0,0,1], size_xyz = (5,0.2,2)) #right 0.5m
        self._environmentController.addCollisionBox(pose_xyz_xyzw = [ -0.6,      0,  0,0,0,0,1], size_xyz = (0.2,5,2)) #back 0.5m

        self._environmentController.addCollisionBox(pose_xyz_xyzw = [-0.04, -0.135,  0,0,0,0,1], size_xyz = (0.22,0.27,0.2)) #Protect power cable
        self._environmentController.addCollisionBox(pose_xyz_xyzw = [-0.04,  0.135,  0,0,0,0,1], size_xyz = (0.22,0.27,0.2)) #Protect safety button cable
        self._environmentController.addCollisionBox(pose_xyz_xyzw = [-0.15,   0.45,  0,0,0,0,1], size_xyz = (0.44,0.30,0.45)) #Protect side cam

        # self._environmentController.addCollisionBox(pose_xyz_xyzw = [0.0,0.0,0.09, 0,0,0,1], size_xyz = (0.035,0.035,0.18), attach_link = "panda_link8", reference_frame = "panda_tcp", attach_ignored_links = ["panda_leftfinger","panda_rightfinger"])

        if stay_up:
            self._environmentController.addCollisionBox(pose_xyz_xyzw = [    0,      0, -1,0,0,0,1], size_xyz = (10,5,2.14)) #bottom

    def _long_real_reset(self, cubePose):
        lr_gym.utils.beep.boop()
        lr_gym.utils.beep.beep()
        lr_gym.utils.beep.boop()
        lr_gym.utils.beep.beep()
        over_center_jpose = self.jList2Dict([0, 0, 0, -1.97139, 0, 1.97139, 0])
        self._environmentController.moveToJointPoseSync(over_center_jpose, 0.3, 0.5)
        all_traj_files = {  "right" : os.path.dirname(os.path.abspath(__file__))+"/../../../assets/reset_traj_right.pkl",
                            "left" : os.path.dirname(os.path.abspath(__file__))+"/../../../assets/reset_traj_left.pkl",
                            "front" : os.path.dirname(os.path.abspath(__file__))+"/../../../assets/reset_traj_front.pkl",
                            "back" : os.path.dirname(os.path.abspath(__file__))+"/../../../assets/reset_traj_back.pkl"}
        traj_files = []
        if cubePose.position[1]>0:
            traj_files.append(all_traj_files["left"])
        else:
            traj_files.append(all_traj_files["right"])
        if cubePose.position[0] > 0.525:
            traj_files.append(all_traj_files["front"])
        else:
            traj_files.append(all_traj_files["back"])
        for f in traj_files:
            try:
                r = lr_gym_utils.play_joint_trajectory.play(pickle.load( open(f, "rb" )), "panda_arm_position_trajectory_controller", scaling = 0.7)
            except FileNotFoundError as e:
                ggLog.warn(f"trajectory file {f} not found, skipping trajectory execution")
                continue
            except TrajMoveFailError as e:
                ggLog.warn(f"Trajectory execution failed")
                r = -100
            if r<0:
                ggLog.warn(f"trajectory execution for file {f} failed with r={r}.")
                try:
                    input("Press Enter to continue.")
                except EOFError as e:
                    ggLog.error("Error getting input: "+str(e))


    def initializeEpisode(self) -> None:
        # ggLog.info("Initializing episode...........................................")
        # traceback.print_stack()

        if not self._ever_initialized:
            self._ever_initialized = True
            if isinstance(self._environmentController, SimulatedEnvController):
                self._environmentController.spawnModel( xacro_file_path =rospkg.RosPack().get_path("autoencoding_rl")+"/urdf/generic_rounded_cube.sdf.xacro",
                                                        pose = Pose(2,0,0.032,0,0,0,1),
                                                        model_name = "cube",
                                                        format = "sdf",
                                                        args = {"red"   : self._cube_color[0],
                                                                "green" : self._cube_color[1],
                                                                "blue"  : self._cube_color[2]})
                self._environmentController.setupLight( 
                    gazebo_msgs.srv.SetLightPropertiesRequest(  light_name = "sun",                   # name of Gazebo Light
                                                                cast_shadows = True,
                                                                diffuse = std_msgs.msg.ColorRGBA(r=0.8,g=0.8,b=0.8,a=1.0),
                                                                specular = std_msgs.msg.ColorRGBA(r=0.2,g=0.2,b=0.2,a=1.0),
                                                                attenuation_constant = 0.9,
                                                                attenuation_linear = 0.1,
                                                                attenuation_quadratic = 0.001,
                                                                direction = geometry_msgs.msg.Vector3(*self._light_direction),
                                                                pose = geometry_msgs.msg.Pose(  position = geometry_msgs.msg.Point(0,0,0),
                                                                                                orientation = geometry_msgs.msg.Quaternion(0,0,0,1))))

        self._success = False
        self._ep_min_cube2Goal_dist = float("+inf")
        self._ep_min_cube2tip_dist = float("+inf")
        self._total_cube_travel_dist = 0
        self._total_valid_cube_travel_dist = 0
        self._ep_cube2goal_dist_sum = 0
        self._ep_cube2tip_dist_sum = 0
        self._goalPosition_xy = self._goal_sampler()
        cp = self._sampleInitialCubePosition()

        ee_start_xy = self._sampleInitialPosition(cube_position=cp)

        reset_routine_orient_xyzw = self._reset_orientation_quat_xyzw
        pushing_orient = self._pushing_orient_quat_xyzw
        over_center_jpose = self.jList2Dict([0, 0, 0, -1.97139, 0, 1.97139, 0])
        cubePrePushHeight = 0.075
        cubePushEndHeight = 0.015
        cubePushStartHeight = cubePushEndHeight+0.015
        cubePostPushHeight = 0.15 # a high value here prevents the cube being still detected in the right position in case it gets stuck in the gripper and lifted over the destination (happens if the cube is prefectly diagonal...)


        # self._environmentController.setLinksStateDirect({   ("cube","cube") : LinkState(position_xyz = (0.5, 0, 0.031),
        #                                                                                 orientation_xyzw = (0,0,0,1),
        #                                                                                 pos_velocity_xyz = (0,0,0),
        #                                                                                 ang_velocity_xyz = (0,0,0))
        #                                                 })
        if isinstance(self._environmentController, SimulatedEnvController):

            # ---------------------------------------------------------------
            # Simulation reset
            # ---------------------------------------------------------------

            
            #Move the cube temporarily out of the way
            self._environmentController.setLinksStateDirect({   ("cube","cube") : LinkState(position_xyz = (2,2, 0.038),
                                                                                            orientation_xyzw = (0,0,0,1),
                                                                                            pos_velocity_xyz = (0,0,0),
                                                                                            ang_velocity_xyz = (0,0,0))
                                                            })
            moved = False
            for i in range(5):
                # ggLog.info(f"Moving to ee pose {goal}")
                try:
                    self._environmentController.moveToEePoseSync([ee_start_xy[0],ee_start_xy[1],self._ee_height]+pushing_orient, False,
                                                                    self._velocity_scaling, self._acceleration_scaling,
                                                                    "pushing_ee_tip", "world")
                    moved = True
                    break
                except Exception as e:
                    ggLog.warn("Reset ee move failed. exception = "+str(e))
                    try:
                        # Sometimes the controller seems to crash and then come back using a joint move
                        self._environmentController.moveToJointPoseSync(jointPositions = self.jList2Dict([-0.1866635247951125, 0.20335169120806906, 0.18006042354188967,-2.401792641898931, -0.06842146836912466, 2.598572881865177, 0.7374865895450897]),
                                                                        velocity_scaling = self._velocity_scaling,
                                                                        acceleration_scaling = self._acceleration_scaling)
                    except Exception as e2:
                        ggLog.error("Failed to move to joint pose while recovering from failed ee move. exception = "+str(e2))
                    #self._actionsFailsInLastStepCounter+=1
                    self._environmentController.freerun(1)
            if not moved:
                ggLog.error("Failed to move to initial end effector position.")
            # ggLog.info("Moved to initial ee pose")

            self._environmentController.freerun(0.5)
            self._environmentController.setLinksStateDirect({   ("cube","cube") : LinkState(position_xyz = (cp[0],cp[1], 0.032),
                                                                                            orientation_xyzw = (0,0,0,1),
                                                                                            pos_velocity_xyz = (0,0,0),
                                                                                            ang_velocity_xyz = (0,0,0))
                                                            })
            self._environmentController.freerun(1.0)
            # ggLog.info("Moved cube")
            
        else:

            # ---------------------------------------------------------------
            # Real-world reset
            # ---------------------------------------------------------------

            self._setCollisionObjects(stay_up=True)

            # ggLog.info("Move over the center of the manipulation area")
            # input("...")
            # ggLog.debug("Moving to initial joint pose")
            # ggLog.debug("Moved to initial joint pose")
            # ggLog.info("Move over the cube")
            # input("...")

            cubeAtPose = False
            cp_tries = 0
            while not cubeAtPose:
                if cp_tries>0:
                    ggLog.info("Retrying to place cube")
                
                self._environmentController.moveToJointPoseSync(over_center_jpose, self._velocity_scaling, self._acceleration_scaling)
                status = "start joint pose"
                cubePose = self._environmentController.getLinksState(requestedLinks=[("cube","cube")])[("cube","cube")].pose


                grip_orient_xyzw = reset_routine_orient_xyzw
                over_cube_start_pose = [cubePose.position[0],cubePose.position[1],cubePrePushHeight]+grip_orient_xyzw
                in_cube_start_pose = [cubePose.position[0],cubePose.position[1],cubePushStartHeight]+grip_orient_xyzw
                in_cube_end_pose = [cp[0],cp[1],cubePushEndHeight]+reset_routine_orient_xyzw
                over_cube_end_pose = [cp[0],cp[1],cubePostPushHeight]+reset_routine_orient_xyzw
                try:
                    self._environmentController.moveToEePoseSync(over_cube_start_pose, False,
                                                                    self._velocity_scaling, self._acceleration_scaling,"pushing_ee_tip", "world", blocking = False)
                    status = "over cube pre"
                    self._setCollisionObjects(stay_up=False)
                    # ggLog.info("Move to the cube (slowly)")
                    # input("...")
                    self._environmentController.moveToEePoseSync(in_cube_start_pose, True,
                                                                    0.4, 0.9, "pushing_ee_tip","world", blocking = False)
                    status = "in cube start"

                    # # would be better to put a constrain: https://answers.ros.org/question/358995/moveit-parametically-disable-one-joint/
                    # jointStates = self._environmentController.getJointsState(self._panda_joints)
                    # jointStates[("panda","panda_joint1")] = jointStates[("panda","panda_joint1")] * 0.9
                    # self._environmentController.moveToJointPoseSync(jointPositions = jointStates, velocity_scaling = 0.8, acceleration_scaling = 0.5)

                    # ggLog.info("Move to the destination")
                    # input("...")
                    self._environmentController.moveToEePoseSync(in_cube_end_pose, True,
                                                                    self._velocity_scaling, self._acceleration_scaling, "pushing_ee_tip", "world", blocking = False)
                    status = "in cube end"
                
                    # ggLog.info("Move up over the destination")
                    # input("...")
                    self._environmentController.moveToEePoseSync(over_cube_end_pose,True,
                                                                    self._velocity_scaling, self._acceleration_scaling, "pushing_ee_tip", "world", blocking = False)
                    status = "over cube end"
                except MoveFailError as e:
                    ggLog.warn(f"Initialization move failed: {e}")
                    if status == "in cube start":
                        abort_pose = over_cube_start_pose
                    elif status == "in cube end":
                        abort_pose = over_cube_start_pose
                    else:
                        abort_pose = None
                    if abort_pose is not None:
                        try:
                            self._environmentController.moveToEePoseSync(abort_pose, True,
                                                                        self._velocity_scaling, self._acceleration_scaling, "pushing_ee_tip", "world", blocking = False)
                        except MoveFailError as e:
                            ggLog.warn(F"Failed to move to abort pose, skipping")


                self._setCollisionObjects(stay_up=True)

                ok_threshold = 0.03
                for _ in range(5):
                    cubePose = self._environmentController.getLinksState(requestedLinks=[("cube","cube")])[("cube","cube")].pose
                    error = np.linalg.norm([cp[0]-cubePose.position[0],cp[1]-cubePose.position[1]])
                    if error > ok_threshold:
                        #Just retry maybe it's a bad detection
                        self._environmentController.freerun(0.5)
                    else:
                        cubeAtPose = True
                if not cubeAtPose and cp_tries > 1:
                    self._long_real_reset(cubePose)
                elif not cubeAtPose and cp_tries > 3:
                    if error > ok_threshold:
                        lr_gym.utils.beep.boop()
                        ggLog.error(f"Cube is not at expected position. It should be at {cp} but it is at {cubePose.position} (diff = {[cp[0]-cubePose.position[0],cp[1]-cubePose.position[1]]}).")
                        ggLog.error(f"Please put the cube at {cp} and press ENTER.")
                        input()
                        cp_tries = -1
                        cubePose = self._environmentController.getLinksState(requestedLinks=[("cube","cube")])[("cube","cube")].pose
                        error = np.linalg.norm([cp[0]-cubePose.position[0],cp[1]-cubePose.position[1]])
                        if error > ok_threshold:
                            lr_gym.utils.beep.boop()
                            ggLog.error(f"Cube is still not at expected position. It should be at {cp} but it is at {cubePose.position}.")
                            ggLog.error(f"Do you want to continue anyway keeping the cube at its current position? (pressing no will keep retrying) (y/n)")
                            response = input()
                            if response == "y":
                                ggLog.error("Continuing with wrong cube initial position")
                                cubeAtPose = True
                        else:
                            cubeAtPose = True
                cp_tries+=1 

            # ggLog.info("Move over the center of the manipulation area")
            # input("...")
            self._environmentController.moveToJointPoseSync(over_center_jpose, self._velocity_scaling, self._acceleration_scaling)
            # ggLog.info("Move over the initial ee position")
            # input("...")
            self._environmentController.moveToEePoseSync([ee_start_xy[0],ee_start_xy[1],0.1]+pushing_orient,False,
                                                            self._velocity_scaling, self._acceleration_scaling, "pushing_ee_tip", "world")
            self._setCollisionObjects(stay_up=False)           
            # ggLog.info("Move to the initial ee position")
            # input("...")
            self._environmentController.moveToEePoseSync([ee_start_xy[0],ee_start_xy[1],self._ee_height]+pushing_orient, False,
                                                            self._velocity_scaling, self._acceleration_scaling, "pushing_ee_tip", "world")

        self._reset_counter += 1
        self._state_vec_history = []
        return




    def getObservation(self, state) -> Dict[str, np.ndarray]:

        if self._obs_only_vec:
            vec_obs = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_X,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_Y,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_YAW,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.TIME,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_X,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_Y]]
        else:
            vec_obs = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_X,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_Y,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.TIME,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_X,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_Y]]
        img_obs = state[1]
        if not np.all(np.isfinite(vec_obs)):
            raise RuntimeError(f"Nan/inf detected in vec observation.\n vec = {vec_obs}")
        if not self._obs_only_vec:
            if not np.all(np.isfinite(img_obs)):
                raise RuntimeError(f"Nan/inf detected in img observation.\n img = {img_obs}")

        if self._obs_only_img:
            return {"img" : img_obs}
        if self._obs_only_vec:
            return {"vec" : vec_obs}
        
        return {"vec" : vec_obs, "img" : img_obs}


    def _points_mostly_up(self, vec3):
        return vec3[2] > np.linalg.norm(vec3[0:2])

    def _cube_quat2Yaw(self, quat):
        xaxis = np.array([1,0,0])
        rotated_xaxis = quaternion.rotate_vectors(quat, xaxis)
        if not self._points_mostly_up(rotated_xaxis):
            return autoencoding_rl.utils.angle_between(xaxis, rotated_xaxis)
        else:
            zaxis = np.array([0,0,1])
            rotated_zaxis = quaternion.rotate_vectors(quat, zaxis)
            return autoencoding_rl.utils.angle_between(zaxis, rotated_zaxis)



    def _getStateCached(self) -> Any:
        """Get the an observation of the environment keeping a cache of the last observation.

        Returns
        -------
        Any
            An observation of the environment. See the environment implementation for details on its format

        """
        if self._stepCounter != self._lastStepGotState or self._reset_counter != self._lastEpGotState or self._lastState is None:
            # ggLog.info("State cache miss")
            self._lastStepGotState = self._stepCounter
            self._lastEpGotState = self._reset_counter
            self._lastState = self._getState()
            self._state_vec_history.append(self._lastState[0])

        return self._lastState

    def getState(self) -> State:
        return self._getStateCached()


    def _getState(self) -> State:
        """Get an observation of the environment.

        Returns
        -------
        NDArray[(15,), np.float32]
            numpy ndarray. The content of each field is specified at the self.observation_space_high definition

        """
        # ggLog.info(f"GetState ep = {self._reset_counter}, step = {self._stepCounter}")
        t0 = time.monotonic()
        linkStates = self._environmentController.getLinksState(requestedLinks=[("panda","pushing_ee_tip"),("cube","cube")])
        t1 = time.monotonic()
        # ggLog.info(f"Waited links for {t1-t0}s")
        tipPose = linkStates[("panda","pushing_ee_tip")].pose
        cubePose = linkStates[("cube","cube")].pose
        # ggLog.info("Got links states")

        # t2 = time.monotonic()
        # jointStates = self._environmentController.getJointsState([("panda","panda_joint1"),
        #                                                          ("panda","panda_joint2"),
        #                                                          ("panda","panda_joint3"),
        #                                                          ("panda","panda_joint4"),
        #                                                          ("panda","panda_joint5"),
        #                                                          ("panda","panda_joint6"),
        #                                                          ("panda","panda_joint7")])
        # t3 = time.monotonic()
        # ggLog.info(f"Waited joints for {t3-t2}s")
        # ggLog.info("Got joint states")


        tip_quat = quaternion.from_float_array([tipPose.orientation.w,tipPose.orientation.x,tipPose.orientation.y,tipPose.orientation.z])
        tip_orientation_rpy = quaternion.as_euler_angles(tip_quat)


        cube_quat = quaternion.from_float_array([cubePose.orientation.w,cubePose.orientation.x,cubePose.orientation.y,cubePose.orientation.z])

        # ggLog.info(f"tip_quat = {tip_quat}")
        # ggLog.info(f"tip_orientation_rpy = {tip_orientation_rpy}")

        #print("got ee pose "+str(eePose))

        #print(jointStates)

        #Real franka gripper does not give force information, effort and evlocity are always zero in joint_states
        vec_state = [None]*len(PandaPushingEnv2DOF.VEC_STATE_IDX)

        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_X] = tipPose.position[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_Y] = tipPose.position[1]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_Z] = tipPose.position[2]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_ROLL] = tip_orientation_rpy[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_PITCH] = tip_orientation_rpy[1]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_YAW] = tip_orientation_rpy[2]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.JOINT1_POS] = 0 #jointStates[("panda","panda_joint1")].position[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.JOINT2_POS] = 0 #jointStates[("panda","panda_joint2")].position[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.JOINT3_POS] = 0 #jointStates[("panda","panda_joint3")].position[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.JOINT4_POS] = 0 #jointStates[("panda","panda_joint4")].position[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.JOINT5_POS] = 0 #jointStates[("panda","panda_joint5")].position[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.JOINT6_POS] = 0 #jointStates[("panda","panda_joint6")].position[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.JOINT7_POS] = 0 #jointStates[("panda","panda_joint7")].position[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.ACTION_FAILS] = self._environmentController.actionsFailsInLastStep()
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X] = cubePose.position[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y] = cubePose.position[1]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_YAW] = self._cube_quat2Yaw(cube_quat)
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.TIME] = self._stepCounter/self._maxStepsPerEpisode*2-1
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_X] = self._goalPosition_xy[0]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_Y] = self._goalPosition_xy[1]
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.MAX_STEPS] = self._maxStepsPerEpisode
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_TOLERANCE] = self._goalTolerancePosition
        vec_state[PandaPushingEnv2DOF.VEC_STATE_IDX.TERMINATE_ON_SUCCESS] = self._terminate_on_success
        
        vec_state_arr = np.array(vec_state,dtype=np.float32)

        self._getVec_duration_avg.addValue(newValue=time.monotonic()-t0)

        #print(f"type(self._environmentController) = {self._environmentController}")
        if not self._obs_only_vec:
            t4 = time.monotonic()
            img_obs = self._environmentController.getRenderings([self._camera_name])[0]
            t5 = time.monotonic()
            self._getImg_duration_avg.addValue(newValue=t5-t4)
            # ggLog.info(f"Waited img for {t5-t4}s")
            if img_obs is None:
                ggLog.error("No camera image received. Observation will contain and empty image.")
                img_obs = np.zeros([self._img_channels, self._obs_img_height, self._obs_img_width])
            else:
                img_obs = self._reshapeFrame(img_obs)
        else:
            img_obs = None

        self._getState_duration_avg.addValue(newValue=time.monotonic()-t0)
        return [vec_state_arr, img_obs]

    def buildSimulation(self, backend : str = "gazebo"):
        if backend == "gazebo":
            self._mmRosLauncher = lr_gym_utils.ros_launch_utils.MultiMasterRosLauncher( rospkg.RosPack().get_path("autoencoding_rl")+
                                                                                            "/launch/panda_pushing.launch",
                                                                                            cli_args=[  "gui:=false",
                                                                                                        "noplugin:=false",
                                                                                                        "enable_depth:=false",
                                                                                                        "color_fps:=5",
                                                                                                        "wall_sim_speed:="+str(self._wall_sim_speed),
                                                                                                        "gazebo_seed:="+str(self._seed),
                                                                                                        # "cube_red:="+str(self._cube_color[0]),
                                                                                                        # "cube_green:="+str(self._cube_color[1]),
                                                                                                        # "cube_blue:="+str(self._cube_color[2]),
                                                                                                        "camera_x:="+str(self._camera_position[0]),
                                                                                                        "camera_y:="+str(self._camera_position[1]),
                                                                                                        "camera_z:="+str(self._camera_position[2]),
                                                                                                        "camera_roll_absx:="+str(self._camera_orientation[0]),
                                                                                                        "camera_pitch_absy:="+str(self._camera_orientation[1]),
                                                                                                        "camera_yaw_absz:="+str(self._camera_orientation[2]),
                                                                                                        "light_direction_x:="+str(self._light_direction[0]),
                                                                                                        "light_direction_y:="+str(self._light_direction[1]),
                                                                                                        "light_direction_z:="+str(self._light_direction[2])
                                                                                                        ])
            self._mmRosLauncher.launchAsync()

        elif backend == "real":
            if self._real_robot_pc_ip is None:
                raise AttributeError("real_robot_pc_ip must be set to use the real setup.")
            self._mmRosLauncher = lr_gym_utils.ros_launch_utils.MultiMasterRosLauncher( rospkg.RosPack().get_path("autoencoding_rl")+
                                                                                            "/launch/panda_pushing.launch",
                                                                                            cli_args=[  "simulated:=false",
                                                                                                        "remote_robot:=true"],
                                                                                            basePort = 11311,
                                                                                            ros_master_ip = self._real_robot_pc_ip)
            self._mmRosLauncher.launchAsync()
        else:
            raise NotImplementedError("Backend '"+backend+"' not supported")

    def _destroySimulation(self):
        self._mmRosLauncher.stop()

    def getSimTimeFromEpStart(self):
        return self._environmentController.getEnvSimTimeFromStart()

    def getUiRendering(self) -> Tuple[np.ndarray, float]:
        img = self._environmentController.getRenderings([self._camera_name])[0]
        if img is None:
            npImg = None
            time = -1
        else:
            npImg = lr_gym.utils.utils.image_to_numpy(img)
            time = img.header.stamp.to_sec()
        return npImg, time

    def getInfo(self,state=None) -> Dict[Any,Any]:
        i = super().getInfo(state=state)
        i["success"] = self._success
        i["min_cube_dist"] = self._ep_min_cube2Goal_dist
        i["avg_cube_dist"] = self._ep_cube2goal_dist_sum/self._actionsCounter if self._actionsCounter>0 else float("nan")
        i["avg_tip_dist"] = self._ep_cube2tip_dist_sum/self._actionsCounter if self._actionsCounter>0 else float("nan")
        i["min_tip_dist"] = self._ep_min_cube2tip_dist
        i["total_cube_travel"] = self._total_cube_travel_dist
        # ggLog.info(f"Setting success_ratio to {i['success_ratio']}")
        return i


    def setGoalInState(self, state, goal):
        """To be implemented in subclass.

        Update the provided state with the provided goal. Useful for goal-oriented environments, especially when using HER.
        It's used by ToGoalEnvWrapper.
        """
        state[0][PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_X] = goal[0]
        state[0][PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_Y] = goal[1]

    @staticmethod
    def getGoalFromState(state):
        """To be implemented in subclass.

        Get the goal for the provided state. Useful for goal-oriented environments, especially when using HER.
        """
        return state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_X,PandaPushingEnv2DOF.VEC_STATE_IDX.GOAL_Y]]

    def getAchievedGoalFromState(self, state):
        """To be implemented in subclass.

        Get the currently achieved goal from the provided state. Useful for goal-oriented environments, especially when using HER.
        """
        # if not self._obs_only_vec:
        #     ggLog.warn("WARNING: cube position is being seen by the agent through the achieved goal! Need to modify HER to avoid this")
        return state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_X,PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_Y]]

    def getPureObservationFromState(self, state):
        """To be implemented in subclass.

        Get the pure observation from the provided state. Pure observation means the observation without goal and achieved goal.
        Useful for goal-oriented environments, especially when using HER.
        """
        # if self._obs_only_img:
        #     return self.getObservation(state)
        # elif self._obs_only_vec:
        #     obs = self.getObservation(state)
        #     return {"vec" : obs["vec"][0:-2]}
        # else:
        #     obs = self.getObservation(state)
        #     return {"vec" : obs["vec"][0:-2],
        #             "img" : obs["img"]}

        if self._obs_only_vec:
            vec_obs = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_X,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_Y,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.CUBE_YAW,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.TIME]]
        else:
            vec_obs = state[0][[PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_X,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.TIP_Y,
                                PandaPushingEnv2DOF.VEC_STATE_IDX.TIME]]
        img_obs = state[1]
        if np.any(np.isnan(vec_obs)) or np.any(np.isnan(img_obs)):
            raise RuntimeError(f"Nan detected in observation.\n vec = {vec_obs}\n img = {img_obs}")
        if np.any(np.isinf(vec_obs)) or np.any(np.isinf(img_obs)):
            raise RuntimeError(f"inf detected in observation.\n vec = {vec_obs}\n img = {img_obs}")
        if self._obs_only_img:
            return {"img" : img_obs}
        if self._obs_only_vec:
            return {"vec" : vec_obs}
        
        return {"vec" : vec_obs, "img" : img_obs}