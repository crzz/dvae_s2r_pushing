#!/usr/bin/env python3
"""This file implements PandaMoveitReachingEnv."""

import rospy
import rospy.client

import gym
import numpy as np
from typing import Tuple, List, Dict, Union, Any
from nptyping import NDArray
import quaternion
import lr_gym_utils.msg
import lr_gym_utils.srv
from geometry_msgs.msg import PoseStamped
import actionlib
import rospkg
import cv2
import lr_gym

from lr_gym.envs.ControlledEnv import ControlledEnv
from lr_gym.envControllers.MoveitGazeboController import MoveitGazeboController
from lr_gym.envControllers.SimulatedEnvController import SimulatedEnvController
from autoencoding_rl.envs.PandaPushingEnv2DOF import PandaPushingEnv2DOF

import lr_gym_utils.ros_launch_utils
import lr_gym.utils.dbg.ggLog as ggLog
import math
from lr_gym.utils.utils import JointState, LinkState
import time

import sensor_msgs


Action = NDArray[(2,), np.float32]
Observation = NDArray[(15,), np.float32]
State = List[Union[NDArray[(15+2,), np.float32],sensor_msgs.msg.Image]] #Observation plus 2D cube position

class PandaPushingEnv2DOF_no_img(PandaPushingEnv2DOF):

    def __init__(   self,
                    goalPosition_xy : Tuple[float,float] = (0,0),
                    initialCubePosition_xy : Tuple[float,float] = (0,0),
                    maxStepsPerEpisode : int = 50,
                    operatingArea = np.array([[0.30, -0.225, 0], [0.75, 0.225, 0.5]]),
                    startSimulation : bool = True,
                    backend="gazebo",
                    environmentController = None,
                    real_robot_pc_ip : str = None,
                    wall_sim_speed : bool = False,
                    seed = 0,
                    terminate_on_success : bool = True,
                    dict_obs : bool = True):
        """Short summary.

        Parameters
        ----------
        goalPose : Tuple[float,float,float,float,float,float,float]
            end-effector pose to reach (x,y,z, qx,qy,qz,qw)
        maxStepsPerEpisode : int
            maximum number of frames per episode. The step() function will return
            done=True after being called this number of times
        render : bool
            Perform rendering at each timestep
            Disable this if you don't need the rendering
        goalTolerancePosition : float
            Position tolerance under which the goal is considered reached, in meters
        goalToleranceOrientation_rad : float
            Orientation tolerance under which the goal is considered reached, in radiants


        """

        self._real_robot_pc_ip = real_robot_pc_ip
        self._wall_sim_speed = wall_sim_speed
        self._seed = seed
        self._terminate_on_success = terminate_on_success

        vec_observation_space_high = np.array([ np.finfo(np.float32).max, # end-effector x position
                                                np.finfo(np.float32).max, # end-effector y position
                                                np.finfo(np.float32).max, # cube x position
                                                np.finfo(np.float32).max, # cube y position
                                                np.finfo(np.float32).max, # cube yaw
                                                1.0, # time
                                                ])
        if dict_obs:
            self.observation_space = gym.spaces.Dict({"vec": gym.spaces.Box(-vec_observation_space_high, vec_observation_space_high)})

        if environmentController is None:                
            self._environmentController = MoveitGazeboController(jointsOrder = [("panda","panda_joint1"),
                                                                                ("panda","panda_joint2"),
                                                                                ("panda","panda_joint3"),
                                                                                ("panda","panda_joint4"),
                                                                                ("panda","panda_joint5"),
                                                                                ("panda","panda_joint6"),
                                                                                ("panda","panda_joint7")],
                                                            endEffectorLink  = ("panda", "pushing_ee_tip"),
                                                            referenceFrame   = "world",
                                                            initialJointPose = {("panda","panda_joint1") : -0.1866635247951125,
                                                                                ("panda","panda_joint2") : 0.20335169120806906,
                                                                                ("panda","panda_joint3") : 0.18006042354188967,
                                                                                ("panda","panda_joint4") :-2.401792641898931,
                                                                                ("panda","panda_joint5") : -0.06842146836912466,
                                                                                ("panda","panda_joint6") : 2.598572881865177,
                                                                                ("panda","panda_joint7") : 0.7374865895450897})
        else:
            self._environmentController = environmentController

        super(PandaPushingEnv2DOF,self).__init__(   maxStepsPerEpisode = maxStepsPerEpisode,
                                                startSimulation = startSimulation,
                                                simulationBackend=backend,
                                                environmentController=self._environmentController)

        self._camera_name = "front_camera/color/image_raw" #this should work also with the gazebo plugin because of how lr_realsense names the sensors in the urdf
        self._environmentController.setCamerasToObserve([self._camera_name])

        self._environmentController.setJointsToObserve( [("panda","panda_joint1"),
                                                        ("panda","panda_joint2"),
                                                        ("panda","panda_joint3"),
                                                        ("panda","panda_joint4"),
                                                        ("panda","panda_joint5"),
                                                        ("panda","panda_joint6"),
                                                        ("panda","panda_joint7")])


        self._environmentController.setLinksToObserve( [("panda","pushing_ee_tip"),("cube","cube")])

        self._goalPosition_xy = goalPosition_xy
        self._initialCubePosition_xy = initialCubePosition_xy
        self._goalTolerancePosition = 0.05
        self._lastMoveFailed = False
        self._maxPositionChange = 0.025

        self._environmentController.startController()

        self._operatingArea = operatingArea #min xyz, max xyz
        self._sample_initial_ee_position = True
        self._sample_initial_cube_position = True


        self.success_ratio_buff_size = 50
        self.outcomes = [0]*self.success_ratio_buff_size
        self.successes = 0
        self.episodes = 0
        self.outcome_buff_i = -1

        self._velocity_scaling = 0.8
        self._acceleration_scaling = 0.6

        self._ee_height = 0.025

        self._reset_counter = 0
        self._successRatio = 0

        self._pushing_orient_quat_xyzw = [-0.70710678,0.70710678,0.0,0.0]
        self._reset_orientation_quat_xyzw = [1.0,0.0,0.0,0.0]

        
    def getObservation(self, state) -> np.ndarray:
        vec_obs = state[0][[self.VEC_STATE_IDX.TIP_X,
                            self.VEC_STATE_IDX.TIP_Y,
                            self.VEC_STATE_IDX.CUBE_X,
                            self.VEC_STATE_IDX.CUBE_Y,
                            self.VEC_STATE_IDX.CUBE_YAW,
                            self.VEC_STATE_IDX.TIME]]
        if np.any(np.isnan(vec_obs)):
            raise RuntimeError(f"Nan detected in observation.\n vec = {vec_obs}")
        if np.any(np.isinf(vec_obs)):
            raise RuntimeError(f"inf detected in observation.\n vec = {vec_obs}")
        return {"vec":vec_obs}

    def getState(self) -> State:
        """Get an observation of the environment.

        Returns
        -------
        NDArray[(15,), np.float32]
            numpy ndarray. The content of each field is specified at the self.observation_space_high definition

        """

        linkStates = self._environmentController.getLinksState(requestedLinks=[("panda","pushing_ee_tip"),("cube","cube")])
        tipPose = linkStates[("panda","pushing_ee_tip")].pose
        cubePose = linkStates[("cube","cube")].pose
        # ggLog.info("Got links states")

        jointStates = self._environmentController.getJointsState([("panda","panda_joint1"),
                                                                 ("panda","panda_joint2"),
                                                                 ("panda","panda_joint3"),
                                                                 ("panda","panda_joint4"),
                                                                 ("panda","panda_joint5"),
                                                                 ("panda","panda_joint6"),
                                                                 ("panda","panda_joint7")])
        # ggLog.info("Got joint states")


        tip_quat = quaternion.from_float_array([tipPose.orientation.w,tipPose.orientation.x,tipPose.orientation.y,tipPose.orientation.z])
        tip_orientation_rpy = quaternion.as_euler_angles(tip_quat)
        # ggLog.info(f"tip_quat = {tip_quat}")
        # ggLog.info(f"tip_orientation_rpy = {tip_orientation_rpy}")

        cube_quat = quaternion.from_float_array([cubePose.orientation.w,cubePose.orientation.x,cubePose.orientation.y,cubePose.orientation.z])
        cube_orientation_rpy = quaternion.as_euler_angles(cube_quat)

        #print("got ee pose "+str(eePose))

        #print(jointStates)

        #Real franka gripper does not give force information, effort and evlocity are always zero in joint_states

        vec_state = [None]*len(self.VEC_STATE_IDX)

        vec_state[self.VEC_STATE_IDX.TIP_X] = tipPose.position[0]
        vec_state[self.VEC_STATE_IDX.TIP_Y] = tipPose.position[1]
        vec_state[self.VEC_STATE_IDX.TIP_Z] = tipPose.position[2]
        vec_state[self.VEC_STATE_IDX.TIP_ROLL] = tip_orientation_rpy[0]
        vec_state[self.VEC_STATE_IDX.TIP_PITCH] = tip_orientation_rpy[1]
        vec_state[self.VEC_STATE_IDX.TIP_YAW] = tip_orientation_rpy[2]
        vec_state[self.VEC_STATE_IDX.JOINT1_POS] = jointStates[("panda","panda_joint1")].position[0]
        vec_state[self.VEC_STATE_IDX.JOINT2_POS] = jointStates[("panda","panda_joint2")].position[0]
        vec_state[self.VEC_STATE_IDX.JOINT3_POS] = jointStates[("panda","panda_joint3")].position[0]
        vec_state[self.VEC_STATE_IDX.JOINT4_POS] = jointStates[("panda","panda_joint4")].position[0]
        vec_state[self.VEC_STATE_IDX.JOINT5_POS] = jointStates[("panda","panda_joint5")].position[0]
        vec_state[self.VEC_STATE_IDX.JOINT6_POS] = jointStates[("panda","panda_joint6")].position[0]
        vec_state[self.VEC_STATE_IDX.JOINT7_POS] = jointStates[("panda","panda_joint7")].position[0]
        vec_state[self.VEC_STATE_IDX.ACTION_FAILS] = self._environmentController.actionsFailsInLastStep()
        vec_state[self.VEC_STATE_IDX.CUBE_X] = cubePose.position[0]
        vec_state[self.VEC_STATE_IDX.CUBE_Y] = cubePose.position[1]
        vec_state[self.VEC_STATE_IDX.CUBE_YAW] = cube_orientation_rpy[2]
        vec_state[self.VEC_STATE_IDX.TIME] = self._stepCounter/self._maxStepsPerEpisode*2-1
        
        vec_state_arr = np.array(vec_state,dtype=np.float32)

        return [vec_state_arr]

    def buildSimulation(self, backend : str = "gazebo"):
        if backend == "gazebo":
            self._mmRosLauncher = lr_gym_utils.ros_launch_utils.MultiMasterRosLauncher( rospkg.RosPack().get_path("autoencoding_rl")+
                                                                                            "/launch/panda_pushing.launch",
                                                                                            cli_args=[  "gui:=false",
                                                                                                        "noplugin:=false",
                                                                                                        "enable_depth:=false",
                                                                                                        "color_fps:=0.1",
                                                                                                        "wall_sim_speed:="+str(self._wall_sim_speed),
                                                                                                        "gazebo_seed:="+str(self._seed)
                                                                                                        ])
            self._mmRosLauncher.launchAsync()
        elif backend == "real":
            if self._real_robot_pc_ip is None:
                raise AttributeError("real_robot_pc_ip must be set to use the real setup.")
            self._mmRosLauncher = lr_gym_utils.ros_launch_utils.MultiMasterRosLauncher( rospkg.RosPack().get_path("autoencoding_rl")+
                                                                                            "/launch/panda_pushing.launch",
                                                                                            cli_args=[  "simulated:=false",
                                                                                                        "remote_robot:=true"],
                                                                                            basePort = 11311,
                                                                                            ros_master_ip = self._real_robot_pc_ip)
            self._mmRosLauncher.launchAsync()
        else:
            raise NotImplementedError("Backend '"+backend+"' not supported")
