#!/usr/bin/env python3

import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import argparse
import time
import signal
import os
import pandas
from typing import List
import numpy as np

fig = None
ax = None

def makePlot(   csvfile : str,
                x_data_id : str,
                max_x : float,
                y_data_ids : List[str],
                y_error_ids : List[str],
                y_data_colors=List[np.ndarray]):
    df = pd.read_csv(csvfile)

    if max_x is not None:
        df = df.loc[df[x_data_id] < max_x]

    # print(f"{len(df[x_data_id])} x samples")

    # sns.set_theme(style="darkgrid")
    global fig
    global ax
    if fig is None:
        fig, ax = plt.subplots(1, 1)
    ax.cla()

    #sns.set_style("dark")
    #sns.set_context("paper")
    for i in range(len(y_data_ids)):
        y_data_id = y_data_ids[i]
        y_error_id = y_error_ids[i]
        color = y_data_colors[i]
        color_err = (color + np.array([255,255,255], dtype=np.int32))/2
        color_err = color_err.astype(np.int32)
        color = color.astype(np.int32)
        print(f"color = {color}")
        print(f"color_err = {color_err}")
        color = "#%0.2X"%color[0].item()+"%0.2X"%color[1].item()+"%0.2X"%color[2].item()
        color_err = "#%0.2X"%color_err[0].item()+"%0.2X"%color_err[1].item()+"%0.2X"%color_err[2].item()


        # print(f"{len(df[y_data_id])} y {y_data_id} samples")
        ax.errorbar(x=df[x_data_id],
                    y=df[y_data_id],
                    yerr=df[y_error_id],
                    color=color, 
                    ecolor = color_err,
                    label = y_data_id) #, ax = ax) #
        # p.set_yscale("log")
    #plt.legend(loc='lower right', labels=names)
    ax.legend()
    ax.set_title(os.path.dirname(args["csvfile"]).split("/")[-1]+"/"+os.path.basename(csvfile))
    if max_x is not None:
        p.set_xlim(-10,max_x)
    fig.tight_layout()



ctrl_c_received = False
def signal_handler(sig, frame):
    #print('You pressed Ctrl+C!')
    global ctrl_c_received
    ctrl_c_received = True

ap = argparse.ArgumentParser()
ap.add_argument("--csvfile", required=True, type=str, help="Csv file to read from")
ap.add_argument("--nogui", default=False, action='store_true', help="Dont show the plot window, just save to file")
ap.add_argument("--once", default=False, action='store_true', help="Plot only once")
ap.add_argument("--useepisodesx", default=False, action='store_true', help="Use episodes on the x axis")
# ap.add_argument("--usestepsx", default=False, action='store_true', help="Use steps count on the x axis")
ap.add_argument("--maxx", required=False, default=None, type=float, help="Maximum x value to plot")
ap.add_argument("--period", required=False, default=5, type=float, help="Seconds to wait between plot update")
ap.add_argument("--steps", default=False, action='store_true', help="Plot episode steps instead of reward")
ap.set_defaults(feature=True)
args = vars(ap.parse_args())
signal.signal(signal.SIGINT, signal_handler)

matplotlib.rcParams['figure.raise_window'] = False
#matplotlib.use('Tkagg')
if args["nogui"]:
    plt.ioff()
else:
    plt.ion()

if args["useepisodesx"]:
    x_data_id = 'episodes'
else:
    x_data_id = "env_steps"

if args["steps"]:
    y_data_ids=["steps_mean"]
    y_error_ids=["steps_std"]
else:
    y_data_ids=["reward_mean"]
    y_error_ids=["reward_std"]

csvfile = os.path.abspath(args["csvfile"])
#fig, ax = plt.subplots(figsize=(11, 8.5))
while not ctrl_c_received:
    #print("Plotting")
    try:
        makePlot(csvfile,
                 x_data_id,
                 max_x = args["maxx"],
                 y_data_ids=y_data_ids,
                 y_error_ids=y_error_ids,
                 y_data_colors=[np.array([255,64,64], dtype=np.int32),np.array([64,64,255], dtype=np.int32),np.array([64,255,64], dtype=np.int32)])
        plt.savefig(csvfile+".plot.pdf")
        #plt.show(block=True)
        if not args["nogui"]:
            #plt.draw()
            if args["once"]:
                plt.show(block=True)
                break
            else:
                plt.show(block=False)
            plt.pause(0.01)
    except pandas.errors.EmptyDataError:
        print("No data...")
    except FileNotFoundError:
        print("File not present...")
    if args["once"]:
        break
    plt.pause(args["period"])
