from typing import Final
import torch as th
import torch.nn as nn
from typing import List
from copy import deepcopy

# @th.jit.script
def _run_efficient(x : th.Tensor, modules : List[th.nn.Module], streams):
    current_stream = th.cuda.current_stream()
    outs = [None] * len(modules)
    inputs_num = x.size()[0]
    if inputs_num == 1:
        for i in range(len(modules)):
            with th.cuda.stream(streams[i]):
                streams[i].wait_stream(current_stream)
                outs[i]=modules[i](x[0])
    else:
        for i in range(len(modules)): # apparently should use streams or something like that
            with th.cuda.stream(streams[i]):
                streams[i].wait_stream(current_stream)
                outs[i]=modules[i](x[i])
    for s in streams:
        current_stream.wait_stream(s)
    return th.stack( outs, dim=1 )

class Parallel(nn.ModuleList):
    """
    Parallelly runs provided modules. Returns one batch containig an ensemble of outputs in each element.
    E.g.: You have 3 submodules, each returning an output of size (5,), you input a (1024,10) batch, you get a (1024,3,5) output
    """
    def __init__(self, modules, return_mean : bool = False):
        super().__init__( modules )
        self._output_mean : Final = return_mean
        self._streams = [th.cuda.Stream() for _ in range(len(modules))]

    def forward(self, x):
        # out = []
        # if isinstance(x,list) or isinstance(x,tuple):
        #     i = 0
        #     for submodule in self:
        #         out.append(submodule(x[i]))
        # else:
        #     for submodule in self:
        #         out.append(submodule(x))
                
        # stacked_outs = th.stack( out, dim=1 )

        if isinstance(x,list) or isinstance(x,tuple):
            x_tens = th.stack(x, dim = 0)
        else:
            x_tens = x.unsqueeze(0)

        modules = list(self)
        
        # streams = [th.cuda.Stream() for _ in range(len(modules))]
        streams = [th.cuda.current_stream() for _ in range(len(modules))]
        # streams = self._streams
        stacked_outs = _run_efficient(x_tens, modules, streams)

        if self._output_mean:
            return th.mean(stacked_outs, dim=1)
        else:
            return stacked_outs

    def __deepcopy__(self, memo):
        # create a copy with self.linked_to *not copied*, just referenced.
        streams = self._streams
        self._streams = None
        thisfunc = self.__deepcopy__
        self.__deepcopy__ = None
        copy = deepcopy(self, memo)
        self.__deepcopy__ = thisfunc
        copy.__deepcopy__ = deepcopy(thisfunc, memo)
        self._streams = streams
        copy._streams = [th.cuda.Stream() for _ in range(len(streams))]
        return copy