#!/bin/bash

rostopic pub /move_helper/move_to_ee_pose/goal lr_gym_utils/MoveToEePoseActionGoal "header:         
  seq: 0                                                                                                                                                                                      
  stamp:                                                                                                                                                                                      
    secs: 0                                                                                                                                                                                   
    nsecs: 0                                                                                                                                                                                  
  frame_id: ''                                                                                                                                                                                
goal_id:                                                                                                                                                                                      
  stamp:                                                                                                                                                                                      
    secs: 0                                                                                                                                                                                   
    nsecs: 0                                                                                                                                                                                  
  id: ''                                                                                                                                                                                      
goal:                                                                                                                                                                                         
  pose:                                                                                                                                                                                       
    header:                                                                                                                                                                                   
      seq: 0                                                                                                                                                                                  
      stamp: {secs: 0, nsecs: 0}                                                                                                                                                              
      frame_id: 'world'                                                                                                                                                                       
    pose:                                                                                                                                                                                     
      position: {x: 0.525, y: 0.0, z: 0.1}                                                                                                                                                    
      orientation: {x: 1.0, y: 0.0, z: 0.0, w: 0.0}                                                                                                                                           
  end_effector_link: 'panda_tcp'                                                                                                                                                              
  do_cartesian: false                                                                                                                                                                         
  velocity_scaling: 0.1                                                                                                                                                                       
  acceleration_scaling: 0.1"