#!/usr/bin/env python3

import rospy
import actionlib
import lr_gym_utils.msg
import lr_gym
import lr_gym.utils.utils
import numpy as np

def connectRosAction(actionName : str, msgType):
    ac = actionlib.SimpleActionClient(actionName, msgType)
    rospy.loginfo("Waiting for action "+ac.action_client.ns+"...")
    ac.wait_for_server()
    rospy.loginfo(ac.action_client.ns+" connected.")
    return ac

def waitMove(moveEeClient, goal):
    r = moveEeClient.wait_for_result()
    if r:
        if moveEeClient.get_result().succeded:
            # ggLog.info("waited cartesian....")
            return
        else:
            raise RuntimeError(f"Failed to move to cartesian pose. Goal={goal}. result = "+str(moveEeClient.get_result()))
    else:
        moveEeClient.cancel_goal()
        moveEeClient.cancel_all_goals()
        r = moveEeClient.wait_for_result(timeout = rospy.Duration(10.0))
        if r:
            raise RuntimeError(f"Failed to move to cartesian pose: action timed out. Action canceled. Goal={goal}. Result = {moveEeClient.get_result()}")
        else:
            raise RuntimeError(f"Failed to move to cartesian pose: action timed out. Action failed to cancel. Goal={goal}")


rospy.init_node('show_operating_area', anonymous=True)

height = 0.005
area = np.array([[0.30, -0.225, 0], [0.75, 0.225, 0.5]])
center = (area[0] + area[1])/2
center[2] = height
corners = [center,
           [area[0,0], area[0,1],height],
           [area[0,0], area[1,1],height],
           [area[1,0], area[1,1],height],
           [area[1,0], area[0,1],height]]
moveEeClient = connectRosAction('/move_helper/move_to_ee_pose', lr_gym_utils.msg.MoveToEePoseAction)

goal = lr_gym_utils.msg.MoveToEePoseGoal()
goal.end_effector_link = "pushing_ee_tip"
goal.velocity_scaling = 0.1
goal.acceleration_scaling = 0.1
goal.do_cartesian = False
orientation_xyzw = [0.924, 0.383, 0, 0]
for c in corners:
    rospy.loginfo(f"Moving over {c}...")
    c = np.array(c)
    overdiff = np.array([0.0,0.0,height])
    goal.pose = lr_gym.utils.utils.buildPoseStamped(c+overdiff,orientation_xyzw, "world")
    moveEeClient.send_goal(goal)
    waitMove(moveEeClient,goal)
    rospy.loginfo(f"Moving to {c}...")
    rospy.sleep(1.0)
    goal.pose = lr_gym.utils.utils.buildPoseStamped(c,orientation_xyzw, "world")
    goal.do_cartesian = True
    moveEeClient.send_goal(goal)
    waitMove(moveEeClient,goal)
    rospy.sleep(1.0)
    input("Press enter to continue")
    goal.do_cartesian = False
    rospy.loginfo(f"Moving up from {c}...")
    goal.pose = lr_gym.utils.utils.buildPoseStamped(c+overdiff,orientation_xyzw, "world")
    moveEeClient.send_goal(goal)
    waitMove(moveEeClient,goal)


rospy.spin()
