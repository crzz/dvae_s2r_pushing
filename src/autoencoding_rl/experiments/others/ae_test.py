#!/usr/bin/env python3

import time
import lr_gym
import inspect
import lr_gym.utils.dbg.ggLog as ggLog
from lr_gym.envs.CartpoleContinuousVisualEnv import CartpoleContinuousVisualEnv
from lr_gym.envs.GymEnvWrapper import GymEnvWrapper
from lr_gym.envs.RecorderGymWrapper import RecorderGymWrapper
from lr_gym.envControllers.GazeboController import GazeboController
from autoencoding_rl.latent_extractors.SimpleAutoencoder import SimpleAutoencoder
from autoencoding_rl.utils import Transition, ImageComparisonWindow    

import torch as th
import numpy as np

def saveDbgImg(sae, obss, comparisonWindow, id):
    with th.no_grad():
        preprocessed_obs_batch = sae.preprocess_observations(obss.to("cuda"))
        encodedDecoded_obs_batch = sae(preprocessed_obs_batch)

        encodedDecoded_obs_batch = sae.postprocess_observations(encodedDecoded_obs_batch)
        preprocessed_obs_batch = sae.postprocess_observations(preprocessed_obs_batch)
    
    comparisonWindow.displayExample([preprocessed_obs_batch,
                                     preprocessed_obs_batch,
                                     encodedDecoded_obs_batch,
                                     encodedDecoded_obs_batch],
                                    id = id)

def sample_batch(obss, sampleSize):
    obs_count = obss.size()[0]
    p = th.ones(obs_count, device='cuda') / obs_count
    idx = p.multinomial(num_samples=sampleSize, replacement=False)
    return obss[idx]


def main():
    logFolder = lr_gym.utils.utils.lr_gym_startup(__file__, inspect.currentframe(), folderName = "ae_test/"+str(int(time.time())))
    
    RANDOM_SEED=0

    img_height = 128
    img_width = 128
    targetFps = 10
    stepLength_sec = (1/targetFps)/3 #Frame stacking reduces by 3 the fps
    videoSaveFreq_ep = 50
    env = RecorderGymWrapper(GymEnvWrapper(CartpoleContinuousVisualEnv(startSimulation = True,
                                                    simulatorController = GazeboController(stepLength_sec = stepLength_sec),
                                                    stepLength_sec = stepLength_sec,
                                                    obs_img_height_width = (img_height,img_width),
                                                    wall_sim_speed = False,
                                                    seed = RANDOM_SEED,
                                                    continuousActions = True),
                                            episodeInfoLogFile = logFolder+"/GymEnvWrapper_log.csv"),
                            fps = targetFps, outFolder = logFolder+"/videos/RecorderGymEnvWrapper", saveBestEpisodes = True,
                            saveFrequency_ep = videoSaveFreq_ep)

    #setup seeds for reproducibility
    env.action_space.seed(RANDOM_SEED)
    device = "cuda"

    lr_gym.utils.utils.torch_selectBestGpu()

    time.sleep(10)

    observations = []
    for epcount in range(100):
        done = False
        observation = env.reset()
        observations.append(observation)
        while not done:
            observation, reward, done, info = env.step(env.action_space.sample())
            observations.append(observation)
    obs_count = len(observations)
    env.close()

    sae = SimpleAutoencoder(encoding_size = 5, image_channels_num = 3, net_input_width = img_width, net_input_height = img_height)
    sae.to(device)
    optimizer = th.optim.Adam(sae.parameters(), lr = 0.001)
    optimizer.zero_grad()

    observations = th.tensor(np.array(observations), device = device)

    comparisonWindow = ImageComparisonWindow(outFolder = logFolder+"/sae_dbg")

    print("Training....")
    with th.set_grad_enabled(True):
        for epoch in range(50):
            for iteration in range(40):
                if iteration % 2 == 0:
                    sae.decoder.requires_grad_(False)
                    sae.encoder.requires_grad_(True)
                else:
                    sae.decoder.requires_grad_(True)
                    sae.encoder.requires_grad_(False)

                optimizer.zero_grad()
                batch = sample_batch(observations,128)
                preproc_batch = sae.preprocess_observations(batch)
                loss = sae.compute_loss(preproc_batch)
                loss.backward()
                th.nn.utils.clip_grad_value_(sae.parameters(), 10)
                optimizer.step()
            dbgObss = sample_batch(observations,5)
            saveDbgImg(sae,dbgObss,comparisonWindow, id = f"epoch_{epoch}")
            print(f"Epoch {epoch}: loss = {loss.item()}")



if __name__ == "__main__":
    main()