#!/usr/bin/env python3

import os
from lr_gym.envs.GymEnvWrapper import GymEnvWrapper
from lr_gym.envs.RecorderGymWrapper import RecorderGymWrapper

from lr_gym.envs.CartpoleContinuousVisualEnvDict import CartpoleContinuousVisualEnvDict
from lr_gym.envs.GymEnvWrapper import GymEnvWrapper
from lr_gym.envs.RecorderGymWrapper import RecorderGymWrapper
from lr_gym.envControllers.GazeboController import GazeboController



def build_cartpole_env(seed : int, logFolder : str, videoSaveFreq_ep = 10, pixel_resolution = 64):
    img_height = pixel_resolution
    img_width = pixel_resolution
    targetFps = 10
    stepLength_sec = (1/targetFps)/3 #Frame stacking reduces by 3 the fps
    videoSaveFreq_ep = 50
    env = RecorderGymWrapper(GymEnvWrapper(CartpoleContinuousVisualEnvDict(startSimulation = True,
                                                    simulatorController = GazeboController(stepLength_sec = stepLength_sec),
                                                    stepLength_sec = stepLength_sec,
                                                    obs_img_height_width = (img_height,img_width),
                                                    wall_sim_speed = False,
                                                    seed = seed,
                                                    continuousActions = True),
                                            episodeInfoLogFile = logFolder+"/GymEnvWrapper_log.csv"),
                            fps = targetFps, outFolder = logFolder+"/videos/RecorderGymEnvWrapper", saveBestEpisodes = True,
                            saveFrequency_ep = videoSaveFreq_ep)
    #setup seeds for reproducibility
    env.action_space.seed(seed)

    return env, targetFps


def env_builder(seed, logFolder, is_eval, env_builder_args):
    return build_cartpole_env(  seed=seed,
                                logFolder=logFolder,
                                videoSaveFreq_ep = 1 if is_eval else 10,
                                pixel_resolution=env_builder_args["pixel_resolution"])