#!/usr/bin/env python3

import os
import dmc2gym
import dmc2gym.wrappers

from lr_gym.envs.GymEnvWrapper import GymEnvWrapper
from lr_gym.envs.GymToLr import GymToLr
from lr_gym.envs.ObsToDict import ObsToDict
from lr_gym.envs.RecorderGymWrapper import RecorderGymWrapper

import drqv2.dmc
import lr_gym.utils.dbg.ggLog as ggLog


def build_dmcartpole_env(seed : int, action_repeat : int, logFolder : str, videoSaveFreq_ep = 10, noimg : bool = False, task = "balance_sparse"):
    base_stepLength_sec = 0.01
    stepLength_sec = base_stepLength_sec*(action_repeat)
    targetFps = 1/stepLength_sec
    pixel_resolution = 84
    episode_length = 1000/action_repeat
        
    if noimg:
        env = GymEnvWrapper(ObsToDict(GymToLr(dmc2gym.make(   "cartpole",
                                                                task,
                                                                seed=seed,
                                                                visualize_reward=False,
                                                                from_pixels=False,
                                                                frame_skip=action_repeat,
                                                                episode_length=episode_length*action_repeat,
                                                                flatten_obs=True),
                                                stepSimDuration_sec=base_stepLength_sec,
                                                maxStepsPerEpisode = episode_length),
                                    key="vec"),
                            episodeInfoLogFile = logFolder+"/GymEnvWrapper_log.csv")
    else:
        env = GymEnvWrapper(ObsToDict(GymToLr(dmc2gym.wrappers.DMCWrapper(env = drqv2.dmc.make(f"cartpole_{task}",
                                                                                                frame_stack=3,
                                                                                                action_repeat=action_repeat,
                                                                                                seed=seed,
                                                                                                pixel_size=pixel_resolution),
                                                                                            task_kwargs = {'random' : seed}),
                                                                    stepSimDuration_sec=base_stepLength_sec,
                                                                    maxStepsPerEpisode = episode_length),
                                                        key = "camera"),
                                                episodeInfoLogFile = logFolder+"/GymEnvWrapper_log.csv")
    # ggLog.info(f"env.observation_space = {env.observation_space}, dtype = {env.observation_space.dtype}")
    # if isinstance(env.observation_space, gym.spaces.Dict):
    #     for subname, subspace in env.observation_space.spaces.items():
    #         ggLog.info(f"env.observation_space['{subname}'] = {subspace}, dtype = {subspace.dtype}")

    #setup seeds for reproducibility
    env.action_space.seed(seed)


    # ggLog.info(f"observation_space = {env.observation_space}")
    # ggLog.info(f"action_space = {env.action_space}")
    return env, targetFps
