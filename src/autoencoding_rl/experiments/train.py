from builtins import NotImplementedError
import inspect
import os
import os.path
import time
from doctest import UnexpectedException
from typing import Callable, Tuple

import gym
import lr_gym
import lr_gym.utils.dbg.ggLog as ggLog
import torch as th
from autoencoding_rl.AeRecorderGymWrapper import AeRecorderGymWrapper
from lr_gym.envs.RecorderGymWrapper import RecorderGymWrapper
from autoencoding_rl.AutoencodingSAC2 import AutoencodingSAC2
from autoencoding_rl.DecoupledExtractorPolicy import DecoupledExtractor
from autoencoding_rl.latent_extractors.dyn_vae.MultiObsDynVAE3 import \
    MultiObsDynVAE3
from autoencoding_rl.latent_extractors.dyn_vae.MultiObsVAE3 import \
    MultiObsVAE3
from autoencoding_rl.latent_extractors.DynLatentExtractor import \
    DynLatentExtractor
from autoencoding_rl.latent_extractors.FakeLatentExtractor import \
    FakeLatentExtractor
from lr_gym.utils.sb3_callbacks import CheckpointCallbackRB, EvalCallback_ep
from stable_baselines3 import SAC
from autoencoding_rl.buffers import GenericHerReplayBuffer, RandomHoldoutBuffer, ThDictReplayBuffer_updatable
from stable_baselines3.common.vec_env import VecEnv, DummyVecEnv
from stable_baselines3.her.her_replay_buffer import HerReplayBuffer


default_args = dict(automatic_grad_steps = False,
                    le_arch_dynamics = [1024],
                    le_arch_enc_ensemble_size = 3,
                    le_arch_frame_stack_size = 1,
                    le_arch_img_dec_ensemble_size = 1,
                    le_arch_img_encoding_size = 20,            
                    le_arch_reward = None,
                    le_arch_reward_ensemble_size = 1,
                    le_arch_state_combiner = "identity",
                    le_arch_type = "dvae3",
                    le_arch_use_coord_conv = True,
                    le_arch_vec_decoder = "identity",
                    le_arch_vec_encoder = "identity",
                    le_arch_backbone = "conv_small",
                    le_batch_size = 96,
                    le_bestModelThreshold = 0.95,
                    le_decoder_weight_decay_lambda = 1e-7,
                    le_arch_deconv_backbone = "conv",
                    le_disable_validation_set = False,
                    le_arch_dyn_ensemble_size = 3,
                    le_encoding_noise_std = 0.0,
                    le_freeze_decoders = False,
                    le_freeze_dynamics = False,
                    le_freeze_encoders = False,
                    le_grad_steps = 50,
                    le_loss_kld_weight = 0.01,
                    le_loss_latent_weight = 1e-6,
                    le_learning_starts = 10000,
                    le_loss_use_log = True,
                    le_lr = 0.005,
                    le_pretrain_grad_steps = 20,
                    le_loss_reconstruction_weight = 0.0,
                    le_retrain_epochs = 50,
                    le_retrain_period = -1,
                    le_reset_decoder = False,
                    le_reset_encoder = False,
                    le_reset_on_retrain = False,
                    le_loss_reward_weight = 0.00001,
                    le_reward_scaling = 1,
                    le_tau = 1.0,
                    le_train_freq = (1,"episode"),
                    le_use_log_reward = False,
                    le_validation_batch_size = 256,
                    le_validation_ratio = 0.01,
                    le_loss_vec_weight = 0.005,
                    parallelize_experience_collection = False,
                    replay_buffer_size = 100000,
                    sac_arch_actor_ensemble_size = 1,
                    sac_arch_policy = [1024],
                    sac_batchSize = 256,
                    sac_ent_coef = "auto_0.1",
                    sac_gamma = 0.99,
                    sac_grad_steps = 250,
                    sac_learning_starts = 10000,
                    sac_lr = 0.001,
                    sac_target_entropy = "auto",
                    sac_tau = 0.005,
                    sac_train_freq=(1,"episode"),
                    storage_torch_device = "auto_cuda",
                    random_steps = None,

                    ae_video_save_freq_ep = 10,
                    checkpointing_freq_ep = 100,
                    checkpointing_save_best=True,
                    debug = True,
                    env_builder_args = {},
                    eval_determiniastic = False,
                    eval_period_ep = 10,
                    folderName = None,
                    gpuid = None,
                    load_replay_buffer = False,
                    loading_pretrained = False,                    
                    modelFile = None,
                    run_id_prefix = "",
                    save_replay_buffer = False,
                    seed = 0,
                    trainSteps = 1000000,
                    use_her = False,
                    video_save_freq_ep = -1)


def daesac_train(   env_builder : Callable[[bool, int, str],Tuple[gym.Env, float]],
                    
                    le_arch_backbone,
                    le_arch_deconv_backbone,
                    le_arch_dyn_ensemble_size,
                    le_arch_dynamics,
                    le_arch_enc_ensemble_size,
                    le_arch_frame_stack_size,
                    le_arch_img_dec_ensemble_size,
                    le_arch_img_encoding_size,            
                    le_arch_reward,
                    le_arch_reward_ensemble_size,
                    le_arch_state_combiner,
                    le_arch_type,
                    le_arch_use_coord_conv,
                    le_arch_vec_decoder,
                    le_arch_vec_encoder,

                    le_batch_size,
                    le_bestModelThreshold,
                    le_decoder_weight_decay_lambda,
                    le_disable_validation_set,
                    le_encoding_noise_std,
                    le_freeze_decoders,
                    le_freeze_dynamics,
                    le_freeze_encoders,
                    le_grad_steps,
                    le_learning_starts,
                    le_loss_kld_weight,
                    le_loss_latent_weight,
                    le_loss_reconstruction_weight,
                    le_loss_reward_weight,
                    le_loss_use_log,
                    le_loss_vec_weight,
                    le_lr,
                    le_pretrain_grad_steps,
                    le_reset_decoder,
                    le_reset_encoder,
                    le_reset_on_retrain,
                    le_retrain_epochs,
                    le_retrain_period,
                    le_reward_scaling,
                    le_tau,
                    le_train_freq,
                    le_use_log_reward,
                    le_validation_batch_size,
                    le_validation_ratio,

                    automatic_grad_steps,
                    parallelize_experience_collection,
                    replay_buffer_size,

                    sac_arch_actor_ensemble_size,
                    sac_arch_policy,
                    sac_batchSize,
                    sac_ent_coef,
                    sac_gamma,
                    sac_grad_steps,
                    sac_learning_starts,
                    sac_lr,
                    sac_target_entropy,
                    sac_tau,
                    sac_train_freq,
                    storage_torch_device,
                    random_steps,

                    ae_video_save_freq_ep = 10,
                    checkpointing_freq_ep = 10,
                    checkpointing_save_best=True,
                    debug = True,
                    env_builder_args = {},
                    eval_deterministic = False,
                    eval_period_ep = 10,
                    folderName : str = None,
                    gpuid = None,
                    load_replay_buffer = False,
                    loading_pretrained : bool = False,                    
                    modelFile = None,
                    run_id_prefix : str = "",
                    save_replay_buffer = False,
                    seed : int = 0,
                    trainSteps = 1000000,
                    use_her : bool = False,
                    video_save_freq_ep = -1,
                    start_collecting = None,
                    ae_save_pickles = False,
                    preevaluation_episodes = 0
                    ) -> None:
    """

    """
 
    if start_collecting is not None and start_collecting>0:
        raise NotImplementedError("start_collecting is not supported yet")

    logFolder = lr_gym.utils.utils.lr_gym_startup(__file__, inspect.currentframe(), run_id_prefix = run_id_prefix, folderName = folderName, seed = seed)
    

    th.backends.cuda.matmul.allow_tf32 = False
    th.backends.cudnn.allow_tf32 = False
    
    env, targetFps = env_builder(seed, logFolder, False, env_builder_args)
    if ae_video_save_freq_ep>0:
        aeRecEnv = AeRecorderGymWrapper(env,targetFps,logFolder+"/videos/AeRecorderGymWrapper",saveBestEpisodes=True,saveFrequency_ep=ae_video_save_freq_ep,
                                        save_pickles=ae_save_pickles)
        env = aeRecEnv
    if video_save_freq_ep>0:
        recEnv = RecorderGymWrapper(env,
                                    fps = targetFps, outFolder = logFolder+"/videos/RecorderGymWrapper",
                                    saveBestEpisodes = True,
                                    saveFrequency_ep = video_save_freq_ep)
        env = recEnv
    if eval_period_ep>=0:
        eval_env = env_builder(seed, logFolder+"/eval", True, env_builder_args)[0]
        eval_aeRecEnv = AeRecorderGymWrapper(eval_env,targetFps,logFolder+"/eval/videos/AeRecorderGymWrapper",saveBestEpisodes=True,saveFrequency_ep=1)
        eval_env = eval_aeRecEnv
    else:
        eval_env = None

    try:
        if gpuid is None:
            device = lr_gym.utils.utils.torch_selectBestGpu()
        else:
            th.cuda.set_device(gpuid)
            device = th.device(type="cuda", index= gpuid)
        if storage_torch_device == "auto_cuda":
            storage_torch_device = device
        
        print("Built environment, will now start...")
        time.sleep(1)

        if not isinstance(env, VecEnv):
            env = DummyVecEnv([lambda: env])

        if use_her:
            replay_buffer_class = HerReplayBuffer
            replay_buffer_kwargs = {"replay_buffer_class" : RandomHoldoutBuffer,
                                    "replay_buffer_kwargs" : {  "buffer_class" : ThDictReplayBuffer_updatable,
                                                                "validation_ratio" : le_validation_ratio,
                                                                "disable" : le_disable_validation_set,
                                                                "storage_torch_device" : storage_torch_device},
                                    "n_sampled_goal" : 4,
                                    "online_sampling" : False}
        else:
            replay_buffer_class = RandomHoldoutBuffer
            replay_buffer_kwargs = {"buffer_class" : ThDictReplayBuffer_updatable,
                                    "validation_ratio" : le_validation_ratio,
                                    "disable" : le_disable_validation_set,
                                    "storage_torch_device" : storage_torch_device}
                                    
        if modelFile is None:
            augment_callback = None
            # augment_callback = nn.Sequential(   nn.ReplicationPad2d(4),
            #                                     kornia.augmentation.RandomCrop((pixel_resolution,pixel_resolution)))
            obs_space = env.observation_space # Passing env to the constructor function breaks pickling
            # print(obs_space)
            act_space = env.action_space
            def latent_extractor_constructor(debug_out_folder = logFolder+"/feature_extractor"):
                dae = None
                if le_arch_type == "dvae3":
                    dae = MultiObsDynVAE3(  observation_shape = obs_space,
                                            img_encoding_size = le_arch_img_encoding_size,
                                            action_size = act_space.shape[0],
                                            dynamics_nn_arch = le_arch_dynamics,
                                            rewardLossWeight = le_loss_reward_weight,
                                            kldLossWeight = le_loss_kld_weight,
                                            vecLossWeight = le_loss_vec_weight,
                                            torchDevice = device,
                                            encoder_ensemble_size = le_arch_enc_ensemble_size,
                                            dynamics_ensemble_size = le_arch_dyn_ensemble_size,
                                            backbone = le_arch_backbone,
                                            deconv_backbone = le_arch_deconv_backbone,
                                            augment_callback = augment_callback,
                                            state_combiner_nn_arch = le_arch_state_combiner,
                                            reconstructionLossWeight = le_loss_reconstruction_weight,
                                            frame_stack_size=le_arch_frame_stack_size,
                                            learning_rate = le_lr,
                                            vec_encoder_nn_arch = le_arch_vec_encoder,
                                            vec_decoder_nn_arch = le_arch_vec_decoder,
                                            img_decoder_ensemble_size = le_arch_img_dec_ensemble_size,
                                            reward_scaling=le_reward_scaling,
                                            reward_nn_arch=le_arch_reward,
                                            reward_ensemble_size = le_arch_reward_ensemble_size,
                                            log_reward = le_use_log_reward)
                elif le_arch_type == "vae3":
                    dae = MultiObsVAE3( observation_shape = obs_space,
                                        img_encoding_size = le_arch_img_encoding_size,
                                        action_size = act_space.shape[0],
                                        rewardLossWeight = le_loss_reward_weight,
                                        kldLossWeight = le_loss_kld_weight,
                                        vecLossWeight = le_loss_vec_weight,
                                        torchDevice = device,
                                        encoder_ensemble_size = le_arch_enc_ensemble_size,
                                        backbone = le_arch_backbone,
                                        deconv_backbone = le_arch_deconv_backbone,
                                        augment_callback = augment_callback,
                                        state_combiner_nn_arch = le_arch_state_combiner,
                                        reconstructionLossWeight = le_loss_reconstruction_weight,
                                        frame_stack_size=le_arch_frame_stack_size,
                                        learning_rate = le_lr,
                                        vec_encoder_nn_arch = le_arch_vec_encoder,
                                        vec_decoder_nn_arch = le_arch_vec_decoder,
                                        img_decoder_ensemble_size = le_arch_img_dec_ensemble_size,
                                        reward_scaling=le_reward_scaling,
                                        reward_nn_arch=le_arch_reward,
                                        reward_ensemble_size = le_arch_reward_ensemble_size,
                                        log_reward = le_use_log_reward)
                if dae is not None:
                    return DynLatentExtractor(  debug_out_folder = debug_out_folder,
                                                torchDevice = device,
                                                bestModelThreshold = le_bestModelThreshold,
                                                dyn_ae = dae,
                                                logarithmic_loss=le_loss_use_log)
                elif le_arch_type=="fake":
                    return FakeLatentExtractor( debug_out_folder = debug_out_folder,
                                                torchDevice = device,
                                                encoding_size = le_arch_img_encoding_size,
                                                observation_shape=obs_space,
                                                encoding_noise_std=le_encoding_noise_std)
                else:
                    raise UnexpectedException(f"Unexpected le_arch_type {le_arch_type}")

            model = AutoencodingSAC2(env,
                                    verbose=1,
                                    batch_size=sac_batchSize,
                                    buffer_size=replay_buffer_size, # 64*64*2*3*4*buf_size = 24576*buf_size = buf_size_bytes
                                    gamma=sac_gamma,
                                    learning_rate=sac_lr,
                                    policy_learning_starts=sac_learning_starts,
                                    policy_gradient_steps=sac_grad_steps,
                                    policy_train_period=sac_train_freq,
                                    seed = seed,
                                    device = device,
                                    ent_coef = sac_ent_coef,
                                    tau = sac_tau,
                                    target_entropy = sac_target_entropy,
                                    feature_extractor_batch_size = le_batch_size,
                                    feature_extractor_retrain_epochs = le_retrain_epochs,
                                    feature_extractor_grad_steps = le_grad_steps,
                                    feature_extractor_pretrain_grad_steps = le_pretrain_grad_steps,
                                    validation_batch_size=le_validation_batch_size,
                                    feature_extractor_train_period = le_train_freq,
                                    feature_extractor_retrain_period = le_retrain_period,
                                    feature_extractor_retrain_grad_steps = le_pretrain_grad_steps,
                                    feature_extractor_tau=le_tau,
                                    parallelize_experience_collection = parallelize_experience_collection,
                                    automatic_grad_steps=automatic_grad_steps,
                                    random_steps = random_steps,
                                    # policy_retrain_period=-1,
                                    # policy_retrain_grad_steps=100000,
                                    #feature_extractor_max_tot_grad_steps = 10*1500, #1500 episodes
                                    policy_kwargs= {"net_arch" : sac_arch_policy,
                                                    "actor_ensemble_size" : sac_arch_actor_ensemble_size,
                                                    "features_extractor_class" : DecoupledExtractor,
                                                    "share_features_extractor" : True,
                                                    "features_extractor_kwargs" :  {"latent_extractor_constructor":latent_extractor_constructor,
                                                                                    "debug_out_folder" : logFolder+"/feature_extractor"}},
                                    replay_buffer_class = replay_buffer_class,
                                    replay_buffer_kwargs = replay_buffer_kwargs,
                                    reset_on_retrain=le_reset_on_retrain)
        else:
            model = AutoencodingSAC2.load(modelFile, fe_logfolder = logFolder+'/feature_extractor', load_rng_state=not loading_pretrained,
                                          custom_objects={"buffer_size":replay_buffer_size,
                                                            "replay_buffer_class" : replay_buffer_class,
                                                            "replay_buffer_kwargs" : replay_buffer_kwargs,
                                                            "start_collecting" : start_collecting,
                                                            "random_steps" : random_steps})
            ggLog.info("Loaded model from "+modelFile)
            if load_replay_buffer:
                modelFolder = os.path.dirname(modelFile)
                _,_,ep,steps,succ_rate,_ = os.path.basename(modelFile).split(".")[0].split("_")
                replay_buffer_fname = f"model_checkpoint_replay_buffer_{ep}_{steps}_steps"
                replay_buffer_path = modelFolder+"/"+replay_buffer_fname
                model.load_replay_buffer(replay_buffer_path)
                ggLog.info("Loaded replay buffer from "+replay_buffer_path)
            model._last_obs = None #Last obs is invalid, this makes BaseAlgorithm reset the env (stable_baselines3/common/base_class.py:420), which is good
            model.set_env(env)

        if le_reset_encoder:
            model.get_latent_extractor().reset_encoders()
        if le_reset_decoder:
            model.get_latent_extractor().reset_decoders()
        if le_freeze_dynamics:
            model.get_latent_extractor().freeze_dynamics()
        if le_freeze_encoders:
            model.get_latent_extractor().freeze_encoder()
        if le_freeze_decoders:
            model.get_latent_extractor().freeze_decoder()

        
        model.get_latent_extractor().set_hyperparams(bestModelThreshold = le_bestModelThreshold,
                                                     logarithmic_loss=le_loss_use_log)

        for ae in model.get_latent_extractor().getAutoencoders().values():
            if type(ae) == MultiObsDynVAE3 or type(ae)==MultiObsVAE3:
                ae.set_hyperparams( reward_loss_weight = le_loss_reward_weight,
                                    kld_loss_weight = le_loss_kld_weight,
                                    vec_loss_weight = le_loss_vec_weight,
                                    reconstruction_loss_weight = le_loss_reconstruction_weight,
                                    checkDimensions = debug,
                                    checkFinite = debug,
                                    learning_rate = le_lr)
            else:
                raise NotImplementedError()
        model.set_hyperparams(   feature_extractor_batch_size = le_batch_size,
                                 feature_extractor_retrain_epochs =  le_retrain_epochs,
                                 feature_extractor_grad_steps =  le_grad_steps,
                                 feature_extractor_train_period =  le_train_freq,
                                 feature_extractor_retrain_period =  le_retrain_period,
                                 feature_extractor_pretrain_grad_steps =  le_pretrain_grad_steps,
                                 feature_extractor_learning_starts =  le_learning_starts,
                                 automatic_grad_steps =  automatic_grad_steps,
                                 validation_batch_size =  le_validation_batch_size,
                                 policy_learning_starts =  sac_learning_starts,
                                 policy_retrain_period =  None,
                                 policy_retrain_grad_steps =  None,
                                 policy_train_period =  sac_train_freq,
                                 policy_gradient_steps =  sac_grad_steps,
                                 feature_extractor_tau = le_tau,
                                 feature_extractor_retrain_grad_steps = le_pretrain_grad_steps,
                                 reset_on_retrain=le_reset_on_retrain,
                                 random_steps=random_steps)




        if ae_video_save_freq_ep>0:
            aeRecEnv.setLatentExtractor(model.get_latent_extractor())
        if eval_env is not None:
            eval_aeRecEnv.setLatentExtractor(model.get_latent_extractor())
        callbacks = []
        if checkpointing_freq_ep > 0:
            callbacks.append(CheckpointCallbackRB(save_freq_ep=checkpointing_freq_ep, save_path=logFolder+"/checkpoints",
                                                    name_prefix='model_checkpoint',
                                                    save_replay_buffer=save_replay_buffer,
                                                    save_freq=None,
                                                    save_best=checkpointing_save_best))
        if eval_period_ep>=0:
            callbacks.append(EvalCallback_ep(eval_env, best_model_save_path=logFolder+"/eval/EvalCallback",
                                            log_path=logFolder+"/eval/EvalCallback", eval_freq_ep=eval_period_ep,
                                            deterministic=eval_deterministic, render=False, verbose=True))

        if use_her:
            ggLog.info(f"Replay buffer will take {model.replay_buffer.replay_buffer.memory_size()/1024/1024/1024} GiB on {model.replay_buffer.replay_buffer.storage_torch_device()}")
        else:
            ggLog.info(f"Replay buffer will take {model.replay_buffer.memory_size()/1024/1024/1024} GiB on {model.replay_buffer.storage_torch_device()}")
        ggLog.info(f"{model.get_latent_extractor().getModelSizeRecap()}")


        if preevaluation_episodes > 0:
            ggLog.info(f"Starting preevaluation, will do {preevaluation_episodes} episodes")
            if eval_env is not None:
                preeval_env = eval_env
            else:
                preeval_env = env
            preeval_results = lr_gym.utils.utils.evaluatePolicy(env = preeval_env, model = model, episodes = preevaluation_episodes)
            formatted_results = ''.join([f'{k} : {v}\n' for k,v in preeval_results.items()])
            ggLog.info(f"preevaluation finished:\n{formatted_results}")


        ggLog.info("Learning...")
        t_preLearn = time.monotonic()
        model.learn(total_timesteps=trainSteps, callback=callbacks, reset_num_timesteps=loading_pretrained, eval_env = eval_env)
        duration_learn = time.monotonic() - t_preLearn
        ggLog.info("Learned. Took "+str(duration_learn)+" seconds.")
        model.save(logFolder+"/trained_model_trainSteps.zip")

    finally:
        env.close()
        if eval_env is not None:
            eval_env.close()


    return None










def sac_train(  seed,
                env_builder,
                batch_size,
                buffer_size,
                gamma,
                learning_rate,
                ent_coef,
                learning_starts,
                tau,
                gradient_steps,
                target_entropy,
                train_freq,
                net_arch,
                folderName,
                noise_sigma,
                env_builder_args = {},
                video_save_freq_ep = -1):

    th.backends.cuda.matmul.allow_tf32 = False
    th.backends.cudnn.allow_tf32 = False
    
    logFolder = lr_gym.utils.utils.lr_gym_startup(__file__, inspect.currentframe(),folderName=folderName, seed = seed)
    device = lr_gym.utils.utils.torch_selectBestGpu()

    env, targetFps = env_builder(seed= seed, logFolder=logFolder, is_eval = False, env_builder_args = env_builder_args)


    if video_save_freq_ep>0:
        recEnv = RecorderGymWrapper(env,
                                    fps = targetFps, outFolder = logFolder+"/videos/RecorderGymWrapper",
                                    saveBestEpisodes = True,
                                    saveFrequency_ep = video_save_freq_ep)
        env = recEnv

    try:
        # actionsize = env.action_space.shape[-1]
        # action_noise = NormalActionNoise(mean=np.zeros(actionsize), sigma=noise_sigma * np.ones(actionsize))

        model = SAC("MultiInputPolicy", env, verbose=1,
                    batch_size=batch_size,
                    buffer_size=buffer_size,
                    gamma=gamma,
                    learning_rate=learning_rate,
                    ent_coef=ent_coef,
                    learning_starts=learning_starts,
                    tau=tau,
                    gradient_steps=gradient_steps,
                    train_freq=train_freq,
                    target_entropy=target_entropy,
                    seed = seed,
                    device=device,
                    policy_kwargs=dict(net_arch=net_arch))

        ggLog.info("Learning...")
        t_preLearn = time.time()
        model.learn(total_timesteps=1000000)
        duration_learn = time.time() - t_preLearn
        ggLog.info("Learned. Took "+str(duration_learn)+" seconds.")
    finally:
        env.close()
