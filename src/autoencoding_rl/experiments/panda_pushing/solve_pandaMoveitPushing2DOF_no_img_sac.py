#!/usr/bin/env python3

import time
import argparse
import multiprocessing
import inspect
import lr_gym

from lr_gym.envs.RecorderGymWrapper import RecorderGymWrapper
from autoencoding_rl.utils import launchRun


from stable_baselines3 import SAC
from stable_baselines3.sac import MlpPolicy
import lr_gym.utils.dbg.ggLog as ggLog




from autoencoding_rl.experiments.panda_pushing.panda_pushing_builder import buildEnv
from autoencoding_rl.experiments.train import sac_train


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--evaluate", default=None, type=str, help="Load and evaluate model file")
    ap.add_argument("--resumeFolder", default=None, type=str, help="Resume an entire run composed of multiple seeds")
    ap.add_argument("--seedsNum", default=1, type=int, help="Number of seeds to test with")
    ap.add_argument("--seeds", nargs="+", required=False, type=int, help="Seeds to use")
    ap.add_argument("--real", default=False, action='store_true', help="Run on real robot")
    ap.add_argument("--no_rb_checkpoint", default=False, action='store_true', help="Do not save replay buffer checkpoints")
    ap.add_argument("--robot_pc_ip", default=None, type=str, help="Ip of the pc connected to the robot (which runs the control, using its rt kernel)")
    ap.add_argument("--seedsOffset", default=0, type=int, help="Offset the used seeds by this amount")
    ap.add_argument("--xvfb", default=False, action='store_true', help="Run with xvfb")
    ap.add_argument("--maxProcs", default=int(multiprocessing.cpu_count()/2), type=int, help="Maximum number of parallel runs")
    ap.add_argument("--offline", default=False, action='store_true', help="Train offline")
    ap.set_defaults(feature=True)
    args = vars(ap.parse_args())

    if args["real"] and args["maxProcs"]>0:
        AttributeError("Cannot run multiple processes in the real")

    def env_builder(seed, logFolder, is_eval, env_builder_args):
        return buildEnv(logFolder, real = args["real"], robot_pc_ip= args["robot_pc_ip"], evaluation = is_eval, seed = seed, use_img_obs = False)


    def runFunction(seed, folderName, resumeModelFile):
        if resumeModelFile is not None:
            raise AttributeError("resume is not supported")
        return sac_train(   seed=seed,
                            folderName=folderName,
                            env_builder=env_builder,
                            batch_size=256,
                            buffer_size=30000,
                            ent_coef="auto",
                            gamma=0.99,
                            gradient_steps=500,
                            learning_rate=0.001,
                            learning_starts=1000,
                            net_arch = [32,32],
                            target_entropy="auto",
                            noise_sigma=0,
                            tau=0.005,
                            train_freq=(1,"episode"))

    
    launchRun(evalFunction=None,
                evalModelFile=args["evaluate"],
                seedsNum=args["seedsNum"],
                seedsOffset=args["seedsOffset"],
                runFunction=runFunction,
                maxProcs=args["maxProcs"],
                xvfb=args["xvfb"],
                launchFilePath=__file__,
                resumeFolder = args["resumeFolder"])
