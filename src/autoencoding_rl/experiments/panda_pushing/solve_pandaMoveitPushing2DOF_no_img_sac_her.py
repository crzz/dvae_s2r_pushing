#!/usr/bin/env python3

import time
import argparse
import multiprocessing
import inspect
import lr_gym

from lr_gym.envs.RecorderGymWrapper import RecorderGymWrapper
from autoencoding_rl.utils import launchRun


from stable_baselines3 import SAC
from stable_baselines3.sac import MlpPolicy
import lr_gym.utils.dbg.ggLog as ggLog
from stable_baselines3 import HerReplayBuffer



from autoencoding_rl.experiments.panda_pushing.panda_pushing_builder import buildEnv


def sac_train(  seed,
                env_builder,
                batch_size=256,
                buffer_size=150000,
                gamma=0.99,
                learning_rate=0.007,
                ent_coef="auto",
                learning_starts=1000,
                tau=0.005,
                gradient_steps=500,
                train_freq=(1,"episode"),
                net_arch = [32,32],
                folderName = "./",
                noise_sigma = 0.1):

    logFolder = lr_gym.utils.utils.lr_gym_startup(__file__, inspect.currentframe(),folderName=folderName)
    device = lr_gym.utils.utils.torch_selectBestGpu()

    env, targetFps = buildEnv(logFolder, evaluation = False, seed = seed,
                                use_img_obs = False,
                                real = False,
                                dict_obs=False,
                                random_goal=True,
                                goal_env=True)
    env = RecorderGymWrapper(env,
                                fps = targetFps, outFolder = logFolder+"/videos/RecorderGymWrapper",
                                saveBestEpisodes = True,
                                saveFrequency_ep = 10)

    # actionsize = env.action_space.shape[-1]
    # action_noise = NormalActionNoise(mean=np.zeros(actionsize), sigma=noise_sigma * np.ones(actionsize))

    model = SAC(     "MultiInputPolicy", env, verbose=1,
                 batch_size=batch_size,
                 buffer_size=buffer_size,
                 gamma=gamma,
                 learning_rate=learning_rate,
                 ent_coef=ent_coef,
                 learning_starts=learning_starts,
                 tau=tau,
                 gradient_steps=gradient_steps,
                 train_freq=train_freq,
                 seed = seed,
                 device=device,
                 policy_kwargs=dict(net_arch=net_arch),
                 replay_buffer_class=HerReplayBuffer,
                # Parameters for HER
                 replay_buffer_kwargs=dict(
                    n_sampled_goal=4,
                    goal_selection_strategy="future",
                    online_sampling=True,
                    max_episode_length=60))

    ggLog.info("Learning...")
    t_preLearn = time.time()
    model.learn(total_timesteps=1000000)
    duration_learn = time.time() - t_preLearn
    ggLog.info("Learned. Took "+str(duration_learn)+" seconds.")


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--evaluate", default=None, type=str, help="Load and evaluate model file")
    ap.add_argument("--resumeFolder", default=None, type=str, help="Resume an entire run composed of multiple seeds")
    ap.add_argument("--seedsNum", default=1, type=int, help="Number of seeds to test with")
    ap.add_argument("--seeds", nargs="+", required=False, type=int, help="Seeds to use")
    ap.add_argument("--real", default=False, action='store_true', help="Run on real robot")
    ap.add_argument("--no_rb_checkpoint", default=False, action='store_true', help="Do not save replay buffer checkpoints")
    ap.add_argument("--robot_pc_ip", default=None, type=str, help="Ip of the pc connected to the robot (which runs the control, using its rt kernel)")
    ap.add_argument("--seedsOffset", default=0, type=int, help="Offset the used seeds by this amount")
    ap.add_argument("--xvfb", default=False, action='store_true', help="Run with xvfb")
    ap.add_argument("--maxProcs", default=int(multiprocessing.cpu_count()/2), type=int, help="Maximum number of parallel runs")
    ap.add_argument("--offline", default=False, action='store_true', help="Train offline")
    ap.set_defaults(feature=True)
    args = vars(ap.parse_args())

    if args["real"] and args["maxProcs"]>0:
        AttributeError("Cannot run multiple processes in the real")


    def env_builder(seed, logFolder, is_eval):
        return buildEnv(logFolder, real = args["real"], robot_pc_ip= args["robot_pc_ip"], evaluation = is_eval, seed = seed)


    def runFunction(seed, folderName, resumeModelFile):
        if resumeModelFile is not None:
            raise AttributeError("resume is not supported")
        return sac_train(   seed=seed,
                            folderName=folderName,
                            env_builder=env_builder,
                            batch_size=4096,
                            buffer_size=30000,
                            gamma=0.99,
                            learning_rate=0.001,
                            ent_coef="auto",
                            learning_starts=1000,
                            tau=0.005,
                            gradient_steps=60,
                            train_freq=(1,"episode"),
                            net_arch = [32,32])

    
    launchRun(evalFunction=None,
                evalModelFile=args["evaluate"],
                seedsNum=args["seedsNum"],
                seedsOffset=args["seedsOffset"],
                runFunction=runFunction,
                maxProcs=args["maxProcs"],
                xvfb=args["xvfb"],
                launchFilePath=__file__,
                resumeFolder = args["resumeFolder"])
