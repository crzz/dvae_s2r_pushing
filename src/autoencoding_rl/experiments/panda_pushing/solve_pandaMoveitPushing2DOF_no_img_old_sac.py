#!/usr/bin/env python3

import time
import argparse
import multiprocessing
import inspect
import lr_gym

from lr_gym.envs.RecorderGymWrapper import RecorderGymWrapper
from autoencoding_rl.utils import launchRun


from stable_baselines3 import SAC
from stable_baselines3.sac import MlpPolicy
import lr_gym.utils.dbg.ggLog as ggLog
from lr_gym.envs.GymEnvWrapper import GymEnvWrapper

from autoencoding_rl.envs.old.PandaPushingEnv2DOF_no_img import PandaPushingEnv2DOF_no_img


from lr_gym.envControllers.MoveitGazeboController import MoveitGazeboController


def buildEnv(logFolder : str, seed):
    goal = (0.45,-0.15)
    initial_cube_pos = (0.6,0)
    maxActionsPerEpisode = 60
    initialJointPose = {("panda","panda_joint1") : 0.0,
                        ("panda","panda_joint2") : 0.0,
                        ("panda","panda_joint3") : 0.0,
                        ("panda","panda_joint4") :-1.97139,
                        ("panda","panda_joint5") : 0.0,
                        ("panda","panda_joint6") : 1.971398,
                        ("panda","panda_joint7") : 0.0}
    environmentController = MoveitGazeboController( jointsOrder = [("panda","panda_joint1"),
                                                                    ("panda","panda_joint2"),
                                                                    ("panda","panda_joint3"),
                                                                    ("panda","panda_joint4"),
                                                                    ("panda","panda_joint5"),
                                                                    ("panda","panda_joint6"),
                                                                    ("panda","panda_joint7")],
                                                    endEffectorLink  = ("panda", "pushing_ee_tip"),
                                                    referenceFrame   = "world",
                                                    initialJointPose = initialJointPose)

    ggEnv = PandaPushingEnv2DOF_no_img(goalPosition_xy = goal,
                                        initialCubePosition_xy = initial_cube_pos,
                                        maxActionsPerEpisode = maxActionsPerEpisode,
                                        backend="gazebo",
                                        environmentController = environmentController,
                                        seed=seed)

    env = GymEnvWrapper(ggEnv,
                        episodeInfoLogFile = logFolder+"/GymEnvWrapper_log.csv")
    # env = RecorderGymWrapper(env,fps=1, outFolder = logFolder+"/videos/RecorderGymWrapper", saveBestEpisodes=True, saveFrequency_ep = 100)
    print("Environment created")
    return env, 2

def sac_train(  seed,
                env_builder,
                batch_size,
                buffer_size,
                gamma,
                learning_rate,
                ent_coef,
                learning_starts,
                tau,
                gradient_steps,
                train_freq,
                net_arch,
                folderName,
                noise_sigma):

    logFolder = lr_gym.utils.utils.lr_gym_startup(__file__, inspect.currentframe(),folderName=folderName)
    device = lr_gym.utils.utils.torch_selectBestGpu()

    env, targetFps = env_builder(seed= seed, logFolder=logFolder, is_eval = False)
    # actionsize = env.action_space.shape[-1]
    # action_noise = NormalActionNoise(mean=np.zeros(actionsize), sigma=noise_sigma * np.ones(actionsize))

    model = SAC( MlpPolicy, env, verbose=1,
                 batch_size=batch_size,
                 buffer_size=buffer_size,
                 gamma=gamma,
                 learning_rate=learning_rate,
                 ent_coef=ent_coef,
                 learning_starts=learning_starts,
                 tau=tau,
                 gradient_steps=gradient_steps,
                 train_freq=train_freq,
                 seed = seed,
                 device=device,
                 policy_kwargs=dict(net_arch=net_arch))

    ggLog.info("Learning...")
    t_preLearn = time.time()
    model.learn(total_timesteps=1000000)
    duration_learn = time.time() - t_preLearn
    ggLog.info("Learned. Took "+str(duration_learn)+" seconds.")


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--evaluate", default=None, type=str, help="Load and evaluate model file")
    ap.add_argument("--resumeFolder", default=None, type=str, help="Resume an entire run composed of multiple seeds")
    ap.add_argument("--seedsNum", default=1, type=int, help="Number of seeds to test with")
    ap.add_argument("--seeds", nargs="+", required=False, type=int, help="Seeds to use")
    ap.add_argument("--real", default=False, action='store_true', help="Run on real robot")
    ap.add_argument("--no_rb_checkpoint", default=False, action='store_true', help="Do not save replay buffer checkpoints")
    ap.add_argument("--robot_pc_ip", default=None, type=str, help="Ip of the pc connected to the robot (which runs the control, using its rt kernel)")
    ap.add_argument("--seedsOffset", default=0, type=int, help="Offset the used seeds by this amount")
    ap.add_argument("--xvfb", default=False, action='store_true', help="Run with xvfb")
    ap.add_argument("--maxProcs", default=int(multiprocessing.cpu_count()/2), type=int, help="Maximum number of parallel runs")
    ap.add_argument("--offline", default=False, action='store_true', help="Train offline")
    ap.set_defaults(feature=True)
    args = vars(ap.parse_args())

    if args["real"] and args["maxProcs"]>0:
        AttributeError("Cannot run multiple processes in the real")


    def env_builder(seed, logFolder, is_eval):
        return buildEnv(logFolder, seed = seed)


    def runFunction(seed, folderName, resumeModelFile):
        if resumeModelFile is not None:
            raise AttributeError("resume is not supported")
        return sac_train(   seed=seed,
                            folderName=folderName,
                            env_builder=env_builder,
                            batch_size=256,
                            buffer_size=30000,
                            gamma=0.99,
                            learning_rate=0.001,
                            ent_coef="auto",
                            learning_starts=1000,
                            tau=0.005,
                            gradient_steps=450,
                            train_freq=(1,"episode"),
                            net_arch = [32,32],
                            noise_sigma=0)

    
    launchRun(evalFunction=None,
                evalModelFile=args["evaluate"],
                seedsNum=args["seedsNum"],
                seedsOffset=args["seedsOffset"],
                runFunction=runFunction,
                maxProcs=args["maxProcs"],
                xvfb=args["xvfb"],
                launchFilePath=__file__,
                resumeFolder = args["resumeFolder"])
