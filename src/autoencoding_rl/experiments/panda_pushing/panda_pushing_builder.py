
import time
import numpy as np

from autoencoding_rl.envs.PandaPushingEnv2DOF import PandaPushingEnv2DOF

from lr_gym.envs.GymEnvWrapper import GymEnvWrapper
from lr_gym.envControllers.PandaMoveitRosController import PandaMoveitRosController
from lr_gym.envControllers.MoveitGazeboController import MoveitGazeboController
from lr_gym.envs.ObsDict2FlatBox import ObsDict2FlatBox
from lr_gym.envs.ObsToGoalEnvObs import ObsToGoalEnvObs
from lr_gym.envs.SubProcGazeboEnvWrapper import SubProcGazeboEnvWrapper
from lr_gym.envs.NestedDictFlattenerGymWrapper import NestedDictFlattenerGymWrapper
import lr_gym.utils.dbg.ggLog as ggLog


def countdown(delay):
    for i in range(delay):
        print(f"Starting in {delay-i}")
        time.sleep(1.0)

def buildLrEnv( logFolder : str,
                real : bool,
                robot_pc_ip : str  = None,
                evaluation : bool = False,
                seed : int = 0,
                pixel_resolution = 128,
                use_img_obs : bool = True,
                only_img_obs : bool = False,
                terminate_on_success = True,
                random_goal : bool = False,
                dict_obs = True,
                goal_env = False,
                sim_cube_color = [0.175,0.175,0.175],
                sim_light_direction = [0.01,0.01,-1],
                camera_offset_xyz = (0.0,0.0,0.0),
                camera_offset_rpy = (0.0,0.0,0.0),
                allow_successful_initial_cube_position = False,
                maxStepsPerEpisode = 60,
                prevent_ee_out = False,
                variations_sampler = None):

    area_size =   np.array([0.45,   0.45, 1])
    area_center = np.array([0.5225, 0,    0])
    operating_area = np.array([ area_center-area_size/2,
                                area_center+area_size/2])

    if random_goal:
        goal_area = np.array([ area_center-(area_size-0.02)/2,
                               area_center+(area_size-0.02)/2])                         
        goal = lambda : (np.random.rand(3)*(goal_area[1]-goal_area[0])+goal_area[0])[[0,1]]
    else:
        goal = (0.45,-0.15)

    initialJointPose = {("panda","panda_joint1") : 0.0,
                        ("panda","panda_joint2") : 0.0,
                        ("panda","panda_joint3") : 0.0,
                        ("panda","panda_joint4") :-1.97139,
                        ("panda","panda_joint5") : 0.0,
                        ("panda","panda_joint6") : 1.971398,
                        ("panda","panda_joint7") : 0.0}
    jointsOrder =  [("panda","panda_joint1"),
                    ("panda","panda_joint2"),
                    ("panda","panda_joint3"),
                    ("panda","panda_joint4"),
                    ("panda","panda_joint5"),
                    ("panda","panda_joint6"),
                    ("panda","panda_joint7")]                        
    endEffectorLink  = ("panda", "pushing_ee_tip")
    referenceFrame   = "world"
    max_obs_delay = 0.05
    if not real:
        environmentController = MoveitGazeboController( jointsOrder = jointsOrder,
                                                        endEffectorLink  = endEffectorLink,
                                                        referenceFrame   = referenceFrame,
                                                        initialJointPose = initialJointPose,
                                                        maxObsDelay = max_obs_delay,
                                                        blocking_observation = True)
    else:
        environmentController = PandaMoveitRosController(jointsOrder =  jointsOrder,
                                                        endEffectorLink  = endEffectorLink,
                                                        referenceFrame   = referenceFrame,
                                                        initialJointPose = initialJointPose,
                                                        default_collision_objs =   [([    0,      0, -1,0,0,0,1], [10.0,5.00,2.0]), #bottom
                                                                                    ([    1,      0,  0,0,0,0,1], [0.20,5.00,2.0]), #front 0.9m
                                                                                    ([    0,   0.47,  0,0,0,0,1], [5.00,0.20,2.0]), #left 0.37m
                                                                                    ([    0,   -0.6,  0,0,0,0,1], [5.00,0.20,2.0]), #right 0.5m
                                                                                    ([ -0.6,      0,  0,0,0,0,1], [0.20,5.00,2.0]), #back 0.5m

                                                                                    ([-0.04, -0.135,  0,0,0,0,1], [0.22,0.27,0.2]), #Protect power cable
                                                                                    ([-0.04,  0.135,  0,0,0,0,1], [0.22,0.27,0.2]), #Protect safety button cable
                                                                                    ([-0.15,   0.45,  0,0,0,0,1], [0.44,0.30,0.2])],
                                                        maxObsDelay = max_obs_delay,
                                                        blocking_observation = True)
    ggEnv = PandaPushingEnv2DOF(goalPosition_xy = goal,
                                operatingArea=operating_area,
                                maxStepsPerEpisode = maxStepsPerEpisode,
                                backend= "real" if real else "gazebo",
                                real_robot_pc_ip=robot_pc_ip,
                                environmentController = environmentController,
                                camera_resolution=pixel_resolution,
                                rgb = True,
                                imgEncoding="int",
                                wall_sim_speed= real,
                                seed = seed,
                                obs_only_img = only_img_obs,
                                terminate_on_success = terminate_on_success,
                                obs_only_vec = not use_img_obs,
                                log_folder=logFolder+"/PandaPushingEnv2DOF/",
                                cube_color = sim_cube_color,
                                light_direction = sim_light_direction,
                                camera_offset_xyz = camera_offset_xyz,
                                camera_offset_rpy = camera_offset_rpy,
                                allow_successful_initial_cube_position = allow_successful_initial_cube_position,
                                prevent_ee_out = prevent_ee_out,
                                variations_sampler = variations_sampler)
    if not dict_obs:
        ggEnv = ObsDict2FlatBox(ggEnv, key="vec")
    if goal_env:
        ggEnv = ObsToGoalEnvObs(env=ggEnv)
    # ggLog.info(f"pushing: observation_space = {ggEnv.observation_space}")
    return ggEnv

def buildEnv(   logFolder : str,
                real : bool,
                robot_pc_ip : str  = None,
                evaluation : bool = False,
                seed : int = 0,
                pixel_resolution = 128,
                use_img_obs : bool = True,
                only_img_obs : bool = False,
                terminate_on_success = True,
                random_goal : bool = False,
                dict_obs = True,
                goal_env = False,
                sim_cube_color = [0.175,0.175,0.175],
                sim_light_direction = [0.01,0.01,-1],
                camera_offset_xyz = (0.0,0.0,0.0),
                camera_offset_rpy = (0.0,0.0,0.0),
                subproc = False,
                allow_successful_initial_cube_position = False,
                maxStepsPerEpisode = 60,
                prevent_ee_out = False,
                variations_sampler = None):

    def buildit():
        return buildLrEnv(  logFolder = logFolder,
                            real = real,
                            robot_pc_ip = robot_pc_ip,
                            evaluation = evaluation,
                            seed = seed,
                            pixel_resolution = pixel_resolution,
                            use_img_obs = use_img_obs,
                            only_img_obs = only_img_obs,
                            terminate_on_success = terminate_on_success,
                            random_goal = random_goal,
                            dict_obs = dict_obs,
                            goal_env = goal_env,
                            sim_cube_color = sim_cube_color,
                            sim_light_direction = sim_light_direction,
                            camera_offset_xyz = camera_offset_xyz,
                            camera_offset_rpy = camera_offset_rpy,
                            allow_successful_initial_cube_position = allow_successful_initial_cube_position,
                            maxStepsPerEpisode = maxStepsPerEpisode,
                            prevent_ee_out = prevent_ee_out,
                            variations_sampler = variations_sampler)

    if subproc:
        ggEnv = SubProcGazeboEnvWrapper(buildit)
    else:
        ggEnv = buildit()
    targetFps = 2
    env = GymEnvWrapper(ggEnv,
                        episodeInfoLogFile = logFolder+"/GymEnvWrapper_log.csv")
    if goal_env:
        env = NestedDictFlattenerGymWrapper(env)
    # env = RecorderGymWrapper(env,fps=targetFps, outFolder = logFolder+"/videos/RecorderGymWrapper", saveBestEpisodes=True, saveFrequency_ep = video_saveFrequency_ep)
    env.action_space.seed(seed)
    print("Built environment")
    countdown(10)
    targetFps = 2
    return env, targetFps

def env_builder(seed, logFolder, is_eval, env_builder_args):
    return buildEnv(logFolder,
                    real = env_builder_args["real"],
                    robot_pc_ip= env_builder_args["robot_pc_ip"],
                    evaluation = is_eval,
                    seed = seed,
                    pixel_resolution=env_builder_args["pixel_resolution"],
                    sim_cube_color=env_builder_args["cube_color"],
                    sim_light_direction=env_builder_args["light_direction"],
                    camera_offset_xyz=env_builder_args["camera_offset_xyz"],
                    camera_offset_rpy=env_builder_args["camera_offset_rpy"],
                    allow_successful_initial_cube_position=env_builder_args["allow_successful_initial_cube_position"],
                    subproc=is_eval,
                    random_goal=env_builder_args["random_goal"],
                    goal_env=env_builder_args["goal_env"],
                    maxStepsPerEpisode = env_builder_args["max_steps"],
                    terminate_on_success=env_builder_args["terminate_on_success"],
                    prevent_ee_out = env_builder_args["prevent_ee_out"],
                    variations_sampler = env_builder_args["variations_sampler"])