#!/usr/bin/env python3

import os.path
import os
import argparse
import multiprocessing
from shutil import copyfile


import lr_gym.utils.dbg.ggLog as ggLog

from autoencoding_rl.utils import launchRun
from autoencoding_rl.experiments.train import daesac_train

from autoencoding_rl.experiments.panda_pushing.panda_pushing_builder import env_builder



terminate_on_success = False
args = None
max_steps = 40

def runFunction(seed, folderName, resumeModelFile):
    if resumeModelFile is not None:
        pathList = os.path.abspath(resumeModelFile).split("/")
        rootFolderResumeSource = "/"+"/".join(pathList[:pathList.index("checkpoints")])
        os.makedirs(folderName,exist_ok=True)
        copyfile(src=rootFolderResumeSource+"/GymEnvWrapper_log.csv", dst=folderName+"/GymEnvWrapper_log.csv")
    return daesac_train(  seed=seed,
                            trainSteps=100000*max_steps,
                            folderName=folderName,
                            gpuid = None, # seed%th.cuda.device_count(),
                            modelFile=resumeModelFile,
                            env_builder=env_builder,
                            env_builder_args = {"cube_color":args["cube_color"],
                                                "light_direction":args["light_direction"],
                                                "pixel_resolution":128,
                                                "camera_offset_xyz":args["camera_offset_xyz"],
                                                "camera_offset_rpy":args["camera_offset_rpy"],
                                                "allow_successful_initial_cube_position":True,
                                                "real" : args["real"],
                                                "robot_pc_ip" : args["robot_pc_ip"],
                                                "random_goal" : False,
                                                "goal_env" : False,
                                                "max_steps" : max_steps,
                                                "terminate_on_success" : terminate_on_success,
                                                "prevent_ee_out" : True,
                                                "variations_sampler" : None},
                            eval_period_ep=-1,
                            storage_torch_device="auto_cuda",                            
                            
                            automatic_grad_steps=False,
                            le_arch_backbone = "mobilenetv3",
                            le_arch_deconv_backbone = "conv",
                            le_arch_dyn_ensemble_size = 5,
                            le_arch_dynamics = [64,64],
                            le_arch_enc_ensemble_size = 1,
                            le_arch_frame_stack_size=1,
                            le_arch_img_dec_ensemble_size = 1,
                            le_arch_img_encoding_size = 5,            
                            le_arch_reward = None,
                            le_arch_reward_ensemble_size=5,
                            le_arch_state_combiner = "identity",
                            le_arch_type = "vae3",
                            le_arch_use_coord_conv=True,
                            le_arch_vec_decoder=[],
                            le_arch_vec_encoder="identity",
                            le_batch_size = 128,
                            le_bestModelThreshold = 0.9,
                            le_decoder_weight_decay_lambda=None,
                            le_disable_validation_set = True,
                            le_encoding_noise_std=0.0,
                            le_freeze_decoders=False,
                            le_freeze_dynamics=False,
                            le_freeze_encoders=False,
                            le_grad_steps = 50,
                            le_learning_starts = 1000,
                            le_loss_kld_weight = 0.0001,
                            le_loss_latent_weight=None,
                            le_loss_reconstruction_weight=0.0,
                            le_loss_reward_weight = 0,
                            le_loss_use_log=True,
                            le_loss_vec_weight = 0.2,
                            le_lr = 0.0001,
                            le_pretrain_grad_steps = 16,
                            le_reset_decoder=False,
                            le_reset_encoder=False,
                            le_retrain_epochs = 100,
                            le_retrain_period = -1,
                            le_reward_scaling = 100.0,
                            le_reset_on_retrain = False,
                            le_tau=1.0,
                            le_train_freq=(1,"episode"),
                            le_use_log_reward = terminate_on_success,
                            le_validation_batch_size = 256,
                            le_validation_ratio=0.01,
                            parallelize_experience_collection=True,
                            random_steps = 1000,
                            replay_buffer_size = 30000,
                            sac_arch_actor_ensemble_size=5,
                            sac_arch_policy = [64,64],
                            sac_batchSize = 256,
                            sac_ent_coef = "auto",
                            sac_gamma = 0.99,
                            sac_grad_steps = 60,
                            sac_learning_starts = 1000,
                            sac_lr = 0.005,
                            sac_target_entropy="auto",
                            sac_tau=0.005,
                            sac_train_freq=(1,"episode"))



def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("--evaluate", default=None, type=str, help="Load and evaluate model file")
    ap.add_argument("--resumeFolder", default=None, type=str, help="Resume an entire run composed of multiple seeds")
    ap.add_argument("--seedsNum", default=1, type=int, help="Number of seeds to test with")
    ap.add_argument("--seeds", nargs="+", required=False, type=int, help="Seeds to use")
    ap.add_argument("--real", default=False, action='store_true', help="Run on real robot")
    ap.add_argument("--no_rb_checkpoint", default=False, action='store_true', help="Do not save replay buffer checkpoints")
    ap.add_argument("--robot_pc_ip", default=None, type=str, help="Ip of the pc connected to the robot (which runs the control, using its rt kernel)")
    ap.add_argument("--seedsOffset", default=0, type=int, help="Offset the used seeds by this amount")
    ap.add_argument("--xvfb", default=False, action='store_true', help="Run with xvfb")
    ap.add_argument("--maxProcs", default=int(multiprocessing.cpu_count()/2), type=int, help="Maximum number of parallel runs")
    ap.add_argument("--offline", default=False, action='store_true', help="Train offline")
    ap.add_argument("--pretrained", default=None, type=str, help="Start from a pretrained model")
    ap.add_argument("--nodynamics", default=False, action='store_true', help="Use a regular VAE instead of DVAE")
    ap.add_argument("--cube_color", nargs="+", default=[0.175,0.175,0.175], type=float, help="Simulated cube color")
    ap.add_argument("--light_direction", nargs="+", default=[0.01,0.01,-1.0], type=float, help="Simulated light direction")
    ap.add_argument("--camera_offset_xyz", nargs="+", default=[0,0,0], type=float, help="Simulated camera position offset")
    ap.add_argument("--camera_offset_rpy", nargs="+", default=[0,0,0], type=float, help="Simulated camera orientation offset")
    ap.set_defaults(feature=True)
    global args
    args = vars(ap.parse_args())

    if args["real"] and args["maxProcs"]>0:
        AttributeError("Cannot run multiple processes in the real")



    
    launchRun(evalFunction=None,
                evalModelFile=args["evaluate"],
                seedsNum=args["seedsNum"],
                seedsOffset=args["seedsOffset"],
                runFunction=runFunction,
                maxProcs=args["maxProcs"],
                xvfb=args["xvfb"],
                launchFilePath=__file__,
                resumeFolder = args["resumeFolder"],
                pretrainedModelFile=args["pretrained"])


if __name__ == "__main__":
    main()
