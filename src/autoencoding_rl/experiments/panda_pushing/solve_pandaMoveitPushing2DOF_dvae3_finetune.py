#!/usr/bin/env python3

import os.path
import os
import argparse
import multiprocessing
from shutil import copyfile


import lr_gym.utils.dbg.ggLog as ggLog

from autoencoding_rl.utils import launchRun
from autoencoding_rl.experiments.train import daesac_train

from autoencoding_rl.experiments.panda_pushing.panda_pushing_builder import env_builder



args = {}
terminate_on_success = False
ep_length = 40
resuming = False
def runFunction(seed, folderName, resumeModelFile):
    # prerun = ep_length*100 if args["exp"] in ["sim-minimal", "sim-small", "sim-medium"] else 0
    if args["exp"] in ["real-minimal","real-small","real-large"]:
        le_lr = 0.0025
        le_gs = 30
    elif args["exp"] == "sim-large":
        le_lr = 0.005
        le_gs = 25
    else:
        le_lr = 0.001
        le_gs = 25
    return daesac_train(    seed=seed,
                            trainSteps=ep_length*2600,
                            folderName=folderName,
                            gpuid = None, # seed%th.cuda.device_count(),
                            modelFile=resumeModelFile,
                            env_builder=env_builder,
                            checkpointing_freq_ep = 10,
                            env_builder_args = {"cube_color":args["cube_color"],
                                                "light_direction":args["light_direction"],
                                                "pixel_resolution":128,
                                                "camera_offset_xyz":args["camera_offset_xyz"],
                                                "camera_offset_rpy":args["camera_offset_rpy"],
                                                "allow_successful_initial_cube_position":True,
                                                "real" : args["real"],
                                                "robot_pc_ip" : args["robot_pc_ip"],
                                                "random_goal" : False,
                                                "goal_env" : False,
                                                "max_steps" : ep_length,
                                                "terminate_on_success" : terminate_on_success,
                                                "prevent_ee_out" : True,
                                                "variations_sampler" : args["exp"]},
                            automatic_grad_steps=False,
                            eval_period_ep=-1,
                            preevaluation_episodes = 100,
                            le_batch_size = 64,
                            le_bestModelThreshold = -1.0,
                            le_disable_validation_set = False,
                            le_freeze_decoders=False,
                            le_freeze_dynamics= False,
                            le_freeze_encoders=False,
                            le_grad_steps = 50,
                            le_learning_starts = 1000,
                            le_loss_kld_weight = None,
                            le_loss_reward_weight = None,
                            le_loss_vec_weight = None,
                            le_lr = le_lr,
                            le_pretrain_grad_steps = 20,
                            le_reset_decoder = args["reset_dec"],
                            le_reset_encoder = args["reset_enc"],
                            le_retrain_epochs = 50,
                            le_retrain_period = -1,
                            le_reset_on_retrain = False,
                            le_validation_batch_size = 512,
                            loading_pretrained = not resuming,
                            load_replay_buffer = resuming,
                            parallelize_experience_collection=True,
                            random_steps = 100,
                            replay_buffer_size = 30000,
                            sac_batchSize = 256,
                            sac_ent_coef = "auto",
                            sac_gamma = 0.99,
                            sac_grad_steps = 60,
                            sac_learning_starts = 1100,
                            sac_lr = 0.005,
                            storage_torch_device="auto_cuda",

                            le_arch_backbone = None,
                            le_arch_deconv_backbone = None,
                            le_arch_dyn_ensemble_size = None,
                            le_arch_dynamics = None,
                            le_arch_enc_ensemble_size = None,
                            le_arch_frame_stack_size = None,
                            le_arch_img_dec_ensemble_size = None,
                            le_arch_img_encoding_size = None,
                            le_arch_reward = None,
                            le_arch_reward_ensemble_size = None,
                            le_arch_state_combiner = None,
                            le_arch_type = None,
                            le_arch_use_coord_conv = None,
                            le_arch_vec_decoder = None,
                            le_arch_vec_encoder = None,

                            le_decoder_weight_decay_lambda = None,
                            le_encoding_noise_std = None,
                            le_loss_latent_weight = None,
                            le_loss_reconstruction_weight = None,
                            le_loss_use_log = None,
                            le_reward_scaling = None,
                            le_tau = None,
                            le_train_freq = None,
                            le_use_log_reward = not terminate_on_success,
                            le_validation_ratio = 0.01,

                            sac_arch_actor_ensemble_size = None,
                            sac_arch_policy = None,
                            sac_target_entropy = None,
                            sac_tau = None,
                            sac_train_freq = None,
                            save_replay_buffer=args["real"]
                            )
                            
def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("--resumeFolder", default=None, type=str, help="Resume an entire run composed of multiple seeds")
    ap.add_argument("--seedsNum", default=1, type=int, help="Number of seeds to test with")
    ap.add_argument("--seeds", nargs="+", required=False, type=int, help="Seeds to use")
    ap.add_argument("--real", default=False, action='store_true', help="Run on real robot")
    ap.add_argument("--no_rb_checkpoint", default=False, action='store_true', help="Do not save replay buffer checkpoints")
    ap.add_argument("--robot_pc_ip", default=None, type=str, help="Ip of the pc connected to the robot (which runs the control, using its rt kernel)")
    ap.add_argument("--seedsOffset", default=0, type=int, help="Offset the used seeds by this amount")
    ap.add_argument("--xvfb", default=False, action='store_true', help="Run with xvfb")
    ap.add_argument("--maxProcs", default=int(multiprocessing.cpu_count()/2), type=int, help="Maximum number of parallel runs")
    ap.add_argument("--offline", default=False, action='store_true', help="Train offline")
    ap.add_argument("--pretrained", default=None, type=str, help="Start from a pretrained model")
    ap.add_argument("--cube_color", nargs="+", default=[0.175,0.175,0.175], type=float, help="Simulated cube color")
    ap.add_argument("--light_direction", nargs="+", default=[0.01,0.01,-1.0], type=float, help="Simulated light direction")
    ap.add_argument("--camera_offset_xyz", nargs="+", default=[0,0,0], type=float, help="Simulated camera position offset")
    ap.add_argument("--camera_offset_rpy", nargs="+", default=[0,0,0], type=float, help="Simulated camera orientation offset")
    ap.add_argument("--reset_enc", default=False, action='store_true', help="Reset the encoder")
    ap.add_argument("--reset_dec", default=False, action='store_true', help="Reset the decoder")
    ap.add_argument("--exp", default=None, type=str, help="Experiment kind")

    ap.set_defaults(feature=True)
    global args
    args = vars(ap.parse_args())

    if args["real"] and args["maxProcs"]>0:
        AttributeError("Cannot run multiple processes in the real")

    global resuming
    resuming = args["resumeFolder"] is not None

    
    launchRun(evalFunction=None,
                evalModelFile=None,
                seedsNum=args["seedsNum"],
                seedsOffset=args["seedsOffset"],
                runFunction=runFunction,
                maxProcs=args["maxProcs"],
                xvfb=args["xvfb"],
                launchFilePath=__file__,
                resumeFolder = args["resumeFolder"],
                pretrainedModelFile=args["pretrained"])


if __name__ == "__main__":
    main()