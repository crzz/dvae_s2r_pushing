#!/usr/bin/env python3


import inspect
import lr_gym
import lr_gym.utils.dbg.ggLog as ggLog
import torch as th
from autoencoding_rl.AutoencodingSAC2 import AutoencodingSAC2
import pickle
import os
import argparse
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import sklearn
from sklearn.decomposition import PCA
import torch as th

import seaborn as sns
import matplotlib.pyplot as plt
import autoencoding_rl.utils as utils
import cv2
import datetime

def pca_analysis(all_latents, all_obs, display_axis_x, display_axis_y, logFolder):
    a,b = display_axis_x, display_axis_y
    pca = PCA(n_components=5)
    all_letents_normalized = sklearn.preprocessing.normalize(all_latents, axis = 0)
    pca.fit(all_letents_normalized)
    ggLog.info(f"explained variance ratios = {pca.explained_variance_ratio_}")
    ggLog.info(f"singular values = {pca.singular_values_}")

    all_transformed_latents = pca.transform(all_latents)

    ggLog.info(f"reduced_latents.shape = {all_transformed_latents.shape}")

    plt.ioff()
    plt.clf()

    sns.set_theme(style="ticks") #"darkgrid")
    sns.set_context("paper")
    p = sns.scatterplot(x = all_transformed_latents[:,a], y = all_transformed_latents[:,b])
    p.grid(linestyle='dotted',which="both")
    plt.tight_layout()
    plt.savefig(logFolder+f"/pca_{a}_{b}.pdf",bbox_inches='tight')

    minx = np.min(all_transformed_latents[:,a])
    miny = np.min(all_transformed_latents[:,b])
    maxx = np.max(all_transformed_latents[:,a])
    maxy = np.max(all_transformed_latents[:,b])
    cx = (maxx+minx)/2
    cy = (maxy+maxx)/2
    minx = cx - (cx - minx)*1.1
    maxx = cx + (maxx - cx)*1.1 
    miny = cy - (cy - miny)*1.1
    maxy = cy + (maxy - cy)*1.1 
    ggLog.info(f"minx = {minx}, maxx={maxx}, miny={miny}, maxy={maxy}")
    steps = 40
    stepx = (maxx-minx)/(steps+1)
    stepy = (maxy-miny)/(steps+1)

    desired_transformed_latent_grid = [[None]*steps for _ in range(steps)]
    obs_grid = [[None]*steps for _ in range(steps)]
    selected_obss_idx = [[None]*steps for _ in range(steps)]
    dists_grid = [[None]*steps for _ in range(steps)]
    selected_transformed_latents_grid = [[None]*steps for _ in range(steps)]
    for row in range(steps):
        for col in range(steps):
            desired_transformed_latent_grid[row][col] = np.array([minx+stepx/2+stepx*col, miny+stepy/2+stepy*(steps-1-row)])
    
    for row in range(steps):
        for col in range(steps):
            desired_transformed_latent = desired_transformed_latent_grid[row][col]
            diffs = all_transformed_latents[:,[a,b]] - desired_transformed_latent
            dists = np.linalg.norm(diffs,axis=1)
            closest_idx = dists.argmin()
            dist = dists[closest_idx]
            diff = diffs[closest_idx]
            # ggLog.info(f"{desired_transformed_latent} of {row},{col} matched with {closest_idx}")
            dists_grid[row][col] = dist
            selected_transformed_latents_grid[row][col] = all_transformed_latents[closest_idx]
            if abs(diff[0]) < stepx/2 and abs(diff[1]) < stepy/2:
                selected_obss_idx[row][col] = closest_idx
            else:
                selected_obss_idx[row][col] = -1

    # for row in range(steps):
    #     ggLog.info("\t".join([f'[{coords[0]:.2f},{coords[1]:.2f}]' for coords in desired_transformed_latent_grid[row]]))

    # for row in range(steps):
    #     ggLog.info("\t".join([f'[{coords[0]:.2f},{coords[1]:.2f}]' for coords in selected_transformed_latents_grid[row]]))

    # for row in range(steps):
    #     ggLog.info("\t".join([f'{idx:.2f}' for idx in dists_grid[row]]))

    for row in range(steps):
        # ggLog.info("\t".join([str(idx) for idx in selected_obss_idx[row]]))
        for col in range(steps):
            idx = selected_obss_idx[row][col]
            if idx>=0:
                obs_grid[row][col] = utils.tensorToHumanCvImageRgb(all_obs[idx]["img"].squeeze())
            else:
                obs_grid[row][col] = utils.tensorToHumanCvImageRgb(th.zeros_like(all_obs[0]["img"]).squeeze())
    obs_grid_img = utils.stitchImages(obs_grid)

    cv2.imwrite(filename=logFolder+f"/obs_grid_{a}_{b}.png",img=obs_grid_img)

def load_run_data(modelFile, run_folder, logFolder):    
    observations_folder = run_folder+"/videos/AeRecorderGymWrapper"                            
    ggLog.info(f"Loading model from {modelFile}")
    model = AutoencodingSAC2.load(modelFile, fe_logfolder = logFolder+'/feature_extractor', load_rng_state=False)
    latent_extractor = model.get_latent_extractor()
    ggLog.info("Loaded model from "+modelFile)
    ggLog.info(f"{latent_extractor.getModelSizeRecap()}")

    ep_ids = []
    ep_recordings = []
    for episode_file_name in os.listdir(observations_folder):
        if episode_file_name.endswith(".pkl"):
            ep_ids.append(int(episode_file_name.split("_")[1]))
            with open(observations_folder+"/"+episode_file_name, mode="rb") as ep_file:
                ep_recording = pickle.load(ep_file)
                ep_recordings.append(ep_recording)

    ggLog.info(f"Loaded {len(ep_recordings)} episodes")

    # ep_recordings = ep_recordings[0:10]

    all_obs = []
    for ep in ep_recordings:
        all_obs = all_obs + ep["observations"] #[0:10]
    for i in range(len(all_obs)):
        all_obs[i] = {k : np.expand_dims(v, axis=0) for k,v in all_obs[i].items()}

    ggLog.info(f"Loaded {len(all_obs)} steps")
    

    ggLog.info("Encoding...")
    all_latents = []
    for obs in tqdm(all_obs):
        all_latents.append(latent_extractor.encode(obs))
    ggLog.info(f"Encoded all observations")

    all_latents = th.concat(all_latents)
    ggLog.info(f"all_latents.size() = {all_latents.size()}")
    # all_latents = all_latents[:,0:5] #REmove vec part
    all_latents = all_latents.cpu().numpy()

    all_states = []
    for id in ep_ids:
        all_states.append(np.genfromtxt(run_folder+"/PandaPushingEnv2DOF/ep_{id:06}.csv", delimiter=","))
    all_states = np.concatenate(all_states)

    return all_obs, all_latents, all_states
    
def analyze_latent_space(
                    run_folder : str,
                    folderName : str = None,
                    gpuid = None,
                    modelFile = None,
                    run_id_prefix : str = "",
                    seed : int = 0
                    ) -> None:
    """

    """
 
    logFolder = lr_gym.utils.utils.lr_gym_startup(__file__, inspect.currentframe(), run_id_prefix = run_id_prefix, folderName = folderName, seed = seed)
    
    th.backends.cuda.matmul.allow_tf32 = False
    th.backends.cudnn.allow_tf32 = False
    th.cuda.set_device(gpuid)

    all_obs, all_latents = load_run_data(modelFile, run_folder, logFolder)

    couples = [[0,1], [0,2], [1,2], [2,3], [3,4]]
    for a,b in couples:
        pca_analysis(all_latents, all_obs, a, b, logFolder)

    return None

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("--run_folder", required=True, type=str, help="Folder to take episode files from")
    ap.add_argument("--seed", default="0", required=False, type=int, help="Seed to use")
    ap.add_argument("--pretrained", required = True, default=None, type=str, help="Start from a pretrained model")

    ap.set_defaults(feature=True)
    global args
    args = vars(ap.parse_args())
    analyze_latent_space(run_folder=args["run_folder"],
                            gpuid=0,
                            modelFile=args["pretrained"],
                            seed=args["seed"],
                            folderName = "./"+os.path.basename(__file__)+"/"+datetime.datetime.now().strftime('%Y%m%d-%H%M%S'))

if __name__ == "__main__":
    main()



