#!/usr/bin/env python3

import os.path
import os
import argparse
import multiprocessing
from shutil import copyfile
import dmc2gym
import dmc2gym.wrappers

import lr_gym.utils.dbg.ggLog as ggLog
from lr_gym.envs.GymEnvWrapper import GymEnvWrapper
from lr_gym.envs.GymToLr import GymToLr
from lr_gym.envs.ObsToDict import ObsToDict
from autoencoding_rl.utils import launchRun
import drqv2.dmc

from autoencoding_rl.experiments.train import daesac_train

def build_dmcheetah_env(seed : int, logFolder : str, videoSaveFreq_ep = 10, pixel_resolution = 84, env_builder_args = {}):
    base_stepLength_sec = 0.01
    stepLength_sec = base_stepLength_sec*env_builder_args["action_repeat"]
    maxStepsPerEpisode = 1000/env_builder_args["action_repeat"]
    targetFps = 1/stepLength_sec
        
    env = GymEnvWrapper(ObsToDict(GymToLr(dmc2gym.wrappers.DMCWrapper(env = drqv2.dmc.make("cheetah_run",
                                                                                            frame_stack=3,
                                                                                            action_repeat=env_builder_args["action_repeat"],
                                                                                            seed=seed,
                                                                                            pixel_size=pixel_resolution),
                                                                        task_kwargs = {'random' : seed}),
                                            stepSimDuration_sec=base_stepLength_sec,
                                            maxStepsPerEpisode=maxStepsPerEpisode),
                                    key = "camera"),
                        episodeInfoLogFile = logFolder+"/GymEnvWrapper_log.csv")
    #setup seeds for reproducibility
    env.action_space.seed(seed)

    return env, targetFps






if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("--evaluate", default=None, type=str, help="Load and evaluate model file")
    ap.add_argument("--resume", default=None, type=str, help="Load model file and replay buffer and resume training")
    ap.add_argument("--seedsNum", default=1, type=int, help="Number of seeds to test with")
    ap.add_argument("--real", default=False, action='store_true', help="Run on real robot")
    ap.add_argument("--no_rb_checkpoint", default=False, action='store_true', help="Do not save replay buffer checkpoints")
    ap.add_argument("--robot_pc_ip", default=None, type=str, help="Ip of the pc connected to the robot (which runs the control, using its rt kernel)")
    ap.add_argument("--seedsOffset", default=0, type=int, help="Offset the used seeds by this amount")
    ap.add_argument("--xvfb", default=False, action='store_true', help="Run with xvfb")
    ap.add_argument("--maxProcs", default=int(multiprocessing.cpu_count()/2), type=int, help="Maximum number of parallel runs")
    ap.add_argument("--wallsimspeed", default=False, action='store_true', help="Run the simulation as fast as the real world")
    ap.set_defaults(feature=True)
    args = vars(ap.parse_args())

    if args["real"] and args["maxProcs"]>0:
        AttributeError("Cannot run multiple processes in the real")

    pixel_resolution = 84

    def buildEnv(seed, logFolder, is_eval, env_builder_args):
        if is_eval:
            return build_dmcheetah_env(seed,logFolder+"/eval", videoSaveFreq_ep = 10, pixel_resolution=pixel_resolution, env_builder_args=env_builder_args)
        else:
            return build_dmcheetah_env(seed,logFolder, pixel_resolution= pixel_resolution, env_builder_args=env_builder_args)


    def runFunction(seed, folderName, resumeModelFile):
        if resumeModelFile is not None:
            pathList = os.path.abspath(resumeModelFile).split("/")
            rootFolderResumeSource = "/"+"/".join(pathList[:pathList.index("checkpoints")])
            os.makedirs(folderName,exist_ok=True)
            copyfile(src=rootFolderResumeSource+"/GymEnvWrapper_log.csv", dst=folderName+"/GymEnvWrapper_log.csv")
        return daesac_train(seed=seed,
                                trainSteps=1000000,
                                folderName=folderName,
                                gpuid = None, # seed%th.cuda.device_count(),
                                modelFile=resumeModelFile,
                                env_builder=buildEnv,
                                eval_period_ep=10,
                                env_builder_args = {"action_repeat" : 2},                                
                                
                                automatic_grad_steps = False,
                                le_arch_backbone  = "conv_small",
                                le_batch_size = 96,
                                le_bestModelThreshold = 0.95,
                                le_arch_deconv_backbone = "conv",
                                le_disable_validation_set = True,
                                le_arch_dyn_ensemble_size = 3,
                                le_arch_dynamics = [64],
                                le_arch_enc_ensemble_size = 1,
                                le_arch_frame_stack_size = 1,
                                le_grad_steps = 100,
                                le_arch_img_encoding_size = 50,            
                                le_loss_kld_weight = 0.01,
                                le_learning_starts = 1000,
                                le_loss_use_log = True,
                                le_lr = 0.001,
                                le_pretrain_grad_steps = 20,
                                le_loss_reconstruction_weight=0.0,
                                le_retrain_epochs = 20,
                                le_loss_reward_weight = 0.00001,
                                le_arch_state_combiner = "identity",
                                le_train_freq = (1,"step"),
                                le_arch_type = "dvae3",
                                le_validation_batch_size = 256,
                                le_validation_ratio = 0.01,
                                le_loss_vec_weight = 0.005,
                                parallelize_experience_collection = False,
                                replay_buffer_size = 50000,
                                sac_batchSize = 256,
                                sac_ent_coef = "auto",
                                sac_gamma = 0.99,
                                sac_grad_steps = 1,
                                sac_learning_starts = 1000,
                                sac_lr = 0.001,
                                sac_arch_policy = [512],
                                sac_target_entropy = "auto",
                                sac_tau = 0.01,
                                sac_train_freq=(2,"step"),
                                
                                le_arch_img_dec_ensemble_size = 1,
                                le_arch_reward = [64,64],
                                le_arch_reward_ensemble_size = 1,
                                le_arch_use_coord_conv = True,
                                le_arch_vec_decoder = [64,64],
                                le_arch_vec_encoder = "identity",
                                le_decoder_weight_decay_lambda = None,
                                le_encoding_noise_std = 0.0,
                                le_freeze_decoders = False,
                                le_freeze_dynamics = False,
                                le_freeze_encoders = False,
                                le_loss_latent_weight = None,
                                le_reset_decoder = False,
                                le_reset_encoder = False,
                                le_reset_on_retrain = False,
                                le_retrain_period = -1,
                                le_reward_scaling = 1.0,
                                le_tau = 1.0,
                                le_use_log_reward = False,
                                sac_arch_actor_ensemble_size = 5,
                                storage_torch_device = 'auto_cuda',
                                random_steps = 1000)
    
    launchRun(evalFunction=None,
                evalModelFile=args["evaluate"],
                seedsNum=args["seedsNum"],
                seedsOffset=args["seedsOffset"],
                runFunction=runFunction,
                maxProcs=args["maxProcs"],
                xvfb=args["xvfb"],
                launchFilePath=__file__)