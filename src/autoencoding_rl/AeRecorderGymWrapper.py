import gym
import cv2
import os
import time
import lr_gym.utils.dbg.ggLog as ggLog
import numpy as np
from autoencoding_rl.utils import ObsConverter
from autoencoding_rl.latent_extractors.LatentExtractor import LatentExtractor
from autoencoding_rl.utils import build_s2s_img
import math
from vidgear.gears import WriteGear
import pickle

class AeRecorderGymWrapper(gym.Wrapper):
    """
    """
    def __init__(self, env : gym.Env, fps : float, outFolder : str,
                        saveBestEpisodes = False, 
                        saveFrequency_ep = 1,
                        saveFrequency_step = -1,
                        save_pickles = False):
        super().__init__(env)
        self._outFps = fps
        self._frameRepeat = 1
        if fps < 30:
            self._frameRepeat = int(math.ceil(30/fps))
            self._outFps = fps*self._frameRepeat
        self._obsBuffer = []
        self._actionBuffer = []
        self._rewardBuffer = []
        self._episodeCounter = 0
        self._outFolder = outFolder
        self._saveBestEpisodes = saveBestEpisodes
        self._saveFrequency_ep = saveFrequency_ep
        self._saveFrequency_step = saveFrequency_step
        self._bestReward = float("-inf")
        self._epReward = 0
        self._last_saved_ep_steps = -1
        self._obs_converter = ObsConverter(observation_shape = env.observation_space)
        self._latentExtractor = None
        self._max_episode_steps = self.spec.max_episode_steps # 
        self._save_pickles = save_pickles

        try:
            os.makedirs(self._outFolder)
        except FileExistsError:
            pass
        try:
            os.makedirs(self._outFolder+"/best")
        except FileExistsError:
            pass

    
    def setLatentExtractor(self, latentExtractor : LatentExtractor):
        self._latentExtractor = latentExtractor

    def step(self, action):
        stepRet =  self.env.step(action)
        new_obs, reward, done, info = stepRet
        self._saveFrame(self._lastObs, action, reward)
        self._lastObs = new_obs
        return stepRet

    def _saveFrame(self, obs, action, reward):
        # img = self._obs_converter.getImgPart(obs)
        self._obsBuffer.append(obs)
        self._actionBuffer.append(action)
        self._rewardBuffer.append(reward)
        self._epReward += reward

    def _writeVideo(self, outFilename : str, obss, actions, rewards):
        if len(obss)>0:
            s2s_imgs = []
            s2s_vecs = []
            s2s_rews = []
            for i in range(len(obss)):
                imgs, vecs, rews = build_s2s_img(obss[i], actions[i], self._latentExtractor, rewards[i])
                s2s_imgs.append(imgs)
                s2s_vecs.append(vecs)
                s2s_rews.append(rews)
            ggLog.info(f"AeRecorderGymWrapper saving {len(obss)} frames video to "+outFilename)
            #outFile = self._outVideoFile+str(self._episodeCounter).zfill(9)
            if not outFilename.endswith(".avi"):
                outFilename+=".avi"
            in_resolution_wh = (s2s_imgs[0].shape[1], s2s_imgs[0].shape[0]) # npimgs are hwc
            # ggLog.info(f"in_resolutio_wh = {in_resolution_wh}")
            height = in_resolution_wh[1]
            minheight = 480
            if height<minheight:
                height = minheight
            out_resolution_wh = [int(height/in_resolution_wh[1]*in_resolution_wh[0]), height]
            if out_resolution_wh[0] % 2 != 0:
                out_resolution_wh[0] += 1
            # ggLog.info(f"out_resolution_wh = {out_resolution_wh}")
            # videoWriter = cv2.VideoWriter(  outFilename,
            #                                 cv2.VideoWriter_fourcc(*'mp4v'), # alternatives: MPEG, FFV1
            #                                 self._outFps,
            #                                 out_resolution_wh)
            output_params = {   "-c:v": "libx264",
                                "-crf": 18,
                                "-profile:v":
                                "baseline",
                                "-input_framerate":self._outFps,
                                "-disable_force_termination" : True,
                                "-level" : 3.0,
                                "-pix_fmt" : "yuv420p"} 
            retries = 3
            wroteVideo = False
            while not wroteVideo:
                try:
                    writer = WriteGear(output_filename=outFilename, logging=False, **output_params)
                    i = 0
                    for npimg in s2s_imgs:
                        # img = tensorToHumanCvImageRgb(thimg)
                        npimg = cv2.resize(npimg,dsize=out_resolution_wh,interpolation=cv2.INTER_NEAREST)             
                        npimg = self._preproc_frame(npimg)
                        for _ in range(self._frameRepeat):
                            # videoWriter.write(npimg)
                            # print(f"Saving frame with shape {npimg.shape}")
                            writer.write(npimg)
                        i+=1
                    writer.close()
                    wroteVideo = True
                except ValueError as e:
                    ggLog.warn(f"Video write failed. Will retry {retries} times.")
                    retries -= 1
                    if retries<0:
                        raise e from None


            with open(outFilename+".txt", 'a') as f:
                for i in range(len(s2s_vecs)):
                    f.write(f"{s2s_vecs[i][0]}, \t{s2s_vecs[i][1]}, \t{s2s_rews[i][0]}, \t{s2s_rews[i][1]}, \t{actions[i]}\n")
            # videoWriter.release()

    def _writePickle(self, outFilename : str, obss, actions, rewards):
        if len(obss)>0:
            episode = { "observations" : obss,
                        "actions" : actions,
                        "rewards" : rewards}
            with open(outFilename,mode="wb") as file:
                pickle.dump(episode, file)

    def _preproc_frame(self, img_whc):
        if img_whc.dtype == np.float32 or img_whc.dtype == np.float64 :
            img_whc = np.uint8(img_whc*255)

        if len(img_whc.shape) == 2:
            img_whc = np.expand_dims(img_whc,axis=2)
        if img_whc.shape[2] == 1:
            img_whc = np.repeat(img_whc,3,axis=2)
        
        if img_whc.shape[2] != 3 or img_whc.dtype != np.uint8:
            raise RuntimeError(f"Unsupported image format, dtpye={img_whc.dtype}, shape={img_whc.shape}")
        return img_whc


    def reset(self, **kwargs):
        save_video = False
        outfile_path = None
        if self._epReward > self._bestReward:
            self._bestReward = self._epReward
            if self._saveBestEpisodes:
                save_video = True
                outfile_path = self._outFolder+"/best/ep_"+(f"{self._episodeCounter}").zfill(6)+f"_{self._epReward}"
        if self._saveFrequency_ep>0 and self._episodeCounter % self._saveFrequency_ep == 0:
            save_video = True
            outfile_path = self._outFolder+"/ep_"+(f"{self._episodeCounter}").zfill(6)+f"_{self._epReward}"
        elif self._saveFrequency_step>0 and int(self.num_timesteps/self._saveFrequency_step) != int(self._last_saved_ep_steps/self._saveFrequency_step):
            save_video = True
            outfile_path = self._outFolder+"/ep_"+(f"{self._episodeCounter}").zfill(6)+f"_{self._epReward}"
            self._last_saved_ep_steps = self.num_timesteps

        if save_video:
            self._writeVideo(outfile_path+".mp4",self._obsBuffer, self._actionBuffer, self._rewardBuffer)
            if self._save_pickles:
                self._writePickle(outfile_path+".pkl",self._obsBuffer, self._actionBuffer, self._rewardBuffer)

        obs = self.env.reset(**kwargs)
        if self._epReward>self._bestReward:
            self._bestReward = self._epReward
        self._epReward = 0
        self._lastObs = obs
        self._episodeCounter +=1
        self._obsBuffer = []
        self._actionBuffer = []
        self._rewardBuffer = []

        return obs

    def close(self):
        # self._saveLastEpisode(self._outFolder+(f"/ep_{self._episodeCounter}").zfill(6)+f"_{self._epReward}.mp4")
        return self.env.close()

    def setSaveAllEpisodes(self, enable : bool, disable_after_one_episode : bool = False):
        self._saveAllEpisodes = enable
        self._disableAfterEp = disable_after_one_episode
