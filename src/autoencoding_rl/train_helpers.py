
from queue import Full
import torch as th
import time
import torch.multiprocessing as mp
import threading
import queue

from autoencoding_rl.latent_extractors.LatentExtractor import LatentExtractor
from autoencoding_rl.utils import replay_data2transition_batch
import lr_gym.utils.dbg.ggLog as ggLog
import autoencoding_rl
import lr_gym
from stable_baselines3.her.her_replay_buffer import HerReplayBuffer

def evaluateFeatureExtractor(evaluation_samples : int, latent_extractor : LatentExtractor, replayBuffer, env, max_batch_size : int = 512):
    with th.no_grad():
        totLoss = 0.0
        eval_batch_size = min([evaluation_samples, max_batch_size])
        iterations = int(evaluation_samples/eval_batch_size)
        for i in range(iterations):
            if isinstance(replayBuffer, HerReplayBuffer):
                transition_batch = replay_data2transition_batch(replayBuffer.replay_buffer.sample(eval_batch_size,
                                                                                    env=env,
                                                                                    validation_set = True))
            else:
                transition_batch = replay_data2transition_batch(replayBuffer.sample(eval_batch_size,
                                                                                    env=env,
                                                                                    validation_set = True))
            transition_batch = autoencoding_rl.utils.transitionToDevice(transition = transition_batch, torchDevice = latent_extractor.torchDevice())
            #ggLog.info("transition_batch['camera'].dtype = "+str(transition_batch.observation['camera'].dtype))
            totLoss += latent_extractor.evaluate(transition_batch)
        avgLoss = totLoss/iterations
    return avgLoss

def trainFeatureExtractor(grad_steps : int, latent_extractor : LatentExtractor, batch_size : int, replayBuffer, env, validation_batch_size : int, episode_count: int = -1):
    # ggLog.info("Training feature extractor")
    t0 = time.monotonic()
    stop = False
    try:
        dataqueue = queue.Queue(maxsize=10)
        # training_done = threading.Event()
        # training_done.clear()
        # ggLog.info("Created queue")
        def loadData(): #dataqueue, training_done):
            # ggLog.info("Started data load")
            for i in range(grad_steps):
                # tdc0 = time.monotonic()
                rb_batch = replayBuffer.sample(  batch_size, env=env)
                # tdc01 = time.monotonic()
                training_batch = replay_data2transition_batch(rb_batch)
                # tdc1 = time.monotonic()
                training_batch = autoencoding_rl.utils.transitionToDevice(transition = training_batch, torchDevice = latent_extractor.torchDevice())
                # tdc15 = time.monotonic()
                # th.cuda.synchronize()
                # tdc2 = time.monotonic()
                # ggLog.info(f"Batch loading time {tdc2-tdc0}, sampling time = {tdc01-tdc0}, convert_time = {tdc1-tdc01}")

                if validation_batch_size!=0:
                    if isinstance(replayBuffer, HerReplayBuffer):
                        validation_batch = replay_data2transition_batch(replayBuffer.replay_buffer.sample(validation_batch_size,
                                                                                            env=env,
                                                                                            validation_set = True))
                    else:
                        validation_batch = replay_data2transition_batch(replayBuffer.sample(validation_batch_size,
                                                                                            env=env,
                                                                                            validation_set = True))
                    validation_batch = autoencoding_rl.utils.transitionToDevice(transition = validation_batch, torchDevice = latent_extractor.torchDevice())
                else:
                    validation_batch = None
                # ggLog.info("Loading in queue")
                loadedBatch = False
                while not (stop or loadedBatch):
                    try:
                        dataqueue.put((training_batch,validation_batch), timeout=5.0)
                        loadedBatch = True
                    except Full:
                        pass
                if stop:
                    break
                # ggLog.info(f"Loaded step {i}. \t sampling = {tdc01-tdc0}, totransition = {tdc1-tdc01}, todevice = {tdc15-tdc1}")
            # training_done.wait()
        dataloader = threading.Thread(target=loadData) #, args=(dataqueue,training_done))
        dataloader.start()
        # training_done.set()
        # loadData(dataqueue,training_done)

        lossum = 0
        # tdcf = time.monotonic()
        for i in range(grad_steps):
            # ggLog.info("getting from queue")
            # ggLog.info(f"Data sampling: validation={tdcf-tdc1:.5f}, training={tdc1-tdc0:.5f}")
            batches = dataqueue.get()
            # ggLog.info(f"Training step {i}")
            lossum += latent_extractor.train_model(training_batch = batches[0], validation_batch=batches[1], episode_count=episode_count)
    finally:
        # training_done.set()
        stop = True
        dataloader.join()
        # tf = time.monotonic()
        # ggLog.info(f"Performed {grad_steps} gradient steps, took {tf-t0:.5f}s, data loading took {tdcf-t0:.5f}")
    return lossum / grad_steps

def retrainFeatureExtractor(epochs : int,
                            grad_steps_per_epoch : int,
                            log_id : str,
                            latent_extractor : LatentExtractor,
                            batch_size : int,
                            replayBuffer,
                            env,
                            episode_count : int = -1,
                            validation_batch_size = 0,
                            reset = False,
                            evaluate = True):
    ggLog.info(f"Starting autoencoder retraining round (buffer size = {replayBuffer.size()})")
    if batch_size > replayBuffer.size():
        raise RuntimeError(f"Not enough samples in replay buffer for autoencoder training round. batch size = {batch_size}, buffer size = {replayBuffer.size()}")
    bestLoss = float("inf")
    if reset:
        latent_extractor.reset()
    t_prepretrain = time.monotonic()


    avgLoss = evaluateFeatureExtractor(evaluation_samples=replayBuffer.size(),
                                                latent_extractor=latent_extractor,
                                                replayBuffer=replayBuffer,
                                                env=env)        
    ggLog.info(f"Initial eval loss = {avgLoss}")
    if epochs<1:
        return
        
    for epoch in range(epochs):
        #Train
        # ggLog.info("Training")
        t0 = time.monotonic()
        avgLoss = trainFeatureExtractor(grad_steps = grad_steps_per_epoch,
                                        latent_extractor=latent_extractor,
                                        replayBuffer=replayBuffer,
                                        batch_size=batch_size,
                                        env=env,
                                        validation_batch_size=validation_batch_size,
                                        episode_count = episode_count)     
        # th.cuda.synchronize()
        t1 = time.monotonic()  
        if evaluate:             
            avgLoss = evaluateFeatureExtractor(evaluation_samples=int(replayBuffer.size()/10),
                                                        latent_extractor=latent_extractor,
                                                        replayBuffer=replayBuffer,
                                                        env=env)
        # th.cuda.synchronize()
        tf = time.monotonic()
        ggLog.info(f"Finished epoch {epoch}/{epochs}, did {grad_steps_per_epoch} steps, loss = {avgLoss}, \t t_tot = {tf-t0:.5f}s, t_train = {t1-t0:.5f}s")

        latent_extractor.displayDebugInfo(replay_data2transition_batch(replayBuffer.sample(5,
                                                                                            env=env)),
                                                id = log_id+str(epoch))

        if evaluate and avgLoss <= bestLoss:
            bestEpoch = epoch
            bestLoss = avgLoss
            bestState = latent_extractor.state_dict()

        lr_gym.utils.utils.haltOnSigintReceived()

    if evaluate:
        ggLog.info(f"Best epoch was {bestEpoch} with validation loss {bestLoss}, reloading its weights...")
        latent_extractor.load_state_dict(bestState)
        ggLog.info(f"Loaded weights. Retraining finished.")

    avgLoss = evaluateFeatureExtractor(evaluation_samples=replayBuffer.size(),
                                        latent_extractor=latent_extractor,
                                        replayBuffer=replayBuffer,
                                        env=env)        
    ggLog.info(f"Final eval loss = {avgLoss}")

    t_postretrain = time.monotonic()
    ggLog.info(f"Training round finished. Took {t_postretrain-t_prepretrain}s")